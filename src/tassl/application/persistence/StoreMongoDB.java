/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.persistence;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.bson.Document;


/**
 *
 * @author Javier Diaz-Montes
 */
public class StoreMongoDB extends Store{

    /*AgentResourceManager*/
    
    @Override
    public void dbWriteAgentProvisioningRequest(String dbName,long startRequest, long endRequest, String [] allMachines, List<String> operations, 
            String appname, String wflId, String stageId, String providerType) throws UnknownHostException{
        
        List<String> allMachinesList=Arrays.asList(allMachines);//limitation in Document API
        
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();            
        
        
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("ProvisioningRequest");

        Document doc = new Document("requestStartTime", startRequest)
               .append("requestEndTime", endRequest)
               .append("requestDuration", (endRequest-startRequest))
               .append("providerType", providerType) //ResourceCloud or ResourceCluster
               .append("workflowId", wflId)
               .append("stageId", stageId)                
               .append("application", appname)
               .append("machines", allMachinesList)
               .append("operations", operations);

        collection.insertOne(doc);
		
    }
    
    @Override
    public void dbWriteAgentProvisioningDeallocate(String dbName,long startRequest, long endRequest, List<String> toRelease, 
            String appname, String wflId, String stageId, String providerType) throws UnknownHostException{
        
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();            
        
        
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("ProvisioningRequest");

        Document doc = new Document("startTime", startRequest)
                .append("endTime", endRequest)
                .append("duration", (endRequest-startRequest))
                .append("operations", "deallocate")
                .append("providerType", providerType) //ResourceCloud or ResourceCluster
                .append("workflowId", wflId)
                .append("stageId", stageId)                
                .append("application", appname)
                .append("machines", toRelease);
               

        collection.insertOne(doc);
		
    }
    
    @Override
    public void dbWriteAgentProvisioningAllocate(String dbName,long startRequest, long endAllocate, List<String>toAllocate, String appname, String wflId, String stageId, String providerType,
                            long startMachineprovisioning, long endMachineprovisioning, long startWorkerprovisioning, long endWorkerprovisioning, int numReqMachines, List<String> failedMachines, 
                            int numReqWorkers, List<String>failedWorkers, HashMap<String,String> additionalValues)throws UnknownHostException{
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        
		
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Provisioning");
        
        
        Document docToSet = new Document("startTime", startRequest)
                .append("endTime", endAllocate)
                .append("duration", (endAllocate-startRequest))
                .append("operation", "allocate")
               .append("providerType", providerType) //ResourceCloud or ResourceCluster
               .append("workflowId", wflId)
               .append("stageId", stageId)                
               .append("application", appname)
               .append("machineList", toAllocate);
        
        
        if(endWorkerprovisioning-startWorkerprovisioning>2000L){//else no workers started
            int numFailedWork=0;
            for(String work:failedWorkers){
                if(!work.startsWith("fail")){
                    numFailedWork++;
                }
            }
        
            docToSet.append("workers", new Document()
                                        .append("startTime",startWorkerprovisioning)
                                        .append("endTime", endWorkerprovisioning)
                                        .append("duration", (endWorkerprovisioning-startWorkerprovisioning))
                                        .append("reqWorkers", numReqWorkers)
                                        .append("failedWorkers", numFailedWork));
        }
        
        
        
        if(startMachineprovisioning!=0){//else no provisioning needed        
            docToSet.append("machines", new Document()
                                            .append("startTime",startMachineprovisioning)
                                            .append("endTime", endMachineprovisioning)
                                            .append("duration", (endMachineprovisioning-startMachineprovisioning))
                                            .append("reqMachines", numReqMachines)
                                            .append("failedMachines", failedMachines.size())
                                            .append("listFailedMachines", failedMachines));
        }
        
        if(additionalValues.containsKey("imageId")){
            docToSet.append("imageId", additionalValues.get("imageId"));
        }
        if(additionalValues.containsKey("queueType")){            
            docToSet.append("queueType", additionalValues.get("queueType"));
            docToSet.append("queueName", additionalValues.get("queueName"));
        }        
        
        collection.insertOne(docToSet);
        
    }
    
    @Override
    public void dbWriteAgentReleaseMachine(String dbName,String iptype, long startTimeStamp, long endTimeStamp, String appName, String providerType, boolean failure, 
            HashMap<String,String> additionalValues) throws UnknownHostException{
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        		
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Machinelive");
        
        String status;
        if(failure){
            status="failure";
        }else{
            status="released";
        }
        
        Document docToSet=new Document();
        
        docToSet.append("iptype",iptype)
                .append("startTime", startTimeStamp)
                .append("endTime", endTimeStamp)
                .append("duration", (endTimeStamp-startTimeStamp))
                .append("providerType", providerType) //ResourceCloud or ResourceCluster               
                .append("application", appName)
                .append("status",status);
        
        if(additionalValues.containsKey("imageId")){
            docToSet.append("imageId", additionalValues.get("imageId"));
        }
        if(additionalValues.containsKey("queueType")){            
            docToSet.append("queueType", additionalValues.get("queueType"));
            docToSet.append("queueName", additionalValues.get("queueName"));
        }   

        collection.insertOne(docToSet);
        
    }
    
    @Override
    public void dbWriteAgentReleaseWorker(String dbName,String iptype, long startTimeStamp, long endTimeStamp, String appName, String providerType, boolean failure, 
            HashMap<String,String> additionalValues) throws UnknownHostException{
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        		
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Workerlive");
        
        String status;
        if(failure){
            status="failure";
        }else{
            status="released";
        }
        
        Document docToSet=new Document();
        
        //Currently it assumes a worker started when machine was allocated
        
        docToSet.append("iptype",iptype)
                .append("startTime", startTimeStamp)
                .append("endTime", endTimeStamp)
                .append("duration", (endTimeStamp-startTimeStamp))
                .append("providerType", providerType) //ResourceCloud or ResourceCluster               
                .append("application", appName)
                .append("status",status);
        
        if(additionalValues.containsKey("imageId")){
            docToSet.append("imageId", additionalValues.get("imageId"));
        }
        if(additionalValues.containsKey("queueType")){            
            docToSet.append("queueType", additionalValues.get("queueType"));
            docToSet.append("queueName", additionalValues.get("queueName"));
        }   

        collection.insertOne(docToSet);
        
    }
        
    
    /*AgentLite info -- ExecTime Tasks*/   
    
    @Override
    public void dbWriteAgentStartTimeTask(String dbName,int taskId, String app, Long startTime, String workflowId, String stageId) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();        
		
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("TaskExec");
        
        Document doc = new Document("taskId", taskId)
               .append("application", app)
               .append("startTime", startTime)
               .append("endTime", 0)
               .append("duration", -1)
               .append("workflowId", workflowId)
               .append("stageId", stageId);
        
        collection.insertOne(doc);

    }
    
    
    @Override
    public void dbWriteAgentEndTimeTask(String dbName,int taskId, Long endTime, String machineType, String workflowId, Long startTime, 
            String getInputTime, String putOutputTime)throws UnknownHostException{
        String [] getInputValues= getInputTime.split(",");
        Long getInputStart=new Long(getInputValues[0]);
        Long getInputEnd=new Long(getInputValues[1]);
        double sizeInput=Double.parseDouble(getInputValues[2]);
        int numberOfFilesIn=Integer.parseInt(getInputValues[3]);
        
        String [] putOutputValues= putOutputTime.split(",");
        Long putOutputStart=new Long(putOutputValues[0]);
        Long putOutputEnd=new Long(putOutputValues[1]);
        double sizeOutput=Double.parseDouble(putOutputValues[2]);
        int numberOfFilesOut=Integer.parseInt(putOutputValues[3]);
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        
		
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("TaskExec");
        
                 //this will be slow with a large db. Can we make taskId unique in cometcloud? if not, maybe have a hashcode as key that we pass around?
                           // what about a hashcode using taskId+workflowId+startTime -- The problem is making the hascode unique
        collection.updateOne(new Document("taskId", taskId).append("workflowId", workflowId).append("endTime", 0).append("startTime", startTime),
                             new Document("$set", new Document("endTime", endTime)
                                                    .append("duration",(endTime-startTime))
                                                    .append("machineType",machineType)
                                                    .append("getInputStart", getInputStart)
                                                    .append("getInputEnd", getInputEnd)
                                                    .append("getDuration", (getInputEnd-getInputStart))
                                                    .append("sizeInput", sizeInput)
                                                    .append("numberFilesInput", numberOfFilesIn)
                                                    .append("putOutputStart", putOutputStart)
                                                    .append("putOutputEnd", putOutputEnd)
                                                    .append("putDuration", (putOutputEnd-putOutputStart))
                                                    .append("sizeOutput", sizeOutput)
                                                    .append("numberFilesOutput", numberOfFilesOut)
                            ));        
    }
    
    /*File Server Information*/
    @Override
    public void dbWriteAgentStoreTransferInfo(String dbName,List<Document> sites, List<Document> workers)throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();        
		
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("TransferDataSites");
        MongoCollection<Document> collection1=db.getCollection("TransferDataWorker");
        
        if(!sites.isEmpty()){
            collection.insertMany(sites);
        }
        if(!workers.isEmpty()){
            collection1.insertMany(workers);
        }

    }    
    
    @Override
    public void dbWriteKnownApp(String dbName, String appName) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Apps");
        
        Document doc = new Document("name", appName);        
        collection.insertOne(doc);
    }

    @Override
    public void dbWriteKnownMachine(String dbName, String machineType) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Machines");
        
        Document doc = new Document("name", machineType);        
        
        collection.insertOne(doc);
    }
    
    /*READ Methods*/    
    
    @Override
    public List<String> loadDBs() throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        
        MongoIterable<String> dbs=mongoClient.listDatabaseNames();
        
        List<String> dbList=new ArrayList<>();
        for(String dbName:dbs){
            if(!dbName.equals("local")){
                dbList.add(dbName);
            }
        }
        return dbList;
    }
    
    @Override
    public HashSet<String> loadApps(String dbName)throws UnknownHostException{
        HashSet<String> apps=new HashSet<>();
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Apps");
        
        FindIterable<Document> iterable = collection.find();
        
        for(Document doc:iterable){
            apps.add((String)doc.get("name"));
        }
        
        return apps;
    }
    
    @Override
    public HashSet<String> loadMachines(String dbName)throws UnknownHostException{
        HashSet<String> apps=new HashSet<>();
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Machines");
        
        FindIterable<Document> iterable = collection.find();
        
        for(Document doc:iterable){
            apps.add((String)doc.get("name"));
        }
        
        return apps;
    }
    
    @Override
    public List<Document> dbReadAgentAppExec(String dbName, String appName, int lastNrecods) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("TaskExec");
        
        int toSkip=0;
        if(lastNrecods!=0){
            toSkip=Math.max(0,(int)(collection.count() - lastNrecods));
        }
        
        List<Document> results= new ArrayList<>();
        
        FindIterable<Document> iterable = collection.find(
                    new Document("application", appName)
                          .append("getDuration", new Document("$exists", true ))                 
                    )
                    .skip(toSkip);


        for(Document document:iterable){
            results.add(document);
        }
        
        return results;
        
    }

    @Override
    public List<Document> dbReadAgentProvisioningStats (String dbName, String appName, int lastNrecods) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Provisioning");
        
        int toSkip=0;
        if(lastNrecods!=0){
            toSkip=Math.max(0,(int)(collection.count() - lastNrecods));
        }
        
        List<Document> results= new ArrayList<>();
        
        FindIterable<Document> iterable = collection.find(
                    new Document("application", appName)                 
                    )
                    .skip(toSkip);


        for(Document document:iterable){
            results.add(document);
        }
        
        return results;
        
    }
    
        
    @Override
    public List<Document> dbReadAgentWorkerFailure(String dbName, String appName, int lastNrecods) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Workerlive");
        
        int toSkip=0;
        if(lastNrecods!=0){
            toSkip=Math.max(0,(int)(collection.count() - lastNrecods));
        }
        
        List<Document> results= new ArrayList<>();
        
        FindIterable<Document> iterable = collection.find(
                    new Document("application", appName)                 
                    )
                    .skip(toSkip);


        for(Document document:iterable){
            results.add(document);
        }
        
        return results;
   
    }
    
    @Override
    public List<Document> dbReadAgentMachineFailure(String dbName, String appName, int lastNrecods) throws UnknownHostException{
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        MongoCollection<Document> collection=db.getCollection("Machinelive");
        
        int toSkip=0;
        if(lastNrecods!=0){
            toSkip=Math.max(0,(int)(collection.count() - lastNrecods));
        }
        
        List<Document> results= new ArrayList<>();
        
        FindIterable<Document> iterable = collection.find(
                    new Document("application", appName)                 
                    )
                    .skip(toSkip);


        for(Document document:iterable){
            results.add(document);
        }
        
        return results;
      
    }
    
    @Override
    public HashMap<String,List<Document>> dbReadAgentTransferInfo(String dbName, int lastNrecods) throws UnknownHostException{
        HashMap<String,List<Document>> resultsH=new HashMap<>();
        
        MongoClient mongoClient;
        
        mongoClient = MongoClientProvider.getMongoClient();
        	
        MongoDatabase db = mongoClient.getDatabase(dbName);		
        
        MongoCollection<Document> collection=db.getCollection("TransferDataSites");
        
        int toSkip=0;
        if(lastNrecods!=0){
            toSkip=Math.max(0,(int)(collection.count() - lastNrecods));
        }
        
        List<Document> results = new ArrayList<>();
        
        FindIterable<Document> iterable = collection.find().skip(toSkip);

        for(Document document:iterable){
            results.add(document);
        }
        resultsH.put("sites", results);
        
        
        MongoCollection<Document> collection1=db.getCollection("TransferDataWorker");
        
        FindIterable<Document> iterable1 = collection1.find().skip(toSkip);

        List<Document> results1= new ArrayList<>();
        for(Document document:iterable1){
            results1.add(document);
        }
        resultsH.put("workers", results1);
        
        return resultsH;
        
    }
}
