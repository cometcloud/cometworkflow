/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.persistence;

import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.bson.Document;

/**
 *
 * @author Javier Diaz-Montes
 */
public abstract class Store implements Serializable{

    /*AgentResourceManager*/
    
    /**
     * Agent - Record time to process a scheduling request to (de)allocate machines
     * 
     * @param dbName
     * @param startRequest
     * @param endRequest
     * @param allMachines
     * @param operations
     * @param appname
     * @param wflId
     * @param stageId
     * @param providerType
     */
    abstract public void dbWriteAgentProvisioningRequest(String dbName, long startRequest, long endRequest, String [] allMachines, List<String> operations, 
            String appname, String wflId, String stageId, String providerType) throws UnknownHostException;
  
    
    /**
     *Agent - Record time to deallocate machines a scheduling request 
     * @param dbName
     * @param startRequest
     * @param endRequest
     * @param toRelease
     * @param appname
     * @param wflId
     * @param stageId
     * @param providerType
     * @throws UnknownHostException
     */
    abstract public void dbWriteAgentProvisioningDeallocate(String dbName,long startRequest, long endRequest, List<String> toRelease,  
            String appname, String wflId, String stageId, String providerType) throws UnknownHostException;
            
            
    /**
     * Agent - Record time to allocate machines a scheduling request is done and machines are allocated and workers active
     * @param dbName
     * @param startRequest
     * @param endAllocate
     * @param toAllocate
     * @param appname
     * @param wflId
     * @param stageId
     * @param providerType
     * @param startMachineprovisioning
     * @param endMachineprovisioning
     * @param startWorkerprovisioning
     * @param endWorkerprovisioning
     * @param numReqMachines
     * @param failedMachines
     * @param numReqWorkers
     * @param failedWorkers
     * @param additionalValues
     * @throws UnknownHostException
     */
    abstract public void dbWriteAgentProvisioningAllocate(String dbName,long startRequest, long endAllocate, List<String>toAllocate, String appname, String wflId, String stageId, String providerType,
                            long startMachineprovisioning, long endMachineprovisioning, long startWorkerprovisioning, long endWorkerprovisioning, int numReqMachines, List<String> failedMachines, 
                            int numReqWorkers, List<String>failedWorkers, HashMap<String,String> additionalValues)throws UnknownHostException;
    
    /**
     * Agent - Record live of machine. When Clouds the live is from instantiation to termination/failure. 
     *          When Cluster the live is until it fails (when queues enabled then it will be from allocation to deallocation/failure)
     * 
     * @param dbName
     * @param iptype = ip when cluster or vmtype when cloud
     * @param startTimeStamp
     * @param endTimeStamp
     * @param appName
     * @param providerType
     * @param failure
     * @param additionalValues
     */
    abstract public void dbWriteAgentReleaseMachine(String dbName,String iptype, long startTimeStamp, long endTimeStamp, String appName, String providerType, boolean failure, 
            HashMap<String,String> additionalValues) throws UnknownHostException;
    
    /**
     * Agent - Record live of worker. 
     * 
     * @param dbName
     * @param iptype = ip when cluster or vmtype when cloud
     * @param startTimeStamp
     * @param endTimeStamp
     * @param appName
     * @param providerType
     * @param failure
     * @param additionalValues
     */
    abstract public void dbWriteAgentReleaseWorker(String dbName,String iptype, long startTimeStamp, long endTimeStamp, String appName, String providerType, boolean failure, 
            HashMap<String,String> additionalValues) throws UnknownHostException;
    
    /*AgentLite info -- ExecTime Tasks*/
    /**
     * Agent - Record in the DB when a task is received for computation
     * 
     * @param dbName
     * @param taskId
     * @param app
     * @param startTime
     * @param workflowId
     * @param stageId
     */
    abstract public void dbWriteAgentStartTimeTask(String dbName,int taskId, String app, Long startTime, String workflowId, String stageId)throws UnknownHostException;
    
    /**
     * Agent - Record in the DB when a task is completed
     * 
     * @param dbName
     * @param taskId
     * @param endTime
     * @param machineType
     * @param workflowId
     * @param startTime
     * @param getInputTime
     * @param putOutputTime
     */
    abstract public void dbWriteAgentEndTimeTask(String dbName,int taskId, Long endTime, String machineType, String workflowId, Long startTime,
            String getInputTime, String putOutputTime)throws UnknownHostException;
    
    /*File Server Information*/     
    
    /**
     * Agent - Store transfer information in DB
     * @param dbName
     * @param sites List of files transferred to/from other sites
     * @param workers List of files transferred to/from workers within this site
     */
    abstract public void dbWriteAgentStoreTransferInfo(String dbName,List<Document> sites, List<Document> workers)throws UnknownHostException;

    
    /**
     * Load list of dbs. Each db has information of a site
     * @return List of db names
     * @throws UnknownHostException
     */
    abstract public List<String> loadDBs() throws UnknownHostException;
    
    
        /**
     * Load set of known applications
     * @param dbName
     * @return Set
     * @throws UnknownHostException
     */
    abstract public HashSet<String> loadApps(String dbName)throws UnknownHostException;
    
    /**
     * Load set of known machines
     * 
     * @param dbName
     * @return Set
     * @throws UnknownHostException
     */
    abstract public HashSet<String> loadMachines(String dbName)throws UnknownHostException;
    
    /**
     * Store application name into table/collection known applications
     * @param dbName
     * @param appName
     */
    abstract public void dbWriteKnownApp(String dbName, String appName) throws UnknownHostException;
    
    /**
     * Store machine name into table/collection of known machines 
     * @param dbName
     * @param machineType
     */
    abstract public void dbWriteKnownMachine(String dbName, String machineType) throws UnknownHostException;
    
    
    /**
     * Reads Application execution records. Last N records for each application
     * 
     * @param dbName     
     * @param appName
     * @param lastNrecods Number of records to load. Use 0 for all.
     * @return List with all records
     * @throws UnknownHostException
     */
    abstract public List<Document> dbReadAgentAppExec(String dbName, String appName, int lastNrecods) throws UnknownHostException;
    
    
    /**
     * Reads information of the last N transfers between different sites and between a site and its workers     
     * 
     * @param dbName
     * @param lastNrecods
     * @return HashMap {"sites":List<Documents>,"workers":List<Documents>}
     * @throws UnknownHostException
     */
    abstract public HashMap<String,List<Document>> dbReadAgentTransferInfo(String dbName, int lastNrecods) throws UnknownHostException;
    
    /**
     * Reads Machine failure information. Last N records for each application
     * 
     * @param dbName
     * @param appName
     * @param lastNrecods
     * @return List<Documents>
     * @throws UnknownHostException
     */
    abstract public List<Document> dbReadAgentMachineFailure(String dbName, String appName, int lastNrecods) throws UnknownHostException;
    
    /**
     * Reads Worker failure information. Last N records for each application
     * 
     * @param dbName
     * @param appName
     * @param lastNrecods
     * @return List<Documents>
     * @throws UnknownHostException
     */
    abstract public List<Document> dbReadAgentWorkerFailure(String dbName, String appName, int lastNrecods) throws UnknownHostException;
    
    /**
     * Reads Provisioning information. Last N records for each application
     * 
     * @param dbName
     * @param appName
     * @param lastNrecods
     * @return List<Documents>
     * @throws UnknownHostException
     */
    abstract public List<Document> dbReadAgentProvisioningStats (String dbName, String appName, int lastNrecods) throws UnknownHostException;
    
}
