/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.commons.lang.StringUtils;
import tassl.application.utils.CommonUtils;
import tassl.workflow.Workflow;
import tassl.workflow.WorkflowStage;
import tassl.workflow.WorkflowXMLParser;

/**
 *
 * @author Javier Diaz-Montes
 */
public class WorkflowManager extends Thread{
    HashMap <String,Workflow> workflows;
    HashMap <String,String> workflowsStatus;
    int servPort;   //port of workflow manager
    String servAddress;//public ip of workflow manager
    Properties properties;
    String workflowMasterAddress;
    int workflowMasterPort;
    
    //{"workflowId":"address:port","workflowId":"address:port"}
    HashMap<String,String> subscriptions;
    
    boolean alive=true;
    boolean registeredInMaster=false; //to register port in the WorkflowMaster
    
    int schedulerPort;
    String schedulerAddress;
    WorkflowManager(Properties p, String wflMasterAdd, int wflMasterPort){        
        properties=p;
        workflows=new HashMap<String,Workflow>();
        workflowsStatus=new HashMap<String,String>();
        workflowMasterAddress=wflMasterAdd;
        workflowMasterPort=wflMasterPort;
        subscriptions=new HashMap();
    }
    
    private void setLog(String filename){
        FileHandler fh;
        try {
            fh=new FileHandler(filename, true);
            
            Logger l = Logger.getLogger(""); //Everyone will print here. Create new logger if different file wanted
            
            for (Handler i:l.getHandlers()){
                l.removeHandler(i);
            }
            
            l.setUseParentHandlers(false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.ALL);  
            
        } catch (SecurityException e){  
            System.out.println("ERROR: WorkflowManager: Setting up log");
            e.printStackTrace();
        }catch(IOException e){
            System.out.println("ERROR: WorkflowManager: Setting up log");
            e.printStackTrace();
        }             
    }
     
    @Override
    public void run() {        
        
        String portTemp=properties.getProperty("portManager");
        if(portTemp!=null){
            servPort=Integer.parseInt(portTemp);
        }else{
            System.err.println("ERROR: AgentLite: You need to specify the portManager parameter in the configuration file. ");
            System.exit(1);
        }
                
        servAddress=properties.getProperty("publicIpManager");
        if(servAddress==null){
            System.err.println("ERROR: WorkflowManager: You need to specify the public IP of this machine using the publicIpManager parameter in the configuration file. ");
            System.exit(1);
        }
        
        if(properties.getProperty("StartCentralManager", "true").equals("true")){
            schedulerAddress=properties.getProperty("CentralManagerAddress");
            schedulerPort=Integer.parseInt(properties.getProperty("CentralManagerPort"));
            int monitorInterval=Integer.parseInt(properties.getProperty("MonitorInterval","60"));
            double defaultReferenceBenchmark=Double.parseDouble(properties.getProperty("DefaultReferenceBenchmark","5757.0"));//This is to calculate speedup of resources when no real data exists and user 
                                                                                                                    //did not include a reference benchmark score in the workflow properties to relate with his/her task exec time. 
                                                                                                                    //default 5757 is the whetstone score of a medium OpenStack VM in FutureGrid Sierra.
            AutonomicScheduler scheduler=new AutonomicScheduler(schedulerAddress, schedulerPort, workflowMasterAddress, workflowMasterPort,monitorInterval,defaultReferenceBenchmark);
            scheduler.start();
        }
        
        System.out.println("Starting Server in "+servPort);
        
        ServerSocket servSock;
        try {
            servSock = new ServerSocket(servPort);
            servSock.setReuseAddress(true);
            
            setLog(properties.getProperty("logFile", "WorkflowManager.log"));
        
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "Starting Server in "+servPort); 
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "WFM ready at "+new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss.SSS").format(Calendar.getInstance().getTime())); 
            
            while (alive) {
                try {
                    Socket clientSocket = servSock.accept();
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "connection received at "+new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss.SSS").format(Calendar.getInstance().getTime())); 
                    BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                    String command=input.readLine();
                    
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "command: /"+command+"/"); 
                    if(command.equals("regWorkflow")){//register a workflow 
                        Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "About to start a new WF "+new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss.SSS").format(Calendar.getInstance().getTime())); 
                                                                        
                        String templine;
                        String workflowstring="";
                        while (!(templine = input.readLine()).equals("EOF")) {                            
                            workflowstring+=templine;
                        }
                        
                        Workflow wf= WorkflowXMLParser.XMLParser(workflowstring);
//                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "regWorkflow string: "+workflowstring); 
                        
                        //subscription to get updates when a stage gets done               
                        String subscription=input.readLine();
                        String subscriber="";
                        boolean subscribeError=false;
                        if(subscription.equals("true")){
                            //get subscriber info address:port
                            //this will be notified
                            subscriber=input.readLine();
                            if((subscriber.split(":")).length!=2){
                                out.println("ERROR: an address:port string is needed to get subscribed");
                            }
                        }
                        
                        if(!subscribeError){
                            if (wf!=null){                      
                                //register workflow, set id to the workflow and its stages
                                String workflowId=regWorkflow(wf);
                                out.println(workflowId);

                                if(subscription.equals("true")){
                                    subscriptions.put(workflowId, subscriber);
                                }

                                boolean scheduledstages=scheduleStages(workflowId);                            
                                Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "Return message: "
                                        + "{0}:{1}", new Object[]{workflowId, scheduledstages});                             
                            }else{
                                out.println("ERROR: WorkflowManager. Creating Workflow, please check the structure of the xml file.");
                            }
                        }
                        
                    }else if(command.equals("checkstatus")){//check status of a workflow
                        String templine = input.readLine();
                        String workflowId;
                        try{
                            workflowId=templine;
                            out.println(workflowsStatus.get(workflowId));
                        }catch(NumberFormatException e){
                            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, e);                     
                        }
                    }else if(command.equals("cancelWorkflow")){
                        String workflowId=input.readLine();
                        List<String> temp=new ArrayList();
                        temp.add(workflowId);
                        sendInfo(this.schedulerAddress,this.schedulerPort,"cancelWorkflow",temp);
                                                
                        workflowsStatus.put(workflowId,"Cancelled:Cancelled");
                        
                    }else if(command.equals("getSupportedApps")){                                             
                        
                        HashMap<String,HashMap<String,String>> supportedApps=this.getSupportedApps();
                        
                        for(String site:supportedApps.keySet()){
                            out.println(site);
                            out.println("    "+supportedApps.get(site).toString().replace("{", "").replace("}", "").replace(",", "\n   ").trim());
                        }
                        out.println("EOF");
                    }else if(command.equals("getResults")){
                        String workflowId = input.readLine();                        
                        
                        String listDirectories=workflows.get(workflowId).getResultList();
                        
                        out.println(listDirectories);
                        
                        
                    }else if(command.equals("addStageWorkflow")){
                        String workflowId = input.readLine();                        
                        
                        //receive the xml with the stage info
                        //receive also the objective and dependencies
                        String templine;
                        String workflowstring="";
                        while (!(templine = input.readLine()).equals("EOF")) {                            
                            workflowstring+=templine;
                        }
                        
                        Workflow wf= WorkflowXMLParser.XMLParser(workflowstring);
                        if (wf!=null && !workflows.get(workflowId).isWorkflowCanceled()){
                            //add new stages to the workflow
                            boolean status=workflows.get(workflowId).addStages(wf.getWorkflowStages());
                            if(status){
                                out.println("OK");
                                //change status workflow
                                workflowsStatus.put(workflowId, "StagesAdded:StagesAdded");                        
                                //schedule stages
                                boolean scheduledstages=scheduleStages(workflowId);                            
                                Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "Return message(After new stages are added): "
                                        + "{0}:{1}", new Object[]{workflowId, scheduledstages});                        
                            }else{
                                out.println("ERROR: WorkflowManager. Adding stages, please verify that stageIds are unique and different from existing ones.");
                            }
                        }else{
                            out.println("ERROR: WorkflowManager. Creating Workflow, please check the structure of the xml file.");
                        }
                        
                    }else if(command.equals("stageDone")){// the workflowMaster uses this to notify when a stage is done.
                        String []stagedone=input.readLine().split("\\.");
                        String latestStage=input.readLine();//only used if non-blocking transitions
                        boolean iterate=Boolean.parseBoolean(input.readLine());
                        String workflowId=stagedone[0];
                        String stageId=stagedone[1];
                        System.out.println("workflow:"+workflowId+" stage:"+stageId);
                        //set stage to DONE
                        setStageDone(workflowId, stageId, latestStage, iterate);
                        
                        boolean scheduledstages=scheduleStages(workflowId);                            
                        Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "Scheduling new stage: "
                                + "{0}:{1}", new Object[]{workflowId, scheduledstages}); 
                    }else if(command.equals("checkWorkflowManagerStatus")){// Moustafa - check if the workflow master is alive.
                        out.println("OK");                            
                    }else if(command.equals("scheduleStagesNBT")){                                               
                        HashMap <String,List<String>> wflIdStageList=new HashMap();
                        
                        String received=input.readLine();//workflowId:stageId,stageId;workflowId:stageId,stageId
                        String [] workflowStrings=received.split(";");
                        for(int i=0;i<workflowStrings.length;i++){
                            String [] partWS=workflowStrings[i].split(":");//workflowId
                            if(!workflows.get(partWS[0]).isWorkflowCanceled()){
                                wflIdStageList.put(partWS[0], Arrays.asList(partWS[1].split(",")));
                            }
                        }
                        
                        //schedule stage ({wkfid:{stageid,stageid},...})
                        this.scheduleNonBlockingStages(wflIdStageList);
                        
                    }else if(command.equals("forceWorkflowReschedule")){                        
                        List temp=new ArrayList();
                        String workflowId=input.readLine();
                        
                        temp.add(workflowId);
                        out.println("OK");
                        sendInfo(this.schedulerAddress, this.schedulerPort, "forceWorkflowReschedule",temp);
                    }else if(command.equals("forceRescheduleAll")){
                        out.println("OK");
                        sendInfo(this.schedulerAddress, this.schedulerPort, "forceRescheduleAll",null);
                    }else{
                        Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "Unknown command: "+command); 
                    }
                    input.close();
                    input.close();
                    out.close();
                } catch (IOException e) {                    
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, e);                     
                }
            }
            
        } catch (IOException ex) {            
            System.out.println("ERROR: starting Server in "+servPort);
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);            
        }
        
    }
    
    
    protected HashMap getSupportedApps() throws IOException{
        
        Object [] inout=CommonUtils.getOutputInput(schedulerAddress, schedulerPort);
        DataInputStream in=(DataInputStream)inout[0];
        DataOutputStream out=(DataOutputStream)inout[1];
        
        out.writeUTF("getSupportedApps");
        
        int length = in.readInt(); //data length
        byte[] bytesdata = new byte[length];                         
        in.readFully(bytesdata);
                                                
        HashMap<String,HashMap<String,String>> receivedObject=null;
        try {
            receivedObject = (HashMap)programming5.io.Serializer.deserialize(bytesdata);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        }
        CommonUtils.closeInOut(in, out);
        return receivedObject; 
    }
    
        
    /**
     *
     * @param workflowId id of affected workflow
     * @param stageId  stage that has to be set to done
     * @param latestStage same as stageId parameter or the id of the final stage in a non-blocking transition series
     * @param iterate
     * @return
     */
    protected boolean setStageDone(String workflowId, String stageId, String latestStage, boolean iterate){
        boolean status=true;
        
        //set stage done
        workflows.get(workflowId).setStageDone(stageId);
        
        //set following non-blocking stages as done
        String stagesInvolved=setStageDoneNB(workflowId,stageId);
        
        
        //set end time
        workflows.get(workflowId).getStage(stageId).setEndTime();
        System.out.println("CWDEBUG: workflowmanager. "
                + "Stage "+stageId+" duration is "+workflows.get(workflowId).getStage(stageId).getDurationStage()/1000.0 + " seconds");
        Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, workflowId+"." +stageId+ " "
                + "duration in seconds:"+workflows.get(workflowId).getStage(stageId).getDurationStage()/1000.0); 
        
        //check if loop continues
        if(workflows.get(workflowId).isEndLoop(latestStage)){
            if(iterate){//duplicate all stages in within
                workflows.get(workflowId).newIteration(latestStage);
                
                
                System.out.println("CWDEBUG.workflowmanager. Workflow: \n"+ workflows.get(workflowId).toString());
            
                for(String wfs:workflows.get(workflowId).getWorkflowStages().keySet()){
                    System.out.println("Stage="+wfs);
                    System.out.println("   Dependencies="+workflows.get(workflowId).getWorkflowStages().get(wfs).getDependencies().toString());
                }

                for(String sl:workflows.get(workflowId).getLoops().keySet()){
                    System.out.println("loop "+sl);
                    System.out.println("    "+workflows.get(workflowId).getLoops().get(sl));
                    System.out.println("Loops involved "+workflows.get(workflowId).getWorkflowStages().get(sl).getLoopsInvolved().toString());
                }
                
                System.out.println("loopOrder: "+workflows.get(workflowId).getLoopsOrder().toString());
                
                System.out.println("Loops Original Name");
                for(String sl:workflows.get(workflowId).getLoopsOrigName().keySet()){
                    System.out.println("loop "+sl);
                    System.out.println("    "+workflows.get(workflowId).getLoopsOrigName().get(sl));
                    System.out.println("Loops involved "+workflows.get(workflowId).getWorkflowStages().get(sl).getLoopsInvolved().toString());
                }
                
            }else{
                //workflows.get(workflowId).prepareForOuterLoop(stageId); //lets not do it here, bc we will not get the right results in the stage after the loop
                                
                //CWDEBUG
                System.out.println("CWDEBUG.workflowmanager.Workflow: \n"+ workflows.get(workflowId).toString());
            
                for(String wfs:workflows.get(workflowId).getWorkflowStages().keySet()){
                    System.out.println("Stage="+wfs);
                    System.out.println("   Dependencies="+workflows.get(workflowId).getWorkflowStages().get(wfs).getDependencies().toString());
                }

                for(String sl:workflows.get(workflowId).getLoops().keySet()){
                    System.out.println("loop "+sl);
                    System.out.println("    "+workflows.get(workflowId).getLoops().get(sl));
                    System.out.println("Loops involved "+workflows.get(workflowId).getWorkflowStages().get(sl).getLoopsInvolved().toString());
                }
                System.out.println("loopOrder: "+workflows.get(workflowId).getLoopsOrder().toString());
            }
            
        }
        
        
        //Tell scheduler stage is done           
        DataOutputStream output=null;  
        Socket sock;        
        try {     
            sock = new Socket(schedulerAddress, schedulerPort);            
            output = new DataOutputStream(sock.getOutputStream());    
            output.writeUTF("stageDone");            
            output.writeUTF(workflowId);
            output.writeUTF(stageId);
            output.writeUTF(stagesInvolved);//stages involved. only used to print accounting
            if(workflows.get(workflowId).isDone()){
                output.writeUTF("end");
            }else{
                output.writeUTF("noend");
            }            
            output.writeDouble(workflows.get(workflowId).getStage(stageId).getDurationStage()/1000.0);
        } catch (UnknownHostException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        } catch (IOException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        }finally{            
            CommonUtils.closeInOut(null, output);
        }
        
        //if there is a subscriber then notify it
        if(subscriptions.containsKey(workflowId)){
            String []parts=subscriptions.get(workflowId).split(":");
            
            String message="stageDone\n"+
                            workflowId+"\n"
                            +stageId+"\n";
            
            contactSubscriber(parts[0],Integer.parseInt(parts[1]), message);
            
        }
                
        return status;
    }
    
    /**
     * Set stage to done. If there is
     * @param workflowId
     * @param stageId
     * @return stages involved. this is to print it in the accounting
     */
    private String setStageDoneNB(String workflowId, String stageId){
        String toReturn = stageId;
        List<String> NBtransitions=workflows.get(workflowId).getStage(stageId).getNonBlockingTransitions();
        if(!NBtransitions.isEmpty()){            
            for(int i=0;i<NBtransitions.size();i++){
                String stid=NBtransitions.get(i);                
                toReturn+="-"+setStageDoneNB(workflowId,stid);
            }
            workflows.get(workflowId).setStageDone(stageId);
        }else{
            workflows.get(workflowId).setStageDone(stageId);
        }
        return toReturn;
    }
    
    /**
     * Send messages to the Subscriber. It uses a PrintWriter for that.
     * @param address
     * @param port
     * @param message The message is a string. If multiple lines are sent, they should be separated by the '\n' character
     * @return
     */
    public boolean contactSubscriber(String address, int port, String message){
        boolean status=true;
        PrintWriter out=null;
        try {
            Socket sock = new Socket();        
            sock.connect(new InetSocketAddress(address, port), 10000);//5 second timeout.
            
            out = new PrintWriter(sock.getOutputStream(), true);
            for(String i:message.split("\n")){
                out.println(i);
            }
            out.println("EOF");
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        }catch (IOException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        }finally{            
            if(out!=null){
                out.close();
            }
        }
        return status;
    }
    
    /**
     * Find stages that are ready to execute, generate their tasks by
     * calling the taskgenerator(workflowmaster) and schedule them by
     * calling the autonomic scheduler
     * 
     * @param wflId
     * @return true if there are stages to execute
     */
    public boolean scheduleStages(String wflId){
        boolean morestages=true;
        Workflow wf=workflows.get(wflId);
        if(!wf.isDone()){
            List stages=wf.getExecutableStages();        
            if (stages.size()>0){
                Iterator<String> it = stages.iterator();
                workflowsStatus.put(wflId, "GeneratingTasksFor:"+StringUtils.join(it,","));
                //Generating Tasks
                String stagesTasksInfo=stageToTasks(wflId,stages);
                
                if (stagesTasksInfo.startsWith("ERROR")){
                    workflowsStatus.put(wflId, stagesTasksInfo+". After "+"GeneratingTasksFor:"+StringUtils.join(it,","));
                }else{
                    System.out.println("WorkflowManager: "+stagesTasksInfo);
                    System.out.println(Arrays.toString(stages.toArray()));

                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "WorkflowManager: Tasks of the available stages "
                            + " of workflow "+wflId+" have been generated: "+stagesTasksInfo);            

                    workflowsStatus.put(wflId, "ReadyToSchedule:"+stagesTasksInfo);

                    //send back stage1-firsttaskid,#tasks;stage2-firsttaskid,#tasks;  
                    //thus, last taskid = firsttaskid+#tasks-1
                    String []temp=stagesTasksInfo.split(";");
                    for (int i=0;i<stages.size();i++){
                        String []oneS=temp[i].split("-");
                        WorkflowStage s=(WorkflowStage)stages.get(i);
                        if(!s.getId().equals(oneS[0])){
                            System.out.println("CWDEBUG: ERROR: stages not in order");
                        }else{
                            System.out.println("CWDEBUG: Stage order Ok");
                            /* Not USED. Set to remove in 11/20/2014 //s.setTasksInfo(oneS[1]);      */
                            s.setStartTime();
                        }
                    }
                                        
                    //Send it to scheduler
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "WorkflowManager: Sending tasks"+
                            " to scheduler. WorkflowId={0} Details={1}", new Object[]{wflId, stagesTasksInfo});   
                    if (!scheduleStagesTasks(wflId,stages)){
                        workflowsStatus.put(wflId, "ErrorSendingScheduler:"+stagesTasksInfo);
                    }else{
                        workflowsStatus.put(wflId, "Scheduling_Executing:"+stagesTasksInfo);
                    }
                }
            }
        }else{            
            workflowsStatus.put(wflId, "done:"+ StringUtils.join(wf.getCompletedStages().toArray(),","));
            morestages=false;
        }
        
        return morestages;
    }
    
    /**
     * Schedule partial stages (after non-blocking transition) They will
     * be added to the original stage in the scheduling
     * 
     * @param wflIdStageList
     * 
     */
    public void scheduleNonBlockingStages(HashMap<String,List<String>> wflIdStageList){
        
        for(String wflId:wflIdStageList.keySet()){
            //get workflowStage objects for each stage of this workflow
            Workflow wf=workflows.get(wflId);            
            List <WorkflowStage> stages=new ArrayList();
            for(String stageId:wflIdStageList.get(wflId)){
                stages.add(wf.getStage(stageId));
            }

            String statusString=workflowsStatus.get(wflId);
            workflowsStatus.put(wflId, statusString+".NB-GeneratingTasksFor:"+StringUtils.join(stages.toArray(),","));
            
            //Generating Tasks. it also takes care to add them to original stage, and its objective 
            String stagesTasksInfo=stageToTasks(wflId,stages);

            if (stagesTasksInfo.startsWith("ERROR")){                
                workflowsStatus.put(wflId, statusString+"."+stagesTasksInfo+". After NB-GeneratingTasksFor:"+StringUtils.join(stages.toArray(),","));
                Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, "WorkflowManager. : After NB-GeneratingTasksFor:"+StringUtils.join(stages.toArray(),","));            
            }else{
                System.out.println("WorkflowManager: "+stagesTasksInfo);
                System.out.println(Arrays.toString(stages.toArray()));

                Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "WorkflowManager: NB-Tasks of the available stages "
                        + " of workflow "+wflId+" have been generated: "+stagesTasksInfo);            

                

                //for rescheduling
                HashMap <String, String> wflStage_workerTasks=new HashMap();                
                HashMap<String,WorkflowStage> wflStageList=new HashMap();
                
                String status="";
                if(!statusString.contains("NBReadyToSchedule_")){
                    status="NBReadyToSchedule_";
                }
                String nbStagestoadd="";
                
                //sent back stage2-firsttaskid,#tasks,stage1,0.3; //here stage2 follows a non-blocking transition originated by stage1, and stage1 is completed at 30%
                //thus, last taskid = firsttaskid+#tasks-1                                
                String []temp=stagesTasksInfo.split(";");
                for (int i=0;i<stages.size();i++){
                    String []oneS=temp[i].split("-");     
                    String [] partsOneS=oneS[1].split(",");
                    String originalStage=partsOneS[2];
                    
                    System.out.println("CWDEBUG: scheduleNonBlockingStages -  NumTasks="+partsOneS[1]+" OrigCompletion="+partsOneS[3]);                    
                    if(partsOneS[1].equals("0") && partsOneS[3].equals("1.0")){
                        //this case means that a task of a stage finishes and it does not generate any task in the following non-blocking stage.
                        //This can cause the workflow to block.
                        System.out.println("CWDEBUG: Previous stage didn't generate tasks and original stage is 100 done. Call to see if this stage is done as well.");
                        //call master to check again if it is done          
                        List param=new ArrayList();
                        param.add(wflId+"."+oneS[0]);
                        this.sendInfo(workflowMasterAddress, workflowMasterPort, "checkStageDoneNoNewTask", param);
                        
                    }else{                    
                    
                        if(!wflStage_workerTasks.containsKey(wflId+"."+originalStage)){
                            wflStage_workerTasks.put(wflId+"."+originalStage, "null;null;null");
                            wflStageList.put(wflId+"."+originalStage,workflows.get(wflId).getStage(originalStage));
                        }
                    }
                    //the non-blocking stages we only include stage id
                    if(!statusString.contains(oneS[0])){
                        nbStagestoadd+=","+oneS[0];
                    }
                    
                    //just a test
                    WorkflowStage s=(WorkflowStage)stages.get(i);
                    if(!s.getId().equals(oneS[0])){
                        System.out.println("CWDEBUG: ERROR: stages not in order");
                    }else{
                        System.out.println("CWDEBUG: Stage order Ok");                        
                        s.setStartTime();
                    }
                }
                workflowsStatus.put(wflId, statusString+""+status+""+nbStagestoadd);
                
                
                System.out.println("CWDEBUG: wflStage_workerTasks: "+wflStage_workerTasks.toString());
                Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO,"CWDEBUG: wflStage_workerTasks: "+wflStage_workerTasks.toString());
                
                //Send it to scheduler
                Logger.getLogger(WorkflowManager.class.getName()).log(Level.INFO, "NB-WorkflowManager: Sending tasks"+
                        " to scheduler. WorkflowId={0} Details={1}", new Object[]{wflId, stagesTasksInfo});   
                if (!scheduleStagesTasksNBT(wflStage_workerTasks,wflStageList)){
                    workflowsStatus.put(wflId, statusString+".NBErrorSendingScheduler:"+nbStagestoadd);
                }else{
                    if(!statusString.contains("NBScheduling_Executing_")){
                        status="NBScheduling_Executing_";
                    }else{
                        status="";
                    }
                    workflowsStatus.put(wflId, statusString+""+status+""+nbStagestoadd);
                }
            }
            
        }
        
    }
    
    public boolean sendInfo(String address, int port, String command, List param){
        boolean status=true;
        DataInputStream in=null;
        DataOutputStream out=null;
        try {
            Object []stream;
            
            stream=CommonUtils.getOutputInput(address,port);
            
            in=(DataInputStream)stream[0];
            out=(DataOutputStream)stream[1];
            if (in!=null&&out!=null){                
                if(command.equals("checkStageDoneNoNewTask")){   
                   out.writeUTF("checkStageDoneNoNewTask");
                   out.writeUTF((String)param.get(0));
                }else if(command.equals("forceWorkflowReschedule")){
                    out.writeUTF("forceWorkflowReschedule");
                    out.writeUTF((String)param.get(0));
                }else if(command.equals("forceRescheduleAll")){
                    out.writeUTF("forceRescheduleAll");
                }else if(command.equals("cancelWorkflow")){
                    out.writeUTF("cancelWorkflow");
                    out.writeUTF((String)param.get(0));
                }
            }else{
                status=false;
            }
        } catch (IOException ex) {            
            
            status=false;
        } finally{            
            if (out!=null){
                try {
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }  
            if(in!=null){
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }    
        return status;
    }
    
    /**
     * Schedule stages of a workflow
     * @param wflId
     * @param stages
     * @return true if no problem,or false otherwise
     */
    private boolean scheduleStagesTasks(String wflId, List stages){
        boolean status=true;
        Socket sock;        
        DataOutputStream output=null;        
        try {
            sock = new Socket(schedulerAddress, schedulerPort);            
            output = new DataOutputStream(sock.getOutputStream());            
            byte[] forwardObj = null;
            forwardObj = programming5.io.Serializer.serializeBytes(stages);
            
            output.writeUTF("scheduleStagesTasks");            
            output.writeUTF(wflId);            
            output.writeInt(forwardObj.length);            
            output.write(forwardObj);            
            
                        
        } catch (UnknownHostException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        } catch (IOException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        } finally{            
            if (output!=null){
                try {
                    output.close();
                } catch (IOException ex) {
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }            
        }        
        
        return status;        
    }
    
    /**
     * Schedule tasks from a stage involved in a non-blocking transition
     * @param wflStage_workerTasks. Format that AS rescheduling understand
     * @param wflStageList {workflowId.stageId:WorkflowStage,...}
     * @return
     */
    private boolean scheduleStagesTasksNBT(HashMap <String, String> wflStage_workerTasks,HashMap<String,WorkflowStage> wflStageList){
        boolean status=true;
        Socket sock;        
        DataOutputStream output=null;        
        try {
            sock = new Socket(schedulerAddress, schedulerPort);            
            output = new DataOutputStream(sock.getOutputStream());            
            byte[] forwardObj = null;
            forwardObj = programming5.io.Serializer.serializeBytes(wflStage_workerTasks);
            
            byte[] forwardObj1 = null;
            forwardObj1 = programming5.io.Serializer.serializeBytes(wflStageList);
            
            output.writeUTF("scheduleStagesTasksNBT");            
            output.writeInt(forwardObj.length);            
            output.write(forwardObj);       
            output.writeInt(forwardObj1.length);            
            output.write(forwardObj1);               
                        
        } catch (UnknownHostException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        } catch (IOException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            status=false;
        } finally{            
            if (output!=null){
                try {
                    output.close();
                } catch (IOException ex) {
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }            
        }        
        
        return status;        
    }
    
    /**
     * Call the taskgenerator to get list of tasks
     * 
     * @param wflId
     * @param stages
     * @return a String with task information. 
     *   Send back stage1-firsttaskid,#tasks;stage2-firsttaskid,#tasks;  //when non-blocking transitions not involved (called from scheduleStages)
     *   Send back stage2-firsttaskid,#tasks,stage1,0.3; //(when called from scheduleNonBlockingStages) here stage2 follows a non-blocking transition originated by stage1, and stage1 is completed at 30%
     */
    private String stageToTasks(String wflId,List <WorkflowStage>stages){
        String stagesTasksInfo="";
        Socket sock;
        DataInputStream input=null;
        DataOutputStream output=null;
        try {
            sock = new Socket(workflowMasterAddress, workflowMasterPort);
            input=new DataInputStream(sock.getInputStream());
            output = new DataOutputStream(sock.getOutputStream());

            output.writeUTF("generateTasks");
            if(!registeredInMaster){                
                output.writeUTF("newWorkflowManager");
                output.writeInt(servPort);
                output.writeUTF(servAddress);
                registeredInMaster=true;
            }else{
                output.writeUTF("registeredWorkflowManager");
            }
            output.writeUTF(wflId);
            byte[] forwardObj = null;
            forwardObj = programming5.io.Serializer.serializeBytes(stages);                
            output.writeInt(forwardObj.length);
            output.flush();
            output.write(forwardObj);
            output.flush();

            //send back stage1-firsttaskid,#tasks;stage2-firsttaskid,#tasks;  //when non-blocking transitions not involved
            //thus, last taskid = firsttaskid+#tasks-1            
            //or
            //send back stage2-firsttaskid,#tasks,stage1,0.3; //here stage2 follows a non-blocking transition originated by stage1, and stage1 is completed at 30%
            stagesTasksInfo=input.readUTF();
            
            //TODO.. if #tasks is 0 then set stage to done, if everything before is 100% completed. 
            
            
            //identify the ones involved in a non-blocking transition
            HashMap<String,String[]> nonBlockingStages=new HashMap();
            for(String stageString:stagesTasksInfo.split(";")){
                String [] tempStr=stageString.split(",");
                if(tempStr.length==4){
                    //stageId:{originalStageId,percentage}
                    nonBlockingStages.put(tempStr[0].split("-")[0], new String[]{tempStr[2],tempStr[3]});
                }
            }                       
            
            //get tasks lists
            int length = input.readInt(); //data length             
            byte[] bytesdata = new byte[length];
            input.readFully(bytesdata);            
            HashMap <String,HashMap>stagesTaskProp=new HashMap();
            try {
                stagesTaskProp = (HashMap)programming5.io.Serializer.deserialize(bytesdata);                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            }                                    
            //put it into stages directly
            for (WorkflowStage tempS:stages){ //update stage with its tasks properties
                tempS.setTaskProp(stagesTaskProp.get(tempS.getId()));
                
                //check if stage is involved in a non-blocking transition to ADD tasks and objective to original stage
                if(nonBlockingStages.containsKey(tempS.getId())){
                    //ADD tasks to original stage for scheduling (this is the first "from" stage that led to non-blocking transitions)
                    WorkflowStage originalStage=workflows.get(wflId).getStage(nonBlockingStages.get(tempS.getId())[0]);
                    originalStage.setTaskProp(stagesTaskProp.get(tempS.getId()));
                    //ADD objective                    
                    String objectiveType=tempS.getObjectiveType();
                    //get objective from stage
                    double objectiveValue=tempS.getObjectiveValue(objectiveType);
                    //get percentace that can be released
                    double percentage=Double.parseDouble(nonBlockingStages.get(tempS.getId())[1]);
                    Double valueToAdd=Double.valueOf(objectiveValue*percentage);
                    //add objective to original stage for scheduling
                    originalStage.addObjectiveAddOnFromNonBlockingT(tempS.getId(), valueToAdd.toString());
                    
                }
            }            
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            stagesTasksInfo="ERROR: in Manager. unkown host";
        } catch (IOException ex) {
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
            stagesTasksInfo="ERROR: in Manager. Problem communicating with master";
        } finally{
            if (input!=null){
                try {
                    input.close();
                } catch (IOException ex) {
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (output!=null){
                try {
                    output.close();
                } catch (IOException ex) {
                    Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }            
        }
        return stagesTasksInfo;
    }
    private String regWorkflow(Workflow wf){
        String id=getRandomId();
        wf.setId(id);
        workflows.put(id, wf);
        workflowsStatus.put(id,"Registered:Registered");
        return id;
    }
    
    private String getRandomId(){
        String randomInt="0";
        do{
            Random random = new Random(System.nanoTime());
            randomInt = random.nextInt(1000000000)+"";      
        }while(workflows.containsKey(randomInt));
        
        return randomInt;
    }
    public static void main(String[] args) {        

        if (args != null) {
            try {
                                
                List<String> appPropertyFiles = CommonUtils.getMultipleStringArg(args,"-propertyFile");
                Properties p = new Properties();
                for (String propertyFile : appPropertyFiles) {
                    p.load(new FileInputStream(propertyFile));
                }
                
                String tempuri=p.getProperty("workflowmasterURI");
                String masterAddress="";
                int masterPort=0;
                try{
                    if (tempuri!=null){
                        String []parts=tempuri.split(":");
                        masterAddress=parts[0];
                        masterPort=Integer.parseInt(parts[1]);                    
                    }else{
                        System.err.println("ERROR: Please specify the URI of the workflowMaster. workflowmasterURI=address:port");
                        System.exit(1);
                    }
                }catch(ArrayIndexOutOfBoundsException e){
                    System.err.println("ERROR: Please specify the URI of the workflowMaster. workflowmasterURI=address:port");
                    System.exit(1);
                }
                
                WorkflowManager wfM=new WorkflowManager(p,masterAddress,masterPort);
                wfM.start();
                
            } catch (IOException e) {
                System.err.println("ERROR: Check loading property files. "+e.getMessage());
            }
        } else {
            System.err.println("ERROR: WorkFlowManager: You need to specify a configuration file with properties of the service.");            
            System.exit(1);
        }
        
        
    }
}
