/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import programming5.io.ArgHandler;
import tassl.application.utils.CommonUtils;

/**
 *
 * @author Javier Diaz-Montes
 */
public class WorkflowClient {
    PrintWriter out = null;
    BufferedReader in = null;
    int servPort;        
    String servAddress;
    Properties properties;
    
    public WorkflowClient(String servIp,int servPort, Properties p){
        this.servPort=servPort;
        this.servAddress=servIp;
        this.properties=p;
    }
    
    private void setLog(String filename){
        FileHandler fh;
        try {
            fh=new FileHandler(filename, true);
            Logger l = Logger.getLogger(WorkflowClient.class.getName());
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.ALL);  
        } catch (SecurityException e){            
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }             
    }
    
    public boolean connect(){
        boolean status=true;
        Socket clientSocket = null;
        try {
            clientSocket = new Socket(servAddress, servPort);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: "+servAddress+":"+servPort);
            status=false;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                               + "the connection to: "+servAddress+":"+servPort);
            status=false;
        }
        return status;
    }
    public void disconnect(){
        out.close();
        try {
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String submit(String workflowString){   
        String answer="";
        
        connect();
        
        BufferedReader br;
        try {
            
            br = new BufferedReader(new FileReader(workflowString));        
            String line="";
            
            out.println("regWorkflow");
            
            while ((line = br.readLine()) != null) {
                
                if(line.contains("PropertyFile")){ //we send property values instead of the file
                    line=replaceFileValue(line);
                }
                out.println(line);
            }            
            out.println("EOF");
            
            out.println("false");//subscription mode.
            
            answer=in.readLine();                        
            disconnect();
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex){
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return answer;
    }
    
    /**
     * Read file and replace \n with the equivalent in XML - &#10;
     * @param line path to the property file
     * @return the content of the file in a single string
     */
    public String replaceFileValue(String line){
        String newline="";
        
        try{
            
            StringReader reader = new StringReader(line);

            reader.skip(line.indexOf("value=\"")+ "value=\"".length());                    
            char c;               
            String propertyFile="";
            while((c=(char)reader.read())!= -1 && c != '\"'){
                propertyFile+=c;
            }
            reader.close();
                    
            //open propertyfile
            BufferedReader br = new BufferedReader(new FileReader(propertyFile));
            String propertiesValues="";
            String propertyLine;
            while ((propertyLine = br.readLine()) != null) {                
                propertiesValues+=propertyLine+"&#10;"; //new line character in XML
            }
            br.close();
            
            newline=line.replace(propertyFile, propertiesValues);
                        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex){
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return newline;
    }
    
    public String checkStatus(String wfId){
        String answer="";
        connect();
        out.println("checkstatus");
        out.println(wfId);
        try {     
            answer=in.readLine();
        } catch (IOException ex) {
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        disconnect();
        return answer;
    }
    

    /**
     * Get list of Result locations and retrieve them
     * 
     * @param wfid
     * @param user
     * @param path
     */
    public void getResults(String wfid,String user,String path){
        String answer="";
                
        connect();
        out.println("getResults");
        out.println(wfid);
        try {     
            answer=in.readLine();
        } catch (IOException ex) {
            Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        disconnect();
        
        String [] list=answer.split(";");
        for(String val:list){
            CommonUtils.execute("scp -r "+user+"@"+val+" "+path);                
        }
        
    }
    
    public boolean cancelWorkflow(String wfid){
        boolean status=false;               
        if(connect()){            
            out.println("cancelWorkflow");
            out.println(wfid);
            status=true;
            disconnect();
        }else{
            status=false;
        }      
        return status;
                
    }
    
    public String getSupportedApps() throws IOException{
        String toPrint;
        if(connect()){
            out.println("getSupportedApps");
            
            toPrint="";
            String templine="";
            while (!(templine = in.readLine()).equals("EOF")) {                            
                toPrint+=templine+"\n";
            }
            
        }else{
            toPrint="ERROR connecting to the workflow manager";
        }
        return toPrint;
    }
    
    public static void main(String[] args) {
        int servPort = 0; 
        String servAddress="";
        String operationArg=null;
                
        if (args.length!=0) {
            try {                
                ArgHandler argHandler = new ArgHandler(args);
                if(argHandler.getSwitchArg("-help")){
                    usage();
                    System.exit(-1);
                }
                if (argHandler.getSwitchArg("-serverPort")){
                    servPort = argHandler.getIntArg("-serverPort");//workflow manager port
                }else{
                    System.out.println("ERROR: WorkFlowManager: You need to specify the workflow manager port (-port). ");
                    usage();
                    System.exit(-1);
                }
                if (argHandler.getSwitchArg("-serverAddress")){
                    servAddress = argHandler.getStringArg("-serverAddress");//workflow manager address
                }else{
                    System.out.println("ERROR: WorkFlowManager: You need to specify the workflow manager address (-serverAddress). ");
                    usage();
                    System.exit(-1);
                }
                Vector<String> appPropertyFiles = argHandler.getMultipleStringArg("-propertyFile");
                Properties p = new Properties();
                for (String propertyFile : appPropertyFiles) {
                    p.load(new FileInputStream(propertyFile));                
                }
                WorkflowClient wfC=new WorkflowClient(servAddress,servPort, p);
                wfC.setLog(p.getProperty("logFile", "WorkflowClient.log"));
                //operations
                if (argHandler.getSwitchArg("-regWorkflow")){
                    operationArg = argHandler.getStringArg("-regWorkflow");//workflow file
                    String answer=wfC.submit(operationArg);
                    System.out.println(answer);
                }else if (argHandler.getSwitchArg("-checkStatus")){
                    operationArg = argHandler.getStringArg("-checkStatus");//workflow id            	
                    String answer=wfC.checkStatus(operationArg);
                    String []status=answer.split(":");
                    if(answer.equals("null")){
                        System.out.println("Invalid workflow identifier");
                    }else{
                        System.out.println("The workflow status is: "+status[0]);
                        status[0]=status[0].trim();
                        if (status[0].equals("Scheduling_Executing")){                        
                            for(String fs:status[1].split(";")){
                                if(!fs.startsWith("NB")){
                                    String []parts=fs.split("-");
                                    System.out.println("    Stage: "+parts[0]+ " has "+parts[1].split(",")[1]+" tasks");
                                }else{
                                    String[] nbpart=fs.split("_,");
                                    System.out.println("      Additional non-blocking transtition Stages "+nbpart[1]+" are in status "+nbpart[0]);
                                }
                            }
                        }else if(status[0].equals("ERROR")){
                            System.out.println("    "+answer);
                        }
                    }
                }else if (argHandler.getSwitchArg("-getResults")){
                    String wfid=argHandler.getStringArg("-getResults");//workflow id 
                    
                    if(argHandler.getSwitchArg("-user")){
                        
                        String user=argHandler.getStringArg("-user");
                        
                        if(argHandler.getSwitchArg("-path")){
                            String path=argHandler.getStringArg("-path");
                            wfC.getResults(wfid,user,path);
                        }else{
                            System.err.println("Please provide the -path option with the path to place the results");
                            usage();
                        }
                        
                    }else{
                        System.err.println("Please provide the -user option with your remote userId");
                        usage();
                    }
                    
                }else if (argHandler.getSwitchArg("-cancelWorkflow")){
                    String wfid=argHandler.getStringArg("-cancelWorkflow");//workflow id 
                    if(wfC.cancelWorkflow(wfid)){
                        System.out.println("Workflow "+wfid+" has been Cancelled.");
                    }else{
                        System.err.println("Error contacting workflow manager at "+servAddress+":"+servPort);
                    }
                    
                }else if (argHandler.getSwitchArg("-getSupportedApps")){
                    System.out.println(wfC.getSupportedApps());
                    
                }else{
                    usage();
                }
                
            } catch (Exception e) {
                System.err.println("Check your arguments "+e.getMessage());
                Logger.getLogger(WorkflowClient.class.getName()).log(Level.SEVERE, null, e);
            }
        } else {
            //System.out.println("ERROR: WorkFlowManager: At least You need to specify the port (-port). The publicIp is also recommended (-publicIp)");            
            usage();
            System.exit(-1);
        }
        
        
        
    }
    private static void usage(){
        System.out.println("Usage: -serverPort <port> -serverAddress <serverAddress> "
                + "(-regWorkflow <workflowFile> "
                + "| -checkStatus <workflowId> "
                + "| -cancelWorkflow <workflowId> "
                + "| -getSupportedApps"
                + "| -getResults <workflowId> -user <remoteUserId> -path <local path>)");
        System.out.println("Parenthesis means that you need to specify one of the options.");
    }
}
