/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import tassl.application.cometcloud.FileProperties;
import tassl.workflow.WorkflowStage;
import tassl.workflow.resources.Resource;

/**
 *
 * @author Javier Diaz-Montes
 */
public abstract class PolicyAbstract {
    
    //{task:workerForScheduler,...}
    protected HashMap <Integer,WorkerForScheduler> MapTask;
    
    //These five variables are READ-ONLY in the sense that can only be modified in the AutonomicScheduler class
    //<"ip:port":Resource>
    protected HashMap <String,Resource>globalResources; //this stores resources of each agent
    //"ip:port"
    protected List <String>AgentList; //this is used to keep a order in the list of agents
    //this is used to keep the network speed between agents (MB/s)
    //[0]- {0 2 4}; [1]- {4 0 3}; [2] - {1 3 0} 
    List <List<Double>> AgentToAgentNetworkPush; //main list element push data to secondary list elements
    List <List<Double>> AgentToAgentNetworkPull; //main list element pull data from secondary list elements
    //this is used to keep the network speed between agent and working machine (MB/s)
    //[0]- {10 33}; [1]- {2 11}; [2] - {48 48} 
    List <Double[]> AgentToMachine;          //0 element is push to machine, 1 element is pull from machine
    
    public PolicyAbstract(){
        this.globalResources=new HashMap();
        this.AgentList=new ArrayList();
        this.AgentToAgentNetworkPush=new ArrayList();
        this.AgentToAgentNetworkPull=new ArrayList();
        this.AgentToMachine=new ArrayList();
        this.MapTask=new HashMap();
    }
    
    public PolicyAbstract(HashMap globalResources, List AgentList,List AgentToAgentNetworkPush,List AgentToAgentNetworkPull, List AgentToMachine){
        this.initialize(globalResources,AgentList,AgentToAgentNetworkPush,AgentToAgentNetworkPull,AgentToMachine);
    }
    
    public final void initialize(HashMap globalResources, List AgentList,List AgentToAgentNetworkPush,List AgentToAgentNetworkPull, List AgentToMachine){
        this.globalResources=globalResources;
        this.AgentList=AgentList;
        this.AgentToAgentNetworkPush=AgentToAgentNetworkPush;
        this.AgentToAgentNetworkPull=AgentToAgentNetworkPull;
        this.AgentToMachine=AgentToMachine;
        this.MapTask=new HashMap();
    }
    
    public HashMap <Integer,WorkerForScheduler> getMapTask(){
        return MapTask;
    }
    
    protected List<Integer> getOriginFiles(List <FileProperties>inputFiles){
        List <Integer>originFiles=new ArrayList();      
        for(FileProperties fp:inputFiles){                                
            String sourceAddress=fp.getLocation().split(":")[0];          
            //System.out.println("CWDEBUG: policyabstract: file "+fp.getName()+" is at "+sourceAddress);
            if(sourceAddress.contains("@")){
                sourceAddress=sourceAddress.split("@")[1];
            }
            System.out.println("CWDEBUG: policyabstract: file "+fp.getName()+" is at "+sourceAddress);
            for(int j=0;j<AgentList.size();j++){                                    
                if(AgentList.get(j).startsWith(sourceAddress) && agentUp(sourceAddress)){
                    originFiles.add(j);
                    break;
                }
            }
        }
        return originFiles;
    }
    
    private boolean agentUp(String sourceAddress){
        for(String agentStr:globalResources.keySet()){
            if(agentStr.startsWith(sourceAddress)){
                return true;
            }
        }
        return false;
    }
    
    protected List<Integer> getDestinationFiles(List <FileProperties>outputFiles){
        List <Integer>destinationFiles=new ArrayList();
        for(FileProperties fp:outputFiles){
            String temp=fp.getLocation();
            if (!temp.equals("")){
                String sourceAddress=temp.split(":")[0];                
                if(sourceAddress.contains("@")){
                    sourceAddress=sourceAddress.split("@")[1];
                }
                System.out.println("CWDEBUG: policyabstract: file "+fp.getName()+" goes to "+sourceAddress);
                for(int j=0;j<AgentList.size();j++){                            
                    if(AgentList.get(j).startsWith(sourceAddress)){
                        destinationFiles.add(j);
                        break;
                    }
                }
            }
        }
        return destinationFiles;
    }
    
    /**
     * Check if all input files of a task can be moved to the site under consideration
     * @param inputFiles files to transfer
     * @param siteIndex destination
     * @return boolean
     */
    protected boolean checkInputConstraints(List <FileProperties>inputFiles, int siteIndex){
                
        //destination info
        Resource siteRes=globalResources.get(AgentList.get(siteIndex));
        String siteName=siteRes.getIdentifier(); //sitename
        String sizeZone=siteRes.getZone(); //zone
        
        System.out.println("*******CWDEBUG.policyAbstract. Considering site: "+AgentList.get(siteIndex)+" sitename:"+siteName+" zone:"+sizeZone);
        
        for(FileProperties fp:inputFiles){
            System.out.println("*******CWDEBUG.policyAbstract. File: "+fp.toString());
                                                
            if (!fp.getConstraints().isEmpty() && !fp.getConstraints().contains(siteName) && !fp.getConstraints().contains(sizeZone)){
                System.out.println("*******CWDEBUG.policyAbstract WARNING: it cannot be moved to site:"+siteName);
                return false;
            }                    
        }        
        
        return true;
    }
    
    /**
     * Calculate information about transferring input files to the siteIndex site.
     * 
     * @param originFiles
     * @param inputFiles
     * @param siteIndex
     * @return [time to transfer, costInData, costOutData]
     */
    protected double[] calculateInputTransfer(List<Integer> originFiles, List <FileProperties>inputFiles, int siteIndex){    
        double [] result=new double[3];
        double totalCostInData=0.0; // moving data inside cloud
        double totalCostOutData=0.0; //from the sites where we pull data
               
        double totalTransfer=0.0;
        
        String agentSite=AgentList.get(siteIndex);
        double CostDataIn=globalResources.get(agentSite).getCostDataIn();
        
        //System.out.println("CWDEBUG: autonomicScheduler. in calculateInputTransfer");
        
        //System.out.println("CWDEBUG: autonomicScheduler. in calculateInputTransfer size originfiles"+originFiles.size());
        
        for(int j=0;j<originFiles.size();j++){                    
            int indexOrigin=originFiles.get(j); //site where the file is
            System.out.println("CWDEBUG: autonomicScheduler. "+inputFiles.get(j).getName()+" siteIndex "+siteIndex+" is "
                    + "fileOrigin "+indexOrigin);
            double speed=AgentToAgentNetworkPull.get(siteIndex).get(indexOrigin);//speed in MB
            //System.out.println("CWDEBUG: autonomicScheduler. network speed when "+AgentList.get(siteIndex)+" is "
            //        + "pulling file from "+AgentList.get(indexOrigin) +" is: "+speed);
            if(speed!=0.0){
                double size=((FileProperties)inputFiles.get(j)).getSize()/(1024D*1024D); //size in MB                        
                totalTransfer+=size/speed;
                
                totalCostInData+=size/(1024D)*CostDataIn; //cost if per GB
                totalCostOutData+=size/(1024D)*(globalResources.get(AgentList.get(indexOrigin)).getCostDataOut());
                //System.out.println("CWDEBUG: autonomicScheduler. Cost in "+CostDataIn+ " size "+ size +" total "+totalCostInData);
                //System.out.println("CWDEBUG: autonomicScheduler. Cost out "+(globalResources.get(AgentList.get(indexOrigin)).getCostDataOut())+ " size "+ size +" total "+totalCostOutData);
            }
            //transfer from login node to VM. FIXED                        
            totalTransfer+=((FileProperties)inputFiles.get(j)).getSize()/(1024D*1024D)/AgentToMachine.get(siteIndex)[0];

            //System.out.println("CWDEBUG: autonomicScheduler. total incoming transfer time if task goes to "+ 
            //        AgentList.get(siteIndex)+" would be "+totalTransfer);
        }
        result[0]=totalTransfer;
        result[1]=totalCostInData;
        result[2]=totalCostOutData;
        return result;
    }
    
    /**
     *
     * @param destinationFiles
     * @param outputFiles
     * @param siteIndex
     * @return [time to transfer, costInData, costOutData]
     */
    public double[] calculateOutputTransfer(List<Integer> destinationFiles, List <FileProperties>outputFiles, int siteIndex){
        double [] result=new double[3];
        double totalCostInData=0.0; // moving data inside destination site
        double totalCostOutData=0.0; //moving data outside this site
        
        double totalOutTransfer=0.0;
        
        String agentSite=AgentList.get(siteIndex);
        double CostDataOut=globalResources.get(agentSite).getCostDataOut();
        
        for(int j=0;j<destinationFiles.size();j++){              
            int indexDest=destinationFiles.get(j); //site where the file has to go

            double speed=AgentToAgentNetworkPush.get(siteIndex).get(indexDest);//speed in MB
            System.out.println("CWDEBUG: autonomicScheduler. calculateOutputTransfer. network speed when "+AgentList.get(siteIndex)+" is "
                    + "pushing file to "+AgentList.get(indexDest) +" is: "+speed);
            if(speed!=0.0){
                double size=((FileProperties)outputFiles.get(j)).getSize()/(1024D*1024D); //size in MB                        
                totalOutTransfer+=size/speed;
                
                totalCostOutData+=size/(1024D)*CostDataOut;
                totalCostInData+=size/(1024D)*(globalResources.get(AgentList.get(indexDest)).getCostDataIn());
                
            }
            //transfer from VM to loginnode. FIXED
            totalOutTransfer+=((FileProperties)outputFiles.get(j)).getSize()/(1024D*1024D)/AgentToMachine.get(siteIndex)[1];

            System.out.println("CWDEBUG: autonomicScheduler. total output transfer time if task goes to "+ 
                    AgentList.get(siteIndex)+" would be "+totalOutTransfer);
        }      
                
        result[0]=totalOutTransfer;
        result[1]=totalCostInData;
        result[2]=totalCostOutData;
        return result;        
    }
    
    /**
     *
     * @param wkflId
     * @param stage
     * @param globalSlots available scheduling slots (workers)
     * @return mapTasks. It will be empty if something failed
     */
    public abstract HashMap scheduleStage(String wkflId, WorkflowStage stage,
            HashMap <String,List<WorkerForScheduler>>globalSlots);
    
}
