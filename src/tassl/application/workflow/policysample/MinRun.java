/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow.policysample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.TaskProperties;
import tassl.application.workflow.PolicyAbstract;
import tassl.application.workflow.WorkerForScheduler;
import tassl.workflow.WorkflowStage;

/**
 *
 * @author Javier Diaz-Montes
 */
public class MinRun extends PolicyAbstract{

    public MinRun(){
        super();
    }
    
    @Override
    public HashMap scheduleStage(String wkflId, WorkflowStage stage, HashMap<String, List<WorkerForScheduler>> globalSlots) {
        HashMap localMapTasks=new HashMap();
        
        String properties="";//not needed in these algorithms
                
        //NOTE: we assume that we only have one objective for now. 
        String objective=stage.getObjectiveType();
        
        if(objective.equals("MinRunningTime")){
            properties="";
            long start=System.currentTimeMillis();
            localMapTasks=MinRunningTime(wkflId,stage,properties,globalSlots);       
            long end=System.currentTimeMillis();
            System.out.println("CWDEBUG: MinRunningTime. seconds SchedulingTime:"+(end-start)/1000.0);
        }else if(objective.equals("MinRunningTimeNoData")){
            properties="nodata";
            long start=System.currentTimeMillis();
            localMapTasks=MinRunningTimeNoData(wkflId,stage,properties,globalSlots);       
            long end=System.currentTimeMillis();
            System.out.println("CWDEBUG: MinRunningTime. seconds SchedulingTime:"+(end-start)/1000.0);
        }else if(objective.equals("MinRunningTimeLimit")){
            properties="";
            long start=System.currentTimeMillis();
            localMapTasks=MinRunningTimeLimit(wkflId,stage,properties,globalSlots);       
            long end=System.currentTimeMillis();
            System.out.println("CWDEBUG: MinRunningTimeLimit. seconds SchedulingTime:"+(end-start)/1000.0);
        }else{
            String msg="MinRun Policies. Policy named \""+objective+"\" not found";
            Logger.getLogger(MinRun.class.getName()).log(Level.SEVERE, msg);
            System.out.println("CWDEBUG: "+msg);
        }
        
        return localMapTasks;
    }
    
    /**
    * This assign each task to the resource that gives the minimum completion time. Data is not considered.
    * This is going to use all VMs
    * @param wflId
    * @param stage
    * @param properties we use it here to be data aware or not. for experiments
    * @return
    */
    protected HashMap MinRunningTimeNoData(String wflId, WorkflowStage stage, Object properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){
        return MinRunningTime(wflId, stage, properties, globalSlots);
    }
    
    /**
    * This assign each task to the resource that gives the minimum completion time.
    * This is going to use all VMs
    * @param wflId
    * @param stage
    * @param properties we use it here to be data aware or not. for experiments
    * @return
    */
    protected HashMap MinRunningTime(String wflId, WorkflowStage stage, Object properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){         
   
        double criticalPath=0.0; // estimated duration of the critical path
        
        double costResourceIndexMinInStage=0.0;
        double costResourceIndexMinOutStage=0.0;
        double transferTimeforResourceIndexMinInStage=0.0;
        double transferTimeforResourceIndexMinOutStage=0.0;
        
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        for(int taskid:tasksP.keySet()){
            double taskduration=tasksP.get(taskid).getMinTime() + (Math.random()
                    * (tasksP.get(taskid).getMaxTime() - tasksP.get(taskid).getMinTime()));
            
            List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
            List <FileProperties>outputFiles=tasksP.get(taskid).getOutputs();
            //find origin of each input file
            List <Integer>originFiles=getOriginFiles(inputFiles);        
            //find destination of each output file
            List <Integer>destinationFiles=getDestinationFiles(outputFiles);
            
            String datacenterMin="";
            int resourceIndexMin=-1;
            double completionTime=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn=0.0;
            double transferTimeforResourceIndexMinOut=0.0;
            double costResourceIndexMinIn=0.0;
            double costResourceIndexMinOut=0.0;
            
            
            
            //find the resource where the task is completed in shorter time
            for(String site:globalResources.keySet()){
                
                //get transfer time of all input files if we were to choose this site                
                int siteIndex=AgentList.indexOf(site);    

                //CHECK IF ALL files can be moved there
                //can we put it somewhere else to enforcing it??                
                if(checkInputConstraints(inputFiles,siteIndex)){
                
                    double totalTransferInCost=0.0;
                    double totalTransferOutCost=0.0;                    
                    //calculate input
                    double [] inputT=calculateInputTransfer(originFiles,inputFiles,siteIndex);
                    double totalTransferIn=inputT[0];
                    totalTransferInCost+=inputT[1]+inputT[2];
                    //calculate output
                    double [] outputT=calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                    double totalTransferOut=outputT[0];

                    totalTransferOutCost+=outputT[1]+outputT[2];

                    //TODO: we could check if this particular resource supports the app that the stage requires
                    List <WorkerForScheduler>slots=globalSlots.get(site);
                    for(int i=0;i<slots.size();i++){
                        WorkerForScheduler slotI=slots.get(i);
                        double estComTime;
                        if(properties.equals("nodata")){
                            estComTime=slotI.getExecTime(stage.getApplication(),taskduration)
                                +slotI.getReadyToExecute();  
                            totalTransferIn=0.0;
                            totalTransferOut=0.0;
                        }else{
                            estComTime=slotI.getExecTime(stage.getApplication(),taskduration)
                                +slotI.getReadyToExecute()+ totalTransferIn + totalTransferOut;   
                        }

                        if(estComTime<completionTime){
                            datacenterMin=site;
                            resourceIndexMin=i;
                            completionTime=estComTime;
                            transferTimeforResourceIndexMinIn=totalTransferIn;
                            transferTimeforResourceIndexMinOut=totalTransferOut;
                            costResourceIndexMinIn=totalTransferInCost;
                            costResourceIndexMinOut=totalTransferOutCost;
                        }
                    }
                }
            }
            //we should have the minimum, so we assign the task to that resource
            if(resourceIndexMin!=-1 && !datacenterMin.equals("")){
                System.out.println("CWDEBUG: MinRun.minRunningTime. The choosen on is: "+globalSlots.get(datacenterMin).get(resourceIndexMin).getType_name());
                
                List <WorkerForScheduler>slots=globalSlots.get(datacenterMin);
                slots.get(resourceIndexMin).addReadyToExecute(
                        slots.get(resourceIndexMin).getExecTime(stage.getApplication(),taskduration)                        
                        +transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut);
                MapTask.put(taskid, slots.get(resourceIndexMin));
                
                if(slots.get(resourceIndexMin).getReadyToExecute() > criticalPath){
                    criticalPath=slots.get(resourceIndexMin).getReadyToExecute();
                }
                
                costResourceIndexMinInStage+=costResourceIndexMinIn;
                costResourceIndexMinOutStage+=costResourceIndexMinOut;
                transferTimeforResourceIndexMinInStage+=transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage+=transferTimeforResourceIndexMinOut;
                
            }else{//means that we do not have enough resources
                return new HashMap();
            }            
        }        
        
        String msg="Stage "+ wflId+"."+stage+" COSTTRANSFERIN:"+costResourceIndexMinInStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" COSTTRANSFEROUT:"+costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFERIN:"+transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFEROUT:"+transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
                
                
        System.out.println("CWDEBUG: The critical path of stage "+stage.getId()+" is: "+ criticalPath);
        
       
        
        return MapTask;
        
    }

    /**
    * This assign each task to the resource that gives the minimum completion time.
    * This version uses a maximum of machines specified in the value field
    *      
    * @param wflId
    * @param stage
    * @param properties we use it here to be data aware or not. for experiments
    * @return
    */
    protected HashMap MinRunningTimeLimit(String wflId, WorkflowStage stage, Object properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){         
   
        int limitMachines=(int)stage.getObjectiveValueOriginal("MinRunningTimeLimit");
        System.out.println("CWDEBUG: MinRunningTimeLimit = "+limitMachines);
        List<String[]> usedMachines=new ArrayList();// we need to keep type_name and machine_index to know the machines
               
        
        double criticalPath=0.0; // estimated duration of the critical path
        
        double costResourceIndexMinInStage=0.0;
        double costResourceIndexMinOutStage=0.0;
        double transferTimeforResourceIndexMinInStage=0.0;
        double transferTimeforResourceIndexMinOutStage=0.0;
        
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        for(int taskid:tasksP.keySet()){
            double taskduration=tasksP.get(taskid).getMinTime() + (Math.random()
                    * (tasksP.get(taskid).getMaxTime() - tasksP.get(taskid).getMinTime()));
            
            List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
            List <FileProperties>outputFiles=tasksP.get(taskid).getOutputs();
            //find origin of each input file
            List <Integer>originFiles=getOriginFiles(inputFiles);        
            //find destination of each output file
            List <Integer>destinationFiles=getDestinationFiles(outputFiles);
            
            String datacenterMin="";
            int resourceIndexMin=-1;
            double completionTime=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn=0.0;
            double transferTimeforResourceIndexMinOut=0.0;
            double costResourceIndexMinIn=0.0;
            double costResourceIndexMinOut=0.0;                        
            
            //find the resource where the task is completed in shorter time
            for(String site:globalResources.keySet()){
                
                //get transfer time of all input files if we were to choose this site                
                int siteIndex=AgentList.indexOf(site);    

                //CHECK IF ALL files can be moved there
                //can we put it somewhere else to enforcing it??                
                if(checkInputConstraints(inputFiles,siteIndex)){
                
                    double totalTransferInCost=0.0;
                    double totalTransferOutCost=0.0;                    
                    //calculate input
                    double [] inputT=calculateInputTransfer(originFiles,inputFiles,siteIndex);
                    double totalTransferIn=inputT[0];
                    totalTransferInCost+=inputT[1]+inputT[2];
                    //calculate output
                    double [] outputT=calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                    double totalTransferOut=outputT[0];

                    totalTransferOutCost+=outputT[1]+outputT[2];

                    //TODO: we could check if this particular resource supports the app that the stage requires
                    List <WorkerForScheduler>slots=globalSlots.get(site);
                    System.out.println("CWDEBUG: usedMachines="+usedMachines.size());
                    for(int i=0;i<slots.size();i++){
                        WorkerForScheduler slotI=slots.get(i);
                        System.out.println("CWDEBUG: Used slot? "+slotI.toString()+"="+checkUsedMachine(usedMachines, new String[]{slotI.getType_name(),Integer.toString(slotI.getMachine_index())}));
                        
                        //find any machine until the limit is reached, then only pick from selected ones
                        if(usedMachines.size()<limitMachines || checkUsedMachine(usedMachines, new String[]{slotI.getType_name(),Integer.toString(slotI.getMachine_index())})){
                            
                            double estComTime;
                            estComTime=slotI.getExecTime(stage.getApplication(),taskduration)
                                   +slotI.getReadyToExecute()+ totalTransferIn + totalTransferOut;   
                            
                            System.out.println("CWDEBUG: estimated duration = "+estComTime+" best="+completionTime);
                            if(estComTime<completionTime){
                                datacenterMin=site;
                                resourceIndexMin=i;
                                completionTime=estComTime;
                                transferTimeforResourceIndexMinIn=totalTransferIn;
                                transferTimeforResourceIndexMinOut=totalTransferOut;
                                costResourceIndexMinIn=totalTransferInCost;
                                costResourceIndexMinOut=totalTransferOutCost;
                            }
                        }else{
                            System.out.println("CWDEBUG: Maximum number of machines used. Cannot use slot: "+slotI.toString());
                        }
                    }
                }
            }
            //we should have the minimum, so we assign the task to that resource
            if(resourceIndexMin!=-1 && !datacenterMin.equals("")){
                List <WorkerForScheduler>slots=globalSlots.get(datacenterMin);
                WorkerForScheduler slotI = slots.get(resourceIndexMin);
                slotI.addReadyToExecute(slotI.getExecTime(stage.getApplication(),taskduration)
                        +transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut);
                MapTask.put(taskid, slotI);
                
                //add slot to used machines
                if(!checkUsedMachine(usedMachines, new String[]{slotI.getType_name(),Integer.toString(slotI.getMachine_index())})){
                    System.out.println("CWDEBUG: adding to usedmachines - machine "+slotI.toString());
                    usedMachines.add(new String[]{slotI.getType_name(),Integer.toString(slotI.getMachine_index())});
                }
                
                if(slots.get(resourceIndexMin).getReadyToExecute() > criticalPath){
                    criticalPath=slots.get(resourceIndexMin).getReadyToExecute();
                }
                
                costResourceIndexMinInStage+=costResourceIndexMinIn;
                costResourceIndexMinOutStage+=costResourceIndexMinOut;
                transferTimeforResourceIndexMinInStage+=transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage+=transferTimeforResourceIndexMinOut;
                
            }else{//means that we do not have enough resources
                System.out.println("CWDEBUG: Could not find schedule");
                return new HashMap();
            }            
        }        
        
        String msg="Stage "+ wflId+"."+stage+" COSTTRANSFERIN:"+costResourceIndexMinInStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" COSTTRANSFEROUT:"+costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFERIN:"+transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFEROUT:"+transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: "+msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);
                
                
        System.out.println("CWDEBUG: The critical path of stage "+stage.getId()+" is: "+ criticalPath);
        
       
        
        return MapTask;
        
    }
    
    //return if a machine is used or not.
    private boolean checkUsedMachine(List<String[]> usedMachines, String [] machine){
        boolean exists=false;
        for(int i=0; i<usedMachines.size() && !exists; i++){
            exists=Arrays.equals(usedMachines.get(i),machine);
        }
        return exists;
    }
    
}
