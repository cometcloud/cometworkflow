/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tassl.application.workflow.policysample;

//import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.ceil;
import static java.lang.Thread.sleep;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.TaskProperties;
import tassl.application.workflow.PolicyAbstract;
import tassl.application.workflow.WorkerForScheduler;
import tassl.workflow.WorkflowStage;

/**
 *
 * @author Manuel Diaz-Granados
 */
public class Budget extends PolicyAbstract {

    int b = 1;
    boolean out = true, cheaper = false;
    boolean check = true;
    int numberUsedTasks = 0;
    double minAppBudget;
    double minAppTime;
    double referenceTime = Double.MAX_VALUE;
    double cost = Double.MAX_VALUE;
    double time = 0;
    double referenceCost = 0;
    double totalCost = 0;
    double totalCost2 = 0;
    double totalTransferInCost = 0.0;
    double totalTransferOutCost = 0.0;
    double hours;
    double criticalPath = 0;
    int taskBack;
    int processorBack = -1;
    double taskTimeBack;
    double totalCostNoRound, totalReferenceCost, costResult;
    List<WorkerForScheduler> taskSlots = new ArrayList<WorkerForScheduler>();
    double hour = 1; //to control how many hours are going to be assing in each processor WE SHOULD MODIFY THIS DEPEND ON THE APPLICATION LIKE CALCULATE A RANGE BETWEEN THE MAZ AND MIN TASKS TIMES
    //double taskTime=0;
    double budgetMax = 100000000;
    List<WorkerForScheduler> slots = new ArrayList<WorkerForScheduler>();
    long stime, etime;
    double budget = 900;
    int number_OfProcessors = 0;
    int totalProcessors = 0;
    int number_OfTasks = 0;
    double budget_reference;
    double criticalPathBack = 0;
    double budgetMin = 0;
    double utilityReference = Double.MAX_VALUE;
    List<WorkerForScheduler> redundancyCheck = new ArrayList<WorkerForScheduler>();
    ArrayList<DataTask> listTasks = new ArrayList<DataTask>(); // list of all tasks ordered for cost

    public Budget() {
        super();

    }

    @Override
    public HashMap scheduleStage(String wkflId, WorkflowStage stage, HashMap<String, List<WorkerForScheduler>> globalSlots) {

        HashMap localMapTasks = new HashMap();

         String properties = "";//not needed in these algorithms
        
        //NOTE: we assume that we only have one objective for now. 
        String objective=stage.getObjectiveType();
        long start = System.currentTimeMillis();
        if(objective.equals("BudgetConstraint")){
           localMapTasks = BudgetConstraint(wkflId, stage, properties, globalSlots);

        }else if(objective.equals("TimeCostConstraint")){
            localMapTasks = TimeCostConstraint(wkflId, stage, properties, globalSlots);

        }

       
        for (Integer g : MapTask.keySet()) {
            System.out.println(MapTask.get(g).toString());
        }
        System.out.println("Application final cost: " + totalReferenceCost);

        long end = System.currentTimeMillis();
        System.out.println("CWDEBUG: MinRunningTime. seconds SchedulingTime:" + (end - start) / 1000.0);


        return localMapTasks;
    }

    /**
     * Create a solution within a budget limit
     * @param wkflId
     * @param stage
     * @param properties
     * @param globalSlots
     * @return
     */
    protected HashMap BudgetConstraint(String wkflId, WorkflowStage stage, Object properties,
        HashMap<String, List<WorkerForScheduler>> globalSlots) {
           
        
       
        HashMap<Integer, TaskProperties> tasksP = stage.getTaskProp();
       
        number_OfTasks = tasksP.size();

        int flag, times = 1;
        HashMap localMapTasks = new HashMap();
        localMapTasks = MinRunningTimeInBudget(wkflId, stage, properties, globalSlots);

        List<WorkerForScheduler> slots = new ArrayList<WorkerForScheduler>();
        for (Integer i : MapTask.keySet()) {

            slots.add(MapTask.get(i));

        }
        Integer tasks = -1;
       

        for (Integer g : MapTask.keySet()) {
            System.out.println(MapTask.get(g).toString());
        }
//        try {
//            sleep(4000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, ex);
//        }
        

        totalReferenceCost = RoundSlotsCosts(globalSlots, totalCost);
        double budgetApp = stage.getObjectiveValue("BudgetConstrain");
        this.budget = budgetApp;
        System.out.println("Maximun cost for scheculing: " + totalReferenceCost+" BudgetApp "+budgetApp);
        Integer task = 0; // list iterator of the list of task ordered by cost
        double taskTime;
        tasks = listTasks.get(task).task;

        double timeSlot, floorTimeSlot = 0;
        //safe the cheapest processor
        boolean newSlot = true;
        double epsilon = 0.000001; // error check in double comparison
       
        while ((totalReferenceCost > budgetApp) && times != -1) {

            try {
                flag = 0;
                timeSlot = MapTask.get(tasks).getReadyToExecute() / 3600;

                if (newSlot == true) { // when we have already decreased an hour 
                    floorTimeSlot = Math.floor(timeSlot);


                }
                taskTime = this.TaskTime(tasksP, globalSlots, tasks, MapTask.get(tasks).getSiteAddress(), MapTask.get(tasks).getMachine_indexType(), stage.getApplication());
               // System.out.println(MapTask.get(tasks).toString() + " previous processor " + "TASKTIME " + taskTime);
                hours = taskTime / 3600.0;
                timeSlot -= hours;
                cost = (MapTask.get(tasks).getCost() * hours) + totalTransferInCost + totalTransferOutCost;

                //don't change order 
                WorkerForScheduler machine = null;
                boolean solution = false;


                while ((solution == false) && flag != 2) {
                    machine = TimeTasksProcessors2(tasks, tasksP, globalSlots, cost, taskTime, stage.getApplication());
                    if (machine != null) {
                        cheaper = false; // reinitializing the variable for the next itiration
                        solution = true;
                    } else if (cheaper == true) {
                    //    System.out.println("Not Machine Found");
                        hour += 0.3;
                        flag++; // it means that already checked all aviable processors and It won't do the process more than 2 times
                        cheaper = false;
                    } else {
                        flag = 2;
                    }

                }

                if (flag != 2) {


                    MapTask.get(tasks).removeTime(taskTime);
                    MapTask.get(tasks).RemoveTask(tasks); // we remove the task from the processor that we are going to reassing

                    totalCost = totalCost - cost; //cost of the task in the previous processor

                    taskTime = machine.getReadyToExecute();

                    globalSlots.get(machine.getSiteAddress()).get(machine.getMachine_indexType()).addReadyToExecute(taskTime);


                    hours = taskTime / 3600.0;

                    totalCost += (machine.getTaskCost());
                    totalReferenceCost = RoundSlotsCosts(globalSlots, totalCost);
             //       System.out.println("COST: " + totalReferenceCost);
                    criticalPath = Max(globalSlots);

                    criticalPath = Max(globalSlots);

                   // System.out.println("timeSLot: " + timeSlot + " floorTimeSlot: " + floorTimeSlot);
                    WorkerForScheduler slot1 = globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType());
                    if ((slot1.getListTasks().isEmpty())) {

                    //    System.out.println("IS EMPTY");
                        globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).setTime(0);
                    }
                    if (timeSlot <= (floorTimeSlot + epsilon) || globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).listOfTaskIsEmpty()) { // Plus epsilon because if we have a number really close to the floor number it won't count it as the same

                        MapTask.remove(tasks);
                        MapTask.put(tasks, globalSlots.get(machine.getSiteAddress()).get(machine.getMachine_indexType()));
                        globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).AddTask(tasks);
                        task++;

                        tasks = listTasks.get(task).task;
                        newSlot = true;

                    } else {

                        Integer taskaux = tasks;
                        tasks = globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).getTask(0); // we keep reassining tasks from the same processor until an hour has decreased
                        MapTask.remove(taskaux); //now that we have the same processor reassing the task to its new processor
                        MapTask.put(taskaux, globalSlots.get(machine.getSiteAddress()).get(machine.getMachine_indexType()));
                        globalSlots.get(MapTask.get(taskaux).getSiteAddress()).get(MapTask.get(taskaux).getMachine_indexType()).AddTask(taskaux);

                        //  System.out.println("SAME PROCESSOR: ")
                        for (int i = 0; i < listTasks.size(); i++) {   // we remove the task from the listTask so we don't the task twice
                            if (tasks.equals(listTasks.get(i).task)) {
                                listTasks.remove(i);
                            }
                        }

                        newSlot = false;

                    }

                } else {
                    task++;
                    if (task >= listTasks.size()) {
//                   
                        System.out.println("No solution with determined budget1");

                        task = 0; // we get the first task in the list again so we explore one more time in all task in we can re assing more tasks to get a cheaper solution
                        times--;
                    }
                    tasks = listTasks.get(task).task;
                    newSlot = true;
                }

                if (times == -1) {
                    System.out.println("No solution with determined budget2");
                    return new HashMap();
                }
//                   
            } catch (Exception e) {
//                  

                e.printStackTrace();
            }

        }

        criticalPath = Max(globalSlots);
           

          //  String content = (String) (utility + " " + criticalPath + " " + totalReferenceCost + " " + landa);

           // File file11 = new File("/home/manuel/Documents/cometcloudWorkflo-git/TimeCostSites/TimeCostPlot_" + landa + ".txt");
          
      
       
         // writeToFile(File file11,content);
          

        //  String content2 = (String) (criticalPath + " " + totalReferenceCost + " " + budgetApp);
        //  File file3 = new File("/home/manuel/Documents/cometcloudWorkflo-git/TimeCostSites/BudgetPlotProcessors2.txt");
           
            
      
         // writeToFile(File file3,content2);
           
            
            
          
           
            //for (Integer g : MapTask.keySet()) {

               // String content3 = (MapTask.get(g).toString());

               // File file2 = new File("/home/manuel/Documents/cometcloudWorkflo-git/TimeCostSites/Result_" + budgetApp + ".txt");
               
                
             // writeToFile(file2,content3);
                
                
               
            //}

        

        return MapTask;

    }

    /**
     * Find scheduling solutions while changing budget 
     * @param wkflId
     * @param stage
     * @param properties
     * @param globalSlots
     * @return
     */
    protected HashMap TimeCostConstraint(String wkflId, WorkflowStage stage, Object properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots) {
       // double budgetApp=Double.parseDouble(stage.getObjectives().get("BacktrackBudget"));
        
        double landa = 25;


        double utility = 0;

        HashMap<Integer, TaskProperties> tasksP = stage.getTaskProp();
       
        number_OfTasks = tasksP.size();

        int flag, times = 1;
        HashMap localMapTasks = new HashMap();
        localMapTasks = MinRunningTimeInBudget(wkflId, stage, properties, globalSlots);

        List<WorkerForScheduler> slots = new ArrayList<WorkerForScheduler>();
        for (Integer i : MapTask.keySet()) {

            slots.add(MapTask.get(i));

        }
        Integer tasks = -1;
        System.out.println("RESULT!!!!!!!!!!!!1");

        for (Integer g : MapTask.keySet()) {
            System.out.println(MapTask.get(g).toString());
        }
        try {
            sleep(4000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("RESULT2");

        totalReferenceCost = RoundSlotsCosts(globalSlots, totalCost);
        double budgetApp = totalReferenceCost;

        System.out.println("Maximun cost for scheculing: " + totalReferenceCost);
        Integer task = 0; // list iterator of the list of task ordered by cost
        double taskTime;
        tasks = listTasks.get(task).task;

        double timeSlot, floorTimeSlot = 0;
        //safe the cheapest processor
        boolean newSlot = true;
        double epsilon = 0.000001; // error check in double comparison
        while (budgetApp > 24) { //TODO: CHANGE THE FIXED MAX BUDGET
            while ((totalReferenceCost > budgetApp) && times != -1) { //TODO: THIS MAYBE BE REPLACABLE WITH A SIMPLE CALL TO BudgetConstrain

                try {
                    flag = 0;
                    timeSlot = MapTask.get(tasks).getReadyToExecute() / 3600;

                    if (newSlot == true) { // when we have already decreased an hour 
                        floorTimeSlot = Math.floor(timeSlot);
                        System.out.println("timeSLot: " + timeSlot + " floorTimeSlot: " + floorTimeSlot);

                    }
                    taskTime = this.TaskTime(tasksP, globalSlots, tasks, MapTask.get(tasks).getSiteAddress(), MapTask.get(tasks).getMachine_indexType(), stage.getApplication());
                    System.out.println(MapTask.get(tasks).toString() + " previous processor " + "TASKTIME " + taskTime);
                    hours = taskTime / 3600.0;
                    timeSlot -= hours;
                    cost = (MapTask.get(tasks).getCost() * hours) + totalTransferInCost + totalTransferOutCost;
                    System.out.println("COST ACTAUL TASK: " + cost);
                    //don't change order 
                    WorkerForScheduler machine = null;
                    boolean solution = false;

                    System.out.println("COSTprevious: " + totalReferenceCost);
                    while ((solution == false) && flag != 2) {
                        machine = TimeTasksProcessors2(tasks, tasksP, globalSlots, cost, taskTime, stage.getApplication());
                        if (machine != null) {
                            cheaper = false; // reinitializing the variable for the next itiration
                            solution = true;
                        } else if (cheaper == true) {
                           // System.out.println("Not Machine Found");
                            hour += 0.3;
                            flag++; // it means that already checked all aviable processors and It won't do the process more than 2 times
                            cheaper = false;
                        } else {
                            flag = 2;
                        }

                    }

                    if (flag != 2) {
                 

                        MapTask.get(tasks).removeTime(taskTime);
                        MapTask.get(tasks).RemoveTask(tasks); // we remove the task from the processor that we are going to reassing

                        totalCost = totalCost - cost; //cost of the task in the previous processor

                        taskTime = machine.getReadyToExecute();

                        globalSlots.get(machine.getSiteAddress()).get(machine.getMachine_indexType()).addReadyToExecute(taskTime);
             

                        hours = taskTime / 3600.0;

                        totalCost += (machine.getTaskCost());
                        totalReferenceCost = RoundSlotsCosts(globalSlots, totalCost);
                        System.out.println("COST: " + totalReferenceCost);
                        criticalPath = Max(globalSlots);
                        utility = totalReferenceCost + (landa * (criticalPath / 3600));
                        if (utility < utilityReference) {
                            utilityReference = utility;
                        }
                        criticalPath = Max(globalSlots);
        
                        System.out.println("timeSLot: " + timeSlot + " floorTimeSlot: " + floorTimeSlot);
                        WorkerForScheduler slot1 = globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType());
                        if (!(slot1.getListTasks().isEmpty())) {
                            for (int i = 0; i < slot1.getListTasksSize(); i++) {   // we remove the task from the slot so we don't assing the task twice
                                System.out.println(slot1.getTask(i));
//                   
                            }
                        } else {
                            System.out.println("IS EMPTY");
                            globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).setTime(0);
                        }
                        if (timeSlot <= (floorTimeSlot + epsilon) || globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).listOfTaskIsEmpty()) { // Plus epsilon because if we have a number really close to the floor number it won't count it as the same

                            System.out.println("TASK: " + task);
                            System.out.println("TASK: " + task);
                            MapTask.remove(tasks);
                            MapTask.put(tasks, globalSlots.get(machine.getSiteAddress()).get(machine.getMachine_indexType()));
                            globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).AddTask(tasks);
                            task++;
                            System.out.println("TASK: " + task);
                            System.out.println("TASK: " + task);
                            tasks = listTasks.get(task).task;
                            newSlot = true;

                        } else {
                            System.out.println("TASK: " + task);
                            System.out.println("TASK: " + task);
                            Integer taskaux = tasks;
                            tasks = globalSlots.get(MapTask.get(tasks).getSiteAddress()).get(MapTask.get(tasks).getMachine_indexType()).getTask(0); // we keep reassining tasks from the same processor until an hour has decreased
                            MapTask.remove(taskaux); //now that we have the same processor reassing the task to its new processor
                            MapTask.put(taskaux, globalSlots.get(machine.getSiteAddress()).get(machine.getMachine_indexType()));
                            globalSlots.get(MapTask.get(taskaux).getSiteAddress()).get(MapTask.get(taskaux).getMachine_indexType()).AddTask(taskaux);

                            //  System.out.println("SAME PROCESSOR: ")
                            for (int i = 0; i < listTasks.size(); i++) {   // we remove the task from the listTask so we don't the task twice
                                if (tasks.equals(listTasks.get(i).task)) {
                                    listTasks.remove(i);
                                }
                            }

                            newSlot = false;

                        }

                    } else {
                        task++;
                        if (task >= listTasks.size()) {
//                   
                            System.out.println("No solution with determined budget1");

                            task = 0; // we get the first task in the list again so we explore one more time in all task in we can re assing more tasks to get a cheaper solution
                            times--;
                        }
                        tasks = listTasks.get(task).task;
                        newSlot = true;
                    }

                    if (times == -1) {
                        System.out.println("No solution with determined budget2");
                        return new HashMap();
                    }
//                   
                } catch (Exception e) {
//                  

                    e.printStackTrace();
                }

            }

            criticalPath = Max(globalSlots);
            utility = totalReferenceCost + (landa * (criticalPath / 3600));

            //TODO: STORE SOLUTION AND UTILITY
            
            
          //  String content = (String) (utility + " " + criticalPath + " " + totalReferenceCost + " " + landa);

           // File file11 = new File("/home/manuel/Documents/cometcloudWorkflo-git/TimeCostSites/TimeCostPlot_" + landa + ".txt");
          
      
       
         // writeToFile(File file11,content);
          

        //  String content2 = (String) (criticalPath + " " + totalReferenceCost + " " + budgetApp);
        //  File file3 = new File("/home/manuel/Documents/cometcloudWorkflo-git/TimeCostSites/BudgetPlotProcessors2.txt");
           
            
      
         // writeToFile(File file3,content2);
           
            
            
          
            budgetApp -= 0.3;
            //for (Integer g : MapTask.keySet()) {

               // String content3 = (MapTask.get(g).toString());

               // File file2 = new File("/home/manuel/Documents/cometcloudWorkflo-git/TimeCostSites/Result_" + budgetApp + ".txt");
               
                
             // writeToFile(file2,content3);
                
                
               
            //}

        }
        
        //TODO: CHOOSE ONE SOLUTION DEPENDING UPON UTILITY
        
        return MapTask;

    }

   
//    /**
//     * Calculate scheduling with Budget constraint using Branch & Bound
//     * @param tasks
//     * @param tasksP
//     * @param globalSlots
//     * @param result
//     * @param taskTime
//     */
//    protected void BudgetB(int tasks, HashMap<Integer, TaskProperties> tasksP, HashMap<String, List<WorkerForScheduler>> globalSlots, HashMap<Integer, WorkerForScheduler> result, double taskTime) {
//
//        int processor;
//        etime = System.currentTimeMillis();
//
//        long time = (etime - stime) / 1000;
//        if (time >= 10) {
//            out = false;
//        }
//
//        if (numberUsedTasks == number_OfTasks && out == true) {
//
////           
//            criticalPath = Max(globalSlots);
//            if ((criticalPath == referenceTime && budget_reference > budgetMin) || (criticalPath < referenceTime && budget_reference > 0)) {
//
//                int round = 0;
//                totalReferenceCost = totalCost;
//
//                double hr;
//                for (String site : globalSlots.keySet()) {
//                    number_OfProcessors = globalSlots.get(site).size();
//                    for (int z = 0; z < number_OfProcessors; z++) {
//                        hr = globalSlots.get(site).get(z).getReadyToExecute() / 3600.0;
//                        round = (int) ceil(globalSlots.get(site).get(z).getReadyToExecute() / 3600);
//                       
//                        totalReferenceCost += (round - hr) * (globalSlots.get(site).get(z).getCost());
//                    }
//                }
//
//                budget_reference = budget - totalReferenceCost;
//                if ((criticalPath == referenceTime && budget_reference > budgetMin) || (criticalPath < referenceTime && budget_reference > 0)) {
//                    //updating budget
//                    totalCostNoRound = totalCost;
//                    costResult = totalReferenceCost;
//                    
//                    referenceTime = criticalPath;
//                    budgetMin = budget_reference;
//
//                    result.clear();
//                    for (Integer g : MapTask.keySet()) {
//                        result.put(g, new WorkerForScheduler(MapTask.get(g).getSiteAddress(), MapTask.get(g).getSiteIdentifier(), MapTask.get(g).getSiteZone(), MapTask.get(g).getType_name(),
//                                MapTask.get(g).getMachine_index(), MapTask.get(g).getWorker_index(), MapTask.get(g).getReadyToExecute(), MapTask.get(g).getOverhead()));
//                    }
//                    etime = System.currentTimeMillis();
//                    time = (etime - stime) / 1000;
//                    if (time >= 10) {
//                        System.out.println("HAAA" + Long.toString(time));
//                        out = false;
//                    }
//
//                        ///////////////////////////////write the result in txt file
//                    //    String content =(String)(criticalPath+" "+costResult);
//                       // File file = new File("/home/manuel/Documents/cometcloudWorkflo-git/Time-Cost2/TimeCostPlot.txt");
//
//                   // writeToFile(File file,String content);
//                    
//                    
//                }
//                   //   System.out.println("NOO SOLUTION");
//
//            }
//
//        }
//
//        if (tasks == (number_OfTasks - 1) || out == false) {
//            return;
//
//        } else {
//            tasks++;
//        
//            number_OfProcessors = slots.size();
//            for (processor = 0; processor < number_OfProcessors; processor++) {
//                List<FileProperties> inputFiles = tasksP.get(tasks).getInputs();
//
//                List<FileProperties> outputFiles = tasksP.get(tasks).getOutputs();
//                //find origin of each input file
//                List<Integer> originFiles = getOriginFiles(inputFiles);
//                //find destination of each output file
//                List<Integer> destinationFiles = getDestinationFiles(outputFiles);
//                int siteIndex = AgentList.indexOf(slots.get(processor).getSiteAddress());
//
//                double totalTransferInCost = 0.0;
//                double totalTransferOutCost = 0.0;
//
//                //calculate input
//                double[] inputT = calculateInputTransfer(originFiles, inputFiles, siteIndex);
//                double totalTransferIn = inputT[0];
//                totalTransferInCost += inputT[1] + inputT[2];
//                //calculate outpu
//                double[] outputT = calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
//                double totalTransferOut = outputT[0];
//                totalTransferOutCost += outputT[1] + outputT[2];
//                if (tasks == 0) {
//                    check = RedundancyCheck(slots.get(processor));
//
//                    redundancyCheck.add(slots.get(processor));
//
//                }
//
//                if (check || (processor == 0 && tasks == 0)) {
//
//                    processorBack = processor;
//
//                    taskBack = tasks;
//             
//
//                    taskTime = ((Double) tasksP.get(tasks).getTaskParam().get(3)) / slots.get(processor).getPerformance() + totalTransferIn + totalTransferOut;
//                    taskTimeBack = taskTime;
//                     
//                    slots.get(processor).addReadyToExecute(taskTime);
//                    MapTask.put(tasks, slots.get(processor));
//
//                    hours = taskTime / 3600.0;
//                    totalCost = totalCost + (slots.get(processor).getCost() * hours) + totalTransferInCost + totalTransferOutCost;
//
//                    budget_reference = budget - totalCost;
//
////             
//                    numberUsedTasks++;
//                    if ((budget_reference > 0) && (slots.get(processor).getReadyToExecute() < referenceTime)) {
//
//                        BudgetB(tasks, tasksP, globalSlots, result, taskTime);
//                        if (out == false) {
//                            b = 0;
//                            return;
//                        }
//
//                    } else {
//                      
//                    }
//                } else {
//                 //       ("REDUNDANCY")
//
//                }
////                
//
//                
//                MapTask.remove(tasks);
//                slots.get(processor).removeTime(taskTime);
//
//                hours = taskTime / 3600.0;
//                totalCost = totalCost - (slots.get(processor).getCost() * hours) - totalTransferInCost - totalTransferOutCost;;
//                budget_reference = budget_reference + totalCost;
//
//                numberUsedTasks--;
//                if (b == 0) {
//                    break;
//                }
//            }
//
//        }
//
////        
////       
//        return;
//    }

    /**
     * Auxiliary function to adjust proc time and load balancing.
     * @param task
     * @param tasksP
     * @param globalSlots
     * @param costP
     * @param referenceTaskTime
     * @return
     */
    private WorkerForScheduler TimeTasksProcessors2(int task, HashMap<Integer, TaskProperties> tasksP, HashMap<String, List<WorkerForScheduler>> globalSlots, double costP, double referenceTaskTime, String appName) {
        double hourTask;
        DecimalFormat threeDecimals = new DecimalFormat("#.0000");
        double CostProcessor = Double.valueOf(threeDecimals.format(costP));
        referenceTaskTime = Double.valueOf(threeDecimals.format(referenceTaskTime));
        double taskTime, taskTimeResult = 0, costResult = 0;
        int processor;
        WorkerForScheduler machine = null;
        int processorResult = 0;
        String siteResult = "";
        double hourLocal;

        totalTransferInCost = 0;
        totalTransferOutCost = 0;

        for (String site : globalSlots.keySet()) {

            number_OfProcessors = globalSlots.get(site).size();

            for (processor = 0; processor < number_OfProcessors; processor++) {

           
                taskTime = Double.valueOf(threeDecimals.format(TaskTime(tasksP, globalSlots, task, site, processor, appName)));
                hourTask = taskTime / 3600.0;

                hourLocal = hour * (slots.get(processor).getCost());
                totalCost2 = Double.valueOf(threeDecimals.format((slots.get(processor).getCost() * hourTask) + totalTransferInCost + totalTransferOutCost));
                if (((totalCost2 == CostProcessor) && (taskTime < referenceTaskTime)) || (totalCost2 < CostProcessor)) {
                    cheaper = true;
                    if (slots.get(processor).getAssignHours() < hourLocal) {
                      //  System.out.println("TASKTIME VS REFERENCE TASKT: " + taskTime + " " + referenceTaskTime);
                      //  System.out.println("TOTALCOST2 VS COSTPROCESSOR: " + totalCost2 + " " + CostProcessor);

                        CostProcessor = totalCost2;
                        time = taskTime;

                        processorResult = processor;
                        siteResult = site;
                        taskTimeResult = taskTime;
                        costResult = totalCost2;

                    }
                }

            }

        }
        if (!(siteResult.equals(""))) {

            slots = globalSlots.get(siteResult);
            machine = new WorkerForScheduler(slots.get(processorResult).getSiteAddress(), slots.get(processorResult).getSiteIdentifier(),
                    slots.get(processorResult).getSiteZone(), slots.get(processorResult).getType_name(), slots.get(processorResult).getMachine_index(), slots.get(processorResult).getWorker_index(),
                    0, slots.get(processorResult).getOverhead(), new HashMap(),1.0,1.0);

            machine.addReadyToExecute(taskTimeResult);
           // System.out.println("\n\nTIME ALERT: " + taskTimeResult + "MACHINE ACTUAL TIME: " + machine.getReadyToExecute() + "\n");
           // System.out.println("totalCost:" + costResult);
            slots.get(processorResult).addHours(taskTimeResult / 3600);
            machine.addCost(costResult);
            machine.setMachineTypeIndex(processorResult);
           // System.out.println("Machine assigned: " + machine.toString());
           // System.out.println("Task: " + task);
          
            time = 0;
        }

//        
//       
        return machine;

    }

    private double Max(HashMap<String, List<WorkerForScheduler>> globalSlots) {
        double max = 0;
        for (String site : globalSlots.keySet()) {
            for (int j = 0; j < globalSlots.get(site).size(); j++) {
                if (max <= globalSlots.get(site).get(j).getReadyToExecute()) {
                    max = globalSlots.get(site).get(j).getReadyToExecute();
                }

            }
        }
        return max;
    }

//    private boolean RedundancyCheck(WorkerForScheduler slot) {
//        for (int i = 0; i < redundancyCheck.size(); i++) {
//      
//            if ((slot.getPerformance() == redundancyCheck.get(i).getPerformance()) && (slot.getCost() == redundancyCheck.get(i).getCost()) && (slot.getOverhead() == redundancyCheck.get(i).getOverhead())) {
//
//                return false;
//            }
//
//        }
//        return true;
//    }

    private HashMap MinRunningTimeInBudget(String wflId, WorkflowStage stage, Object properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots) {

        double criticalPath = 0.0; // estimated duration of the critical path

        double costResourceIndexMinInStage = 0.0;
        double costResourceIndexMinOutStage = 0.0;
        double transferTimeforResourceIndexMinInStage = 0.0;
        double transferTimeforResourceIndexMinOutStage = 0.0;

        HashMap<Integer, TaskProperties> tasksP = stage.getTaskProp();
        for (int taskid : tasksP.keySet()) {

            double taskduration = ((Double) tasksP.get(taskid).getTaskParam().get(3));

            List<FileProperties> inputFiles = tasksP.get(taskid).getInputs();
            List<FileProperties> outputFiles = tasksP.get(taskid).getOutputs();
            //find origin of each input file
            List<Integer> originFiles = getOriginFiles(inputFiles);
            //find destination of each output file
            List<Integer> destinationFiles = getDestinationFiles(outputFiles);

            String datacenterMin = "";
            int resourceIndexMin = -1;
            double completionTime = Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn = 0.0;
            double transferTimeforResourceIndexMinOut = 0.0;
            double costResourceIndexMinIn = 0.0;
            double costResourceIndexMinOut = 0.0;

            //find the resource where the task is completed in shorter time
            for (String site : globalResources.keySet()) {

                //get transfer time of all input files if we were to choose this site                
                int siteIndex = AgentList.indexOf(site);

                //CHECK IF ALL files can be moved there
                //can we put it somewhere else to enforcing it??                
                if (checkInputConstraints(inputFiles, siteIndex)) {

                    double totalTransferInCost = 0.0;
                    double totalTransferOutCost = 0.0;
                    //calculate input
                    double[] inputT = calculateInputTransfer(originFiles, inputFiles, siteIndex);
                    double totalTransferIn = inputT[0];
                   // System.out.println(totalTransferIn);
                    totalTransferInCost += inputT[1] + inputT[2];
                    //calculate output
                    double[] outputT = calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                    double totalTransferOut = outputT[0];

                    totalTransferOutCost += outputT[1] + outputT[2];

                    //we could check if this particular resource supports the app that the stage requires
                    List<WorkerForScheduler> slots = globalSlots.get(site);
                    for (int i = 0; i < slots.size(); i++) {
                        WorkerForScheduler slotI = slots.get(i);
                        double estComTime;
                        if (properties.equals("nodata")) {
                            estComTime = //taskduration / slotI.getPerformance()
                                    slotI.getExecTime(stage.getApplication(), taskduration)
                                    + slotI.getReadyToExecute();
                            totalTransferIn = 0.0;
                            totalTransferOut = 0.0;
                        } else {
                           // System.out.println(totalTransferIn);

                            estComTime = //taskduration / slotI.getPerformance()
                                    slotI.getExecTime(stage.getApplication(), taskduration)
                                    + slotI.getReadyToExecute() + totalTransferIn + totalTransferOut;
                        }

                        if (estComTime < completionTime) {
                            datacenterMin = site;
                            resourceIndexMin = i;
                            completionTime = estComTime;
                            transferTimeforResourceIndexMinIn = totalTransferIn;
                          //  System.out.println(totalTransferIn);

                            transferTimeforResourceIndexMinOut = totalTransferOut;
                            costResourceIndexMinIn = totalTransferInCost;
                            costResourceIndexMinOut = totalTransferOutCost;

                        }
                    }
                }
            }
            //we should have the minimum, so we assign the task to that resource
            if (resourceIndexMin != -1 && !datacenterMin.equals("")) {
                List<WorkerForScheduler> slots = globalSlots.get(datacenterMin);
                double timeA = //taskduration / slots.get(resourceIndexMin).getPerformance() 
                        slots.get(resourceIndexMin).getExecTime(stage.getApplication(), taskduration)
                        + transferTimeforResourceIndexMinIn + transferTimeforResourceIndexMinOut;
                slots.get(resourceIndexMin).addReadyToExecute(timeA);
                slots.get(resourceIndexMin).setMachineTypeIndex(resourceIndexMin);
                MapTask.put(taskid, slots.get(resourceIndexMin));
                slots.get(resourceIndexMin).AddTask(taskid);
                if (slots.get(resourceIndexMin).getReadyToExecute() > criticalPath) {
                    criticalPath = slots.get(resourceIndexMin).getReadyToExecute();
                }

                costResourceIndexMinInStage += costResourceIndexMinIn;
                costResourceIndexMinOutStage += costResourceIndexMinOut;
                double costA = //((taskduration / slots.get(resourceIndexMin).getPerformance())
                        (slots.get(resourceIndexMin).getExecTime(stage.getApplication(), taskduration)
                        * slots.get(resourceIndexMin).getCost() / 3600) + costResourceIndexMinOut + costResourceIndexMinIn;
                totalCost += (costA);
                listTasks.add(new DataTask(costA, taskid)); //List of all tasks for Budget Class

                transferTimeforResourceIndexMinInStage += transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage += transferTimeforResourceIndexMinOut;

            } else {//means that we do not have enough resources
                return new HashMap();
            }
        }
        totalCost += costResourceIndexMinInStage + costResourceIndexMinOutStage;
        String msg = "Stage " + wflId + "." + stage + " COSTTRANSFERIN:" + costResourceIndexMinInStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: " + msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);

        msg = "Stage " + wflId + "." + stage + " COSTTRANSFEROUT:" + costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: " + msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);

        msg = "Stage " + wflId + "." + stage + " TRANSFERIN:" + transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: " + msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);

        msg = "Stage " + wflId + "." + stage + " TRANSFEROUT:" + transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: MinRun.minRunningTime: " + msg);
        Logger.getLogger(MinRun.class.getName()).log(Level.INFO, msg);

        System.out.println("CWDEBUG: The critical path of stage " + stage.getId() + " is: " + criticalPath);

        Collections.sort(listTasks);
        Collections.reverse(listTasks);
//        for (int i = 0; i < listTasks.size(); i++) {
//            System.out.println(listTasks.get(i).costTask + " " + listTasks.get(i).task);
//        }
        return MapTask;

    }

    private Double TaskTime(HashMap<Integer, TaskProperties> tasksP, HashMap<String, List<WorkerForScheduler>> globalSlots, int tasks, String site, int processor, String appName) {
        Double taskTime;
        totalTransferInCost = 0;
        totalTransferOutCost = 0;
        List<FileProperties> inputFiles = tasksP.get(tasks).getInputs();

        List<FileProperties> outputFiles = tasksP.get(tasks).getOutputs();
        //find origin of each input file
        List<Integer> originFiles = getOriginFiles(inputFiles);
        //find destination of each output file
        List<Integer> destinationFiles = getDestinationFiles(outputFiles);
        int siteIndex = AgentList.indexOf(site);

        slots = globalSlots.get(site);

        //calculate input
        double[] inputT = calculateInputTransfer(originFiles, inputFiles, siteIndex);
        double totalTransferIn = inputT[0];
        totalTransferInCost = inputT[1] + inputT[2];
        //calculate output
        double[] outputT = calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
        double totalTransferOut = outputT[0];
        totalTransferOutCost = outputT[1] + outputT[2];
        double taskduration=(Double) tasksP.get(tasks).getTaskParam().get(3);
        taskTime = //((Double) tasksP.get(tasks).getTaskParam().get(3) / slots.get(processor).getPerformance()) 
                slots.get(processor).getExecTime(appName, taskduration)
                + totalTransferIn + totalTransferOut;
        return taskTime;
    }

    private double RoundSlotsCosts(HashMap<String, List<WorkerForScheduler>> globalSlots, double cost) {
        double hr;
        double round;
        double costLocal = cost;
        for (String site : globalSlots.keySet()) {
            number_OfProcessors = globalSlots.get(site).size();
            for (int z = 0; z < number_OfProcessors; z++) {
                hr = globalSlots.get(site).get(z).getReadyToExecute() / 3600.0;
                round = (int) ceil(globalSlots.get(site).get(z).getReadyToExecute() / 3600);
                costLocal += (round - hr) * (globalSlots.get(site).get(z).getCost());

            }
        }
        return costLocal;
    }


    private class DataTask implements Comparable<DataTask> {

        double costTask;
        Integer task;

        @Override
        public int compareTo(DataTask other) {
            return Double.compare(costTask, other.costTask);

        }

        DataTask(double costP, Integer taskP) {
            costTask = costP;
            task = taskP;
        }

    }
    
    private void writeToFile(File file,String content){
         FileWriter fw2 = null;
         BufferedWriter bw2;
         try {
             fw2 = new FileWriter(file.getAbsoluteFile(), true);
         } catch (IOException ex) {
             Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, ex);
         }
//        
         bw2 = new BufferedWriter(fw2);

         
        
        try {
            fw2= new FileWriter(file.getAbsoluteFile(),true);
        } catch (IOException ex) {
            Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, ex);
        }
//        
        bw2= new BufferedWriter(fw2);
        if (!file.exists()) {
            try {

                if(file.createNewFile())
                    System.out.println("File Created");
                else
                    System.out.println("ERROR");

              } catch (IOException ex) {
                Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        //FileWriter fw;
         try {

            bw2.write(content);
            bw2.newLine();
            bw2.close();

        } catch (IOException ex) {
            System.out.println("ERROR");

            Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, ex);
        }
			
    }


}
