/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow.policysample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.TaskProperties;
import tassl.application.workflow.PolicyAbstract;
import tassl.application.workflow.WorkerForScheduler;
import tassl.workflow.WorkflowStage;


/**
 *
 * @author Javier Diaz-Montes
 */
public class Deadline extends PolicyAbstract{

    public Deadline(){
        super();
    }

    @Override
    public HashMap scheduleStage(String wkflId, WorkflowStage stage, 
                HashMap<String, List<WorkerForScheduler>> globalSlots) {
        
        HashMap localMapTasks=new HashMap();
        
        String properties="";//not needed in these algorithms
                
        //NOTE: we assume that we only have one objective for now. 
        String objective=stage.getObjectiveType();
        
        if(objective.equals("DeadlineLocalityAware")){
            properties="";
            long start=System.currentTimeMillis();
            localMapTasks=DeadlineLocalityAware(wkflId,stage,properties,globalSlots);       
            long end=System.currentTimeMillis();
            System.out.println("CWDEBUG: DeadlineLocalityAware. seconds SchedulingTime:"+(end-start)/1000.0);
        }else if(objective.equals("DeadlineLocalityAwareData")){
            properties="";
            long start=System.currentTimeMillis();
            localMapTasks=DeadlineLocalityAwareData(wkflId,stage,properties,globalSlots);       
            long end=System.currentTimeMillis();
            System.out.println("CWDEBUG: DeadlineLocalityAwareData. seconds SchedulingTime:"+(end-start)/1000.0);
        }else if (objective.equals("DeadlineLocalityAwareProc")) {
            properties = "";
            long start = System.currentTimeMillis();
            localMapTasks = DeadlineLocalityAwareProc(wkflId, stage, properties,globalSlots);
            long end = System.currentTimeMillis();
            System.out.println("CWDEBUG: DeadlineLocalityAwareProc. seconds SchedulingTime:" + (end - start) / 1000.0);
        }else if (objective.equals("DeadlineLocalityAwareCost")){
            properties = "";
            long start = System.currentTimeMillis();
            localMapTasks = DeadlineLocalityAwareCost(wkflId, stage, properties,globalSlots);
            long end = System.currentTimeMillis();
            System.out.println("CWDEBUG: DeadlineLocalityAwareCost. seconds SchedulingTime:" + (end - start) / 1000.0);
        }/*else if (objective.equals("Deadline")){
            properties = "";
            long start = System.currentTimeMillis();
            localMapTasks = Deadline(wkflId, stage, properties,globalSlots);
            long end = System.currentTimeMillis();
            System.out.println("CWDEBUG: deadline. seconds SchedulingTime:" + (end - start) / 1000.0);
        }*/
        return localMapTasks;
    }
    
    //this is final version
    protected HashMap DeadlineLocalityAware(String wflId, WorkflowStage stage, Object properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){ 
        
        //we know that each vm has only one worker
        //find the resource that gives me the minimum running time and meets deadline.
        
        // consider execution time and transfer time (do not consider estimated time of execution)
        //change the map task to slot:{task,task}
        //send the tasks of each slot to each site, as it is important here. The query will have all the tasks that will consume.        
        
        //MapTask={task:workerForScheduler,...}
              
        //objectives are in a hash table type:value (xml). A stage can have multiple objectives
        double deadline=stage.getObjectiveValue("DeadlineLocalityAware");
        System.out.println("Original Deadline "+deadline);
        deadline=deadline - ((System.currentTimeMillis()-stage.getStartTime())/1000.0);
        System.out.println("Adjusted Deadline is "+deadline);
        
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, "Stage:"+stage+" Deadline:"+deadline);
        
        //we use a 15% shorter deadline to be conservative
        double conservativeDeadline=deadline-deadline*0.15;
        double usedDeadline=0.0; //deadline to use, conservative, actual, or unlimited when overdue
        double criticalPath=0.0; // estimated duration of the critical path
        
        //conservative deadline
        boolean conservative=false;
        
        System.out.println("Conservative Deadline would be "+conservativeDeadline + " Conservative deadline is:"+conservative);
        
        double costResourceIndexMinInStage=0.0;
        double costResourceIndexMinOutStage=0.0;
        double transferTimeforResourceIndexMinInStage=0.0;
        double transferTimeforResourceIndexMinOutStage=0.0;
        
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        
        for(int taskid:tasksP.keySet()){
            double taskduration=tasksP.get(taskid).getMinTime() + (Math.random()
                    * (tasksP.get(taskid).getMaxTime() - tasksP.get(taskid).getMinTime()));
            List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
            List <FileProperties>outputFiles=tasksP.get(taskid).getOutputs();
            
            String datacenterMin="";
            int resourceIndexMin=-1;
            double completionTime=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn=0.0;
            double transferTimeforResourceIndexMinOut=0.0;
            double costResourceIndexMinIn=0.0;
            double costResourceIndexMinOut=0.0;
            System.out.println("CWDEBUG: autonomicscheduler: Task "+taskid);
            //find origin of each input file
            List <Integer>originFiles=getOriginFiles(inputFiles);        
            //find destination of each output file
            List <Integer>destinationFiles=getDestinationFiles(outputFiles);
                      
            boolean overdue=false;
            //explore conservative, then non-conservative and then overdue. 
              //As soon as a task is non-conservative, all of them will be too. 
              // we do not do the same with overdue, for now.
            do{  
                System.out.println("CWDEBUG: autonomicscheduler. conservative:"+conservative+ " overdue:"+overdue);
                if(conservative){ //conservative deadline
                    usedDeadline=conservativeDeadline;
                }else if(!overdue){ //actual deadline
                    usedDeadline=deadline;
                }else{ //overdue. 
                    //TODO: put a maximum Overdue time?? if it cannot be met then return false
                    usedDeadline=Double.MAX_VALUE;
                }
                //TODO:we could check if this particular resource supports the app that the stage requires
                //find the resource where the task is completed in shorter time
                for(String site:globalResources.keySet()){
                    //get transfer time of all input files if we were to choose this site                
                    int siteIndex=AgentList.indexOf(site);    
                    
                    //CHECK IF ALL files can be moved there
                    //can we put it somewhere else to enforcing it??                
                    if(checkInputConstraints(inputFiles,siteIndex)){
                        
                        double totalTransferInCost=0.0;
                        double totalTransferOutCost=0.0;                    
                        //calculate input
                        double [] inputT=calculateInputTransfer(originFiles,inputFiles,siteIndex);
                        double totalTransferIn=inputT[0];
                        totalTransferInCost+=inputT[1]+inputT[2];
                        //calculate output
                        double [] outputT=calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                        double totalTransferOut=outputT[0];                    
                        totalTransferOutCost+=outputT[1]+outputT[2];

                        //calculate running time
                        List <WorkerForScheduler>slots=globalSlots.get(site);
                        System.out.println("CWDEBUG: autonomicScheduler. site "+site+" number of slots "+slots.size()); 
                        if(!overdue){
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(), taskduration)
                                        + totalTransferIn + totalTransferOut ;     

                                if(estComTime<completionTime && (slotI.getReadyToExecute()+estComTime) <= usedDeadline){
                                    datacenterMin=site;
                                    resourceIndexMin=i;
                                    completionTime=estComTime;
                                    transferTimeforResourceIndexMinIn=totalTransferIn;
                                    transferTimeforResourceIndexMinOut=totalTransferOut;
                                    costResourceIndexMinIn=totalTransferInCost;
                                    costResourceIndexMinOut=totalTransferOutCost;
                                }
                            }
                        }else{//if overdue, then we assign task to the resource with minimum time to completion
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(), taskduration)
                                        +slotI.getReadyToExecute() + totalTransferIn + totalTransferOut ;     

                                if(estComTime<completionTime){
                                    datacenterMin=site;
                                    resourceIndexMin=i;
                                    completionTime=estComTime;
                                    transferTimeforResourceIndexMinIn=totalTransferIn;
                                    transferTimeforResourceIndexMinOut=totalTransferOut;
                                    costResourceIndexMinIn=totalTransferInCost;
                                    costResourceIndexMinOut=totalTransferOutCost;
                                }
                            }
                        }
                    }
                }
                if(resourceIndexMin==-1){
                    if(conservative){
                        conservative=false;                        
                    }else if(!overdue){
                        overdue=true;                            
                    }else{//means that we do not have enough resources
                        System.out.println("CWDEBUG: autonomicscheduler stage could not find resources");
                        return new HashMap();
                    }
                }
            }while(resourceIndexMin==-1);
            //we should have the minimum, so we assign the task to that resource
            if(resourceIndexMin!=-1 && !datacenterMin.equals("")){
                List <WorkerForScheduler>slots=globalSlots.get(datacenterMin);
                
                slots.get(resourceIndexMin).addReadyToExecute(
                        slots.get(resourceIndexMin).getExecTime(stage.getApplication(), taskduration)                        
                        +transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut);
                
                MapTask.put(taskid, slots.get(resourceIndexMin));
                System.out.println("CWDEBUG: Deadline.deadlinelocalityaware"
                        + " Task:"+taskid+" Slot:"+slots.get(resourceIndexMin).toString());
                if(slots.get(resourceIndexMin).getReadyToExecute() > criticalPath){
                    criticalPath=slots.get(resourceIndexMin).getReadyToExecute();
                }
                
                costResourceIndexMinInStage+=costResourceIndexMinIn;
                costResourceIndexMinOutStage+=costResourceIndexMinOut;
                transferTimeforResourceIndexMinInStage+=transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage+=transferTimeforResourceIndexMinOut;
                
            }else{//means that we do not have enough resources
                //we should not reach this point
                System.out.println("CWDEBUG: autonomicscheduler no resources. CHECK if site name is empty string!!!");
                return new HashMap();
            }            
        }        
        
        String msg="Stage "+ wflId+"."+stage+" COSTTRANSFERIN:"+costResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.deadlinelocalityaware: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" COSTTRANSFEROUT:"+costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.deadlinelocalityaware: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
                
        msg="Stage "+ wflId+"."+stage+" TRANSFERIN:"+transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAware: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFEROUT:"+transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAware: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        System.out.println("CWDEBUG: The critical path is: "+ criticalPath);
                
        return MapTask;
                
    }

    //this one we move the non-indented code to the AutonomicScheduler to scheduleSingleStage, which will create class object and
    protected HashMap DeadlineLocalityAwareProc(String wflId, WorkflowStage stage, String properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){ 
        
        //we know that each vm has only one worker
        //find the resource that gives me the minimum running time and meets deadline.
        
        // consider execution time and transfer time (do not consider estimated time of execution)
        //change the map task to slot:{task,task}
        //send the tasks of each slot to each site, as it is important here. The query will have all the tasks that will consume.        
        
        //MapTask={task:workerForScheduler,...}
        
        //objectives are in a hash table type:value (xml). A stage can have multiple objectives
        double deadline=stage.getObjectiveValue("DeadlineLocalityAwareProc");
        System.out.println("Original Deadline "+deadline);
        deadline=deadline - ((System.currentTimeMillis()-stage.getStartTime())/1000.0);
        System.out.println("Adjusted Deadline is "+deadline);
        
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, "Stage:"+stage+" Deadline:"+deadline);
        
        //we use a 15% shorter deadline to be conservative
        double conservativeDeadline=deadline-deadline*0.15;
        double usedDeadline=0.0; //deadline to use, conservative, actual, or unlimited when overdue
        double criticalPath=0.0; // estimated duration of the critical path
        
        //conservative deadline
        boolean conservative=false;
        
        System.out.println("Conservative Deadline would be "+conservativeDeadline + " Conservative deadline is:"+conservative);
        
        double costResourceIndexMinInStage=0.0;
        double costResourceIndexMinOutStage=0.0;
        double transferTimeforResourceIndexMinInStage=0.0;
        double transferTimeforResourceIndexMinOutStage=0.0;
        
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        for(int taskid:tasksP.keySet()){
            double taskduration=tasksP.get(taskid).getMinTime() + (Math.random()
                    * (tasksP.get(taskid).getMaxTime() - tasksP.get(taskid).getMinTime()));
            List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
            List <FileProperties>outputFiles=tasksP.get(taskid).getOutputs();
            
            String datacenterMin="";
            int resourceIndexMin=-1;
            double completionTime=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn=0.0;
            double transferTimeforResourceIndexMinOut=0.0;
            double costResourceIndexMinIn=0.0;
            double costResourceIndexMinOut=0.0;
            System.out.println("CWDEBUG: autonomicscheduler: Task "+taskid);
            //find origin of each input file
            List <Integer>originFiles=getOriginFiles(inputFiles);        
            //find destination of each output file
            List <Integer>destinationFiles=getDestinationFiles(outputFiles);
                      
            boolean overdue=false;
            //explore conservative, then non-conservative and then overdue. 
              //As soon as a task is non-conservative, all of them will be too. 
              // we do not do the same with overdue, for now.
            do{  
                System.out.println("CWDEBUG: autonomicscheduler. conservative:"+conservative+ " overdue:"+overdue);
                if(conservative){ //conservative deadline
                    usedDeadline=conservativeDeadline;
                }else if(!overdue){ //actual deadline
                    usedDeadline=deadline;
                }else{ //overdue. 
                    //TODO: put a maximum Overdue time?? if it cannot be met then return false
                    usedDeadline=Double.MAX_VALUE;
                }
                //TODO:we could check if this particular resource supports the app that the stage requires
                //find the resource where the task is completed in shorter time
                for(String site:globalResources.keySet()){
                    //get transfer time of all input files if we were to choose this site                
                    int siteIndex=AgentList.indexOf(site);    
                    
                    //CHECK IF ALL files can be moved there
                    //can we put it somewhere else to enforcing it??                
                    if(checkInputConstraints(inputFiles,siteIndex)){
                    
                        double totalTransferInCost=0.0;
                        double totalTransferOutCost=0.0;                    
                        //calculate input
                        double [] inputT=calculateInputTransfer(originFiles,inputFiles,siteIndex);
                        double totalTransferIn=inputT[0];
                        totalTransferInCost+=inputT[1]+inputT[2];

                        //calculate output
                        double [] outputT=calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                        double totalTransferOut=outputT[0];                   
                        totalTransferOutCost+=outputT[1]+outputT[2];



                        //calculate running time
                        List <WorkerForScheduler>slots=globalSlots.get(site);
                        System.out.println("CWDEBUG: autonomicScheduler. site "+site+" number of slots "+slots.size()); 
                        if(!overdue){
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(),taskduration);

                                if(estComTime<completionTime && (slotI.getReadyToExecute() + estComTime + totalTransferIn + totalTransferOut) <= usedDeadline){
                                    datacenterMin=site;
                                    resourceIndexMin=i;
                                    completionTime=estComTime;
                                    transferTimeforResourceIndexMinIn=totalTransferIn;
                                    transferTimeforResourceIndexMinOut=totalTransferOut;
                                    costResourceIndexMinIn=totalTransferInCost;
                                    costResourceIndexMinOut=totalTransferOutCost;
                                }
                            }
                        }else{//if overdue, then we assign task to the resource with minimum time to completion
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(),taskduration) +slotI.getReadyToExecute();     

                                if(estComTime<completionTime){
                                    datacenterMin=site;
                                    resourceIndexMin=i;
                                    completionTime=estComTime;
                                    transferTimeforResourceIndexMinIn=totalTransferIn;
                                    transferTimeforResourceIndexMinOut=totalTransferOut;
                                    costResourceIndexMinIn=totalTransferInCost;
                                    costResourceIndexMinOut=totalTransferOutCost;
                                }
                            }
                        }
                    }
                }
                if(resourceIndexMin==-1){
                    if(conservative){
                        conservative=false;                        
                    }else if(!overdue){
                        overdue=true;                            
                    }else{//means that we do not have enough resources
                        System.out.println("CWDEBUG: autonomicscheduler stage could not find resources");
                        return new HashMap();
                    }
                }
            }while(resourceIndexMin==-1);
            //we should have the minimum, so we assign the task to that resource
            if(resourceIndexMin!=-1 && !datacenterMin.equals("")){
                List <WorkerForScheduler>slots=globalSlots.get(datacenterMin);
                
                slots.get(resourceIndexMin).addReadyToExecute(                        
                        slots.get(resourceIndexMin).getExecTime(stage.getApplication(),taskduration)
                        +transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut);
                
                MapTask.put(taskid, slots.get(resourceIndexMin));
                System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareProc"
                        + " Task:"+taskid+" Slot:"+slots.get(resourceIndexMin).toString());
                if(slots.get(resourceIndexMin).getReadyToExecute() > criticalPath){
                    criticalPath=slots.get(resourceIndexMin).getReadyToExecute();
                }
                
                costResourceIndexMinInStage+=costResourceIndexMinIn;
                costResourceIndexMinOutStage+=costResourceIndexMinOut;
                transferTimeforResourceIndexMinInStage+=transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage+=transferTimeforResourceIndexMinOut;
                
            }else{//means that we do not have enough resources
                //we should not reach this point
                System.out.println("CWDEBUG: autonomicscheduler no resources. CHECK if site name is empty string!!!");
                return new HashMap();
            }            
        }        
        
        String msg="Stage "+ wflId+"."+stage+" COSTTRANSFERIN:"+costResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareProc: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" COSTTRANSFEROUT:"+costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareProc: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
                
        msg="Stage "+ wflId+"."+stage+" TRANSFERIN:"+transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareProc: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFEROUT:"+transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareProc: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        System.out.println("CWDEBUG: The critical path is: "+ criticalPath);
               
        return MapTask;
                
    }
    
    protected HashMap DeadlineLocalityAwareCost(String wflId, WorkflowStage stage, String properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){ 
        
        //we know that each vm has only one worker
        //find the resource that gives me the minimum running time and meets deadline.
        
        // consider execution time and transfer time (do not consider estimated time of execution)
        //change the map task to slot:{task,task}
        //send the tasks of each slot to each site, as it is important here. The query will have all the tasks that will consume.        
        
        //MapTask={task:workerForScheduler,...}
        
        //objectives are in a hash table type:value (xml). A stage can have multiple objectives
        double deadline=stage.getObjectiveValue("DeadlineLocalityAwareCost");
        System.out.println("Original Deadline "+deadline);
        deadline=deadline - ((System.currentTimeMillis()-stage.getStartTime())/1000.0);
        System.out.println("Adjusted Deadline is "+deadline);
        
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, "Stage:"+stage+" Deadline:"+deadline);
        
        //we use a 15% shorter deadline to be conservative
        double conservativeDeadline=deadline-deadline*0.15;
        double usedDeadline=0.0; //deadline to use, conservative, actual, or unlimited when overdue
        double criticalPath=0.0; // estimated duration of the critical path
        
        //conservative deadline
        boolean conservative=true;
        
        System.out.println("Conservative Deadline would be "+conservativeDeadline + " Conservative deadline is:"+conservative);
        
        double costResourceIndexMinInStage=0.0;
        double costResourceIndexMinOutStage=0.0;
        double transferTimeforResourceIndexMinInStage=0.0;
        double transferTimeforResourceIndexMinOutStage=0.0;
        
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        for(int taskid:tasksP.keySet()){
            //MOUSTAFA - CHANGED TASK DURATION TO USE MAX TIME INSTEAD OF RANDOM (AVERAGE)
            double taskduration=tasksP.get(taskid).getMaxTime();
//            double taskduration=tasksP.get(taskid).getMinTime() + (Math.random()
//                    * (tasksP.get(taskid).getMaxTime() - tasksP.get(taskid).getMinTime()));
            List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
            List <FileProperties>outputFiles=tasksP.get(taskid).getOutputs();
            
            String datacenterMin="";
            int resourceIndexMin=-1;
            double completionTime=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn=0.0;
            double transferTimeforResourceIndexMinOut=0.0;
            double costResourceIndexMinIn=Double.MAX_VALUE;
            double costResourceIndexMinOut=Double.MAX_VALUE;
            double costResourceIndexMinVM=Double.MAX_VALUE;
            System.out.println("CWDEBUG: autonomicscheduler: Task "+taskid);
            //find origin of each input file
            List <Integer>originFiles=getOriginFiles(inputFiles);        
            //find destination of each output file
            List <Integer>destinationFiles=getDestinationFiles(outputFiles);
                      
            boolean overdue=false;
            //explore conservative, then non-conservative and then overdue. 
              //As soon as a task is non-conservative, all of them will be too. 
              // we do not do the same with overdue, for now.
            do{  
                System.out.println("CWDEBUG: autonomicscheduler. conservative:"+conservative+ " overdue:"+overdue);
                if(conservative){ //conservative deadline
                    usedDeadline=conservativeDeadline;
                }else if(!overdue){ //actual deadline
                    usedDeadline=deadline;
                }else{ //overdue. 
                    //TODO: put a maximum Overdue time?? if it cannot be met then return false
                    usedDeadline=Double.MAX_VALUE;
                }
                //TODO:we could check if this particular resource supports the app that the stage requires
                //find the resource where the task is completed in shorter time
                for(String site:globalResources.keySet()){
                    //get transfer time of all input files if we were to choose this site                
                    int siteIndex=AgentList.indexOf(site);    
                    
                    //CHECK IF ALL files can be moved there
                    //can we put it somewhere else to enforcing it??                
                    if(checkInputConstraints(inputFiles,siteIndex)){

                        double totalTransferInCost=0.0;
                        double totalTransferOutCost=0.0;                    
                        //calculate input
                        double [] inputT=calculateInputTransfer(originFiles,inputFiles,siteIndex);
                        double totalTransferIn=inputT[0];
                        totalTransferInCost+=inputT[1]+inputT[2];

                        //calculate output
                        double [] outputT=calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                        double totalTransferOut=outputT[0];                   
                        totalTransferOutCost+=outputT[1]+outputT[2];

                        //calculate running time
                        List <WorkerForScheduler>slots=globalSlots.get(site);
                        System.out.println("CWDEBUG: autonomicScheduler. site "+site+" number of slots "+slots.size()); 
                        if(!overdue){
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(),taskduration);

                                double costResourceIndexVM=slotI.getCost();
                                System.out.println("CWDEBUG: autonomicScheduler. " + slotI.getType_name()+ " " + (costResourceIndexVM + totalTransferInCost + totalTransferOutCost)+" the min is "+(costResourceIndexMinVM + costResourceIndexMinIn+costResourceIndexMinOut)); 

                                if( (costResourceIndexVM + totalTransferInCost + totalTransferOutCost) <= (costResourceIndexMinVM + costResourceIndexMinIn+costResourceIndexMinOut)){
                                    System.out.println("CWDEBUG: autonomicScheduler. estimated finish time " + (slotI.getReadyToExecute() + estComTime + totalTransferIn + totalTransferOut) +" deadline is "+usedDeadline); 
                                    if((slotI.getReadyToExecute() + estComTime + totalTransferIn + totalTransferOut) <= usedDeadline){
                                        datacenterMin=site;
                                        resourceIndexMin=i;
                                        completionTime=estComTime;
                                        transferTimeforResourceIndexMinIn=totalTransferIn;
                                        transferTimeforResourceIndexMinOut=totalTransferOut;
                                        costResourceIndexMinIn=totalTransferInCost;
                                        costResourceIndexMinOut=totalTransferOutCost;
                                        costResourceIndexMinVM=costResourceIndexVM;
                                    }
                                }
                            }
                        }else{//if overdue, then we assign task to the resource with minimum time to completion
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(),taskduration) +slotI.getReadyToExecute() + totalTransferIn + totalTransferOut ;     

                                if(estComTime<completionTime){
                                    datacenterMin=site;
                                    resourceIndexMin=i;
                                    completionTime=estComTime;
                                    transferTimeforResourceIndexMinIn=totalTransferIn;
                                    transferTimeforResourceIndexMinOut=totalTransferOut;
                                    costResourceIndexMinIn=totalTransferInCost;
                                    costResourceIndexMinOut=totalTransferOutCost;
                                }
                            }
                        }
                    }
                }
                if(resourceIndexMin==-1){
                    if(conservative){
                        conservative=false;                        
                    }else if(!overdue){
                        overdue=true;                            
                    }else{//means that we do not have enough resources
                        System.out.println("CWDEBUG: autonomicscheduler stage could not find resources");
                        return new HashMap();
                    }
                }
            }while(resourceIndexMin==-1);
            //we should have the minimum, so we assign the task to that resource
            if(resourceIndexMin!=-1 && !datacenterMin.equals("")){
                List <WorkerForScheduler>slots=globalSlots.get(datacenterMin);
                
                slots.get(resourceIndexMin).addReadyToExecute(                        
                        slots.get(resourceIndexMin).getExecTime(stage.getApplication(),taskduration)
                        +transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut);
                
                MapTask.put(taskid, slots.get(resourceIndexMin));
                System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareCost"
                        + " Task:"+taskid+" Slot:"+slots.get(resourceIndexMin).toString());
                if(slots.get(resourceIndexMin).getReadyToExecute() > criticalPath){
                    criticalPath=slots.get(resourceIndexMin).getReadyToExecute();
                }
                
                costResourceIndexMinInStage+=costResourceIndexMinIn;
                costResourceIndexMinOutStage+=costResourceIndexMinOut;
                transferTimeforResourceIndexMinInStage+=transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage+=transferTimeforResourceIndexMinOut;
                
            }else{//means that we do not have enough resources
                //we should not reach this point
                System.out.println("CWDEBUG: autonomicscheduler no resources. CHECK if site name is empty string!!!");
                return new HashMap();
            }            
        }        
        
        String msg="Stage "+ wflId+"."+stage+" COSTTRANSFERIN:"+costResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareCost: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" COSTTRANSFEROUT:"+costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareCost: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
                
        msg="Stage "+ wflId+"."+stage+" TRANSFERIN:"+transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareCost: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFEROUT:"+transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareCost: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        System.out.println("CWDEBUG: The critical path is: "+ criticalPath);
        

        return MapTask;
                
    }
    
    protected HashMap DeadlineLocalityAwareData(String wflId, WorkflowStage stage, String properties,
            HashMap<String, List<WorkerForScheduler>> globalSlots){ 
        
        //we know that each vm has only one worker
        //find the resource that gives me the minimum running time and meets deadline.
        
        // consider execution time and transfer time (do not consider estimated time of execution)
        //change the map task to slot:{task,task}
        //send the tasks of each slot to each site, as it is important here. The query will have all the tasks that will consume.        
        
        //MapTask={task:workerForScheduler,...}
        
        //objectives are in a hash table type:value (xml). A stage can have multiple objectives
        double deadline=stage.getObjectiveValue("DeadlineLocalityAwareData");
        System.out.println("Original Deadline "+deadline);
        deadline=deadline - ((System.currentTimeMillis()-stage.getStartTime())/1000.0);
        System.out.println("Adjusted Deadline is "+deadline);
        
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, "Stage:"+stage+" Deadline:"+deadline);
        
        //we use a 15% shorter deadline to be conservative
        double conservativeDeadline=deadline-deadline*0.15;
        double usedDeadline=0.0; //deadline to use, conservative, actual, or unlimited when overdue
        double criticalPath=0.0; // estimated duration of the critical path
        
        //conservative deadline
        boolean conservative=false;
        
        System.out.println("Conservative Deadline would be "+conservativeDeadline + " Conservative deadline is:"+conservative);
        
        double costResourceIndexMinInStage=0.0;
        double costResourceIndexMinOutStage=0.0;
        double transferTimeforResourceIndexMinInStage=0.0;
        double transferTimeforResourceIndexMinOutStage=0.0;
        
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        for(int taskid:tasksP.keySet()){
            double taskduration=tasksP.get(taskid).getMinTime() + (Math.random()
                    * (tasksP.get(taskid).getMaxTime() - tasksP.get(taskid).getMinTime()));
            List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
            List <FileProperties>outputFiles=tasksP.get(taskid).getOutputs();
            
            String datacenterMin="";
            int resourceIndexMin=-1;
            double completionTime=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinIn=Double.MAX_VALUE;
            double transferTimeforResourceIndexMinOut=Double.MAX_VALUE;
            double costResourceIndexMinIn=0.0;
            double costResourceIndexMinOut=0.0;
            System.out.println("CWDEBUG: autonomicscheduler: Task "+taskid);
            //find origin of each input file
            List <Integer>originFiles=getOriginFiles(inputFiles);        
            //find destination of each output file
            List <Integer>destinationFiles=getDestinationFiles(outputFiles);
                      
            boolean overdue=false;
            //explore conservative, then non-conservative and then overdue. 
              //As soon as a task is non-conservative, all of them will be too. 
              // we do not do the same with overdue, for now.
            do{  
                System.out.println("CWDEBUG: autonomicscheduler. conservative:"+conservative+ " overdue:"+overdue);
                if(conservative){ //conservative deadline
                    usedDeadline=conservativeDeadline;
                }else if(!overdue){ //actual deadline
                    usedDeadline=deadline;
                }else{ //overdue. 
                    //TODO: put a maximum Overdue time?? if it cannot be met then return false
                    usedDeadline=Double.MAX_VALUE;
                }
                //TODO:we could check if this particular resource supports the app that the stage requires
                //find the resource where the task is completed in shorter time
                for(String site:globalResources.keySet()){
                    //get transfer time of all input files if we were to choose this site                
                    int siteIndex=AgentList.indexOf(site);    
                    
                    //CHECK IF ALL files can be moved there
                    //can we put it somewhere else to enforcing it??                
                    if(checkInputConstraints(inputFiles,siteIndex)){
                    
                        double totalTransferInCost=0.0;
                        double totalTransferOutCost=0.0;                    
                        //calculate input
                        double [] inputT=calculateInputTransfer(originFiles,inputFiles,siteIndex);
                        double totalTransferIn=inputT[0];
                        totalTransferInCost+=inputT[1]+inputT[2];
                        //calculate output
                        double [] outputT=calculateOutputTransfer(destinationFiles, outputFiles, siteIndex);
                        double totalTransferOut=outputT[0];                    
                        totalTransferOutCost+=outputT[1]+outputT[2];



                        //calculate running time
                        List <WorkerForScheduler>slots=globalSlots.get(site);
                        System.out.println("CWDEBUG: autonomicScheduler. site "+site+" number of slots "+slots.size()); 
                        if(!overdue){
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(),taskduration);// + totalTransferIn + totalTransferOut;     

                                //looking for the site that has the minimum transfer time only.
                                if((totalTransferIn + totalTransferOut)<=(transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut)){ 

                                    //then looking for the VM that gives me the min completion time and fits the deadline
                                    if(estComTime<completionTime && (slotI.getReadyToExecute()+estComTime+totalTransferIn + totalTransferOut) <= usedDeadline){
                                        datacenterMin=site;
                                        resourceIndexMin=i;
                                        completionTime=estComTime;
                                        transferTimeforResourceIndexMinIn=totalTransferIn;
                                        transferTimeforResourceIndexMinOut=totalTransferOut;
                                        costResourceIndexMinIn=totalTransferInCost;
                                        costResourceIndexMinOut=totalTransferOutCost;
                                    }
                                }else{
                                    break; //this resource does not have the minimum transfer
                                }
                            }
                        }else{//if overdue, then we assign task to the resource with minimum time to completion
                            for(int i=0;i<slots.size();i++){
                                WorkerForScheduler slotI=slots.get(i);

                                double estComTime=slotI.getExecTime(stage.getApplication(),taskduration) +slotI.getReadyToExecute() + totalTransferIn + totalTransferOut ;     

                                if(estComTime<completionTime){
                                    datacenterMin=site;
                                    resourceIndexMin=i;
                                    completionTime=estComTime;
                                    transferTimeforResourceIndexMinIn=totalTransferIn;
                                    transferTimeforResourceIndexMinOut=totalTransferOut;
                                    costResourceIndexMinIn=totalTransferInCost;
                                    costResourceIndexMinOut=totalTransferOutCost;
                                }
                            }
                        }
                    }
                }
                if(resourceIndexMin==-1){
                    if(conservative){
                        conservative=false;                        
                    }else if(!overdue){
                        overdue=true;                            
                    }else{//means that we do not have enough resources
                        System.out.println("CWDEBUG: autonomicscheduler stage could not find resources");
                        return new HashMap();
                    }
                }
            }while(resourceIndexMin==-1);
            //we should have the minimum, so we assign the task to that resource
            if(resourceIndexMin!=-1 && !datacenterMin.equals("")){
                List <WorkerForScheduler>slots=globalSlots.get(datacenterMin);
                
                slots.get(resourceIndexMin).addReadyToExecute(
                        slots.get(resourceIndexMin).getExecTime(stage.getApplication(),taskduration)                        
                        +transferTimeforResourceIndexMinIn+transferTimeforResourceIndexMinOut);
                
                MapTask.put(taskid, slots.get(resourceIndexMin));
                System.out.println("CWDEBUG: Deadline.deadlinelocalityawareData"
                        + " Task:"+taskid+" Slot:"+slots.get(resourceIndexMin).toString());
                if(slots.get(resourceIndexMin).getReadyToExecute() > criticalPath){
                    criticalPath=slots.get(resourceIndexMin).getReadyToExecute();
                }
                
                costResourceIndexMinInStage+=costResourceIndexMinIn;
                costResourceIndexMinOutStage+=costResourceIndexMinOut;
                transferTimeforResourceIndexMinInStage+=transferTimeforResourceIndexMinIn;
                transferTimeforResourceIndexMinOutStage+=transferTimeforResourceIndexMinOut;
                
            }else{//means that we do not have enough resources
                //we should not reach this point
                System.out.println("CWDEBUG: autonomicscheduler no resources. CHECK if site name is empty string!!!");
                return new HashMap();
            }            
        }        
        
        String msg="Stage "+ wflId+"."+stage+" COSTTRANSFERIN:"+costResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.deadlinelocalityawareData: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" COSTTRANSFEROUT:"+costResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.deadlinelocalityawareData: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
                
        msg="Stage "+ wflId+"."+stage+" TRANSFERIN:"+transferTimeforResourceIndexMinInStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareData: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        msg="Stage "+ wflId+"."+stage+" TRANSFEROUT:"+transferTimeforResourceIndexMinOutStage;
        System.out.println("CWDEBUG: Deadline.DeadlineLocalityAwareData: "+msg);
        Logger.getLogger(Deadline.class.getName()).log(Level.INFO, msg);
        
        System.out.println("CWDEBUG: The critical path is: "+ criticalPath);
        
        return MapTask;
                
    }
 
    
}
