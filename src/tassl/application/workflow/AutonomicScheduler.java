/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.TaskProperties;
import tassl.application.metrics.RunningAvgMetrics;
import tassl.workflow.WorkflowStage;
import tassl.workflow.resources.Resource;
import tassl.application.utils.CommonUtils;
import tassl.application.utils.RunningStat;
import tassl.workflow.resources.Node;
import tassl.workflow.resources.NodeAcct;
import tassl.workflow.resources.ResourceCloud;
import tassl.workflow.resources.ResourceCluster;
import tassl.workflow.resources.ResourceOSG;
/**
 * Autonomic Scheduler / Resource Manager 
 * 
 * 
 * @author Javier Diaz-Montes
 */
public class AutonomicScheduler extends Thread{
    boolean alive=true;
    
    String schedulerAddress;
    int schedulerPort;
    
    String workflowMasterAddress;
    int workflowMasterPort;
    
    //(NOTE: sites and agents ARE THE SAME)
    
    //<"ip:port":Resource>
    HashMap <String,Resource>globalResources; //this stores resources of each agent
    //"ip:port"
    List <String>AgentList; //this is used to keep a order in the list of agents (NOTE: sites and agents are the same) 
                            //We cannot remove elements from this list
                            //NOTE: Agents of this list will not be in globalResources if the agent is down
    
    //this is used to keep the network speed between agents (MB/s)
    //[0]- {0 2 4}; [1]- {4 0 3}; [2] - {1 3 0} 
    List <List<Double>> AgentToAgentNetworkPush; //main list element push data to secondary list elements
    List <List<Double>> AgentToAgentNetworkPull; //main list element pull data from secondary list elements
    //this is used to keep the network speed between agent and working machine (MB/s)
    //[0]- {10 33}; [1]- {2 11}; [2] - {48 48} 
    List <Double[]> AgentToMachine;          //0 element is push to machine, 1 element is pull from machine
    
    HashMap <String,RunningAvgMetrics> globalMetrics; //this stores average metrics for each agent/site, if agent/site decided to use the Metrics Service
    
    //{appname:AvgTime,appname:AvgTime,...}
    HashMap<String,RunningStat> defaultAppExec; //this stores information of real execution times using as reference defaultReferenceBenchmark score
    double defaultReferenceBenchmark;//This is to calculate speedup of resources when no real data exists and user 
                                    //did not include a reference benchmark score in the workflow properties to relate with his/her estimated task exec time. 
                                        
    List <WorkflowStage>stagesToSchedule;//stages waitting for available resources to be scheduled
        
    HashMap <String, WorkflowStage> scheduledStages; //stages in execution 
            
    HashMap <String,List<Integer>> WkfStageAgent; //this is used to store where a wkf.stage was allocated. 
                                                    // The List of int are the pointers to AgentList.
    
    //wkf.stage:{site:{"worker:1;worker:1;...","taskid|taskid|..."},...} //worker is ip or type of VM. No port
    HashMap <String,HashMap<String,List<String>>>stagesFinalMaps;
    
    //{wkf.stage:{site:{nodeacct, nodeacct},...},...}
    HashMap <String,HashMap<String,List<NodeAcct>>> accountingGlobal;
    
    HashMap <String,List>policyCatalog;
    
    HashMap <String,String>NBstagesDoneAfter;
    
    //this is for the roundRobing scheduling
    int nextResourceIndex;

    private Semaphore mutex = new Semaphore(1, true);
    
    int monitorInterval;
    
    public AutonomicScheduler(String address, int port, String wflMasterAdd, int wflMasterPort, int monitorInterval, double defaultReferenceBenchmark){
        this.schedulerAddress=address;
        this.schedulerPort=port;
        
        this.workflowMasterAddress=wflMasterAdd;
        this.workflowMasterPort=wflMasterPort;
        
        this.globalResources=new HashMap();
        this.stagesToSchedule=new ArrayList();
        this.scheduledStages=new HashMap();
        this.nextResourceIndex=0;
        this.AgentList=new ArrayList();
        this.WkfStageAgent=new HashMap();
        this.AgentToAgentNetworkPush=new ArrayList();
        this.AgentToAgentNetworkPull=new ArrayList();
        this.AgentToMachine=new ArrayList();
        this.globalMetrics=new HashMap<>();
        this.stagesFinalMaps=new HashMap();
        this.accountingGlobal=new HashMap();
        
        this.NBstagesDoneAfter=new HashMap();
        
        this.defaultReferenceBenchmark=defaultReferenceBenchmark;
        this.defaultAppExec=new HashMap<>();
        
        this.monitorInterval=monitorInterval;
        
        this.initPolicyCatalog();
        
    }
    
    protected final void initPolicyCatalog(){        
        policyCatalog=new HashMap();
        
        //add policies that we have as default/examples
        
        //the methods inside the class determine the name of the policy we use in the XML description
        //   policy names have to be different from class name and have as a return type a HashMap (see getPolicies() below)
        
        String pol="tassl.application.workflow.policysample.Deadline";
        policyCatalog.put(pol, getPolicies(pol));
        
        pol="tassl.application.workflow.policysample.MinRun";
        policyCatalog.put(pol, getPolicies(pol));
        
        pol="tassl.application.workflow.policysample.Budget";
        policyCatalog.put(pol, getPolicies(pol));
        
                
    }
        
    public List getPolicies(String fullclassName){
        List policies=new ArrayList();
        String [] namesplit=fullclassName.split("\\.");
        //name of the class
        String classname=namesplit[namesplit.length-1];
        
        Class policyClass; 
        try {
            policyClass = Class.forName(fullclassName);
            for (Method i:policyClass.getDeclaredMethods()){
                String name=i.getName();
                
                //we skip the scheduleStage and constructors, we only want those return hashmap and are public or protected
                if(!name.equals("scheduleStage") && !name.equals(classname) && i.getReturnType()==HashMap.class 
                        && (Modifier.isPublic(i.getModifiers()) || Modifier.isProtected(i.getModifiers()))){
                    policies.add(name);                    
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return policies;
    }
    
    /**
     * Returns the class name that contains a policy with the name indicated 
     * in the argument or NULL if not found.
     * 
     * @param policyName
     * @return
     */
    public String findPolicyClass(String policyName) {

        for (String i : policyCatalog.keySet()) {
            //MOUSTAFA - INSERTED THIS TO CHECK CLASS NAME INSTEAD OF CLASS MODULES 
            //if (i.contains(policyName)) {
            //    return i;
            //}//method is the same name
            
            if (policyCatalog.get(i).contains(policyName)) {
                return i;
            }
        }
        
        return null;
    }
    
    //TODO: create thread to perform scheduling operation every X seconds. 
    
    @Override
    public void run(){
                
        Monitor mon=new Monitor();
        Thread monitoring=new Thread(mon);
        monitoring.start();
        
        System.out.println("Starting Autonomic Scheduler in "+schedulerPort);
        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "Starting Autonomic Scheduler in {0}", schedulerPort); 

        ServerSocket servSock;
        try {
            servSock = new ServerSocket(schedulerPort);
            servSock.setReuseAddress(true);
            DataInputStream input = null;
            DataOutputStream output = null;
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "AS ready at {0}", new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
            while (alive) {
                try {
                    Socket clientSocket = servSock.accept();
                    input = new DataInputStream(clientSocket.getInputStream());
                    output = new DataOutputStream(clientSocket.getOutputStream());

                    String command = input.readUTF();

                    Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "command: " + command);

                    //Agent send all info and gets registered here
                    if (command.equals("SendAllInfo")) {
                        System.out.println("CWDEBUG. autonomicscheduler. in sendAllinfo");
                        String agentAddress=input.readUTF();                        
                        int agentPort=input.readInt();
                        System.out.println("CWDEBUG. autonomicscheduler. agent:"+agentAddress+":"+agentPort);
                        int length = input.readInt(); //data length
                        byte[] bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                        Resource receivedObject=null;
                        try {
                            receivedObject = (Resource)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        if (receivedObject!=null){
                            //we just replace it. The info received from Agent should be more accurate
                            globalResources.put(agentAddress+":"+agentPort, receivedObject);
                            
                            String sitename=agentAddress+":"+agentPort;                            
                            if(AgentList.isEmpty() || !AgentList.contains(sitename)){                                
                                AgentList.add(sitename);
                            }
                            
                            if(globalResources.get(sitename).isUseMeticsService()){
                                retrieveAVGMetrics(sitename);
                                //calculate avg exec time apps, if data avail. {appname:avgTime,appname:avgTime}. Time is relative to referenceBenchScore
                                addMetricsToGeneralAvg(sitename); //this is to help machines/sites without real app execution data
                                
                            }
                            
                            calculateNetworkSpeed(sitename);
                            
                        }
                        //try to schedule pending stages because we may got new resources or new agents
                        
                        schedulePendingStages();
                        mutex.release();
                        
                    //Agent update its info 
                    } else if (command.equals("UpdateInfo")) {

                    //get supported apps
                    }else if (command.equals("getSupportedApps")) {
                        HashMap <String,HashMap<String,String>> supportedapps=new HashMap();
                        for(String site:globalResources.keySet()){
                            supportedapps.put(site, globalResources.get(site).getSupportedApps());
                        }                        
                        byte[] forwardObj;
                        forwardObj= programming5.io.Serializer.serializeBytes(supportedapps);                    

                        output.writeInt(forwardObj.length);                                                        
                        output.write(forwardObj); 
                        
                    //schedule workflow stages
                    } else if (command.equals("scheduleStagesTasks")){
                        String wflId=input.readUTF();
                        int length = input.readInt(); //data length
                        byte[] bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                                                
                        List <WorkflowStage>receivedObject=null;
                        try {
                            receivedObject = (List)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        //we may need to launch this in a thread if it takes too long
                        //       and implement a semaphore to make sure that no concurrent scheduling is happening
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        scheduleStages(wflId,receivedObject);                        
                        mutex.release();
                        
                    }else if(command.equals("scheduleStagesTasksNBT")){
                        int length = input.readInt(); //data length
                        byte[] bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                                                
                        HashMap <String, String> wflStage_workerTasks=null;
                        try {
                            wflStage_workerTasks = (HashMap)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        length = input.readInt(); //data length
                        byte[] bytesdata1 = new byte[length];                         
                        input.readFully(bytesdata1);
                        
                        HashMap<String,WorkflowStage> wflStageList=null;
                        try {
                            wflStageList = (HashMap)programming5.io.Serializer.deserialize(bytesdata1);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        if(wflStageList!=null){
                            for(String wkfStage:wflStageList.keySet()){
                                if(scheduledStages.containsKey(wkfStage)){
                                    WorkflowStage st=wflStageList.get(wkfStage);
                                    WorkflowStage st_AS=scheduledStages.get(wkfStage);
                                    
                                    //update stage AS has
                                    st_AS.replaceTaskProp(st.getTaskProp());
                                    st_AS.setObjectiveAddOnFromNonBlockingT(st.getObjectiveAddOnFromNonBlockingT());
                                }
                                
                            }
                        }
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        //reschedule stage
                        this.rescheduleStages(wflStage_workerTasks);
                        mutex.release();
                        
                    }else if (command.equals("stageDone")){
                        System.out.println("CWDEBUG. autonomicscheduler. Stagedone");
                        String wflId=input.readUTF();
                        String stageId=input.readUTF();
                        String stagesInvolved=input.readUTF();
                        String end=input.readUTF();
                        double totaltime=input.readDouble(); //total time of stage. Not used now.
                        
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        
                        dealStageDone(wflId,stageId,end,stagesInvolved);
                        
                        //try to schedule pending stages because we have free resources
                        schedulePendingStages();
                        
                        mutex.release();
                        
                    }else if (command.equals("cancelWorkflow")){
                        System.out.println("CWDEBUG. autonomicscheduler. cancelWorkflow");
                        String wflId=input.readUTF();
                        
                        boolean resourcesReleased=false;
                        
                        //TODO: tell workflowMaster to delete tasks
                        
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        for(int i=stagesToSchedule.size()-1;i>=0;i--){
                            WorkflowStage wkstage=stagesToSchedule.get(i);
                            if(wkstage.getWflId().equals(wflId)){
                                stagesToSchedule.remove(i);
                            }
                        }
                        
                        List<WorkflowStage> toRM=new ArrayList();
                        for(String wokfStageString:scheduledStages.keySet()){
                            WorkflowStage wkstage=scheduledStages.get(wokfStageString);
                            if(wkstage.getWflId().equals(wflId)){
                                toRM.add(wkstage);                                
                            }
                        }
                        for(WorkflowStage wkstage:toRM){//add new loop bc dealStageDone deletes stage from scheduledStages
                            dealStageDone(wflId,wkstage.getId(),new Long(System.currentTimeMillis()).toString(),"");
                            resourcesReleased=true;
                        }
                        
                        if(resourcesReleased){
                            //try to schedule pending stages because we have free resources
                            schedulePendingStages();
                        }
                        
                        mutex.release();
                    
                    }else if (command.equals("WorkerFailure")){
                        System.out.println("CWDEBUG. autonomicscheduler. WorkerFailure");
                        
                        String agentAddress=input.readUTF();                        
                        int agentPort=input.readInt();
                        
                        //receive info about workflow, workers and tasks
                        int length = input.readInt(); //data length
                        byte[] bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                        
                        //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp:0_CommaSeparated",
                        // "wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp:-1_CommaSeparated",...}
                        // 0 in the accounting part means that we need to stop the clock and -1 means that there was an error in provisioning and we remove this from accounting
                        HashMap <String, String> wflStage_workerTasks=null;
                        try {
                            wflStage_workerTasks = (HashMap)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        //receive new agent information
                        length = input.readInt(); //data length
                        bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                        Resource receivedObject=null;
                        try {
                            receivedObject = (Resource)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        
                        //we just replace it. The info received from Agent should be more accurate
                        globalResources.put(agentAddress+":"+agentPort, receivedObject);
                        
                        
                        //remove workers from stagesFinalMap
                        cleanStagesFinalMaps(wflStage_workerTasks,agentAddress,agentPort);
                        //Tell workflowManagerMaster to reinsert tasks and ask for uncompleted tasks                                
                        //call schedule with the workflowId and stageId so we can get ALL available slots plus those allocated to this workflow stage 
                        rescheduleStages(wflStage_workerTasks);
                        
                        mutex.release();
                        
                    }else if(command.equals("releaseOneWorker")){
                        System.out.println("CWDEBUG. autonomicscheduler. Worker Released");
                        String agentAddress=input.readUTF();                        
                        int agentPort=input.readInt();
                        
                        //receive info about workflow, workers and tasks
                        int length = input.readInt(); //data length
                        byte[] bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                        
                        //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp:0_CommaSeparated","wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp:-1_CommaSeparated",...}
                        // 0 in the accounting part means that we need to stop the clock and -1 means that there was an error in provisioning and we remove this from accounting
                        HashMap <String, String> wflStage_workerTasks=null;
                        try {
                            wflStage_workerTasks = (HashMap)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        //receive new agent information
                        length = input.readInt(); //data length
                        bytesdata = new byte[length];                         
                        input.readFully(bytesdata);
                        Resource receivedObject=null;
                        try {
                            receivedObject = (Resource)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                        
                        //we just replace it. The info received from Agent should be more accurate
                        globalResources.put(agentAddress+":"+agentPort, receivedObject);
                        //remove workers from stagesFinalMap
                        cleanStagesFinalMaps(wflStage_workerTasks,agentAddress,agentPort);
                        
                        mutex.release();
                        
                        this.printAllAccounting();
                        
                    }else if(command.equals("forceWorkflowReschedule")){
                        String workflowId=input.readUTF();
                        this.forceWorkflowReschedule(workflowId);
                    }else if(command.equals("forceRescheduleAll")){
                        this.forceRescheduleAll();
                    }else{
                        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "Unknown command: {0}", command);
                    }

                } catch (IOException e) {
                    Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, e);                
                } finally {
                    closeInOut(input,output);
                }
            }

        } catch (IOException ex) {
            System.out.println("ERROR: starting Server in "+schedulerPort);
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex){
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Calculate execution time for each application that we have real data
     *  as a reference of the defaultReferenceBenchmark
     * This can be used by machines without real data until they get it.
     * 
     * @param sitename     
     */
    protected void addMetricsToGeneralAvg(String sitename){
        // {AppName:{Machine:stats, Machine:stats,...}, AppName:{Machine:stats, Machine:stats,...},...}
        RunningAvgMetrics ravgm=globalMetrics.get(sitename); //ip:port  
        
        if(ravgm!=null){
        
            HashMap<String,HashMap<String,RunningStat>> appExec=globalMetrics.get(sitename).getAppExec();

            for(String appName:appExec.keySet()){
                    //System.out.println("App: "+appName);
                    
                for(String machine:appExec.get(appName).keySet()){
                    //System.out.print("    "+machine);
                    Node machineNode=globalResources.get(sitename).getNodeList().get(machine);
                    if(machineNode!=null){
                        double machineScore=machineNode.getBenchScore();
                        double value=appExec.get(appName).get(machine).Mean();
                        double refValue=value*(machineScore/this.defaultReferenceBenchmark);//multipy to normalize it bc value is supposedly referenceValue/MachineScore
                        
                        //System.out.print("    machineScore="+machineScore+" appValue="+value+"   refValue="+refValue);
                                
                        if(!defaultAppExec.containsKey(appName)){
                            defaultAppExec.put(appName, new RunningStat());
                        }
                        defaultAppExec.get(appName).Push(refValue);
                        
                        //System.out.println("    mean="+defaultAppExec.get(appName).Mean());
                        
                    }else{
                        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.WARNING, "Machine "+machine+" is not currently available in the site "+sitename);
                    }

                }
            }
        }
    }
    
    protected void forceWorkflowReschedule(String workflowId) throws IOException{
        //"wflId.stageId":"null;null;null"
        HashMap wflStage_workerTasks= new HashMap();
        for(String wkfStg:scheduledStages.keySet()){
            if(wkfStg.startsWith(workflowId+".")){
                wflStage_workerTasks.put(wkfStg, "null;null;null");
            }
        }
        if(!wflStage_workerTasks.isEmpty()){
            rescheduleStages(wflStage_workerTasks);
        }
        
    }
    protected void forceRescheduleAll() throws IOException{
        //"wflId.stageId":"null;null;null"
        HashMap wflStage_workerTasks= new HashMap();
        for(String wkfStg:scheduledStages.keySet()){            
            wflStage_workerTasks.put(wkfStg, "null;null;null");
        }
        if(!wflStage_workerTasks.isEmpty()){
            rescheduleStages(wflStage_workerTasks);
        }
        
    }
    
    /**
     * Report failed tasks to the workflowMaster and retrieve
     * @return
     */
    protected boolean rescheduleStages(HashMap <String, String> wflStage_workerTasks) throws IOException{
        //Tell workflowManagerMaster to reinsert tasks and ask for uncompleted tasks         
        Object []stream=CommonUtils.getOutputInput(workflowMasterAddress,workflowMasterPort);
        DataInputStream in=(DataInputStream)stream[0];
        DataOutputStream out=(DataOutputStream)stream[1];
        
        if(in==null || out==null){
            System.out.println("CWDEBUG: ERROR in rescheduleStages. It could not stablish connection with Task Generator. Workflow is not rescheduled");
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, "ERROR in rescheduleStages. It could not stablish connection with Task Generator. Workflow is not rescheduled");
            return false;
            
            //TODO: see what to do now
        }
        
        out.writeUTF("failedTasks");
        
        //send HashMap with wflStage_workerTasks
        byte[] wkflObj;                                                        
        wkflObj= programming5.io.Serializer.serializeBytes(wflStage_workerTasks);         
        out.writeInt(wkflObj.length);                    
        out.write(wkflObj);
        
        //receive info about workflow, done tasks
        int length = in.readInt(); //data length
        byte[] bytesdata = new byte[length];                         
        in.readFully(bytesdata);
        //{"wflId.stageId":"doneTasksCommaSeparated","wflId.stageId":"doneTasksCommaSeparated",...}                        
        HashMap <String, String> wflStage_doneTasks=null;
        try {
            wflStage_doneTasks = (HashMap)programming5.io.Serializer.deserialize(bytesdata);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //reschedule each of the affected stages
        WorkflowStage affectedStage;
        List <Integer> doneTasks=new ArrayList();
        boolean status;
        if(wflStage_doneTasks != null){
            for (String wkfstage:wflStage_doneTasks.keySet()){            
                //get done tasks as a list of integers
                for (String taskid:wflStage_doneTasks.get(wkfstage).split(",")){
                    if(!(taskid.trim()).isEmpty()){
                        doneTasks.add(Integer.parseInt(taskid));
                    }
                }

                //add done tasks to the stage info
                System.out.println("CWDEBUG: stage in reschedule "+wkfstage);
                affectedStage=scheduledStages.get(wkfstage); 
                if (affectedStage!=null){
                    affectedStage.addDonetasks(doneTasks);

                    List <String> nodeAcctList;
                    String [] valuePart=wflStage_workerTasks.get(wkfstage).split(";");                                   
                    //nodes that were released. type/ip:starttime
                    if(!valuePart[2].equals("null")){
                        nodeAcctList=new ArrayList(Arrays.asList(valuePart[2].split(",")));
                    }else{
                        nodeAcctList=new ArrayList();
                    }

                    //reschedule stage by calling the normal method
                    status=this.scheduleSingleStage(wkfstage.split("\\.")[0], affectedStage, nodeAcctList);
                    if(!status){
                        stagesToSchedule.add(affectedStage);

                        System.out.println("CWDEBUG: ERROR rescheduling stage "+wkfstage);
                        System.out.println("CWDEBUG: Adding stage to pending stage list ");
                    }
                }
            }
        }
        
        return true;
    }
    
    
    /**
     * Remove workers that are allocated to each workflow stage but have been reported to be down
     *      
     */
    protected void cleanStagesFinalMaps(HashMap <String, String> wflStage_workerTasks, String agentAddress, int agentPort){
                                                //worker is ip or type of VM. No port
        //stagesFinalMaps --> wkf.stage:{site:{"worker:1;worker:1;...","taskid|taskid|..."},...}  
        //                   HashMap <String,HashMap<String,List<String>>>stagesFinalMaps;
        String [] valuePart;
        String [] workers;
        String tasks;                
        List <String> nodeAcctList;
                
        boolean modified=false;
        for (String wkfstage:wflStage_workerTasks.keySet()){//wkfstage is wkflId.stageId
            valuePart=wflStage_workerTasks.get(wkfstage).split(";");
            
            System.out.println("CWDEBUG: autonomicscheduler.cleanStagesFinalMaps "+wflStage_workerTasks.get(wkfstage));
            
            workers=valuePart[0].split(",");
            tasks=valuePart[1]; //commaSeparated
            
            //nodes that were released. type/ip:starttime
            if(!valuePart[2].equals("null")){
                nodeAcctList=new ArrayList(Arrays.asList(valuePart[2].split(",")));
            }else{
                nodeAcctList=new ArrayList();
            }

            //remove workers from stagesFinalMap. This is done here because we will get from stagesFinalMaps the slots currently allocated to 
            //    the stage when rescheduling the tasks of a stage. Hence we do not want to have the slots/workers that failed.
            //wkf.stage:{site:{"worker:1;worker:1;...","taskid|taskid|..."},...} //worker is ip or type of VM. No port
            List <String>workersFinalMap=new ArrayList();
            HashMap<String,List<String>> stHMtemp=stagesFinalMaps.get(wkfstage);
            if(stHMtemp!=null && stHMtemp.containsKey(agentAddress+":"+agentPort)){
                String workersStr=stagesFinalMaps.get(wkfstage).get(agentAddress+":"+agentPort).get(0);
                
                System.out.println("CWDEBUG AutonomicScheduler. Previously allocated workers workersStr - "+workersStr);
                if(workersStr!=null && !workersStr.isEmpty()){
                    workersFinalMap=new ArrayList(Arrays.asList(workersStr.split(";")));
                }
            }
            
            for(String workerName:workers){   
                System.out.println("CWDEBUG. autonomicscheduler.cleanstagesfinalmaps workerName "+workerName);
                for(int i=0; i<workersFinalMap.size();i++){
                    String [] partWFM=workersFinalMap.get(i).split(":");
                    System.out.println("CWDEBUG. autonomicscheduler.cleanstagesfinalmaps worker "+workersFinalMap.get(i));
                    if (partWFM[0].equals(workerName)){
                        int num=Integer.parseInt(partWFM[1]);
                        if (num==1){
                            workersFinalMap.remove(i);
                            System.out.println("CWDEBUG. autonomicscheduler. calling StopClockResources line 476");
                            //Stop the clock for this resource (VM or machine) 
                            stopClockResource(nodeAcctList, wkfstage, agentAddress+":"+agentPort, partWFM[0]);                            
                            
                        }else{
                            num--;
                            workersFinalMap.set(i, partWFM[0]+":"+num);
                        }
                        modified=true;
                        break;
                    }
                }                                
            }
            if(modified){
                stagesFinalMaps.get(wkfstage).get(agentAddress+":"+agentPort).set(0, StringUtils.join(workersFinalMap.toArray(), ";"));
            }else{
                System.out.println("CWDEBUG. autonomicscheduler.cleanstagesfinalmaps NOT MODIFIED");
            }
        }                

    }
    
    
     /**
     * When a whole site goes down, we need to remove all its workers that are allocated to each workflow stage
     * 
     * @param wflStage_SitesDown {"wkf.stage":{"site","site"},..}     
     */
    protected void cleanStagesFinalMaps(HashMap <String, List<String>> wflStage_SitesDown){
                                                //worker is ip or type of VM. No port
        //stagesFinalMaps --> wkf.stage:{site:{"worker:1;worker:1;...","taskid|taskid|..."},...}  
        //                   HashMap <String,HashMap<String,List<String>>>stagesFinalMaps;
        
        //remove workers from stagesFinalMap. This is done here because we will get from stagesFinalMaps the slots currently allocated to 
        //    the stage when rescheduling the tasks of a stage. Hence we do not want to have the slots/workers that failed.
        //wkf.stage:{site:{"worker:1;worker:1;...","taskid|taskid|..."},...} //worker is ip or type of VM. No port
        
        for(String wkfstage:wflStage_SitesDown.keySet()){
        
            HashMap<String,List<String>> stHMtemp=stagesFinalMaps.get(wkfstage);

            if(stHMtemp!=null){
                
                for(String site:wflStage_SitesDown.get(wkfstage)){
                    
                    //remove the site from the map
                    stHMtemp.remove(site);
                                       
                    
                    //stop all accounting
                    this.stopClockAllResource(wkfstage, site);
                    
                }   
            }        
        }
    }
    
    
    /**
     * Stop the clock of a resource allocated to a stage from a site. The overall time will be used later to bill user (accounting)
     * 
     * @param nodeAcctList
     * @param wkfstage
     * @param site
     * @param machineToStop
     * @return
     */
    private void stopClockResource(List <String> nodeAcctList, String wkfstage, String site, String machineToStop){
        System.out.println("CWDEBUG autonomicscheduler.stopClockResource  "+site+"  "+machineToStop);
        String machine="";
        long starttime=0;
        String action=""; //0 stop clock, -1 remove
        System.out.print("CWDEBUG autonomicscheduler.stopClockResource - Looking ");
        for(int j=0; j<nodeAcctList.size();j++){//find a machine type/ip that match with the one to remove
            String [] tna=nodeAcctList.get(j).split(":");
            
            System.out.print(nodeAcctList.get(j)+ "   ");
            
            if((tna[0]).equals(machineToStop)){
                System.out.println("\n CWDEBUG autonomicscheduler.stopClockResource  "+machineToStop+" - "+nodeAcctList.get(j));
                machine=tna[0];
                starttime=Long.parseLong(tna[1]);
                action=tna[2];
                nodeAcctList.remove(j);
                break;
            }
        }
        if(!machine.isEmpty()){
            if(globalResources.get(site).getClass().getName().equals(ResourceCloud.class.getName())||globalResources.get(site).getClass().getName().equals(ResourceOSG.class.getName())){
                //here we stop a VM of the appropriated type that started at the appropriated time (we do not know ip of VM, not needed for accounting)
                for(NodeAcct naObj:accountingGlobal.get(wkfstage).get(site)){
                    if(naObj.getStartTime()==starttime){
                        if(naObj.getType().equals(machine)){
                            if(naObj.getStopTime()==0){
                                if(action.equals("0")){
                                    naObj.setStopTime();
                                }else{
                                    accountingGlobal.get(wkfstage).get(site).remove(naObj);
                                }
                                break;
                            }
                        }
                    }                                    
                }
            } else if(globalResources.get(site).getClass().getName().equals(ResourceCluster.class.getName())){ 
                //here we know exactly the machine to stop
                for(NodeAcct naObj:accountingGlobal.get(wkfstage).get(site)){
                    if(naObj.getIp().equals(machine)){
                        if(naObj.getStopTime()==0){
                            if(action.equals("0")){
                                naObj.setStopTime();
                            }else{
                                    accountingGlobal.get(wkfstage).get(site).remove(naObj);
                                }
                            break;
                        }
                    }
                }
            }
        }else{
            System.out.println("CWDEBUG autonomicscheduler.stopClockResource  "+machineToStop+" NOT FOUND");
        }
    }
    
    /**
     * Stop the clock of all resources allocated to a stage from a site. The overall time will be used later to bill user (accounting)
     * 
     * @param nodeAcctList
     * @param wkfstage
     * @param site
     * @param machineToStop
     * @return
     */
    private void stopClockAllResource(String wkfstage, String site){        
        
        for(NodeAcct naObj:accountingGlobal.get(wkfstage).get(site)){
            if(naObj.getStopTime()==0){
                naObj.setStopTime();
            }                                    
        }
        
    }
    
    
    /**
     * Do something when a stage is Done. By default tell agent to stop workers
     * 
     * @param wflId
     * @param stageId
     * @param end  indicates if there are more stages in the workflow. values: "end","noend"
     * @param stagesInvolved Stages involved. This is useful when non-blocking stages
     * @return
     */
    protected boolean dealStageDone(String wflId, String stageId, String end, String stagesInvolved){
        boolean status=true;
        
        List <Integer> listindex=WkfStageAgent.get(wflId+"."+stageId);
        if (listindex != null) {//MOUSTAFA - Added in case the Agent is not managing resources i.e. list index is null        
            for (int index:listindex){
                String agent=AgentList.get(index);
                //Resource r=globalResources.get(agent);

                System.out.println("CWDEBUG. autonomicscheduler.release nodes for index "+index);

                //call Agent to sync info and actually stop workers
                String []agentPart=agent.split(":");
                List param=new ArrayList();
                param.add("UpdateInfo");
                param.add("removeAll");
                param.add(wflId+"."+stageId);
                param.add(agent);
                System.out.println("CWDEBUG. autonomicscheduler. Send info agent");
                status=sendInfo(agentPart[0],Integer.parseInt(agentPart[1]), "RemoveStage", param);

                //stop the time of all resources
                stopClockAllResource(wflId+"."+stageId, agent);

            }
        }  
        //print cost VMs for each site
        calculateCostVMs(wflId,stageId);
                
        //delete info
        stagesFinalMaps.remove(wflId+"."+stageId);
        //delete stage
        scheduledStages.remove(wflId+"."+stageId);
        //delete accounting info
        //accountingGlobal.remove(wflId+"."+stageId);
        
        
        
        if(!stagesInvolved.trim().equals("")){            
            stagesInvolved=StringUtils.replace(stagesInvolved, stageId, "");//the first stage will be added in accounting
            NBstagesDoneAfter.put(wflId+"."+stageId, stagesInvolved);
        }
              
        
        this.printAllAccounting();
        
        return status;
    }
    
    protected void calculateCostVMs(String wflId, String stageId){
               
        double totalCost=0.0;
        
        //{site:{nodeacct, nodeacct},...}
        HashMap<String,List<NodeAcct>> acctStage=accountingGlobal.get(wflId+"."+stageId);
        if (acctStage != null) {//MOUSTAFA - Added in case the Agent is not managing resources i.e. acct stage is null
        for(String site:acctStage.keySet()){
            double costSite=0.0;
            for (NodeAcct naObj:acctStage.get(site)){               
                double time=Math.ceil((naObj.getStopTime()-naObj.getStartTime())/3600); //we round to the next hour                
                costSite+=time*naObj.getCost(); //the cost is per hour     
                System.out.println("CWDEBUG:  autonomicscheduler. calculateCostVMs. Type:"+naObj.getType()+",Ip:"+naObj.getIp()+" of site "+site+ 
                        " cost/hour "+naObj.getCost()+" time "+time+ " Totalcost:" +naObj.getCost());
            }
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "STAGE "+wflId+"."+stageId+" VM CostSite "+ site+ ":"+costSite); 
            totalCost+=costSite;
        }
        }        
        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "STAGE "+wflId+"."+stageId+" VM TOTALCost:"+ totalCost); 
    }
    
    protected boolean schedulePendingStages(){
        //this method can be called by several threads
        boolean status=true;
        
        do{
            if(!stagesToSchedule.isEmpty()){
                WorkflowStage stagei=stagesToSchedule.get(0);
                System.out.println("CWDEBUG: pending stage "+stagei.getId() + " of the workflow "+ stagei.getWflId());

                status=scheduleSingleStage(stagei.getWflId(),stagei, new ArrayList());
                System.out.println("CWDEBUG: status:" + status);
                if(!status){
                    break;
                }else{
                    stagesToSchedule.remove(0);
                    scheduledStages.put(stagei.getWflId()+"."+stagei.getId(),stagei);
                }
            }else{
                break;
            }
        }while(status);

        
        return status;
    }
    
    /**
     *  Schedule stages of a workflow. If there is no enough resources, stages are stored in a queue.
     *  
     * @param wflId
     * @param stages
     * @return true if all stages where scheduled, false otherwise
     */
    protected boolean scheduleStages(String wflId, List<WorkflowStage> stages){
        boolean status=true;
        
        System.out.println("CWDEBUG: Schedule stages");
        
        System.out.println("CWDEBUG: Schedule prior stages");
        //first check if there are prior stages to be scheduled
        status=schedulePendingStages();
        
        //no enough resources to schedule tasks
        if(!status){
            System.out.println("CWDEBUG: add stages to pending of scheduling");
            //stagesToSchedule.addAll(stages);
        //we try to schedule as many stages as we can
        }else{
            for(int i=0;i<stages.size();i++){
                if(status){
                    System.out.println("CWDEBUG: stage "+stages.get(i).getId() + " of the workflow "+ stages.get(i).getWflId());
                    status=scheduleSingleStage(wflId, stages.get(i), new ArrayList());
                    if(!status){//add stage to pending stages
                        stagesToSchedule.add(stages.get(i));
                        System.out.println("CWDEBUG: stage added to pending");
                    }else{//add stage to stages under execution
                        scheduledStages.put(wflId+"."+stages.get(i).getId(), stages.get(i));
                        System.out.println("CWDEBUG: stage added to scheduledstages");
                    }
                }else{
                    stagesToSchedule.add(stages.get(i));
                }                
            }
        }
        return status;
    }

    /**
     * Obtain the reference benchmark score for a stage (e.g. UnixBench)
     * if the score is not there, we use a default one which has been 
     *     defined in the workflow manager configuration.
     * 
     * @param stage
     * @return benchmark score
     */
    protected double referenceBenchmarkScore(WorkflowStage stage){
        String propertyFile=(String)stage.getProperties().get("PropertyFile"); //returns the properties contained in the original property file
        double value;
        if(propertyFile.contains("ReferenceBenchmark=")){
            String temp1=propertyFile.split("ReferenceBenchmark=")[1]; //split by our variable of interest
            String temp2=temp1.split("&#10;")[0]; //split by XML \n character
            value=Double.valueOf(temp2);
        }else{
            value=defaultReferenceBenchmark;
        }
        return value;
        
    }
    
    /**
     * Schedule a single stage
     * 
     * @param wflId
     * @param stage
     * @param nodeAcctList List of nodes and timestamps of failed nodes. Only needed for rescheduling.
     * @return status of the scheduling
     */
    protected boolean scheduleSingleStage(String wflId, WorkflowStage stage, List <String> nodeAcctList){
        boolean status=false;
                        
        //this time tells us when the stage is going to be schedule. It is used for accounting.
        long starttimestamp;        
        
        //Do Scheduling to get where to schedule it
        
        //objectives translates into scheduling policies
                       
        
        //NOTE: we assume that we only have one objective for now. 
        String policyName=stage.getObjectiveType(); //the objective indicates the policy scheduling method
        
        System.out.println("CWDEBUG: Policy for this stage is: "+policyName);
        
        double refBenchScore=referenceBenchmarkScore(stage);
        //get resources that are available
        //{site:{slot1,slot2,..},..}
        HashMap <String,List<WorkerForScheduler>>globalSlots= getAllAvailableSlots(wflId, stage.getId(), refBenchScore);
        //{task:workerForScheduler,...}
        HashMap <Integer,WorkerForScheduler>mapTask=new HashMap();


        Class policyClass = null;
        try {

            //"tassl.application.workflow.policysample.Deadline"
            //get class name for the selected policy
            policyClass = Class.forName(findPolicyClass(policyName));            
            Object policyObj = policyClass.newInstance();  

            Method method;                                   
            Class[] argTypes = new Class[]{HashMap.class, List.class, List.class, List.class, List.class};                
            //initialize with resources info
            method = PolicyAbstract.class.getDeclaredMethod("initialize", argTypes);
            method.invoke(policyObj, globalResources,AgentList,AgentToAgentNetworkPush,AgentToAgentNetworkPull,AgentToMachine);

            //the method to call is: public HashMap scheduleStage(String wkflId, WorkflowStage stage, HashMap<String, List<WorkerForScheduler>> globalSlots)
            argTypes = new Class[]{String.class, WorkflowStage.class, HashMap.class};
            method = policyClass.getDeclaredMethod("scheduleStage", argTypes);
            mapTask=(HashMap)method.invoke(policyObj, wflId,stage,globalSlots);

        } catch (IllegalArgumentException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);                                 
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, "Cause: ", ex.getCause());   
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);                                            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);                
        }


        HashMap <String,List<String>> finalMap=getFinalMap(mapTask,stage.getId());        

        //Compare this finalMap with the previous one. create a diff finalMap
                // add negative number of workers when releasing a resource type
                // send list of tasks to all sites to update workers' queries 

        //finalMap- HashMap site:{"type/name:numWorkers;...","task|task|task"}, site:{"type/name:numWorkers;..","task|task"}

        HashMap <String,List<String>> finalMapforAgents;
        if(stagesFinalMaps.containsKey(wflId+"."+stage)){//we are rescheduling

            starttimestamp=System.currentTimeMillis();

            finalMapforAgents=new HashMap();

            HashMap <String,List<String>> previousFinalMap=stagesFinalMaps.get(wflId+"."+stage);                
            for(String siteName:finalMap.keySet()){
                if(previousFinalMap.containsKey(siteName)){
                    List <String> current=new ArrayList(Arrays.asList(finalMap.get(siteName).get(0).split(";")));
                    List <String> previous=new ArrayList(Arrays.asList(previousFinalMap.get(siteName).get(0).split(";")));
                    List <String> adjusted=new ArrayList();

                    //add the current "type/name:numWorkers" elemets that are not already allocated                        
                    for(String c:current){                            
                        if(!previous.contains(c)){
                            System.out.println("CWDEBUG autonomicscheduler. in adjusted.add "+c);
                            adjusted.add(c);
                        }else{
                            System.out.println("CWDEBUG autonomicscheduler. in previous.remove "+c);
                            previous.remove(c);
                        }
                    }

                    //here adjusted only has the new ones to add. Include this in the accounting.
                    //add machines to the accounting
                    //generate the list of nodeAcct
                    List<NodeAcct> nodeAcctObjList=generateNodeAcctList(adjusted, siteName, starttimestamp, stage.getApplication());
                    accountingGlobal.get(wflId+"."+stage.getId()).get(siteName).addAll(nodeAcctObjList);


                    //add the remaining slots of previous with negative value to release them
                    for (String p:previous){
                        String [] part=p.split(":");
                        if(!part[0].trim().isEmpty()){
                            adjusted.add(part[0]+":-1");

                            //this resource is not in nodeAcctList.Create new from the rest
                            List<String> nodeAcctListActive=new ArrayList();
                            List <NodeAcct> acctSite=accountingGlobal.get(wflId+"."+stage.getId()).get(siteName);
                            for(NodeAcct temp:acctSite){
                                if(temp.getStopTime()==0){
                                    nodeAcctListActive.add(temp.getType()+":"+temp.getStartTime()+":0");
                                }
                            }
                            //all the failed 
                            nodeAcctListActive.addAll(nodeAcctList);

                            System.out.println("CWDEBUG. autonomicscheduler. calling StopClockResources line 857");
                            //Stop the clock for this resource (VM or machine) 
                            stopClockResource(nodeAcctListActive, wflId+"."+stage, siteName, part[0]);


                        }

                    }
                    List temp=new ArrayList();
                    temp.add(StringUtils.join(adjusted.toArray(),";"));                        
                    temp.add(finalMap.get(siteName).get(1));
                    finalMapforAgents.put(siteName, temp);

                    previousFinalMap.remove(siteName);
                }else{
                    finalMapforAgents.put(siteName, finalMap.get(siteName));

                    //here adjusted only has the new ones to add. Include this in the accounting.
                    //add machines to the accounting
                    //generate the list of nodeAcct
                    List<NodeAcct> nodeAcctObjList=generateNodeAcctList(Arrays.asList(finalMap.get(siteName).get(0).split(";")), 
                            siteName, starttimestamp, stage.getApplication());
                    if(accountingGlobal.get(wflId+"."+stage.getId()).containsKey(siteName)){
                        accountingGlobal.get(wflId+"."+stage.getId()).get(siteName).addAll(nodeAcctObjList);
                    }else{
                        accountingGlobal.get(wflId+"."+stage.getId()).put(siteName,nodeAcctObjList);
                    }
                }
            }

            if(!previousFinalMap.isEmpty()){
                //add these to release resources
                List <String> adjusted=new ArrayList();
                for (String siteName:previousFinalMap.keySet()){
                    System.out.println("CWDEBUG autonomicscheduler. Terminate all resources from "+siteName);
                    String [] previous=previousFinalMap.get(siteName).get(0).split(";");
                    System.out.print("           CWDEBUG autonomicscheduler. ");
                    for(String a:previous){
                        System.out.print(a+" ");
                    }
                    System.out.print("\n");

                    for (String p:previous){
                        String [] part=p.split(":");
                        if(!part[0].trim().isEmpty()){
                            adjusted.add(part[0]+":-1");

                            //this resource is not in nodeAcctList.Create new from the rest
                            List<String> nodeAcctListActive=new ArrayList();
                            List <NodeAcct> acctSite=accountingGlobal.get(wflId+"."+stage.getId()).get(siteName);
                            for(NodeAcct temp:acctSite){
                                if(temp.getStopTime()==0){
                                    nodeAcctListActive.add(temp.getType()+":"+temp.getStartTime()+":0");
                                }
                            }
                            //all the failed 
                            nodeAcctListActive.addAll(nodeAcctList);

                            System.out.println("CWDEBUG. autonomicscheduler. calling StopClockResources line 910");
                            //Stop the clock for this resource (VM or machine) 
                            stopClockResource(nodeAcctListActive, wflId+"."+stage, siteName, part[0]);

                        }
                    }                        
                    List temp=new ArrayList();
                    temp.add(StringUtils.join(adjusted.toArray(),";"));
                    temp.add("null");
                    finalMapforAgents.put(siteName, temp);                                                
                }
                previousFinalMap.clear();
            }

        }else{//first time the stage is scheduled

            starttimestamp=System.currentTimeMillis();

            finalMapforAgents=finalMap;

            //store accounting info to calculate cost when stage is done

            HashMap<String,List<NodeAcct>> tempSitesAcct=new HashMap();
            for(String siteS:finalMap.keySet()){
                List<String> sitemap=finalMap.get(siteS);       

                //add machines to the accounting
                //generate the list of nodeAcct                    
                List<NodeAcct> nodeAcctObjList=generateNodeAcctList(Arrays.asList(sitemap.get(0).split(";")), siteS, starttimestamp, 
                        stage.getApplication());

                tempSitesAcct.put(siteS, nodeAcctObjList);
            }                
            accountingGlobal.put(wflId+"."+stage.getId(), tempSitesAcct);

        }


        //store map to keep current scheduling status
        stagesFinalMaps.put(wflId+"."+stage, finalMap);

        //call each site to allocate resources
        // this call will return the status of the site and therefore the local resources status gets updated
        if(!finalMap.isEmpty()){
            List failedAllocations=communicateAgentsScheduling(wflId, stage, "exact", finalMapforAgents, starttimestamp, refBenchScore);

            if(!failedAllocations.isEmpty()){    
                Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, "Some Agent reported error when allocating resources");
                //TODO: check if agents in list are up, request updated resources and reschedule
                return false;
            }

            status=true;
        }else{
            
            if ( stage.getTaskProp().isEmpty() ){ //no tasks to schedule. 
                status=true;
            }else{  //there were tasks to schedule but no resources
                status=false;
            }
        }


        //CWDEBUG
        //finalMap- HashMap site:{"type/name:numWorkers;...","task|task|task"}, site:{"type/name:numWorkers;..","task|task"}
        System.out.println("CWDEBUG: autonomicscheduler. CURRENT FINALMAP");
        for(String site:finalMap.keySet()){
            System.out.println("  Site: "+site);
            System.out.println("    Resources: "+finalMap.get(site).get(0));
        }       
        
                
        //CWDEBUG. 
        printAllAccounting();
        
        return status;
    }
  
    private List<NodeAcct> generateNodeAcctList(List<String> machines, String site, long starttimestamp, String appName){
        List<NodeAcct> nodeAcctObjList=new ArrayList();                    
        for(String wk:machines){
            double cost=globalResources.get(site).getNodeList().get(wk.split(":")[0]).getCost();
            if(globalResources.get(site) instanceof ResourceCloud||globalResources.get(site) instanceof ResourceOSG){                  
                nodeAcctObjList.add(new NodeAcct(wk.split(":")[0], "dummyIp", starttimestamp, cost, appName));
            }else if(globalResources.get(site) instanceof ResourceCluster){    
                nodeAcctObjList.add(new NodeAcct("cluster",wk.split(":")[0], starttimestamp, cost, appName));
            }
        }
        return nodeAcctObjList;
    }
    
    /**
     * Look into available resources and determine the number of slots(workers) available.
     * So far slots(workers) are not share across stages.
     * @return hashmap with slots per site.
     */
//    protected HashMap <String,List<WorkerForScheduler>> getAllAvailableSlots(){
//        //{site:{slot1,slot2,..},..}
//        HashMap <String,List<WorkerForScheduler>>globalSlots= new <String,List<WorkerForScheduler>>HashMap();        
//        //get resources that are available
//        for(String site:globalResources.keySet()){                
//            Resource res=globalResources.get(site);
//            globalSlots.put(site,res.getAvailableSlotsScheduling(site));
//        }  
//        return globalSlots;
//    }
    
     /**
     * Look into available resources and determine the number of slots(workers) available. 
     * It includes the slots already allocated to a workflow-stage. This is used when rescheduling.
     * So far slots(workers) are not share across stages.
     * @return hashmap with slots per site.
     */
    protected HashMap <String,List<WorkerForScheduler>> getAllAvailableSlots(String workflow, String stage, double refBenchScore){        
        //{site:{slot1,slot2,..},..}
        HashMap <String,List<WorkerForScheduler>>globalSlots= new HashMap<>();        
        //get resources that are available
        for(String site:globalResources.keySet()){                
            Resource res=globalResources.get(site);
            if(!stagesFinalMaps.containsKey(workflow+"."+stage)){
                globalSlots.put(site,res.getAvailableSlotsScheduling(site, new String[0],refBenchScore,defaultAppExec,this.defaultReferenceBenchmark));
            }else{ 
                List <String>siteAllocation=stagesFinalMaps.get(workflow+"."+stage).get(site);
                String [] allocatedWorkers;
                if(siteAllocation==null || siteAllocation.isEmpty()){
                    allocatedWorkers=new String[0];
                }else{
                    allocatedWorkers=siteAllocation.get(0).split(";");
                    System.out.println("CWDEBUG:ALLOCATED WORKERS: "+siteAllocation.get(0));
                    System.out.println("CWDEBUG:ALLOCATED TASKS: "+siteAllocation.get(1));
                }
                 
                //call function adding the allocated workers info
                globalSlots.put(site,res.getAvailableSlotsScheduling(site, allocatedWorkers,refBenchScore,defaultAppExec,this.defaultReferenceBenchmark));
                //this return slots with speedup calculated and including the defaultAppExec hashmap
                
            }
            
            //update real performance, overhead, failures, and network
            if(res.isUseMeticsService()){
                loadMetricsToWorkerForSchedule(site,globalSlots.get(site));
            }
            
            
        }
        
        return globalSlots;
    }
    
    
    /**
     * Update slots list with metrics retrieved from the Metrics Service
     * @param site
     */
    protected void loadMetricsToWorkerForSchedule(String site, List<WorkerForScheduler> slots){
        
        RunningAvgMetrics ravgm=globalMetrics.get(site); //ip:port  
        
        if(ravgm!=null){
            // {AppName:{Machine:stats, Machine:stats,...}, AppName:{Machine:stats, Machine:stats,...},...}
            HashMap<String,HashMap<String,RunningStat>> appExec=ravgm.getAppExec();
            
            // {Machine:{AppName:stats, AppName:stats,...}, Machine:{AppName:stats, AppName:stats,...},...}
            HashMap<String,HashMap<String,RunningStat>> appExecForWorker=new HashMap<>();
            
            for(String appName:appExec.keySet()){
                HashMap<String,RunningStat> machines=appExec.get(appName);
                for(String machine:machines.keySet()){
                    HashMap<String,RunningStat> appsInside;
                    
                    if(appExecForWorker.containsKey(machine)){
                        appsInside=appExecForWorker.get(machine);
                    }else{
                        appsInside=new HashMap<>();
                        appExecForWorker.put(machine, appsInside);
                    }
                    appsInside.put(appName, machines.get(machine));
                }
            }
                        
            //{AppName:{#Machines:{statsOverhead, statsFailure}, #Machines:{statsOverhead, statsFailure},...}, AppName:{#Machines:{statsOverhead, statsFailure}, #Machines:{statsOverhead, statsFailure},...},...}
            //there is "default" appname that adds all overheads and failures. Overhead is time, and failure is ratio from 0 to 1
            //in the "default" appname, the #Machines==0 has average of all number of machines
            HashMap<String,HashMap<Integer,RunningStat[]>> provMachineOverheadFailure=ravgm.getProvMachineOverheadFailure();
            //there is "default" appname that adds all overheads and failures
            ////{AppName:{#Workers:{statsOverhead, statsFailure}, #Workers:{statsOverhead, statsFailure},...}, AppName:{#Workers:{statsOverhead, statsFailure}, #Workers:{statsOverhead, statsFailure},...},...}
            //in the "default" appname, the #Workers==0 has average of all number of Workers
            HashMap<String,HashMap<Integer,RunningStat[]>> provWorkerOverheadFailure=ravgm.getProvWorkerOverheadFailure();

            //{AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...}, AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...},...}
            //there is "default" appname, "default" machine that adds all worker failures regarless type
            HashMap<String,HashMap<String,Long []>> workerFailure=ravgm.getWorkerFailure();

            //{AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...}, AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...},...}
            //there is "default" appname, "default" machine that adds all machine failures regarless type
            HashMap<String,HashMap<String,Long []>> machineFailure=ravgm.getMachineFailure();

            //WORK OVER WorkerForScheduler list            
            for(WorkerForScheduler s:slots){
                //Overhead
                if(provMachineOverheadFailure.get("default")!=null && provWorkerOverheadFailure.get("default") != null){
                    s.setOverhead(provMachineOverheadFailure.get("default").get(0)[0].Mean() + provWorkerOverheadFailure.get("default").get(0)[0].Mean());
                }
                
                //Performance
                if(appExecForWorker.get(s.getType_name())!=null){                    
                    s.setPerformanceAppReal(appExecForWorker.get(s.getType_name()));
                }
                
                //failures. Gross approx. average of all machines and workers failures
                //IMPROVE: add by app and separate worker from machine failures
                if(machineFailure.get("default")!=null){
                    Long [] failures=machineFailure.get("default").get("default");
                    double failMachine=failures[1]/(failures[0]+failures[1]);
                    s.setFailureRateMachine(failMachine);
                }
                if(workerFailure.get("default")!=null){
                    Long [] failures=workerFailure.get("default").get("default");
                    double failWorker=failures[1]/(failures[0]+failures[1]);
                    s.setFailureRateWorker(failWorker);
                }
            }

            //load AgentToAgentNetworkPush and AgentToAgentNetworkPull
            loadNetworkMetrics(site);        
            printNetworkInfo();
        }
    }
    
    
    protected boolean loadNetworkMetrics(String site){
        
        int siteIndex=AgentList.indexOf(site); //ip:port        
        
        RunningAvgMetrics ravgm=globalMetrics.get(site);
        
        boolean success=false;        
        if(ravgm!=null){
        
            //SiteAddress:{push,pull}. Note that is address, not name, not address:port.
            HashMap<String, RunningStat[]> siteTransfer=ravgm.getSiteTransfer();

            //{push,pull}
            RunningStat[] workerTransfer=ravgm.getWorkerTransfer();
        
        
            boolean newSite=false;
            List<Double> pushList=new ArrayList<>();
            if(siteIndex<AgentToAgentNetworkPush.size()){
                pushList=AgentToAgentNetworkPush.get(siteIndex);
            }else{
                AgentToAgentNetworkPush.add(pushList);
                newSite=true;
            }

            List<Double> pullList=new ArrayList<>();
            if(siteIndex<AgentToAgentNetworkPull.size()){
                pullList=AgentToAgentNetworkPull.get(siteIndex);
            }else{
                AgentToAgentNetworkPull.add(pullList);
            }

            for(int i=0;i<AgentList.size();i++){
                String siteT=AgentList.get(i);
                String siteAddress=siteT.split(":")[0];

                if(i==siteIndex){
                    if(newSite){
                        pushList.add(i, 0.0);
                        pullList.add(i, 0.0);
                    }else{
                        if(pushList.get(i)!=0.0){
                            pushList.remove(i);
                            pushList.add(i, 0.0);
                        }

                        if(pullList.get(i)!=0.0){
                            pullList.remove(i);
                            pullList.add(i, 0.0);
                        }
                    }  

                    continue;
                }
                RunningStat[] AtAObj=siteTransfer.get(siteAddress);   
                //System.out.println("NET "+Arrays.toString(AtAObj));
                addPushPullSites(AtAObj, newSite, pushList,pullList,i);
                
            }

            
            //add it to others lists
            
            for(int i=0;i<AgentToAgentNetworkPush.size() && i<AgentToAgentNetworkPull.size();i++){//both lists should have same length
                if(i==siteIndex){
                    continue;
                }
                pushList= AgentToAgentNetworkPush.get(i);
                pullList=AgentToAgentNetworkPull.get(i);

                String siteT=AgentList.get(i);

                ravgm=globalMetrics.get(siteT);

                RunningStat[] AtAObj=null;
                if(ravgm!=null){
                    AtAObj=siteTransfer.get(site.split(":")[0]);
                }

                addPushPullSites(AtAObj, newSite, pushList,pullList,siteIndex);

            }
            
            


            //load AgentToMachine
            Double []localPushPull=new Double[2];
            AgentToMachine.add(siteIndex,localPushPull);
            if(workerTransfer[0].NumDataValues()>1){
                AgentToMachine.get(siteIndex)[0]=workerTransfer[0].Mean();        
            }else{
                localPushPull[0]=18D;                
            }
            if(workerTransfer[1].NumDataValues()>1){
                AgentToMachine.get(siteIndex)[1]=workerTransfer[1].Mean();
            }else{
                localPushPull[1]=18D;
            }
            success=true;
        }
        return success;
    }    
    /*
     * //this is used to keep the network speed between agents (MB/s)
        //[0]- {0 2 4}; [1]- {4 0 3}; [2] - {1 3 0} 
        List <List<Double>> AgentToAgentNetworkPush; //main list element push data to secondary list elements
        List <List<Double>> AgentToAgentNetworkPull; //main list element pull data from secondary list elements
     */
    
    public void addPushPullSites(RunningStat[] AtAObj, boolean newSite, List<Double> pushList, List<Double> pullList, int i){
        if(AtAObj==null){
            //to unknown
            if(newSite){ //we leave any value already existed
                pushList.add(i, 1.0);
                pullList.add(i, 1.0);
            }

        }else{            
            if(AtAObj[1].NumDataValues()>0){       //push            
                if(!newSite){
                    pushList.remove(i);
                }
                pushList.add(i,AtAObj[1].Mean());

            }else{
                if(newSite){ //we leave any value already existed
                    pushList.add(i, 1.0);
                }                    
            }

            if(AtAObj[0].NumDataValues()>0){//pull

                if(!newSite){
                    pullList.remove(i);
                }
                pullList.add(i,AtAObj[0].Mean());

            }else{
                if(newSite){ //we leave any value already existed
                    pullList.add(i, 1.0);
                }
            }                         
        }
    }
    
    
    /**
     * Return the number of machines and workers per site
     * @param MapTask Map task:slot(worker)
     * @return HashMap site:{"type/name:numWorkers;...","task|task|task"}, site:{"type/name:numWorkers;..","task|task"}
     */
    protected HashMap <String,List<String>> getFinalMap(HashMap <Integer,WorkerForScheduler> MapTask,String stageId){
        //FROM HERE IT WILL BE A NEW METHOD
        //Store the number of workers of each machine type. 
        // In the case of a cloud or osg resource, it does not consider the maximum 
        // number of workers per machine/type
        //site:{vmtype:num,vmtype:num},... or site:{machine:num,machine:num},... or site:{osgtype:num,osgtype:num},... 
        //then look how many workers per osgtype/vmtype/machine and get total VMs, jobs or machines
        HashMap <String, HashMap<String,Integer>>allocatedResources=new HashMap();
        //control used slots to identify the unique ones
        // site-name-machineindex-workerid
        List <String> usedSlots=new ArrayList();
        
        //site:{task,task},site:{task,task}
        HashMap <String, List<String>> siteTasks=new HashMap();
        
        //go over MapTask and identify how many workers of each type we have.
        for(int taskid:MapTask.keySet()){
            WorkerForScheduler slot=MapTask.get(taskid);

            //store the tasks of each site
            List tempST=siteTasks.get(slot.getSiteAddress());
            if(tempST==null){
                tempST=new ArrayList();
                siteTasks.put(slot.getSiteAddress(), tempST);
            }
            tempST.add(String.valueOf(taskid));
            
            HashMap <String,Integer>nodes=allocatedResources.get(slot.getSiteAddress());
            if(nodes==null){
                nodes=new HashMap();
                nodes.put(slot.getType_name(),1);
                usedSlots.add(slot.getSiteAddress()+"-"+slot.getType_name()+"-"+slot.getMachine_index()+"-"+slot.getWorker_index());                
                allocatedResources.put(slot.getSiteAddress(), nodes);
            }else{
                if(!usedSlots.contains(slot.getSiteAddress()+"-"+slot.getType_name()+"-"+slot.getMachine_index()+"-"+slot.getWorker_index())){
                    
                    usedSlots.add(slot.getSiteAddress()+"-"+slot.getType_name()+"-"+slot.getMachine_index()+"-"+slot.getWorker_index());
                    
                    Integer val=nodes.get(slot.getType_name());
                    if(val==null){
                        nodes.put(slot.getType_name(),1);
                    }else{
                        //identify if it is the same
                        nodes.put(slot.getType_name(),(val.intValue()+1));
                    }
                }
            }
        }
        
        System.out.println("CWDEBUG: FINALMAP autonomicscheduler");
        String finalmapS="";
        //consolidate to identify the number of VM considering the max number of workers per type
        // to have a string per site "type/name:numWorkers" and another one with the tasks "task|task|task"
        //finalMap- HashMap site:{"type/name:numWorkers;...","task|task|task"}, site:{"type/name:numWorkers;..","task|task"}
        HashMap <String,List<String>> finalMap=new HashMap();
        for(String site:allocatedResources.keySet()){
            //to store "type/name:numWorkers;..."
            String typeNumWorkers="";
            
            finalmapS+=site+"=";
            
            HashMap <String,Integer>nodes=allocatedResources.get(site);
            Resource res=globalResources.get(site);
            if(res.getClass().getName().equals(ResourceCloud.class.getName())||res.getClass().getName().equals(ResourceOSG.class.getName())){
                HashMap nodelist=res.getNodeList();
                for(String nodetype:nodes.keySet()){
                    int maxWorkersType=((Node)nodelist.get(nodetype)).getTotalWorkers();
                    int workersNeeded=nodes.get(nodetype);
                    System.out.println("CWDEBUG: autonomicscheduler.getFinalMap Site:"+site+" "+nodetype+":"+workersNeeded);
                    while(workersNeeded>0){
                        if(workersNeeded>maxWorkersType){
                            typeNumWorkers+=nodetype+":"+maxWorkersType+";";
                            workersNeeded-=maxWorkersType;
                        }else{
                            typeNumWorkers+=nodetype+":"+workersNeeded+";";
                            workersNeeded=0;
                        }
                    }                    
                }
                
            } else if(res.getClass().getName().equals(ResourceCluster.class.getName())){  
                //it is consolidated, list ready to send
                for(String nodetype:nodes.keySet()){
                    typeNumWorkers+=nodetype+":"+nodes.get(nodetype)+";";
                }
            }
            typeNumWorkers=typeNumWorkers.substring(0,typeNumWorkers.length()-1);//remove extra ;
            
            finalmapS+=typeNumWorkers;
            
            List tempFM=new ArrayList();
            tempFM.add(typeNumWorkers);
            String tasks=StringUtils.join(siteTasks.get(site).toArray(), "|");
            tempFM.add(tasks);
            finalMap.put(site, tempFM);  
            System.out.println("Site:"+site+" TypeNumWorkers:"+typeNumWorkers+" Tasks:"+tasks);
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "Site:"+site+" TypeNumWorkers:"+typeNumWorkers+" Tasks:"+tasks);
            System.out.println("Site:"+site+" TasksNumber:"+siteTasks.get(site).size());
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "Site:"+site+" TasksNumber:"+siteTasks.get(site).size());
        }
        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "ALLOCATED "+stageId+"-"+finalmapS);
        return finalMap;
    }
    
    /**
     * Tell each agent resources that need to provision
     * @param wflId
     * @param stage
     * @param properties
     * @param finalMap
     * @param refBenchScore
     * @return list of sites where failed allocation
     */
    protected List communicateAgentsScheduling(String wflId, WorkflowStage stage, 
            String properties, HashMap <String,List<String>> finalMap, long timestamp, double refBenchScore){
                    
        List failedAllocations=new ArrayList();
        for(String site:finalMap.keySet()){
            List <Integer> resList=WkfStageAgent.get(wflId+"."+stage);
            if(resList==null){
                resList=new ArrayList();
                resList.add(AgentList.indexOf(site));
                WkfStageAgent.put(wflId+"."+stage, resList);
            }else{
                if(!resList.contains(AgentList.indexOf(site))){//we do not add it again when rescheduling
                    resList.add(AgentList.indexOf(site));
                }
            }
            
            List params=new ArrayList();
            if(!finalMap.get(site).get(1).equals("null")){
                params.add("scheduleStage");
                params.add(wflId);                    
                params.add(stage.getId());                
                params.add(finalMap.get(site).get(0));//machine:Numworkers
                params.add(finalMap.get(site).get(1));//task list
                params.add(properties);
                params.add(site);                
                params.add(stage.getApplication());
                params.add(timestamp);
                params.add(refBenchScore);
                params.add(filesForSite(stage,finalMap.get(site).get(1))); //obtain list of input files (stage,task list)
                
                String []agentParts=site.split(":");
                //send info to the agent. 
                System.out.println("CWDEBUG: contacting with:"+site);
                System.out.println("CWDEBUG: workers - "+finalMap.get(site).get(0));
                System.out.println("CWDEBUG: task - "+finalMap.get(site).get(1));
                boolean found=sendInfo(agentParts[0],Integer.parseInt(agentParts[1]), "scheduleStage", params);   
                System.out.println("CWDEBUG: resources allocated successfully? = "+found);            
            }else{//we release all from that stage
                params.add("UpdateInfo");
                params.add("removeAll");
                params.add(wflId+"."+stage.getId());
                params.add(site);
                String []agentParts=site.split(":");
                //send info to the agent. 
                System.out.println("CWDEBUG: contacting with:"+site);
                System.out.println("CWDEBUG: removeALL:"+site+":"+System.currentTimeMillis());
                boolean found=sendInfo(agentParts[0],Integer.parseInt(agentParts[1]), "RemoveStage", params);   
                System.out.println("CWDEBUG: resources allocated successfully? = "+found);            
            }
            
            //TODO: create list with all calls that failed then check if agent is up, request updated resources and reschedule
            
        }
        return failedAllocations;
    }
    
    
    /**
     * Get list of files that a site will need
     * @param stage
     * @param tasks List of tasks in the format: "taskid|taskid|taskid"
     * @return String in the format: "sierra.futuregrid.org:/file/path/,sierra.futuregrid.org:/file/path/|file1,file2" two positional lists
     */
    protected String filesForSite(WorkflowStage stage, String tasks){
        HashMap <Integer,TaskProperties>tasksP=stage.getTaskProp();
        
        String locationList="";
        String fileNameList="";
        int taskid;        
        String [] taskidS=tasks.split("\\|");
        for(int i=0;i<taskidS.length;i++){
            if(!taskidS[i].trim().equals("")){
                taskid=Integer.parseInt(taskidS[i].trim());                
                List <FileProperties>inputFiles=tasksP.get(taskid).getInputs();
                for(FileProperties fp:inputFiles){
                    locationList+=fp.getLocation()+",";
                    fileNameList+=fp.getName()+",";
                }
            }
        }
        if(!locationList.isEmpty() && !fileNameList.isEmpty()){
            locationList=locationList.substring(0, locationList.length()-1);//remove last ,
            fileNameList=fileNameList.substring(0, fileNameList.length()-1);//remove last ,
        }
        
        return locationList+"|"+fileNameList;
    }
    
    
    //assign two workers to each stage from an Agent selected in RoundRobin
    //all workers are from the same site
    protected boolean RoundRobin(String wflId, WorkflowStage stage, String properties){
        int resource=-1;
        boolean status=false;
        int numWorkers=2;
        String agent;
        boolean found=false;
        
        if (!AgentList.isEmpty()){
            //save initial index to stop in case we do not found resources
            int initialIndex=nextResourceIndex;
            do{
                agent=AgentList.get(nextResourceIndex);
                Resource res=globalResources.get(agent);

                //we could check if the site can execute the application of this stage

                //check if it is possible to allocate the number of workers we want  
                System.out.println("CWDEBUG: checking if it is possible to allocate stage "+agent);
                if(res.checkAllocatePossibleStage(numWorkers, properties)){            
                    found=true;
                    resource=nextResourceIndex;
                }else{
                    System.out.println("CWDEBUG: it is not possible to allocate stage in "+agent);
                }


                if(nextResourceIndex+1<AgentList.size()){
                    nextResourceIndex++;
                }else{
                    nextResourceIndex=0;
                }

                //Call agent to communicate decision
                if(found){                    
                    List <Integer>tempR=new ArrayList();
                    tempR.add(resource);
                    WkfStageAgent.put(wflId+"."+stage, tempR);
                    System.out.println("CWDEBUG: communicating with agent "+agent);
                    //send instruction to agents to start machine, and start worker.
                    String []agentParts=AgentList.get(resource).split(":");                         
                    List params=new ArrayList();
                    params.add("scheduleStage");
                    params.add(wflId);                    
                    params.add(stage.getId());
                    String str="";
                    for(int i=0;i<numWorkers;i++){
                        str+="dummy:1;";
                    }
                    str=str.substring(0, str.length()-1);                    
                    params.add(str);
                    params.add("*"); //tasks for query
                    params.add(properties);
                    params.add(AgentList.get(resource));//site address
                    params.add(stage.getApplication());
                    params.add(System.currentTimeMillis());//timestamp
                    params.add("|"); // list of input files (stage,task list)
                    
                    //send info to the agent. if false, then we try next agent
                    found=sendInfo(agentParts[0],Integer.parseInt(agentParts[1]), "scheduleStage", params);   
                    System.out.println("CWDEBUG: task scheduled successfully "+found);
                }

            }while(!found && initialIndex!=nextResourceIndex);
        }else{
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "No agents registered.");
        }
        if(!found){ //because we can have an agent registered but then drop
            resource=-1;
            status=false;
        }else{
            System.out.println("CWDEBUG: stage scheduled to agent "+AgentList.get(resource));
            status=true;
        }
        
        return status;
    }
    
    public boolean sendInfo(String address, int port, String command, List param){
        boolean status=true;
        DataInputStream in=null;
        DataOutputStream out=null;
        try {
            Object []stream;
            if (command.equals("CheckStatus")){
                boolean skiplog=true; //do not print errors when we checking for status
                stream=CommonUtils.getOutputInput(address,port,skiplog);
            }else{
                stream=CommonUtils.getOutputInput(address,port);
            }
            in=(DataInputStream)stream[0];
            out=(DataOutputStream)stream[1];
            if (in!=null&&out!=null){                
                if(command.equals("scheduleStage")){   
                    out.writeUTF((String)param.get(0));
                    out.writeUTF((String)param.get(1));
                    out.writeUTF((String)param.get(2));
                    out.writeUTF((String)param.get(3));
                    out.writeUTF((String)param.get(4));
                    out.writeUTF((String)param.get(5));
                    //we skip the site (6)
                    out.writeUTF((String)param.get(7));
                    out.writeLong((Long)param.get(8));
                    out.writeDouble((Double)param.get(9));
                    //sending list of input files (stage,task list). It can be long
                    String str="";
                    if(param.get(10)!=null){
                        str=(String)param.get(10);                        
                    }else{
                        str="|";
                    }                    
                    byte[] data=str.getBytes("UTF-8");
                    out.writeInt(data.length);
                    out.write(data);
                    
                    //wait for Ok and new site status Object (by replacing that both should be sync) of ERROR
                    String answer=in.readUTF(); //read answer
                    if(answer.equals("OK")){//replace the status of the site
                        int length = in.readInt(); //data length
                        byte[] bytesdata = new byte[length];                         
                        in.readFully(bytesdata);                                               
                        Resource receivedObject=null;
                        try {
                            receivedObject = (Resource)programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }                        
                        globalResources.put((String)param.get(6), receivedObject);
                        String more=in.readUTF();
                        String tasks="";
                        if(more.equals("tasks")){
                            tasks=in.readUTF();
                            askToReinsertTasks(tasks);
                        }
                    }else if(answer.equals("Unchanged")){//no resources provision
                        status=true;
                    }else{
                        status=false;
                    }   
                } else if(command.equals("RemoveStage")){
                    out.writeUTF((String)param.get(0));
                    out.writeUTF((String)param.get(1));
                    out.writeUTF((String)param.get(2));
                    
                    //receive resources object
                    int length = in.readInt(); //data length
                    byte[] bytesdata = new byte[length];                         
                    in.readFully(bytesdata);
                    Resource receivedObject=null;
                    try {
                        receivedObject = (Resource)programming5.io.Serializer.deserialize(bytesdata);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String agent=(String)param.get(3);
                    globalResources.put(agent, receivedObject);
                } else if (command.equals("CheckStatus")){ //check status of an agent/site
                    out.writeUTF("CheckStatus");
                    String agentStatus=in.readUTF();
                    if(agentStatus.equals("OK")){
                        status=true;
                    }else{
                        status=false;
                    }
                } else if (command.equals("SendAllInfoIfRegisteredToMe")){
                    out.writeUTF("SendAllInfoIfRegisteredToMe");
                    out.writeUTF(this.schedulerAddress);
                    out.writeInt(this.schedulerPort);
                }
            }else{
                status=false;
            }
        } catch (IOException ex) {
            if(!command.equals("CheckStatus")){ //ignore exceptions when checking status agent
                Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            status=false;
        } finally{            
            closeInOut(in,out);
        }    
        return status;
    }
    
    private void askToReinsertTasks(String tasks) throws IOException{
        DataOutputStream out=CommonUtils.getOutput(workflowMasterAddress,workflowMasterPort);    
        if(out!=null){
            out.writeUTF("reinsertTasks");
            out.writeUTF(tasks);        
            closeInOut(null,out);
        }else{
            Logger.getLogger(WorkflowManager.class.getName()).log(Level.SEVERE, "Could not contact TaskManager to reinsert tasks");
        }
            
    }
    
    /**
     * Calculate network speed between agents. Bandwidth when site A pulls a file from site B. 
     * 
     * This method is called when a new site is added
     */
    public void calculateNetworkSpeed(String site){
                
        int siteIndex=AgentList.indexOf(site);
        String agentAddress=AgentList.get(siteIndex).split(":")[0];

        List tempPush=new ArrayList();
        List tempPull=new ArrayList();
        
        boolean loadedNetwork=false;
        if(globalResources.get(site).isUseMeticsService()){
            //load real data
            loadedNetwork=loadNetworkMetrics(site);    
            
        }
        if(!loadedNetwork){
            //load static data. 
            
            boolean newSite=false;
            if(siteIndex<AgentToAgentNetworkPull.size()){
                AgentToAgentNetworkPull.remove(siteIndex);
            }else{
                newSite=true;
            }
            
            if(siteIndex<AgentToAgentNetworkPush.size()){
                AgentToAgentNetworkPush.remove(siteIndex);
            } 
            if(siteIndex<AgentToMachine.size()){
                AgentToMachine.remove(siteIndex);
            }       
            
            if (agentAddress.equals("spring.rutgers.edu")){//spring
                Double []localPushPull=new Double[2];
                localPushPull[0]=18D;
                localPushPull[1]=18D;    
                
                AgentToMachine.add(siteIndex,localPushPull);

                for (int i=0;i<AgentList.size();i++){                    
                    if(AgentList.get(i).startsWith("login1.futuregrid.tacc.utexas.edu")){
                        tempPull.add(19.0); //pull from alamo
                        tempPush.add(15.0); //push to alamo
                    }else if (AgentList.get(i).startsWith("sierra.futuregrid.org")){
                        tempPull.add(15.0); //pull from sierra
                        tempPush.add(0.9); //push to sierra
                    }else if (AgentList.get(i).startsWith("login2.uc.futuregrid.org")){
                        tempPull.add(18.0); //pull from hotel
                        tempPush.add(18.0); //push to hotel
                    }else if (AgentList.get(i).startsWith("india.futuregrid.org")){
                        tempPull.add(18.0); //pull from india
                        tempPush.add(2.0); //push to india
                    }else if (AgentList.get(i).startsWith("spring.rutgers.edu")){
                        tempPull.add(0.0); //itself
                        tempPush.add(0.0); //itself
                    }else{
                        tempPull.add(1.0); //unknown
                        tempPush.add(1.0); //unknown
                    }           
                } 
            }else{//unknown
                Double []localPushPull=new Double[2];
                localPushPull[0]=1D;
                localPushPull[1]=1D;                 
                AgentToMachine.add(siteIndex,localPushPull);
                for (int i=0;i<AgentList.size();i++){
                    if (i==siteIndex){
                        tempPull.add(0.0); //itself
                        tempPush.add(0.0); //itself
                    }else{
                        tempPull.add(1.0); //unknown
                        tempPush.add(1.0); //unknown
                    }
                }
            }
            AgentToAgentNetworkPush.add(siteIndex,tempPush);    
            AgentToAgentNetworkPull.add(siteIndex,tempPull);
            
            if(newSite){//add element to each list bc is new
                for(int i=0;i<AgentToAgentNetworkPush.size() && i<AgentToAgentNetworkPull.size();i++){//both lists should have same length
                    if(i==siteIndex){
                        continue;
                    }
                    AgentToAgentNetworkPush.get(i).add(1.0);
                    AgentToAgentNetworkPull.get(i).add(1.0);
                }
            }        
        }
                
        printNetworkInfo();
        
        
        
    }
    
    private void printNetworkInfo(){
        System.out.println("CWDEBUG:autonomicscheduler: Network Info");
        for(int i=0;i<AgentToAgentNetworkPull.size();i++){
            List<Double> pull=AgentToAgentNetworkPull.get(i);
            List<Double> push=AgentToAgentNetworkPush.get(i);
            
            System.out.println(AgentList.get(i));
            
            for(int j=0;j<pull.size();j++){                
                System.out.println("    "+AgentList.get(j)+" "+pull.get(j) +"/"+push.get(j)+" (pull/push)");
            }
            System.out.println("     local: "+AgentToMachine.get(i)[0]+"/"+AgentToMachine.get(i)[1]+" (pull/push)");
        }
    }
    

    private void printAllAccounting(){//parameter is useful only when non-blocking stages involved
        //{wkf.stage:{site:{nodeacct, nodeacct},...},...}
        //HashMap <String,HashMap<String,List<NodeAcct>>> accountingGlobal;
        String toAdd="";
        System.out.println("CWDEBUG: autonomicscheduler. CURRENT ACCOUNTING");
        for(String wkfstage:accountingGlobal.keySet()){
            
            if(NBstagesDoneAfter.containsKey(wkfstage)){
                toAdd=NBstagesDoneAfter.get(wkfstage);
            }else{
                toAdd="";
            }
            
            System.out.println("  stage: "+ wkfstage+""+toAdd);
            HashMap<String,List<NodeAcct>> stageAcct=accountingGlobal.get(wkfstage);
            for(String site:stageAcct.keySet()){
                System.out.println("    site: "+ site);
                for(NodeAcct naObj:stageAcct.get(site)){
                    System.out.println("      "+ naObj.toString());
                }
            }
        }
        
        
    }
    
    protected void retrieveAVGMetrics(String site){
        //System.out.println("CWDEBUG.autonomicscheduler retrieveAVGMetrics for "+site);
        Object []stream=CommonUtils.getOutputInput(globalResources.get(site).getAgentMetricsAddress(), globalResources.get(site).getAgentMetricsPort());
        DataInputStream in=(DataInputStream)stream[0];
        DataOutputStream out=(DataOutputStream)stream[1];

        //TODO IMPROVE: use timestamps to only retrieve data that has changed since last asked. Construct RunningAvgMetrics using individual objs.

        if(out==null){
            return;
        }
        
        RunningAvgMetrics ravgm;
        try {
            out.writeUTF("getAllAVGs");
            out.writeUTF(globalResources.get(site).getDbName());        
            
            Object obj= CommonUtils.readObject(in);
            
            if(obj==null){
                System.out.println("CWDEBUG.autonomicscheduler retrieveAVGMetrics Object is NULL ");
                Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.WARNING, "CWDEBUG.autonomicscheduler retrieveAVGMetrics Object for site "+site+" is NULL ");
            }else{
                ravgm = (RunningAvgMetrics)obj;
                globalMetrics.put(site,ravgm);
            }
                        
            
        } catch (IOException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        }catch (java.lang.NullPointerException ex){
            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            closeInOut(in,out);
        }
        //System.out.println("CWDEBUG.autonomicscheduler end retrieveAVGMetrics for "+site);
    }
    
    private void closeInOut(DataInputStream in, DataOutputStream out){
        if(in!=null){
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(out!=null){
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    //Possible Commands to sync info
    //new:wkflId,stageId,numWorkers
    //replace:wkflId,stageIdNew,stageIdOld
    //add:wkflId,stageId,ip1;ip2;ip3
    //removeAll:wkflId,stageId
    //removeAll:wkflId
        
    //check status of resources...
    private class Monitor implements Runnable{
        boolean active=true;
        
        @Override
        public void run(){
            
            //Although taskhandler can modify it in agentlite, it should be ok to read it.
            //syncronize operations with resources because it can be modified by another thread
   
            if(monitorInterval==0){
                return;
            }
            
            System.out.println("Central Resource Manager Monitor starts");
            while(active) {
                try {
                    Thread.sleep(monitorInterval*1000);
                } catch (InterruptedException ex) {
                    if (active){
                        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }           
                
                //Check agents status
                // if agent is down, update and reschedule
                // if no resources add to pending(it should be like this already)
                List <String>agentsDown=new ArrayList();
                for(int i=0; i< AgentList.size();i++){
                    //check status with timeout, if it is in globalresources then normal. if it is not in globalresoruces and it is active we ask for allinfo object
                    
                    String[] agentParts=AgentList.get(i).split(":");
                    
                    if(!sendInfo(agentParts[0],Integer.parseInt(agentParts[1]),"CheckStatus",new ArrayList())){
                        
                        if(globalResources.containsKey(AgentList.get(i))){
                            System.out.println("AutonomicScheduler.Monitor - Agent Down:"+AgentList.get(i));
                            Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.INFO, "AutonomicScheduler.Monitor - Agent Down:"+AgentList.get(i));
                            agentsDown.add(AgentList.get(i));
                        }
                    }else{
                        if(!globalResources.containsKey(AgentList.get(i))){//if an agent that was down, recovers, then we ask the agent to send us updated info.
                            //TODO: keep a list of down agents and affected workflows so we can tell an agent to 
                                //release those resources (e.g., if it was a simple network problem, but the agent still has allocated resources)
                            sendInfo(agentParts[0],Integer.parseInt(agentParts[1]),"SendAllInfoIfRegisteredToMe",new ArrayList());
                        }
                    }
                    
                    
                }
                if(!agentsDown.isEmpty()){
                    try {
                        mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.                   
                    
                    
                    //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp:0_CommaSeparated","wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp:-1_CommaSeparated",...}
                    // 0 in the accounting part means that we need to stop the clock and -1 means that there was an error in provisioning and we remove this from accounting
                        HashMap <String, String> wflStage_workerTasks=dealDownAgents(agentsDown);
                    
                        if(!wflStage_workerTasks.isEmpty()){
                            //call reschedule
                            rescheduleStages(wflStage_workerTasks);
                        }
                        
                        agentsDown.clear();
                        
                    } catch (IOException ex) {
                        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                    } finally{
                        mutex.release();
                    }
                }
                                
                
                //update Metrics
                try {
                    mutex.acquire();//globalResources, stagesFinalMaps, and WkfStageAgent are part of the critical section.
                    
                    //reset the default AppExec and 
                    defaultAppExec=new HashMap<>();
                    
                    for(String site:globalResources.keySet()){
                        if(globalResources.get(site).isUseMeticsService()){
                            //retrieve remote metrics
                            retrieveAVGMetrics(site);
                            
                            //calculate avg exec time apps, if data avail. {appname:avgTime,appname:avgTime}. Time is relative to referenceBenchScore
                            addMetricsToGeneralAvg(site);
            
                        }
                    }
                
                } catch (InterruptedException ex){
                    Logger.getLogger(AutonomicScheduler.class.getName()).log(Level.SEVERE, null, ex);
                }finally{
                    mutex.release();
                }
                
                
                    
            }

            System.out.println("Central Resource Manager Monitor terminates");
        }
        
        private HashMap <String,String> dealDownAgents(List <String>agentsDown){
            System.out.println("AutonomicScheduler.dealDownAgents");
            
            //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp:0_CommaSeparated","wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp:-1_CommaSeparated",...}
            // 0 in the accounting part means that we need to stop the clock and -1 means that there was an error in provisioning and we remove this from accounting
            HashMap <String, String> wflStage_workerTasks=new HashMap();
            
            //stagesFinalMaps --> wkf.stage:{site:{"worker:1;worker:1;...","taskid|taskid|..."},...}  
            //     HashMap <String,HashMap<String,List<String>>>stagesFinalMaps;
            
            HashMap <String, List<String>> wflStage_SitesDown=new HashMap();
            
            for(String stagesFinalMapsKey:stagesFinalMaps.keySet()){
                Set agentSetStage =stagesFinalMaps.get(stagesFinalMapsKey).keySet(); //agents involved in this stage
                //do intersection
                List<String> agentsDownClone=new ArrayList(agentsDown);
                agentsDownClone.retainAll(agentSetStage);
                
                if(!agentsDownClone.isEmpty()){
                    //create {"wkf.stage":{site,site},..}
                    wflStage_SitesDown.put(stagesFinalMapsKey, agentsDownClone);
                    
                        
                    //create wflStage_workerTasks (actually we only need the tasks                    
                    List <String>siteInfo;
                    String workersCommaSeparated="";
                    String tasksCommaSeparated="";
                    String workersTimeStamp="";
                    for(String site:agentsDownClone){
                        siteInfo=stagesFinalMaps.get(stagesFinalMapsKey).get(site);
                        if(!workersCommaSeparated.isEmpty()){
                            workersCommaSeparated+=",";
                        }
                        workersCommaSeparated+=StringUtils.join(siteInfo.get(0).split(";"),",");
                        if(!tasksCommaSeparated.isEmpty()){
                            tasksCommaSeparated+=",";
                        }
                        tasksCommaSeparated+=StringUtils.join(siteInfo.get(1).split("\\|"),",");                        
                    }
                    workersTimeStamp="notNeeded";//I do not have STARTTIME ??
                    wflStage_workerTasks.put(stagesFinalMapsKey, workersCommaSeparated+";"+tasksCommaSeparated+";"+workersTimeStamp);
                    
                    
                    //remove agents from the list of agents where a workflow is allocated 
                    List<Integer> workersIndexes=WkfStageAgent.get(stagesFinalMapsKey); //HashMap <String,List<Integer>>
                    for(String agentD:agentsDownClone){
                        if(workersIndexes.contains(AgentList.indexOf(agentD))){
                            workersIndexes.remove(new Integer(AgentList.indexOf(agentD)));
                        }
                    }
                    
                }
                        
            }
            
            //call new clean stage that do per site instead of per worker
            cleanStagesFinalMaps(wflStage_SitesDown);
            
                       
            //Remove sites from globalResources
            for(String site:agentsDown){
                globalResources.remove(site);
            }
            
            
            return wflStage_workerTasks;
        }
        
                
                //remove agent from WkfStageAgent  HashMap <String,List<Integer>> WkfStageAgent; //this is used to store where a wkf.stage 
        
                
        public void setActive(boolean active) {
            this.active = active;
        }
    }
}
