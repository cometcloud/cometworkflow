/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import tassl.application.utils.RunningStat;

/**
 * This class is used by the scheduling algorithms to know how many workers we have.
 * A single machine can have multiple workers and therefore are considered as two slots in that machine.
 * 
 * @author Javier Diaz-Montes
 */
public class WorkerForScheduler {
    
    private String siteAddress; //address:port of the site
    private String siteZone; //zone where the site is located
    private String siteIdentifier; //identifier site (siteName)
    private String type_name;//type of VM or name machine
    private int machine_index;//identify the machine when several of the same name(i.e. VMs)
    private int worker_index; //identify the worker inside machine
    private int machine_indexType; //indentify the position in the globalSlots List
    private double readyToExecute; //expected time that this slot will be able to execute    
    private double failureRateWorker; //failure rate due to a worker failure
    private double failureRateMachine; // failure rate due to a machine failure
    private double cost=0;
    private double overhead; //overhead because of starting VM or waitting queue
    private double costTasks=0;
    private double assignHours=0;
    private ArrayList<Integer> listOfTasks = new ArrayList<Integer>(); // saves the task id of all tasks assigned to this slot
    
    
    private double performance;   //performance (Speedup)
    private HashMap<String,RunningStat> performanceAppReal;   //app:time
    
    private HashMap<String,RunningStat>defaultAppExec;//{appname:time,appname:time}
    private double defaultReferenceBenchmark; //default benchmark score using in defaultAppExec
    private double benchScore; //benchmark score of this machine
    
    public WorkerForScheduler(String siteAddress, String siteIdentifier, String zone, 
            String type_name, int machine_index, int worker_index, 
            double readyToExecute, double overhead, HashMap<String,RunningStat>defaultAppExec,double defaultReferenceBenchmark, double benchScore) {
        this.siteAddress = siteAddress;
        this.siteIdentifier = siteIdentifier;
        this.siteZone=zone;
        this.type_name = type_name;
        this.machine_index = machine_index;
        this.worker_index = worker_index;
        this.readyToExecute = readyToExecute;
        this.overhead=overhead;
        this.performanceAppReal=new HashMap();
        this.failureRateWorker=0.0;
        this.failureRateMachine=0.0;
        this.defaultAppExec=defaultAppExec;
        this.defaultReferenceBenchmark=defaultReferenceBenchmark;
        this.benchScore=benchScore;
    }

    //TODO: function to ease getting performance by auto getting performance or performanceAppReal
    
    public HashMap<String, RunningStat> getPerformanceAppReal() {
        return performanceAppReal;
    }
    
    public void setPerformanceAppReal(HashMap<String, RunningStat> performanceAppReal) {
        System.out.println("CWDEBUG.workerforscheduler Set Real Performance for "+type_name);
        this.performanceAppReal = performanceAppReal;
    }

    public void setFailureRateMachine(double failureRate) {
        this.failureRateMachine = failureRate;
    }

    public double getFailureRateMachine() {
        return failureRateMachine;
    }
    
    public void setFailureRateWorker(double failureRate) {
        this.failureRateWorker = failureRate;
    }

    public double getFailureRateWorker() {
        return failureRateWorker;
    }
    
    public void setTime(int time){
        readyToExecute=time;
    }
    public boolean listOfTaskIsEmpty(){
        return listOfTasks.isEmpty();
    }
    public void AddTask(Integer task){
        listOfTasks.add(task);
    }
    
    public Integer getTask(int index){
        return listOfTasks.get(index);
        
    }
    
    public void RemoveTask(Integer taskId){
        for(int i=0;i<listOfTasks.size();i++){
            if(taskId.equals(listOfTasks.get(i)))
                listOfTasks.remove(i);
        }
       
    }

    public double getOverhead() {
        return overhead;
    }

    /**
     * Return speedup of slot
     * 
     * Use getExecTime(appName,time) instead
     * 
     * @return
     * @deprecated
     */
    @Deprecated
    public double getPerformance() {
        return performance;
    }

    
    public double getExecTime(String appName,double time){
        
        //////////////////
        double real=-1;
        if(performanceAppReal.containsKey(appName)){
            real=performanceAppReal.get(appName).Mean();
        }
        double defaultV=-1;
        if(defaultAppExec.containsKey(appName)){
            defaultV=defaultAppExec.get(appName).Mean()/(benchScore/defaultReferenceBenchmark);
            
        }
        System.out.println("CWDEBUG.workerforscheduler "+type_name+"  AppTime="+time+" Real="+real+" Default="+defaultV+" Fixed="+(time/this.performance));
        
        
        /////////
        
        if(performanceAppReal.containsKey(appName)){ //first check real Time
            return performanceAppReal.get(appName).Mean();
            
        }else if(defaultAppExec.containsKey(appName)){ //next check real Time as a function of benchmark score (no real data for this machine)
            double defaultValue=defaultAppExec.get(appName).Mean();//value using defaultReferenceBenchmark
            
            return defaultValue/(benchScore/defaultReferenceBenchmark);
            
        }else{ //else use speedup. Assuming that variable time is as a function of the reference benchmark score used to calculate performance of this machine
            return time/this.performance;
        }        
        
    }
    
    
    public double getCost() {
        return cost;
    }
    public int getListTasksSize(){
        return listOfTasks.size();
    }
    
    public ArrayList getListTasks(){
        return listOfTasks;
    }
     public double getTaskCost() {
        return costTasks;
    }
    public double getAssignHours() {
        return assignHours;
    }
    public String getSiteAddress() {
        return siteAddress;
    }

    public String getSiteIdentifier() {
        return siteIdentifier;
    }

    public String getSiteZone() {
        return siteZone;
    }


    public double getReadyToExecute() {
        return readyToExecute;
    }

    public String getType_name() {
        return type_name;
    }

    public int getMachine_index() {
        return machine_index;
    }
     public int getMachine_indexType() {
        return this.machine_indexType;
    }

    public int getWorker_index() {
        return worker_index;
    }

    public void setOverhead(double overhead) {
        
        if(this.readyToExecute==this.overhead){
            this.readyToExecute=overhead;
        }
        
        this.overhead = overhead;
    }

    public void setCostTasks(double costTasks) {
        this.costTasks = costTasks;
    }

    public void setSiteAddress(String siteAddress) {
        this.siteAddress = siteAddress;
    }

    public void setSiteIdentifier(String siteIdentifier) {
        this.siteIdentifier = siteIdentifier;
    }

    public void setSiteZone(String siteZone) {
        this.siteZone = siteZone;
    }
    public void setMachineTypeIndex(int machineIdexType) {
        this.machine_indexType = machineIdexType;
    }

    public void setReadyToExecute(double readyToExecute) {
        this.readyToExecute = readyToExecute;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public void setMachine_index(int machine_index) {
        this.machine_index = machine_index;
    }

    public void setWorker_index(int worker_index) {
        this.worker_index = worker_index;
    }

    public void setPerformance(double performance) {
        this.performance = performance;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    
    public void addReadyToExecute(double value){
        this.readyToExecute+=value;
    }
    public void addCost(double value){
        this.costTasks=value;
    }
    public void addHours(double value){
        this.assignHours+=value;
    }
    public void setHours(double hour){
        this.assignHours=hour;
    }
     public void removeTime(double value){
        this.readyToExecute=this.readyToExecute-value;
    }
    
    @Override
    public String toString(){
        return "The site is: "+ siteAddress+"/" + siteIdentifier + ". Machine:"+type_name+" "+
                machine_index+"-"+worker_index+" ReadyToExecute:"+readyToExecute+" Cost:"+cost 
                +" performanceAppReal="+performanceAppReal +" defaultAppExec="+defaultAppExec
                +" defaultReferenceBenchmark="+defaultReferenceBenchmark+ " benchScore=" + benchScore;            
    }
    
}
