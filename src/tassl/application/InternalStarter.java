package tassl.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import programming5.concurrent.RequestVariable;
import programming5.io.ArgHandler;
import programming5.io.Debug;
import programming5.io.FileHandler;
import programming5.io.Serializer;
import programming5.net.IncompleteResultException;
import programming5.net.MalformedMessageException;
import programming5.net.Message;
import programming5.net.MessageArrivedEvent;
import programming5.net.MessagingClient;
import programming5.net.NetworkException;
import programming5.net.PluggableClient;
import programming5.net.ServerDaemon;
import programming5.net.ServiceObject;
import programming5.net.ServiceObjectFactory;
import programming5.net.TerminationAwareSubscriber;
import tassl.application.node.DrtsPeerControlWfl;
import tassl.application.node.DrtsPeerWfl;
import tassl.automate.agent.CloudBridgingScheduler;
import tassl.automate.network.ControlConstant;
import tassl.automate.overlay.OverlayControlInterface;
import tassl.automate.overlay.OverlayControlMessage;
import tassl.automate.overlay.chord.ChordID;
import tassl.automate.util.ConfigurationFileManager;
import tassl.automate.util.ReturnObjectHandler;
import tassl.automate.util.SSLClient;
import tassl.automate.util.SSLServerDaemon;
import tassl.automate.util.programming5.TCPServerDaemon;
import tassl.automate.util.programming5.TCPclient;

/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * @author Javier Diaz-Montes,Hyunjoo
 ** InternalStarter should be called in your application starter
 * Nodes will run as NodeType described in nodeFile and exceptionFile (files with -nodeFile and -exceptionFile)
 * If you want to start them manually or dynamically regardless of pre-defined list
 * call main of this class
 */
public class InternalStarter implements ServiceObjectFactory {

    public static enum NodeType {MASTER, WORKER, REQUEST_HANDLER, WORKFLOW_MANAGER, CLOUD_AGENT};
    protected static DrtsPeerWfl peer;
    protected OverlayControlInterface<DrtsPeerWfl> controlInterface;
    protected Vector<String> nodeList;
    protected Hashtable<String, String> baseProperties;
    protected Hashtable<String, Hashtable<String, String>> nodeDescriptors;
    protected Hashtable<String, Hashtable<String, String>> exceptionTable;
    protected String localNode;
    protected ServerDaemon serverDaemon;

    public InternalStarter() {
        try {
            if (serverDaemon == null || !serverDaemon.isAlive()) {
                serverDaemon = new TCPServerDaemon(this, Integer.parseInt(System.getProperty("COMET_CONTROL_PORT",ControlConstant.COMET_CONTROL_PORT).trim()));
                serverDaemon.start();
                System.out.println("start tcpserver daemon");
            }
        } catch (NetworkException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** Creates a new instance of cometStarter */
    public InternalStarter(String nodeFileName, String portFileName, String[] appPropertyFiles, String[] exceptionFileNames, int overlayPort) {
        controlInterface = new OverlayControlInterface<DrtsPeerWfl>(overlayPort);
        nodeList = ConfigurationFileManager.generateNodeList(nodeFileName, portFileName);
        localNode = nodeList.elementAt(0);
        baseProperties = ConfigurationFileManager.parsePropertyFiles(appPropertyFiles);
        nodeDescriptors = new Hashtable<String, Hashtable<String, String>>();
        exceptionTable = ConfigurationFileManager.parseExceptionFiles(exceptionFileNames);

        String logfile=baseProperties.get("logFile");
        if (logfile==null){
            setLog("WorkflowMaster.log");
        }else{
            setLog(logfile);
        }
        
        
        try {
            if (serverDaemon == null || !serverDaemon.isAlive()) {
                String cometControlPortString=baseProperties.get("COMET_CONTROL_PORT");
                if(cometControlPortString == null || cometControlPortString.isEmpty()){
                    cometControlPortString=ControlConstant.COMET_CONTROL_PORT;
                }
                
                if(baseProperties.get("chord.NETWORK_CLASS") != null && baseProperties.get("chord.NETWORK_CLASS").equals("SSL")){                        
                        serverDaemon = new SSLServerDaemon(this, Integer.parseInt(cometControlPortString.trim()));
                        System.out.println("start sslserver daemon");
                }
                else{
                        System.out.println("COMET CONTROL PORT IS "+System.getProperty("COMET_CONTROL_PORT"));
                        serverDaemon = new TCPServerDaemon(this, Integer.parseInt(cometControlPortString.trim()));
                        System.out.println("start tcpserver daemon");
                }
                serverDaemon.start();
            }
        } catch (NetworkException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        }

        //set properties
        System.out.println("==> Node List : "+ nodeList);
        for (String node : nodeList) {
            nodeDescriptors.put(node, ConfigurationFileManager.customizeProperties(baseProperties, node, exceptionTable));
        }

        //set total num of workers
        int numNotWorker=0;
        if (exceptionTable != null) {
            for (String nodekey : exceptionTable.keySet()) {
                Hashtable<String, String> nodeExceptions = new Hashtable<String, String>();
                nodeExceptions = exceptionTable.get(nodekey);
                for (Map.Entry<String, String> exception : nodeExceptions.entrySet()) {
                if (exception.getKey().equalsIgnoreCase("comet.NodeType")
                        && InternalStarter.NodeType.valueOf(exception.getValue()) != InternalStarter.NodeType.WORKER)
                    numNotWorker++;
                }
            }
        }
        System.setProperty("WorkerNum", ""+(nodeList.size()-numNotWorker));
        
        //set role of node
        int masterSequence = 0;
        int workerSequence = 0;
        int reqHandlerSequence = 0;
        int wfManagerSequence = 0;
        int agentSequence = 0;
        for (Hashtable<String, String> propertyTable : nodeDescriptors.values()) {
            InternalStarter.NodeType peerRole;
            String nodeType = propertyTable.get("comet.NodeType");
            if (nodeType != null) {
                peerRole = InternalStarter.NodeType.valueOf(nodeType);
            }
            else {
                peerRole = InternalStarter.NodeType.WORKER;
            }
            switch (peerRole) {
                case MASTER: {
                    propertyTable.put("MasterID", Integer.toString(++masterSequence));
                }
                break;
                case WORKER: {
                    propertyTable.put("WorkerID", Integer.toString(++workerSequence));
                }
                break;
                case REQUEST_HANDLER: {
                    propertyTable.put("ReqHandlerID", Integer.toString(++reqHandlerSequence));
                }
                break;
                case WORKFLOW_MANAGER: {
                    propertyTable.put("WFManagerID", Integer.toString(++wfManagerSequence));
                }
                break;
                case CLOUD_AGENT: {
                    propertyTable.put("AgentID", Integer.toString(++agentSequence));
                }
                break;
            }
        }

        if (Debug.isEnabled("CometStarter")) {
            for (String node : nodeList) {
                Hashtable<String, String> p = nodeDescriptors.get(node);
                Debug.println("[CometStarter] "+node+": nodetype="+p.get("comet.NodeType")+" masterid="+p.get("MasterID")+" workerid="+p.get("WorkerID")+" reqHandlerid="+p.get("ReqHandlerID"), "CometStarter");
            }
        }
    }
    private void setLog(String filename){
        java.util.logging.FileHandler fh;
        try {
            fh=new java.util.logging.FileHandler(filename, true);
            
            Logger l = Logger.getLogger(""); //Everyone will print here. Create new logger if different file wanted
            
            for (Handler i:l.getHandlers()){
                l.removeHandler(i);
            }
            
            l.setUseParentHandlers(false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.ALL);  
            
        } catch (SecurityException e){  
            System.out.println("ERROR: WorkflowMaster: Setting up log");
            e.printStackTrace();
        }catch(IOException e){
            System.out.println("ERROR: WorkflowMaster: Setting up log");
            e.printStackTrace();
        }             
    }
    public void initComet() throws URISyntaxException, IncompleteResultException {
        Debug.println("[CometStarter] localNode= "+localNode, "CometStarter");
        try {
            String joinBitsProperty = baseProperties.get("mapReduce.IDFileName");
            if (joinBitsProperty != null) {
                Vector<ChordID> chordIDs = getBalancedChordIDs(joinBitsProperty);
                if (chordIDs == null) {
                    peer = controlInterface.startAsBootstrap(DrtsPeerControlWfl.class, nodeDescriptors, localNode, 300000);
                } else {
                    peer = controlInterface.startAsBootstrap(DrtsPeerControlWfl.class, nodeDescriptors, localNode, chordIDs, 300000);
                }
            }
            else {
                peer = controlInterface.startAsBootstrap(DrtsPeerControlWfl.class, nodeDescriptors, localNode, 300000);
            }
        }
        catch (InstantiationException ie) {
            System.out.println("Comet init failed");
            Debug.printStackTrace(ie);
        }
    }

    //this method cleans the Overlay once the master finishes
    public void terminateAll(){
        
        System.out.println("Terminate Comet");
        terminateComet();
       
        System.out.println("Terminate Overlay");
        terminateOverlayRemote();
        
        System.out.println("End Listeners");
        endConnectionListeners();
    }
    
    public void terminateComet() {        
        peer.terminateInstance();        
        if(serverDaemon instanceof TCPServerDaemon){            
            ((TCPServerDaemon)serverDaemon).end();            
        }else{
            ((SSLServerDaemon)serverDaemon).end();
        }
    }

    public void terminateOverlayRemote(){
        try {
            controlInterface.terminateOverlay();
        } catch (NetworkException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //this one does not terminate connections, it should not be used.
    public void terminateOverlay() {
        System.out.println("CometStarter: terminateOverlay...");
        List<String> nodeURIList = controlInterface.getNodeURIList();
        for (String node : nodeURIList) {
            try {
                System.out.println("  -> terminate overlay " + node);
                controlInterface.asyncRemoteControl(node, new ReturnObjectHandler<String>() {

                    @Override
                    public void setReturnObject(String result) {
                        try {
                            controlInterface.terminateOverlay();
                        } catch (NetworkException ne) {
                            ne.printStackTrace();
                        }
                    }

                    @Override
                    public void signalException(Exception e) {
                        Debug.printStackTrace(e, "CometStarter");
                    }
                }, "terminateInstance"); //JAVI: it was terminatingOverlay and there is not such method in drtspeer
            } catch (NetworkException ex) {
                Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void endConnectionListeners(){
        try {
            controlInterface.endConnectionListeners();
        } catch (NetworkException ex) {
            ex.printStackTrace();
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Vector<ChordID> getBalancedChordIDs(String joinBitsProperty) {
        Vector<ChordID> chordIDs = new Vector<ChordID>();

        int numTasks = 0;
        String strNumMapTasks = baseProperties.get("NumMapTasks");
        if (strNumMapTasks == null) {
            String fileName = baseProperties.get("InputDataFile");
            File f = new File(fileName);
            numTasks = f.listFiles().length;
        } else {
            numTasks = Integer.parseInt(strNumMapTasks);
        }

        BigInteger[] idMapping = new BigInteger[numTasks + 1];
        try {
            fillIDMapping(idMapping, joinBitsProperty);
//                    int nodeJoinBits = Integer.parseInt(joinBitsProperty);
            int ringBits = Integer.parseInt(baseProperties.get("chord.ID_BITS"));
//                    SimulationHelper simulationHelper = new SimulationHelper();
            BigInteger[] nodeIDs = new BigInteger[nodeList.size()];
            int idRange = numTasks / nodeList.size();
            int idIndex = idRange;
            for (int i = 0; i < nodeIDs.length; i++) {
                nodeIDs[i] = idMapping[idIndex];
                idIndex += idRange;
            }
            java.util.Arrays.sort(nodeIDs);
            for (int i = 0; i < nodeIDs.length; i++) {
                chordIDs.add(new ChordID(nodeIDs[i], ringBits));
            }
            System.out.println("chordIDs=" + chordIDs);
        } catch (IOException ioe) {
            System.out.println("Could not load ID mapping file; starting with random IDs: " + ioe.getMessage());
            return null;
        }
        return chordIDs;
    }

    public static boolean checkRequestHandler(String[] requestHandlerList) {
        boolean inReqHand = false;

        try {
            String localIPaddr = InetAddress.getLocalHost().getHostAddress();
            for (String line : requestHandlerList) {
                String lineIPaddr = InetAddress.getByName(line).getHostAddress();
                //check local host is in the list
                if (localIPaddr.equals(lineIPaddr)==true) {
                    inReqHand = true;
                    break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return inReqHand;
    }

    public void startApp() {
        //if some nodes fail to join the overlay, ignore them (not desirable)
        //nodeList = controlInterface.getRemoteNodeURIList(); //refresh nodeList

        //local node starts first
        peer.ConfigureApp();
        if (System.getProperty("ManualWorkerStart", "false").equals("true")) {
            System.out.println("Hit enter to start workers after task generation");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                in.readLine();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }

        //cloud scheduler starts when
        //this is a master described in comet.properties as a scheduler
        try {
            if (System.getProperty("MasterID") != null
                    && System.getProperty("Scheduler") != null
                    && System.getProperty("Scheduler").equals(peer.getLocalPeerID().replace("//", ""))) {
                Debug.println("[CometStarter] SchedulerClass instantiated", "CometStarter");
                Class schedulerClass = Class.forName(System.getProperty("SchedulerClass"));
                CloudBridgingScheduler sched = (CloudBridgingScheduler) schedulerClass.newInstance();
                new Thread(sched).start();
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(InternalStarter.class.getName()).log(Level.SEVERE, null, ex);
        }

        //remote nodes start
        if (nodeList != null) {
            for (String node : nodeList) {
                try {
                    if (!node.equals(localNode)) {
                        controlInterface.remoteControl(node, "ConfigureApp");
                    }
                }
                catch (IncompleteResultException ire) {
                    Debug.printStackTrace(ire);
                }
                catch (NetworkException ne) {
                    Debug.printStackTrace(ne);
                }
            }
        }
    }

    public void remoteRunMethod(String methodName, Vector<String> remoteNodeList, Serializable... parameters) {
        if (remoteNodeList != null) {
            for (String node : remoteNodeList) {
                try {
                    controlInterface.remoteControl(node, methodName, parameters);
                }
                catch (IncompleteResultException ire) {
                    Debug.printStackTrace(ire);
                }
                catch (NetworkException ne) {
                    Debug.printStackTrace(ne);
                }
            }
        }
    }

    public void fillIDMapping(BigInteger[] idMapping, String mapFileName) throws IOException {
        FileHandler mapFile = new FileHandler(mapFileName, FileHandler.HandleMode.READ);
        for (int i = 0; i < idMapping.length; i++) {
            String line = mapFile.readLine();
            if (line != null) {
                String[] entry = line.split(" ");
                idMapping[i] = new BigInteger(entry[1].trim());
            }
        }
        mapFile.close();
    }

    public DrtsPeerWfl getDrtsPeer() {
        return peer;
    }

    public Vector<String> getNodeMap() {
        return nodeList;
    }

    public ServiceObject getServiceObject() {
        return new CometControlObject();
    }

    protected class CometControlObject implements ServiceObject, TerminationAwareSubscriber<MessageArrivedEvent> {
        MessagingClient serviceConnection;
        public synchronized void signalEvent(MessageArrivedEvent event) {
            try {
                OverlayControlMessage controlMessage = new OverlayControlMessage(event.getContentBytes());
                if (controlMessage.isInitMessage()) {
                    System.out.println("Received InitMessage");
                    Debug.println("initializing "+controlMessage.getURI(), "CometControlObjcet");
                    Hashtable<String, Hashtable<String, String>> recvDescriptors = (Hashtable<String, Hashtable<String, String>>) Serializer.deserialize(controlMessage.getItemAsByteArray(1));

                    //add bootstrap properties
                    Hashtable<String, String> bootstrapProperties = new Hashtable<String, String>(baseProperties);
                    Debug.println("bootstrapProperties "+bootstrapProperties.toString(), "CometStarter");
                    recvDescriptors.put(localNode, bootstrapProperties);

                    //start overlay
                    controlInterface.startAsBootstrap(DrtsPeerControlWfl.class, recvDescriptors, localNode, 300000);
                    Debug.println("NodeURIList: "+controlInterface.getNodeURIList(), "CometControlObjcet");
                    Debug.println("RemoteNodeURIList: "+controlInterface.getRemoteNodeURIList(), "CometControlObjcet");

                    //execute application
                    remoteRunMethod("ConfigureApp", new Vector<String>(recvDescriptors.keySet()));

                } else if(controlMessage.isTerminateMessage()) {
                    System.out.println("Received TerminateMessage");
                    System.out.println("terminating URIs= "+controlMessage.getURI());
                    Vector<String> terminateURIs = (Vector<String>) Serializer.deserialize(controlMessage.getItemAsByteArray(1));
                    for (String terminateURI: terminateURIs) {
                        if (controlInterface.getRemoteNodeURIList().contains(terminateURI)) {
                            Debug.println("terminateURI="+terminateURI, "CometControlObject");
                            controlInterface.terminateInstance(terminateURI);
                        } else {
                            System.out.println(terminateURI+" is not part of the overlay");
                        }
                    }
                } else if(controlMessage.isURIMessage()) {
                    System.out.println(controlMessage.getMessageItem(0));
                    return;
                }

                //send reply
                Message reply = new Message();
                reply.setHeader(OverlayControlMessage.URI_MSG);
                reply.addMessageItem("Done");
                serviceConnection.send(reply.getMessageBytes(), controlMessage.getMessageItem(0));

            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            } catch (ClassNotFoundException ex) {
                System.err.println(ex.getMessage());
            } catch (URISyntaxException ex) {
                System.err.println(ex.getMessage());
            } catch (InstantiationException ex) {
                System.err.println(ex.getMessage());
            } catch (IncompleteResultException ex) {
                System.err.println(ex.getMessage());
            } catch (MalformedMessageException ex) {
                System.err.println(ex.getMessage());
            } catch (NetworkException ex) {
                System.err.println(ex.getMessage());
            }
        }

        public void newClient(PluggableClient client) {
            serviceConnection = (MessagingClient) client;
            serviceConnection.addListener(this);
        }

        public void noMoreEvents() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void subscriptionTerminated() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public void dynamicCometStart(String... propertyFiles) {
        peer = DrtsPeerWfl.joinOverlay(ConfigurationFileManager.parsePropertyFiles(propertyFiles));        
        startApp();
    }

    public void dynamicGroupStart(String... propertyFiles) {
        try {
            System.out.println("Dynamic group join");

            //add bootstrap properties into nodeDescriptor
            String bootstrapPeerID = baseProperties.get("chord.LOCAL_BOOTSTRAP");
            if (bootstrapPeerID==null) {
                System.err.println("chord.LOCAL_BOOTSTRAP should be set in chord.properties");
                System.exit(0);
            }
            Hashtable<String, String> bootstrapProperties = new Hashtable<String, String>(baseProperties);
            Debug.println("bootstrapProperties "+bootstrapProperties.toString(), "CometStarter");
            nodeDescriptors.put(bootstrapPeerID, bootstrapProperties);

            //start remote nodes
            controlInterface.startAsBootstrap(DrtsPeerControlWfl.class, nodeDescriptors, bootstrapPeerID, 300000);
            System.out.println("NodeURIList: "+controlInterface.getNodeURIList());
            System.out.println("RemoteNodeURIList: "+controlInterface.getRemoteNodeURIList());
            remoteRunMethod("ConfigureApp", nodeList);

        } catch (URISyntaxException ex) {
            System.err.println(ex.getMessage());
        } catch (InstantiationException ex) {
            System.err.println(ex.getMessage());
        } catch (IncompleteResultException ex) {
            System.err.println(ex.getMessage());
        }
    }

	private MessagingClient getClient(){
		MessagingClient client;
		if(baseProperties.get("chord.NETWORK_CLASS") != null && baseProperties.get("chord.NETWORK_CLASS").equals("SSL")){
			client = new SSLClient();
		}
		else{
                        System.out.println("Creating TCPClient in cometStarter");
			client = new TCPclient();
		}
		return client;
	}

    public void dynamicGroupLeave(Vector<String> nodeURIs, String bootstrapPeerID) {
        if (nodeURIs != null) {
            try {
                //create overlay control message
                MessagingClient client = getClient();
                OverlayControlMessage terminateMessage = new OverlayControlMessage();
                terminateMessage.addMessageItem("//" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + Integer.parseInt(System.getProperty("COMET_CONTROL_PORT",ControlConstant.COMET_CONTROL_PORT).trim())); //set sender
                terminateMessage.addMessageItem(Serializer.serializeBytes(nodeURIs));

                //set URI with comet control port, not using overlay port
                URI uri = new URI(bootstrapPeerID);
                String cometControlServer = "//" + uri.getHost() + ":" + Integer.parseInt(System.getProperty("COMET_CONTROL_PORT",ControlConstant.COMET_CONTROL_PORT).trim());
                System.out.println("Sending leave message to " + cometControlServer);
                client.send(terminateMessage.getMessageBytes(), cometControlServer);

                //wait reply
                RequestVariable<Object> request = new RequestVariable<Object>();
                if (request.awaitUninterruptibly(300000, TimeUnit.MILLISECONDS)) {
                    Object ret = request.getResult();
                    if (ret != null) {
                        if (ret instanceof Exception) {
                            System.err.print(((Exception) ret).getMessage());
                        }
                    } else {
                        System.out.println(ret);
                    }
                } else {
                    System.err.print("Incomplete Result: Timeout");
                }
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            } catch (URISyntaxException ex) {
                System.err.println("Error in bootstrapURI " + ex.getMessage());
            } catch (NetworkException ex) {
                System.err.println("Error in sending leave message to bootstrap " + ex.getMessage());
            }
        }
    }

    public void sendControlMessage(String messageType, Object item) {
        try {
            //get bootstrap
            String bootstrapPeerID = System.getProperty("chord.LOCAL_BOOTSTRAP", baseProperties.get("chord.LOCAL_BOOTSTRAP"));
            System.out.println("Bootstrap=" + bootstrapPeerID);
            if (bootstrapPeerID == null) {
                System.err.println("chord.LOCAL_BOOTSTRAP should be set in chord.properties");
                System.exit(0);
            }

            //create control message
            MessagingClient client = getClient();
            Message controlMessage = new Message();
            controlMessage.setHeader(messageType);
            controlMessage.addMessageItem("//"+InetAddress.getLocalHost().getCanonicalHostName()+":"+Integer.parseInt(System.getProperty("COMET_CONTROL_PORT",ControlConstant.COMET_CONTROL_PORT).trim()));   //set sender
            controlMessage.addMessageItem(Serializer.serializeBytes(nodeDescriptors));

            //set URI of comet control server
            URI uri = new URI(bootstrapPeerID);
            String cometControlServer = "//" + uri.getHost() + ":" + Integer.parseInt(System.getProperty("COMET_CONTROL_PORT",ControlConstant.COMET_CONTROL_PORT).trim());
            System.out.println("Sending comet control message to " + cometControlServer);
            client.send(controlMessage.getMessageBytes(), cometControlServer);

            //wait reply
            RequestVariable<Object> request = new RequestVariable<Object>();
            if (request.awaitUninterruptibly(300000, TimeUnit.MILLISECONDS)) {
                Object ret = request.getResult();
                if (ret != null) {
                    if (ret instanceof Exception) {
                        System.err.print(((Exception) ret).getMessage());
                    }
                } else
                    System.out.println(ret);
            } else {
                System.err.print("Incomplete Result: Timeout");
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } catch (URISyntaxException ex) {
            System.err.println("Error in bootstrapURI " + ex.getMessage());
        } catch (NetworkException ex) {
            System.err.println("Error in sending join message to bootstrap " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        String param = "";
        for (String arg: args)
            param += arg + " ";

        ArgHandler argHandler = new ArgHandler(args);
        if (param.contains("-nodeFile") && param.contains("-portFile") && param.contains("-exceptionFile")) {
            String nodeFile = argHandler.getStringArg("-nodeFile");
            String portFile = argHandler.getStringArg("-portFile");
            Vector<String> exceptionFiles = argHandler.getMultipleStringArg("-exceptionFile");
            Vector<String> propertyFiles = argHandler.getMultipleStringArg("-propertyFile");
            int overlayPort = ControlConstant.OVERLAY_CONTROL_PORT;
            if (argHandler.getSwitchArg("-overlayPort"))
                overlayPort = argHandler.getIntArg("-overlayPort");
            InternalStarter comet = new InternalStarter(nodeFile, portFile, propertyFiles.toArray(new String[]{}), exceptionFiles.toArray(new String[]{}), overlayPort);
            comet.dynamicGroupStart(propertyFiles.toArray(new String[]{}));

        } else {
            Vector<String> propertyFiles = argHandler.getMultipleStringArg("-propertyFile");
            InternalStarter comet = new InternalStarter();
            comet.dynamicCometStart(propertyFiles.toArray(new String[]{}));
        }
    }
}


