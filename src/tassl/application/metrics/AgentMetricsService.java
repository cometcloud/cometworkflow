/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.metrics;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.bson.Document;
import tassl.application.persistence.Store;
import tassl.application.persistence.StoreMongoDB;
import tassl.application.utils.CommonUtils;
import tassl.application.utils.RunningStat;

/**
 * Service to aggregate and report information about Agents
 * 
 * Agents can send their monitoring information to this service.
 * This service stores data in a persistent DB and can calculate different 
 * metrics with the monitoring information.
 * 
 * 
 * 
 * @author Javier Diaz-Montes
 */
public class AgentMetricsService extends Thread{

    //to make sure all calculations are in the same units
    private  final double TIMEBASE=1000.0; //Time in Seconds
    
    boolean alive=true;
    
    String AgentMetricsAddress;
    int AgentMetricsPort;
    String addressDB;
    String portDB; 
    
    String dbtype;
            
    Store dbObj;
    
    //Running METRICS
    //{dbName:RunningAvgMetric, dbName:RunningAvgMetric}
    HashMap<String,RunningAvgMetrics> avgAgentMetrics;
    
    //all appnames seen for each dbName
    HashMap<String,HashSet<String>> applications;
    //all machines seen for each dbName
    HashMap<String,HashSet<String>> machines;
    
    Properties properties;
    
    public AgentMetricsService(Properties p) {
        this.properties=p;
        avgAgentMetrics=new HashMap<>();
        applications=new HashMap<>();
        machines=new HashMap<>();
        
    }
    
    private void setLog(String filename) {
        FileHandler fh;
        try {            
            fh = new FileHandler(filename, true);
            
            Logger l = Logger.getLogger(""); //Everyone will print here. Create new logger if different file wanted
            
            for (Handler i : l.getHandlers()) {
                l.removeHandler(i);
            }
            
            l.setUseParentHandlers(false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.INFO);

        } catch (SecurityException | IOException e) {
            System.out.println("ERROR: AgentMetricsService: Setting up log");
            e.printStackTrace();
        }
    }
    
    public void loadConfig(){
        
        AgentMetricsAddress=properties.getProperty("AgentMetricsAddress");
        String port=properties.getProperty("AgentMetricsPort");

        if(port==null || port.isEmpty()){
            System.err.println("ERROR: AgentLite: If UseDB is true, then you need to specify the IP address and port of the agent "
                    + "metric service in the fields AgentMetricsAddress and AgentMetricsPort, respectivelly");
            System.exit(1);
        }else{
            AgentMetricsPort=Integer.parseInt(port);
        }

        this.portDB=properties.getProperty("DBPort");
        this.addressDB=properties.getProperty("DBAddress");          
        this.dbtype=properties.getProperty("DBType");

        if(addressDB==null || portDB==null || dbtype==null){
            System.err.println("ERROR: AgentLite: If UseDB is true, then you need to specify the IP address of the database in the "
                    + "field DBAddress, the port in the field DBPort, and the type in the field DBType");
            System.exit(1);
        }

        //Check type of DB if multiple ones are supported

        if(dbtype.equals("mongodb")){
            dbObj=new StoreMongoDB();
        }else{
            System.err.println("Unsupported database type: " + dbtype+". Currenly only mongodb is supported.");
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, "Unsupported database type: {0}", dbtype);
            System.out.println("Leaving now...");
            System.exit(-1);
        }
        
        setLog(properties.getProperty("logFile", "AgentMetricsService.log"));
        
        //call DB and initialize all RUNNING stats
        List<String> dbs;
        try {
            dbs = dbObj.loadDBs();
            
            //if this becomes heavy, we could make agents to ping this service 
            //when they become active and use that as trigger to load that agents info.
            for(String dbnameT:dbs){
                applications.put(dbnameT,dbObj.loadApps(dbnameT));
                machines.put(dbnameT,dbObj.loadMachines(dbnameT));
            
                loadAvgMetrics(dbnameT);
            }
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("ERROR: Could not contact with the DB. Uknown host exception: "+ex.getCause().toString());
            System.exit(-1);
        }
        
        
    }
    
    private void loadAvgMetrics(String dbname){
        RunningAvgMetrics newravg=new RunningAvgMetrics(TIMEBASE);        
        
        int lastNrecods=200; //number of entries to load. 0 for all.
        newravg.updateAppExec(this.getAllAppExec(dbname,applications.get(dbname),lastNrecods));
        
        newravg.updateProvisioningStats(this.getAllProvisioningStats(dbname,applications.get(dbname),lastNrecods));
        
        newravg.updateMachineFailure(this.getAllMachineFailure(dbname,applications.get(dbname),lastNrecods));
        
        newravg.updateWorkerFailure(this.getAllWorkerFailure(dbname,applications.get(dbname),lastNrecods));
                
        newravg.updateTransferInfo(this.getAllTransferInfo(dbname,lastNrecods));
        
        avgAgentMetrics.put(dbname, newravg);
    }
    
    
    @Override
    public void run(){
        
        loadConfig();
        
        System.out.println("Starting Metrics Server in " + AgentMetricsPort);
        
        ServerSocket servSock;
        try {
            servSock = new ServerSocket(AgentMetricsPort);
            servSock.setReuseAddress(true);
            DataInputStream input = null;
            DataOutputStream out=null;
            
            while (alive) {
                try {
                    Socket clientSocket = servSock.accept();
                    input = new DataInputStream(clientSocket.getInputStream());
                    out = new DataOutputStream(clientSocket.getOutputStream());
                    String command = input.readUTF();
                    String dbName = input.readUTF();
                    
                    if(!avgAgentMetrics.containsKey(dbName)){
                        avgAgentMetrics.put(dbName, new RunningAvgMetrics(TIMEBASE));
                    }
                    
                    Logger.getLogger(AgentMetricsService.class.getName()).log(Level.INFO, "command: {0}", command);
                    
                    //get information about an application
                    if (command.equals("getAppTaskAVG")){
                        String appName=input.readUTF();
                        
                        RunningAvgMetrics ravgm=avgAgentMetrics.get(dbName);
                        
                        HashMap<String,RunningStat> stats=ravgm.getAppExec().get(appName);
                         if(!stats.isEmpty()){
                             out.writeUTF("OK");
                             CommonUtils.sendObject(out,stats);
                         }else{
                             out.writeUTF("FAIL");
                         } 
                        
                    //get information about provisioning machines: overhead and failure rate.
                    }else if (command.equals("getProvisioningMachineAVG")){ 
                        String appName=input.readUTF();//"default" as appname aggregates all appname stats
                        RunningAvgMetrics ravgm=avgAgentMetrics.get(dbName);
                        
                        HashMap<Integer,RunningStat[]> stats=ravgm.getProvMachineOverheadFailure().get(appName);
                        if(!stats.isEmpty()){
                            out.writeUTF("OK");
                            CommonUtils.sendObject(out,stats);
                        }else{
                            out.writeUTF("FAIL");
                        }                        
                        
                    //get information about provisioning workers: overhead and failure rate.
                    }else if (command.equals("getProvisioningWorkerAVG")){ 
                        String appName=input.readUTF(); //"default" as appname aggregates all appname stats
                        
                        RunningAvgMetrics ravgm=avgAgentMetrics.get(dbName);
                        
                        HashMap<Integer,RunningStat[]> stats=ravgm.getProvWorkerOverheadFailure().get(appName);
                        if(!stats.isEmpty()){
                            out.writeUTF("OK");
                            CommonUtils.sendObject(out,stats);
                        }else{
                            out.writeUTF("FAIL");
                        }  
                         
                          
                    //get information about number of failures of all machine types for an app
                    }else if (command.equals("getMachineFailureRateAVG")){
                        String appName=input.readUTF(); //"default" as appname aggregates all appname stats
                        
                        RunningAvgMetrics ravgm=avgAgentMetrics.get(dbName);
                        
                        HashMap<String,Long[]> stats=ravgm.getMachineFailure().get(appName);
                        if(!stats.isEmpty()){
                            out.writeUTF("OK");
                            CommonUtils.sendObject(out,stats);
                        }else{
                            out.writeUTF("FAIL");
                        }             
                        
                    //get information about number of failures of all workers types for an app
                    }else if (command.equals("getWorkerFailureRateAVG")){
                        String appName=input.readUTF(); //"default" as appname aggregates all appname stats
                        
                        RunningAvgMetrics ravgm=avgAgentMetrics.get(dbName);
                        
                        HashMap<String,Long[]> stats=ravgm.getWorkerFailure().get(appName);
                        if(!stats.isEmpty()){
                            out.writeUTF("OK");
                            CommonUtils.sendObject(out,stats);
                        }else{
                            out.writeUTF("FAIL");
                        }     
                        
                    //get information about the transfer speed. MB/s
                    }else if(command.equals("getTransferAVG")){
                        RunningAvgMetrics ravgm=avgAgentMetrics.get(dbName);
                        CommonUtils.sendObject(out,ravgm.getSiteTransfer());
                        CommonUtils.sendObject(out,ravgm.getWorkerTransfer());
                       
                    }else if(command.equals("getAllAVGs")){
                        //IMPROVE: optimize by adding timestamps and only returning
                        RunningAvgMetrics ravg=avgAgentMetrics.get(dbName); 
                        if(ravg==null){//just in case it is called while loading data
                            ravg=new RunningAvgMetrics(TIMEBASE);
                        }
                        
                        System.out.println("Object to Send for db "+dbName);
                        System.out.println(ravg.toString());
                        System.out.println("---------------");
                        CommonUtils.sendObject(out,ravg);                                                
                        
                        
                  //WRITE CALLS
                    }else if (command.equals("dbWriteAgentStoreTransferInfo")){                        
                        //read info
                        List<Document> sites=(List<Document>)CommonUtils.readObject(input);
                        List<Document> workers=(List<Document>)CommonUtils.readObject(input);
                                                
                        //update in-memory avg metrics
                        avgAgentMetrics.get(dbName).updateTransferStats(sites,workers);     
                        
                        //store in db
                        dbObj.dbWriteAgentStoreTransferInfo(dbName,sites,workers);
                        
                    }else if(command.equals("dbWriteAgentStartTimeTask")){
                        
                        int taskId=input.readInt();
                        String app=input.readUTF();
                        Long startTime=input.readLong();
                        String workflowId=input.readUTF();
                        String stageId=input.readUTF();
                        
                        //store in db
                        dbObj.dbWriteAgentStartTimeTask(dbName,taskId,app,startTime, workflowId, stageId);
                        
                    }else if(command.equals("dbWriteAgentEndTimeTask")){
                        
                        int taskId=input.readInt();
                        Long endTime=input.readLong();
                        String appName=input.readUTF();
                        String machineType=input.readUTF();
                        String workflowId=input.readUTF();
                        Long startTime=input.readLong();
                        String getInputTime=input.readUTF();
                        String putOutputTime=input.readUTF();
                        
                        //update in-memory metrics                        
                        avgAgentMetrics.get(dbName).updateAppExec(endTime, machineType, startTime, appName);
                        
                        //check application and machine list
                        checkKnownAppMachine(dbName,appName,machineType);
                        
                        //store in db
                        dbObj.dbWriteAgentEndTimeTask(dbName,taskId,endTime, machineType, workflowId, startTime, getInputTime, putOutputTime);
                        
                    }else if(command.equals("dbWriteAgentProvisioningRequest")){
                        
                        long startRequest=input.readLong();
                        long endRequest=input.readLong();
                        String [] allMachines=(String [])CommonUtils.readObject(input);
                        List<String> operations=(List<String>)CommonUtils.readObject(input);
                        String appname=input.readUTF();
                        String wflId=input.readUTF();
                        String stageId=input.readUTF();
                        String providerType=input.readUTF();                        
                        
                        //update in-memory metrics
                        //TODO                        
                        
                        //store in db
                        dbObj.dbWriteAgentProvisioningRequest(dbName,startRequest, endRequest, allMachines, operations, appname, wflId, stageId, providerType);
                        
                        
                    }else if (command.equals("dbWriteAgentProvisioningDeallocate")){
                        long startRequest=input.readLong();
                        long endRequest=input.readLong();
                        List<String> toRelease=(List<String>)CommonUtils.readObject(input);
                        String appname=input.readUTF();
                        String wflId=input.readUTF();
                        String stageId=input.readUTF();
                        String providerType=input.readUTF(); 
                        //update in-memory metrics
                        //TODO 
                        
                        //store in db
                        dbObj.dbWriteAgentProvisioningDeallocate(dbName,startRequest, endRequest, toRelease, appname, wflId, stageId, providerType);
                        
                    }else if(command.equals("dbWriteAgentProvisioningAllocate")){
                        
                        long startRequest=input.readLong();
                        long endAllocate=input.readLong();
                        List<String>toAllocate=(List<String>)CommonUtils.readObject(input);                        
                        String appname=input.readUTF();
                        String wflId=input.readUTF();                        
                        String stageId=input.readUTF();
                        String providerType=input.readUTF();
                        long startMachineprovisioning=input.readLong();
                        long endMachineprovisioning=input.readLong();
                        long startWorkerprovisioning=input.readLong();
                        long endWorkerprovisioning=input.readLong();
                        int numReqMachines=input.readInt();
                        List<String> failedMachines=(List<String>)CommonUtils.readObject(input);
                        int numReqWorkers=input.readInt();
                        List<String>failedWorkers=(List<String>)CommonUtils.readObject(input);
                        HashMap<String,String> additionalValues=(HashMap<String,String>)CommonUtils.readObject(input);
                        int numFailedWork=0;
                        for(String work:failedWorkers){
                            if(!work.startsWith("fail")){
                                numFailedWork++;
                            }
                        }
                        //update in-memory metrics                                
                        // -overhead to allocate                                                
                        // -failure rate when allocating
                        avgAgentMetrics.get(dbName).updateProvisioningStats(appname,startMachineprovisioning,endMachineprovisioning,startWorkerprovisioning,
                                endWorkerprovisioning,numReqMachines,numReqWorkers, failedMachines.size(),numFailedWork);
                                                
                        
                        //check application and machine list
                        checkKnownAppMachine(dbName,appname,null);
                        
                        //store in db
                        dbObj.dbWriteAgentProvisioningAllocate(dbName,startRequest, endAllocate, toAllocate, appname, wflId, stageId, providerType,
                            startMachineprovisioning, endMachineprovisioning, startWorkerprovisioning, endWorkerprovisioning, numReqMachines, 
                            failedMachines, numReqWorkers, failedWorkers, additionalValues);
                        
                        
                        
                    }else if (command.equals("dbWriteAgentReleaseMachine")){                        
                        String iptype=input.readUTF();
                        long startTimeStamp=input.readLong();
                        long endTimeStamp=input.readLong();
                        String appName=input.readUTF();
                        String providerType=input.readUTF();
                        boolean failure=input.readBoolean();
                        HashMap<String,String> additionalValues=(HashMap<String,String>)CommonUtils.readObject(input);           
                        //update in-memory metrics
                        avgAgentMetrics.get(dbName).updateMachineFailure(iptype, appName, failure);
                        
                        //check application and machine list
                        checkKnownAppMachine(dbName,appName,iptype);
                        
                        //store in db
                        dbObj.dbWriteAgentReleaseMachine(dbName,iptype,startTimeStamp, endTimeStamp, appName, providerType, failure, additionalValues);
                        
                    }else if (command.equals("dbWriteAgentReleaseWorker")){
                        String iptype=input.readUTF();
                        long startTimeStamp=input.readLong();
                        long endTimeStamp=input.readLong();
                        String appName=input.readUTF();
                        String providerType=input.readUTF();
                        boolean failure=input.readBoolean();
                        HashMap<String,String> additionalValues=(HashMap<String,String>)CommonUtils.readObject(input);
                        //update in-memory metrics
                        avgAgentMetrics.get(dbName).updateWorkerFailure(iptype, appName, failure);
                        
                        //check application and machine list
                        checkKnownAppMachine(dbName,appName,iptype);
                        
                        //store in db
                        dbObj.dbWriteAgentReleaseWorker(dbName,iptype,startTimeStamp, endTimeStamp, appName, providerType, failure, additionalValues);

                    }else {
                        Logger.getLogger(AgentMetricsService.class.getName()).log(Level.INFO, "Unknown command: {0}", command);
                    }

                }catch (ClassNotFoundException ex){
                    Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
                }catch (UnknownHostException ex) {
                    Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);         
                }catch(ClassCastException ex){
                    Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
                }catch (IOException ex) {
                    Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
                }catch (java.lang.NullPointerException ex){
                    Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
                }finally {                    
                    if (input != null) {
                        try{
                            input.close();
                        }catch (IOException ex) {
                            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if (out != null) {
                        try{
                            out.close();
                        }catch (IOException ex) {
                            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                }
            }
            
        } catch (IOException ex) {
            System.out.println("ERROR: starting Agent Metric Server in "+AgentMetricsPort);
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
            
            System.out.println("Leaving now...");
            System.exit(1);
        }
        
    }
    
    private void checkKnownAppMachine(String dbName, String appName, String machineType) throws UnknownHostException{
        if(applications.get(dbName)==null){
            applications.put(dbName, new HashSet());
        }
        if(appName!=null && !applications.get(dbName).contains(appName)){
            applications.get(dbName).add(appName);
            dbObj.dbWriteKnownApp(dbName, appName);
        }
        
        if(machines.get(dbName)==null){
            machines.put(dbName, new HashSet());
        }       
        if(machineType!=null && !machines.get(dbName).contains(machineType)){
            machines.get(dbName).add(machineType);
            dbObj.dbWriteKnownMachine(dbName, machineType);
        }
        
    }
       
    
    //Methods to aggregate raw data    
    public HashMap<String,List<Document>> getAllAppExec(String dbName, HashSet<String> appNames, int lastNRecods){
        HashMap<String,List<Document>> appInfo=new HashMap<>();
        
        try {            
            for(String appName:appNames){
                appInfo.put(appName,dbObj.dbReadAgentAppExec(dbName,appName,lastNRecods));
            }
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appInfo;
        
    }

    public HashMap<String,List<Document>> getAllProvisioningStats(String dbName, HashSet<String> appNames, int lastNRecods){
        HashMap<String,List<Document>> appInfo=new HashMap<>();
        
        try {            
            for(String appName:appNames){
                appInfo.put(appName,dbObj.dbReadAgentProvisioningStats(dbName,appName,lastNRecods));
            }
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appInfo;
        
    }
    
    public HashMap<String,List<Document>> getAllWorkerFailure(String dbName, HashSet<String> appNames, int lastNRecods){
        HashMap<String,List<Document>> appInfo=new HashMap<>();
        
        try {            
            for(String appName:appNames){
                appInfo.put(appName,dbObj.dbReadAgentWorkerFailure(dbName,appName,lastNRecods));
            }
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appInfo;
        
    }
    
    public HashMap<String,List<Document>> getAllMachineFailure(String dbName, HashSet<String> appNames, int lastNRecods){
        HashMap<String,List<Document>> appInfo=new HashMap<>();
        
        try {            
            for(String appName:appNames){
                appInfo.put(appName,dbObj.dbReadAgentMachineFailure(dbName,appName,lastNRecods));
            }
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return appInfo;
        
    }
    
    public HashMap<String,List<Document>> getAllTransferInfo(String dbName, int lastNRecods){
        HashMap<String,List<Document>> transferInfo=new HashMap<>();       
        
        try{
            
            transferInfo=dbObj.dbReadAgentTransferInfo(dbName,lastNRecods);            
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(AgentMetricsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return transferInfo;
        
    }
    
    public static void main(String[] args) {        
        if (args != null) {
            try {
                
                List<String> appPropertyFiles = CommonUtils.getMultipleStringArg(args,"-propertyFile");
                
                Properties p = new Properties();
                for (String propertyFile : appPropertyFiles) {
                    p.load(new FileInputStream(propertyFile));
                }
                
                for(Object key:p.keySet()){
                    System.setProperty((String)key, (String)p.get(key));
                }
                
                
                AgentMetricsService metricServiceObj = new AgentMetricsService(p);
                metricServiceObj.start();

            } catch (IOException e) {
                System.err.println("ERROR: Check loading property files. " + e.getMessage());
            }
        } else {
            System.err.println("ERROR: AgentMetricService: This service requires to be started with an argument to indicate the configuration file. Use -propertyFile <config.properties>");
            System.exit(1);
        }


    }
}
