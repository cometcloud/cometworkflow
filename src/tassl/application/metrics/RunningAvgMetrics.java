/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.metrics;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.bson.Document;
import tassl.application.utils.RunningStat;

/**
 *
 * @author Javier Diaz-Montes
 */
public class RunningAvgMetrics implements Serializable{
    // {AppName:{Machine:stats, Machine:stats,...}, AppName:{Machine:stats, Machine:stats,...},...}
    HashMap<String,HashMap<String,RunningStat>> appExec;//Use time in Seconds
    
    //{AppName:{#Machines:{statsOverhead, statsFailure}, #Machines:{statsOverhead, statsFailure},...}, AppName:{#Machines:{statsOverhead, statsFailure}, #Machines:{statsOverhead, statsFailure},...},...}
    //there is "default" appname that adds all overheads and failures. Overhead is time, and failure is ratio from 0 to 1
    //in the "default" appname, the #Machines==0 has average of all number of machines
    HashMap<String,HashMap<Integer,RunningStat[]>> provMachineOverheadFailure; //overhead and failures to provision machine
    //there is "default" appname that adds all overheads and failures
    ////{AppName:{#Workers:{statsOverhead, statsFailure}, #Workers:{statsOverhead, statsFailure},...}, AppName:{#Workers:{statsOverhead, statsFailure}, #Workers:{statsOverhead, statsFailure},...},...}
    //in the "default" appname, the #Workers==0 has average of all number of Workers
    HashMap<String,HashMap<Integer,RunningStat[]>> provWorkerOverheadFailure; //overhead and failures to provision workers
    
    //{AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...}, AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...},...}
    //there is "default" appname, "default" machine that adds all worker failures regarless type
    HashMap<String,HashMap<String,Long []>> workerFailure; //worker failures while in running status
    
    //{AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...}, AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...},...}
    //there is "default" appname, "default" machine that adds all machine failures regarless type
    HashMap<String,HashMap<String,Long []>> machineFailure; //machine failures while in running status
    
    //SiteAddress:{push,pull}. Note that is address, not name, not port included.
    HashMap<String, RunningStat[]> siteTransfer;
    //{push,pull}
    RunningStat[] workerTransfer;
    double TIMEBASE;
    
    public RunningAvgMetrics(double TIMEBASE){
        appExec=new HashMap<>();
        provMachineOverheadFailure=new HashMap<>();
        provWorkerOverheadFailure=new HashMap<>();
        workerFailure=new HashMap<>();
        machineFailure=new HashMap<>();
        siteTransfer=new HashMap<>();
        workerTransfer=new RunningStat[2];
        workerTransfer[0]=new RunningStat();
        workerTransfer[1]=new RunningStat();
        
        this.TIMEBASE=TIMEBASE;
    }

    @Override
    public String toString() {
        String provMachineOverheadFailureSTR="{";
        for(String key:provMachineOverheadFailure.keySet()){
            provMachineOverheadFailureSTR+=key+"={";
            for(Integer key1:provMachineOverheadFailure.get(key).keySet()){
                provMachineOverheadFailureSTR+=key1+"="+Arrays.toString(provMachineOverheadFailure.get(key).get(key1))+",";
            }
            if(provMachineOverheadFailure.get(key).size()>0)
                provMachineOverheadFailureSTR=provMachineOverheadFailureSTR.substring(0, provMachineOverheadFailureSTR.length()-1);//remove last comma
            provMachineOverheadFailureSTR+="},";            
        }
        if(provMachineOverheadFailure.size()>0)
            provMachineOverheadFailureSTR=provMachineOverheadFailureSTR.substring(0, provMachineOverheadFailureSTR.length()-1);//remove last comma
        provMachineOverheadFailureSTR+="}";
        
        String provWorkerOverheadFailureSTR="{";
        for(String key:provWorkerOverheadFailure.keySet()){
            provWorkerOverheadFailureSTR+=key+"={";
            for(Integer key1:provWorkerOverheadFailure.get(key).keySet()){
                provWorkerOverheadFailureSTR+=key1+"="+Arrays.toString(provWorkerOverheadFailure.get(key).get(key1))+",";
            }
            if(provWorkerOverheadFailure.get(key).size()>0)
                provWorkerOverheadFailureSTR=provWorkerOverheadFailureSTR.substring(0, provWorkerOverheadFailureSTR.length()-1);//remove last comma
            provWorkerOverheadFailureSTR+="},";
        }
        if(provWorkerOverheadFailure.size()>0)
            provWorkerOverheadFailureSTR=provWorkerOverheadFailureSTR.substring(0, provWorkerOverheadFailureSTR.length()-1);//remove last comma
        provWorkerOverheadFailureSTR+="}";
        
        
        String workerFailureSTR="{";
        for(String key:workerFailure.keySet()){
            workerFailureSTR+=key+"={";
            for(String key1:workerFailure.get(key).keySet()){
                workerFailureSTR+=key1+"="+Arrays.toString(workerFailure.get(key).get(key1))+",";
            }   
            if(workerFailure.get(key).size()>0)
                workerFailureSTR=workerFailureSTR.substring(0, workerFailureSTR.length()-1);//remove last comma
            workerFailureSTR+="},";
        }
        if(workerFailure.size()>0)
            workerFailureSTR=workerFailureSTR.substring(0, workerFailureSTR.length()-1);//remove last comma
        workerFailureSTR+="}";
        
        String machineFailureSTR="{";
        for(String key:machineFailure.keySet()){
            machineFailureSTR+=key+"={";
            for(String key1:machineFailure.get(key).keySet()){
                machineFailureSTR+=key1+"="+Arrays.toString(machineFailure.get(key).get(key1))+",";
            }
            if(machineFailure.get(key).size()>0)
                machineFailureSTR=machineFailureSTR.substring(0, machineFailureSTR.length()-1);//remove last comma
            machineFailureSTR+="},";
        }
        if(machineFailure.size()>0)
            machineFailureSTR=machineFailureSTR.substring(0, machineFailureSTR.length()-1);//remove last comma
        machineFailureSTR+="}";
        
        String siteTransferSTR="{";
        for(String key:siteTransfer.keySet()){
            siteTransferSTR+=key+"="+ Arrays.toString(siteTransfer.get(key))+",";    
        }
        if(siteTransfer.size()>0)
            siteTransferSTR=siteTransferSTR.substring(0, siteTransferSTR.length()-1);//remove last comma
        siteTransferSTR+="}";
        
        return "RunningAvgMetrics{" + "appExec=" + appExec + ", provMachineOverheadFailure=" + provMachineOverheadFailureSTR + 
                ", provWorkerOverheadFailure=" + provWorkerOverheadFailureSTR + ", workerFailure=" + workerFailureSTR + 
                ", machineFailure=" + machineFailureSTR + ", siteTransfer=" + siteTransferSTR + ", workerTransfer=" + Arrays.toString(workerTransfer) + '}';
    }

    public HashMap<String, HashMap<String, RunningStat>> getAppExec() {
        return appExec;
    }

    public HashMap<String, HashMap<Integer, RunningStat[]>> getProvMachineOverheadFailure() {
        return provMachineOverheadFailure;
    }

    public HashMap<String, HashMap<Integer, RunningStat[]>> getProvWorkerOverheadFailure() {
        return provWorkerOverheadFailure;
    }

    public HashMap<String, HashMap<String, Long[]>> getWorkerFailure() {
        return workerFailure;
    }

    public HashMap<String, HashMap<String, Long[]>> getMachineFailure() {
        return machineFailure;
    }

    public HashMap<String, RunningStat[]> getSiteTransfer() {
        return siteTransfer;
    }

    public RunningStat[] getWorkerTransfer() {
        return workerTransfer;
    }
    
    
    
    /**
     * Application Execution Time     
     * @param endTime
     * @param machineType
     * @param startTime
     * @param appName
     */
    public void updateAppExec(Long endTime, String machineType, Long startTime, String appName){
        //HashMap<String,HashMap<String,RunningStat>>
        HashMap<String,RunningStat> runStat=appExec.get(appName);
        if(runStat==null){
            runStat=new HashMap<>();
            appExec.put(appName, runStat);
        }
        RunningStat rs=runStat.get(machineType);
        if(rs==null){
            rs=new RunningStat();
            runStat.put(machineType, rs);
        }
        rs.Push((endTime-startTime)/TIMEBASE);//Use time in Seconds
    }
    
    /**
     * Update Application Execution Time for multiple Applications
     * @param records HashMap<String,List<Document>>, where application is key 
     *                 and the value is a list of Documents with individual task execution entries
     */
    public void updateAppExec(HashMap<String,List<Document>> records){
        for(String appName:records.keySet()){
            for(Document doc:records.get(appName)){
                this.updateAppExec(doc.getLong("endTime"), doc.getString("machineType"), doc.getLong("startTime"), appName);
                
            }
        }
        
    }
    
    /**
     * Update provisioning stats for multiple applications
     * @param records
     */
    public void updateProvisioningStats(HashMap<String,List<Document>> records){
        for(String appName:records.keySet()){
            for(Document doc:records.get(appName)){
                Object machines=doc.get("machines");
                Object worker=doc.get("workers");                
                long startMachineprovisioning=0,endMachineprovisioning=0,startWorkerprovisioning=0,endWorkerprovisioning=0;
                int numReqMachines=0,numFailMachines=0, numReqWorkers=0, numFailWorkers=0;
                if(machines!=null){                
                    startMachineprovisioning=((Document)machines).getLong("startTime");
                    endMachineprovisioning=((Document)machines).getLong("endTime");
                    numReqMachines=((Document)machines).getInteger("reqMachines");
                    numFailMachines=((Document)machines).getInteger("failedMachines");
                }
                if(worker!=null){
                    startWorkerprovisioning=((Document)worker).getLong("startTime");
                    endWorkerprovisioning=((Document)worker).getLong("endTime");                
                    numReqWorkers=((Document)worker).getInteger("reqWorkers");                
                    numFailWorkers=((Document)worker).getInteger("failedWorkers");
                }
                
                this.updateProvisioningStats(appName, startMachineprovisioning, endMachineprovisioning, startWorkerprovisioning, endWorkerprovisioning, numReqMachines, numReqWorkers, numFailMachines, numFailWorkers);
                
            }
        }
    }
    
    /**
     * Update machine failures for multiple applications
     * 
     * @param records
     */
    public void updateMachineFailure(HashMap<String,List<Document>> records){
        for(String appName:records.keySet()){
            for(Document doc:records.get(appName)){
                this.updateMachineFailure(doc.getString("iptype"),appName, Boolean.parseBoolean(doc.getString("status")));                
            }
        }
    }
    
    /**
     * Update worker failures for multiple applications
     * 
     * @param records
     */
    public void updateWorkerFailure(HashMap<String,List<Document>> records){
        for(String appName:records.keySet()){
            for(Document doc:records.get(appName)){                
                this.updateWorkerFailure(doc.getString("iptype"),appName, Boolean.parseBoolean(doc.getString("status")));                
            }
        }
    }
    
    /**
     * Update all transfer information
     * @param records
     */
    public void updateTransferInfo(HashMap<String,List<Document>> records){
        this.updateTransferStats(records.get("sites"), records.get("workers"));
    }
    
    
    /**
     * Transfer Time between Sites and Workers and a site
     * @param sites
     * @param workers
     */
    public void updateTransferStats(List<Document> sites, List<Document> workers){
        //SiteAddress:{push,pull}
        //HashMap<String, RunningStat[]> siteTransfer;
        
        for(Document siteDoc:sites){
            boolean from=true;
            String siteAddress=siteDoc.getString("from");
            if(siteAddress==null){
                siteAddress=siteDoc.getString("to");
                from=false;
            }
                        
            RunningStat [] rs=siteTransfer.get(siteAddress);
            if(rs==null){
                rs=new RunningStat[2]; //pull and push
                rs[0]=new RunningStat(); //pull (from)
                rs[1]=new RunningStat(); //push (to)
                siteTransfer.put(siteAddress, rs);
            }
            double MBTransferred=siteDoc.getDouble("bytes")/(1024*1024);
            double transferSpeed=MBTransferred/((double)siteDoc.getLong("duration")/TIMEBASE);// MB/s
            if(from){                
                rs[0].Push(transferSpeed);                
            }else{
                rs[1].Push(transferSpeed);
            }
        }
        
        //{push,pull}
        //RunningStat[] workerTransfer;
        for(Document workerDoc:workers){
            boolean from=true;
            String siteAddress=workerDoc.getString("from");
            if(siteAddress==null){                
                from=false;
            }
            
            if(workerTransfer==null){
                workerTransfer=new RunningStat[2]; //pull and push
                workerTransfer[0]=new RunningStat(); //pull (from)
                workerTransfer[1]=new RunningStat(); //push (to)
            }
            double MBTransferred=workerDoc.getDouble("bytes")/(1024*1024);
            double transferSpeed=MBTransferred/((double)workerDoc.getLong("duration")/TIMEBASE);// MB/s
            if(from){                
                workerTransfer[0].Push(transferSpeed);                
            }else{
                workerTransfer[1].Push(transferSpeed);
            }            
            
        }
        
    }
    
    /**
     * Number of Workers that failed
     * @param iptype
     * @param appName
     * @param failure
     */
    public void updateWorkerFailure(String iptype, String appName, boolean failure){
        //{AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...}, AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...},...}
        //there is "default" appname, "default" machine that adds all worker failures regarless type
        //HashMap<String,HashMap<String,Long []>> workerFailure;
        HashMap<String,Long []> workerStat=workerFailure.get(appName);
        if(workerStat==null){
            workerStat=new HashMap<>();
            workerFailure.put(appName, workerStat);
        }
        Long [] okfail=workerStat.get(iptype);
        if(okfail==null){
            okfail=new Long[2];
            okfail[0]=0L;
            okfail[1]=0L;
            workerStat.put(iptype, okfail);
        }
        if(failure){
            okfail[1]++;
        }else{
            okfail[0]++;
        }
        
        workerStat=workerFailure.get("default");
        if(workerStat==null){
            workerStat=new HashMap<>();
            workerFailure.put("default", workerStat);
        }
        okfail=workerStat.get("default");
        if(okfail==null){
            okfail=new Long[2];
            okfail[0]=0L;
            okfail[1]=0L;
            workerStat.put("default", okfail);
        }
        if(failure){
            okfail[1]++;
        }else{
            okfail[0]++;
        }
        
    }
    
    /**
     * Number of Machines that failed
     * @param iptype
     * @param appName
     * @param failure
     */
    public void updateMachineFailure(String iptype, String appName, boolean failure){
        //{AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...}, AppName:{Machine:{#ok,#fail}, Machine:{#ok,#fail},...},...}
        //there is "default" appname, "default" machine that adds all machine failures regarless type
        //HashMap<String,HashMap<String,Long []>> machineFailure;
        
        HashMap<String,Long []> workerStat=machineFailure.get(appName);
        if(workerStat==null){
            workerStat=new HashMap<>();
            machineFailure.put(appName, workerStat);
        }
        Long [] okfail=workerStat.get(iptype);
        if(okfail==null){
            okfail=new Long[2];
            okfail[0]=0L;
            okfail[1]=0L;
            workerStat.put(iptype, okfail);
        }
        if(failure){
            okfail[1]++;
        }else{
            okfail[0]++;
        }
        
        workerStat=machineFailure.get("default");
        if(workerStat==null){
            workerStat=new HashMap<>();
            machineFailure.put("default", workerStat);
        }
        okfail=workerStat.get("default");
        if(okfail==null){
            okfail=new Long[2];
            okfail[0]=0L;
            okfail[1]=0L;
            workerStat.put("default", okfail);
        }
        if(failure){
            okfail[1]++;
        }else{
            okfail[0]++;
        }    
        
    }
    
    /**
     * Overhead to provision machines and workers. Failure rate when provisioning machines and workers
     * 
     * @param appname
     * @param startMachineprovisioning
     * @param endMachineprovisioning
     * @param startWorkerprovisioning
     * @param endWorkerprovisioning
     * @param numReqMachines
     * @param numReqWorkers
     * @param numFailMachines
     * @param numFailWorkers
     */
    public void updateProvisioningStats(String appname, long startMachineprovisioning, long endMachineprovisioning,
            long startWorkerprovisioning, long endWorkerprovisioning, int numReqMachines, int numReqWorkers, int numFailMachines, int numFailWorkers){
                       
        if(startMachineprovisioning!=0){
            //machines
            HashMap<Integer,RunningStat[]> runStatMach=provMachineOverheadFailure.get(appname);
            if(runStatMach==null){
                runStatMach=new HashMap<>();
                provMachineOverheadFailure.put(appname, runStatMach);
            }
            RunningStat [] rs=runStatMach.get(numReqMachines);
            if(rs==null){
                rs=new RunningStat[2]; //overhead and failures
                rs[0]=new RunningStat();
                rs[1]=new RunningStat();
                runStatMach.put(numReqMachines, rs);
            }
            rs[0].Push((endMachineprovisioning-startMachineprovisioning)/TIMEBASE);
            rs[1].Push((double)numFailMachines/(double)numReqMachines);

            runStatMach=provMachineOverheadFailure.get("default");//default app as the overhead and failure rate might be similar for different VMs
            if(runStatMach==null){
                runStatMach=new HashMap<>();
                provMachineOverheadFailure.put("default", runStatMach);
            }
            rs=runStatMach.get(numReqMachines);
            if(rs==null){
                rs=new RunningStat[2];
                rs[0]=new RunningStat();
                rs[1]=new RunningStat();
                runStatMach.put(numReqMachines, rs);
            }
            rs[0].Push((endMachineprovisioning-startMachineprovisioning)/TIMEBASE);
            rs[1].Push((double)numFailMachines/(double)numReqMachines);

            rs=runStatMach.get(0); //Aggregates ALL
            if(rs==null){
                rs=new RunningStat[2];
                rs[0]=new RunningStat();
                rs[1]=new RunningStat();
                runStatMach.put(0, rs);
            }
            rs[0].Push((endMachineprovisioning-startMachineprovisioning)/TIMEBASE);
            rs[1].Push((double)numFailMachines/(double)numReqMachines);
        }
        
        if(startWorkerprovisioning!=0){
            //workers
            HashMap<Integer,RunningStat[]> runStatWorker=provWorkerOverheadFailure.get(appname);
            if(runStatWorker==null){
                runStatWorker=new HashMap<>();
                provWorkerOverheadFailure.put(appname, runStatWorker);
            }
            RunningStat []rsW=runStatWorker.get(numReqWorkers);
            if(rsW==null){
                rsW=new RunningStat[2];
                rsW[0]=new RunningStat();
                rsW[1]=new RunningStat();
                runStatWorker.put(numReqWorkers, rsW);
            }
            rsW[0].Push((endWorkerprovisioning-startWorkerprovisioning)/TIMEBASE);
            rsW[1].Push((double)numFailWorkers/(double)numReqWorkers);

            runStatWorker=provWorkerOverheadFailure.get("default");//default app as the overhead and failure rate might be similar for different VMs
            if(runStatWorker==null){
                runStatWorker=new HashMap<>();
                provWorkerOverheadFailure.put("default", runStatWorker);
            }
            rsW=runStatWorker.get(numReqWorkers);
            if(rsW==null){
                rsW=new RunningStat[2];
                rsW[0]=new RunningStat();
                rsW[1]=new RunningStat();
                runStatWorker.put(numReqWorkers, rsW);
            }
            rsW[0].Push((endWorkerprovisioning-startWorkerprovisioning)/TIMEBASE);
            rsW[1].Push((double)numFailWorkers/(double)numReqWorkers);

            rsW=runStatWorker.get(0); //Aggregates All
            if(rsW==null){
                rsW=new RunningStat[2];
                rsW[0]=new RunningStat();
                rsW[1]=new RunningStat();
                runStatWorker.put(0, rsW);
            }
            rsW[0].Push((endWorkerprovisioning-startWorkerprovisioning)/TIMEBASE);
            rsW[1].Push((double)numFailWorkers/(double)numReqWorkers);
        }
    }
    
}
