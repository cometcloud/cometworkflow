/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Javier Diaz-Montes
 */
public class CommonUtils {
    
    
    
    /**
     * Read args array to find a key:value pair
     * @param args String[]
     * @param key String
     * @return Argument value
     */
    public static String getStringArg(String []args, String key){
        String toReturn="";
        
        for(int i=1;i<args.length;i+=2){
            if(args[i-1].equals(key)){
                toReturn=args[i].trim();
                break;
            }
        }        
        
        return toReturn;
    }
    
    /**
     * Read args array to find a key:value pairs
     * @param args
     * @param key
     * @return List of argument values
     */
    public static List<String> getMultipleStringArg(String []args, String key){
        List<String> toReturn=new ArrayList<>();
        
        for(int i=1;i<args.length;i+=2){
            if(args[i-1].equals(key)){
                toReturn.add(args[i].trim());
            }
        }        
        
        return toReturn;
    }
    
    
    /**
     * Get a socket connection with address and port. 
     * @param address
     * @param port
     * @return object[DataOutputStream]
     */
    public static DataOutputStream getOutput(String address, int port) {
        DataOutputStream out = null;
        try {
            Socket clientSocket = new Socket(address, port);
            out = new DataOutputStream(clientSocket.getOutputStream());
        } catch (UnknownHostException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, "ERROR contacting "+address+":"+port, ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, "ERROR contacting "+address+":"+port, ex.getMessage());
        }
        return out;
    }
    
    /**
     * Get a socket connection with address and port. 
     * @param address
     * @param port
     * @return object[DataInputStream,DataOutputStream]
     */
    public static Object [] getOutputInput(String address, int port) {
        Object [] streams=new Object[2];
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            Socket clientSocket = new Socket(address, port);
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new DataInputStream(clientSocket.getInputStream());
        } catch (UnknownHostException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, "ERROR contacting "+address+":"+port, ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, "ERROR contacting "+address+":"+port, ex.getMessage());
        }
        streams[0]=in;
        streams[1]=out;
        return streams;
    }
    
    /**
     * Get a socket connection with address and port. 
     * @param address
     * @param port
     * @param skiplogs
     * @return object[DataInputStream,DataOutputStream]
     */
    public static Object [] getOutputInput(String address, int port, boolean skiplogs) {
        Object [] streams=new Object[2];
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            Socket clientSocket = new Socket(address, port);
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new DataInputStream(clientSocket.getInputStream());
        } catch (UnknownHostException ex) {
            if(!skiplogs){
                Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, "ERROR contacting "+address+":"+port, ex.getMessage());
            }
        } catch (IOException ex) {
            if(!skiplogs){
                Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, "ERROR contacting "+address+":"+port, ex.getMessage());
            }
        }
        streams[0]=in;
        streams[1]=out;
        return streams;
    }
    
    public static void closeInOut(DataInputStream in, DataOutputStream out){
        if (out!=null){
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  
        if(in!=null){
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Execute a system command and returns output status. It will log in info level the stdout and stderr
     * @param command
     * @return output status
     */
    public static int execute(String command){
        int status=1;
        String lineOut="";
        String lineErr="";
        try 
        { 
            String [] cmd={"/bin/bash", "-c", command};
            Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "command:{0}", command);
            Process p;             
            p = Runtime.getRuntime().exec(cmd);           
            
            p.waitFor();
            
            status=p.exitValue();
            
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())); 
            String line=reader.readLine(); 
            
            while(line!=null) 
            { 
                lineOut+=line+"|";                
                line=reader.readLine(); 
            } 
            BufferedReader readerE=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
            String lineE=readerE.readLine(); 
            
            while(lineE!=null) 
            { 
                lineErr+=lineE+"|";                
                lineE=readerE.readLine(); 
            } 
            Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "command was:{0}", command);
            Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "stdout:{0}", lineOut);
            Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "stderr:{0}", lineErr);
        } catch (IOException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
        }catch (InterruptedException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return status;
    } 
    
    /**
     * Executes a system command
     * @param command
     * @param background Whether it waits for the command to finish or not
     * @return String[output status, stdout, stderr]
     */
    public static String[] execute(String command, boolean background){
        String [] output=new String[3];
        String lineOut="";
        String lineErr="";
        output[0]="-1";
        try 
        { 
            //"/bin/bash", "-c", 
            String [] cmd={"/bin/bash", "-c",command};
            
            // commented. launcher.sh is just a script with $1 &
            //if(background){
            //    if(command.startsWith("ssh")){ //for now only with the ssh for workers
            //        cmd[2]="../../scripts/launcher.sh \""+command+"\"";
            //    }
            //}
            
            Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "command:{0}", cmd[2]);
            Process p;             
            p = Runtime.getRuntime().exec(cmd);           
            
            if (!background){
                p.waitFor();
            
            
                output[0]=new Integer(p.exitValue()).toString();

                BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())); 
                

                while(reader.ready()) 
                { 
                    String line=reader.readLine(); 
                    lineOut+=line+"\n";                
                    
                } 
                BufferedReader readerE=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
                 

                while(readerE.ready()) 
                { 
                    String lineE=readerE.readLine(); 
                    lineErr+=lineE+"\n";
                } 
                Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "command was:{0}", cmd[2]);
                Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "stdout:{0}", lineOut.replace("\n", "&;n"));
                Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "stderr:{0}", lineErr.replace("\n", "&;n"));
            }else{
                Logger.getLogger(CommonUtils.class.getName()).log(Level.INFO, "command was:{0}", cmd[2]);
            }
        } catch (IOException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
        }catch (InterruptedException ex) {
            Logger.getLogger(CommonUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        output[1]=lineOut;
        output[2]=lineErr;
        return output;
    } 
    
    
    /**
     * Serializes an object and sends it
     * @param out
     * @param obj
     * @throws IOException
     */
    public static void sendObject(DataOutputStream out, Object obj) throws IOException{
        
        byte[] forwardObj;        
        forwardObj= programming5.io.Serializer.serializeBytes(obj);                    
        out.flush();
        out.writeInt(forwardObj.length);                                               
        out.flush();
        out.write(forwardObj);
        out.flush();

    }
    
    /**
     * Reads a serialized object and deserialize it
     * @param in
     * @return received Object or null if we could not read all bytes.
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws NullPointerException
     */
    public static Object readObject(DataInputStream in) throws IOException, ClassNotFoundException, NullPointerException{
        Object receivedObject = null;
    	
        int length = in.readInt(); //data length
        byte[] bytesdata = new byte[length];
        
        in.readFully(bytesdata);
        
        receivedObject = programming5.io.Serializer.deserialize(bytesdata);
        
        return receivedObject;
    }
    
}
