/*
 * Copyright (c) 2009, NSF Cloud and Autonomic Computing Center, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Cloud and Autonomic Computing Center, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.node.isolate;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import programming5.io.ArgHandler;
import programming5.io.Debug;
import tassl.automate.network.ControlConstant;
import tassl.automate.application.node.isolate.mpi.MPIFrontEndNode;
import tassl.automate.application.node.isolate.mpi.MPIWorker;
import tassl.automate.application.node.isolate.bgp.BlueGeneIsolatedWorker;
import tassl.automate.application.node.isolate.mpi.MPIConstants;
import tassl.automate.comet.CometConstants;

//THIS SHOULD REPLACE THE ORIGINAL WITHOUT PROBLEMS

/**
 *
 * @author hyunjoo
 */
public class CloudBurstStarter {
    public static void main(String[] args) {
        int servPort = ControlConstant.IWORKER_LISTENER_PORT;//default value for server port
        String partitionID = "";//default paritionID value
        String workerID = "";//default workerID value
        String systemType = "";
        String systemQueue = "";
        String systemMode ="";
        String workerDir = null;
        String publicIP=null; //Javi- this is for machines where the hostname is not the fqn and it returns the private ip
                
        if (args != null) {
            try {
                ArgHandler argHandler = new ArgHandler(args);
                if (argHandler.getSwitchArg("-port"))
                    servPort = argHandler.getIntArg("-port");//set the port value given by user from arg -port PORT_NUM
                if (argHandler.getSwitchArg("-publicIp"))
                    publicIP = argHandler.getStringArg("-publicIp");//set the public IP value given by user from arg -publicIp ip/fqn
            	if (argHandler.getSwitchArg("-partition"))
		    partitionID = argHandler.getStringArg("-partition");
                if (argHandler.getSwitchArg("-systemType"))
		    systemType = argHandler.getStringArg("-systemType");
                if (argHandler.getSwitchArg("-systemQueue"))
		    systemQueue = argHandler.getStringArg("-systemQueue");
                if (argHandler.getSwitchArg("-systemMode"))
		    systemMode = argHandler.getStringArg("-systemMode");
                if (argHandler.getSwitchArg("-workerID"))
		    workerID = argHandler.getStringArg("-workerID");
                if (argHandler.getSwitchArg("-workerBaseDir"))
                    workerDir = argHandler.getStringArg("-workerBaseDir");
                Vector<String> appPropertyFiles = argHandler.getMultipleStringArg("-propertyFile");
                for (String propertyFile : appPropertyFiles) {
                    CometConstants.loadProperties(propertyFile);
                }
            } catch (Exception e) {
                System.err.println("Check your arguments "+e.getMessage());
            }
        } else {
            System.out.println("CloudBurstStarter: At least comet.properties and chord.properties should be passed");
            System.exit(0);
        }

        String debugStr = System.getProperty("comet.DEBUG");
        if (debugStr != null) {
            String[] debug = debugStr.split(",");
            for (int i=0; i<debug.length; i++)
                Debug.enable(debug[i]);
        }

        String cloudburstNodeType = System.getProperty("CloudburstNodeType", MPIConstants.CloudburstNodeType_ISOLATED);
        if (cloudburstNodeType.equals(MPIConstants.CloudburstNodeType_ISOLATED)
                || cloudburstNodeType.equals(MPIConstants.CloudburstNodeType_HPC)) {
            if (System.getProperty("IsolatedProxy")==null) {
                System.err.println("IsolatedProxy in comet.properties should be specified");
                System.exit(1);
            }
            
            IsolatedWorker iworker;
            if (publicIP != null &&workerDir != null){
                iworker = new IsolatedWorker(servPort, publicIP,workerDir,partitionID);// create an isolated worker with the given port value
            }else{
                iworker = new IsolatedWorker(servPort);// create an isolated worker with the given port value
            }
            
            new Thread(iworker).start();
        }else if (cloudburstNodeType.equals(MPIConstants.CloudburstNodeType_ISOLATEDWFL)){
            if (System.getProperty("IsolatedProxy")==null) {
                System.err.println("IsolatedProxy in comet.properties should be specified");
                System.exit(1);
            }
            
            IsolatedWorker iworker;
            if (publicIP != null&&workerDir != null){
                iworker = new IsolatedWorker(servPort, publicIP,workerDir,partitionID);// create an isolated worker with the given port value
                iworker.setIWworkflow(true);
            }else{
                iworker = new IsolatedWorker(servPort);// create an isolated worker with the given port value
                iworker.setIWworkflow(true);
            }
            
            new Thread(iworker).start();
        } else if (cloudburstNodeType.equals(MPIConstants.CloudburstNodeType_BG)){
            if (System.getProperty("IsolatedProxy")==null) {
                System.err.println("IsolatedProxy in comet.properties should be specified");
                System.exit(1);
            }

            BlueGeneIsolatedWorker iworker = new BlueGeneIsolatedWorker(servPort,partitionID, workerID,systemType,systemQueue, systemMode);// create an isolated worker with the given port and partition ID values√ü
            new Thread(iworker).start();
        } else if (cloudburstNodeType.equals(MPIConstants.CloudburstNodeType_MPI)){
            try {
                //receive MPINode type (MPIFrontEnd|MPIWorker)
                ServerSocket server = new ServerSocket(ControlConstant.MPINODE_LISTENER_PORT);
                server.setReuseAddress(true);
                Socket client = server.accept();
                DataInputStream input = new DataInputStream(client.getInputStream());
                DataOutputStream output = new DataOutputStream(client.getOutputStream());
                String MPINodeType = input.readUTF();
                System.setProperty("MPINodeType", MPINodeType);
                output.writeUTF(MPINodeType);   //send back to confirm
                output.flush();
                input.close();
                output.close();
                client.close();
                server.close();

                if (MPINodeType.equals(MPIConstants.MPINodeType_MPIFRONTEND)) {
                    if (System.getProperty("IsolatedProxy") == null) {
                        System.err.println("IsolatedProxy in comet.properties should be specified");
                        System.exit(1);
                    }
                    MPIFrontEndNode front = new MPIFrontEndNode();
                    front.startHostlistManager();
                    //waiting for hostlist created
                    while (!front.isCreatedHostlist()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(CloudBurstStarter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    front.startMpdRing();
                    front.startMPIFrontEndNode();
                    front.addListener();
                } else if (MPINodeType.equals(MPIConstants.MPINodeType_MPIWORKER)) {
                    MPIWorker mpiworker = new MPIWorker();
                    new Thread(mpiworker).start();
                } else if (MPINodeType.equals(MPIConstants.MPINodeType_ISOLATED)) {
                    IsolatedWorker iworker = new IsolatedWorker();
                    new Thread(iworker).start();
                }
            } catch (IOException ex) {
                Logger.getLogger(CloudBurstStarter.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        //for debugging
        else if (cloudburstNodeType.equals("Experiments")){
            try {
                //receive MPINode type
                ServerSocket server = new ServerSocket(ControlConstant.MPINODE_LISTENER_PORT);
                server.setReuseAddress(true);
                boolean alive = true;
                while(alive) {
                    Socket client = server.accept();
                    DataInputStream input = new DataInputStream(client.getInputStream());
                    String role = input.readUTF();
                    input.close();
                    client.close();

                    if (role.equals("start")) {
                        Runtime.getRuntime().exec("/home/gundam/SVN/startCloudburst.sh");
                    } else if (role.equals("stop")) {
                        Runtime.getRuntime().exec("killall java");
                    }
                }
                server.close();
            } catch (IOException ex) {
                Logger.getLogger(CloudBurstStarter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
