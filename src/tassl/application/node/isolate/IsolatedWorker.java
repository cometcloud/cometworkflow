/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * <<Isolated Worker Basic architecture>>
 * Isolated workers request tasks to RequestHandlerProxy because they cannot access the Comet space.
 * RequestHandlerProxy forwards it to one of RequestHandlers by round-robin fashion.
 * A RequestHandler performs 'in' to Comet space and forwards the tasks to the isolated worker directly.
 * Basically, there are one RequestHandlerProxy and possibly several RequestHandlers.
 * They can be located on any machine in secured network,
 * but different RequestHandler should be on different machine as well as
 * all RequestHandlers should join the overlay in advance.
 *
 */
package tassl.application.node.isolate;

import java.io.DataInputStream;
import java.net.UnknownHostException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.StringTokenizer;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import programming5.io.Debug;
import tassl.application.cometcloud.WorkflowMeteorGenericWorker;
import tassl.automate.network.ControlConstant;
import tassl.automate.network.MessageType;
import tassl.automate.application.network.SendClient;
import tassl.automate.application.network.TCPClient;
import tassl.automate.application.network.SSLClient;
import tassl.automate.application.node.isolate.TaskListener;
import tassl.automate.application.node.isolate.TaskListenerInterface;
import tassl.automate.application.node.isolate.scalablehandler.HistoryQueue;
import tassl.automate.comet.CometConstants;
import tassl.automate.comet.XmlTuple;
import tassl.automate.comet.xmltuplespace.XmlTupleService;
import tassl.automate.programmodel.masterworker.WorkerFramework;
import tassl.automate.util.Log;

//THIS SHOULD REPLACE THE ORIGINAL WITHOUT PROBLEMS
// THE ONLY THING TO DO IS ADD IN THE REQUEST HANDLERS A writeUTF("RHMSG_TASK") 
//     BEFORE writeIng(ControlConstant.RHMSG_NOTASK) or writeIng(ControlConstant.RHMSG_TASK)

/**
 *
 * @author hyunjoo
 */
public class IsolatedWorker extends Thread implements TaskListenerInterface {

    private static InetAddress destAddr = null;
    private static int destPort;
    private static DataOutputStream output = null;
    
    private static String myName = null;
    protected static boolean ALIVE = true;
    private static int numTry = 0;
    private static long stime_task, etime_task;
    private long runtime = 0;
    private int consumed_tasks = 0;
    private static WorkerFramework worker;
    private int taskWaitPeriod;
    private int servPort;//Moustafa - used to store the value of the task listener server port (in case of multiple isolated worker running on the same machine)
    private String baseDir = null; //Moustafa - added to pass base dir to save log files in (for starting workers remotely)
    //Javi: control number of times doesn't receive task when asked
    private int noTaskCountMax = 1;
    private int noTaskCount = 0;
    private String partitionID = null;//Moustafa - added to pass partition ID or corner ID to the isolated worker on blue gene

    //for scalable handler
    private static Log explog = null;
    private static String myFile = null;
    private static HistoryQueue overhead = null;
    private static HistoryQueue compute = null;
    public static long compute_time = 0;
    
    private boolean IWworkflow=false;
    private boolean processingTask=false;
    private final Object Lock=new Object();  
    public IsolatedWorker() {
        servPort = ControlConstant.IWORKER_LISTENER_PORT;//Moustafa - default port value
    }

    public IsolatedWorker(int port) {
        servPort = port;//Moustafa - set the port value given by user
    }
    
    public IsolatedWorker(int port, String ip) {
        servPort = port;//Moustafa - set the port value given by user
        myName=ip;   //Javier - for machines where the public name is not properly configured
    }

    public IsolatedWorker(int port, String ip, String worker_base_dir, String partitionid) {
        servPort = port;//Moustafa - set the port value given by user
        baseDir = worker_base_dir; //Moustafa - set the worker directory given by the user
        myName = ip;   //Javier - for machines where the public name is not properly configured
        partitionID = partitionid;//Moustafa - pass the partition parameter to the worker

    }

    public void setIWworkflow(boolean IWworkflow) {
        this.IWworkflow = IWworkflow;
    }
    
    private void setLog(String filename) {
        FileHandler fh;
        try {
            fh = new FileHandler(filename, true);

            Logger l = Logger.getLogger(""); //Everyone will print here. Create new logger if different file wanted

            for (Handler i : l.getHandlers()) {
                l.removeHandler(i);
            }

            l.setUseParentHandlers(false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.ALL);

        } catch (SecurityException e) {
            System.out.println("ERROR: AgentLite: Setting up log");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("ERROR: AgentLite: Setting up log");
            e.printStackTrace();
        }
    }
    @Override
    public void run() {        
        if (System.getProperty("log","false").equals("true")){
            String dir= System.getProperty("WorkingDir");            
            if(dir!=null){
                setLog(dir+"/IsolatedWorker_"+servPort+".log");
            }else{
                setLog("IsolatedWorker_"+servPort+".log");
            }
//Moustafa added baseDir to be set by the user separately
            if (baseDir != null) {
                setLog(baseDir + System.getProperty("file.separator") + "IsolatedWorker_" + servPort + ".log");
            } else {
                setLog("IsolatedWorker_" + servPort + ".log");
            }
        }
        
        //manage history queue for scalable handler
        overhead = new HistoryQueue(100);
        compute = new HistoryQueue(100);

        //load worker
        loadWorker();

        //set task polling period
        taskWaitPeriod = Integer.parseInt(System.getProperty("TaskWaitPeriod", "3000"));
        noTaskCountMax = Integer.parseInt(System.getProperty("IsolatedMaxNoTaskRetries", "100"));
        
        //get proxy address from comet.properties
        String proxyAddr = System.getProperty("IsolatedProxy");

        //in case of multiple proxies
        StringTokenizer st = new StringTokenizer(proxyAddr, ";");
        String proxyAddrs[] = new String[st.countTokens()];
        int numProxy = 0;
        while (st.hasMoreTokens()) {
            String token = (String) st.nextToken();
            proxyAddrs[numProxy++] = token;
            System.out.println(token);
        }
        proxyAddr = proxyAddrs[0];

        if (proxyAddr == null) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, "ERROR: Proxy is not set. Leaving now...");
            System.exit(1);
        }

        try {            
            //set proxy address and port
            destAddr = InetAddress.getByName(proxyAddr); // Destination address
            String tempPort=System.getProperty("IsolatedProxyPort").trim();
            if (tempPort!=null){
                destPort=Integer.parseInt(tempPort);
            }else{
                destPort = ControlConstant.PROXY_PORT;   // Destination port
            }

            if (myName == null){
                //get myName
                try{
                    String uri = System.getProperty("chord.LOCAL_URI", "//" + InetAddress.getLocalHost().getHostAddress());
                    String[] localURI = uri.replace("//", "").split(":");
                    myName = localURI[0];
                }catch(UnknownHostException e){
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "Unknown host - setting hostname to localhost. "
                            + "Start Isolated worker with parameter -publicIp to indicate its public IP.");
                    myName="localhost";
                }                
            }

            System.out.println(myName + ": destAddr=" + destAddr.getHostAddress() + " destPort=" + destPort);

        } catch (UnknownHostException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, "ERROR: Unknown host:"+destAddr,ex);
        }

        if (Debug.isEnabled("ScalableHandler")) {
            myFile = "IWoverhead_" + myName + "_" + servPort + ".csv";
            explog = new Log(myFile, false);
        }
        
        ServerSocket servSock;
        
        try{
            servSock = new ServerSocket(servPort);
            servSock.setReuseAddress(true);            
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        DataInputStream input = null;
        DataOutputStream output=null;
        //request task for first time
        RequestTaskClass taskReq=new RequestTaskClass();
        Thread request=new Thread(taskReq);
        request.start();
        Thread computing=null; //we could interrupt the comptutation using this.

        while (ALIVE) {                
            try {
                Socket clientSocket = servSock.accept();

                input = new DataInputStream(clientSocket.getInputStream());
                output = new DataOutputStream(clientSocket.getOutputStream());
                String command = input.readUTF();
                Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "command: " + command);
                //task to be computed
                if (command.equals("RHMSG_TASK")) {
                    if (!processingTask){ //we can replace this with a counter to allow multiple workers.
                        //Start new thread 
                        synchronized(Lock){
                            processingTask=true;
                        }
                        ComputeTaskClass computeThread=new ComputeTaskClass(input);
                        computing=new Thread(computeThread);
                        computing.start();
                        synchronized(Lock){
                            //waits until it is done with the inputStream
                            try {
                                Lock.wait();
                            } catch (InterruptedException ex) {
                                Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }else{
                        Logger.getLogger(IsolatedWorker.class.getName()).log(Level.WARNING, "A task is already being executed");
                    }
                }else if(command.equals("ChangeQuery")){//this has to be specific of the Generic class
                    
                    String type=input.readUTF();
                    if (IWworkflow){
                        if(type.equals("taskids")){
                            String tasks=input.readUTF();
                            ((WorkflowMeteorGenericWorker)worker).setTaskids(tasks);
                            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "Changing query to "+tasks);
                        }
                    }
                    
                    //wake up the worker if it was sleeping
                    synchronized(Lock){
                        if(processingTask==false){
                            requestTask();
                        }
                    }
                    //makes sure it is alive
                    ALIVE=true;
                    
                }else if(command.equals("RequestTask")){//when the Worker is sleeping                    
                    requestTask();
                }else if(command.equals("TerminateWorker")){//when the Worker is sleeping                    
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "Kill signal received.");
                    ALIVE=false;
                    String force=input.readUTF();
                    if(force.equals("true")){
                        if(computing!=null && computing.isAlive()){
                            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "Forcing the Kill. This will interrupt any computation.");
                            computing.interrupt();
                        }
                    }else{
                        Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "The Worker will die after finishing the current task.");
                    }
                }else if(command.equals("CheckStatus")){
                    output.writeUTF("OK");
                }else{
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.WARNING, "Wrong command: "+command);
                }

                //time for task
                stime_task = System.currentTimeMillis();          
            } catch (IOException ex) {
                Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                if(input!=null){
                    try {
                        input.close();
                    } catch (IOException ex) {
                        Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(output!=null){
                    try {
                        output.close();
                    } catch (IOException ex) {
                        Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        

        if (Debug.isEnabled("ScalableHandler")) {
            explog.closeLog();
        }
        
        System.out.println("isolated worker terminated");
    }

    public void loadWorker() {
        try {
            Class workerclass = Class.forName(System.getProperty("WorkerClass"));
            worker = (WorkerFramework) workerclass.newInstance();  
            
            if (IWworkflow){
                ((WorkflowMeteorGenericWorker)worker).loadParams();
                ((WorkflowMeteorGenericWorker)worker).setPeerIP(myName); //set public ip of worker. to prevent having localhost
            }
            
            System.out.println("isolated worker loads " + System.getProperty("WorkerClass") + " " + worker);
        } catch (InstantiationException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, "InstantiationException: ERROR in load WorkerClass",ex);
            System.err.println("InstantiationException: ERROR in load WorkerClass");
            System.exit(1);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, "IllegalAccessException: ERROR in load WorkerClass",ex);
            System.err.println("IllegalAccessException: ERROR in load WorkerClass"); System.exit(1);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, "ClassNotFoundException: ERROR in load WorkerClass",ex);
            System.err.println("ClassNotFoundException: ERROR in load WorkerClass"); System.exit(1);
        }
    }

    public WorkerFramework getWorker() {
        return worker;
    }
    
    private class RequestTaskClass implements Runnable{        
        //first time we request a task we need to wait a bit to make sure that the socket is listening before getting a task
        public void run() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
            requestTask();
        }
    }
    
    public void requestTask(){
        DataOutputStream output=null;
        try {
            System.out.println("requestTask. Address to contact "+destAddr.getHostAddress()+ ":"+destPort);
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "requestTask. Address to contact "+destAddr.getHostAddress()+ ":"+destPort);
            Socket sock = new Socket(destAddr, destPort);
            output = new DataOutputStream(sock.getOutputStream());

            output.writeUTF("WorkerReqTask");   //for scalable handler
            output.writeUTF(myName);
            output.writeInt(servPort);//Moustafa - writes the task listener server port

            //**** added smart task request comparing network overhead and computation time ****//
            //if average compute time is less than average overhead, ask for more tasks
            int numTasks = 1;
            //ask for more tasks only if you have a history of at least 10 tasks.
            if (Boolean.parseBoolean(System.getProperty("DynamicRequestHandler", "false"))
                    && overhead.getSize() > 10 && compute.getSize() > 10 && compute.getAverage() < overhead.getAverage()) {
                if (compute.getAverage() != 0) {
                    numTasks = overhead.getAverage() / compute.getAverage();
                } else if (overhead.getAverage() > 0) {
                    numTasks = overhead.getAverage();
                }
                if (Debug.isEnabled("ScalableHandler")) {
                    explog.writeln("Compute average is: " + compute.getAverage() + " *** Overhead average is: " + overhead.getAverage() + " requesting for " + numTasks + " tasks");
                    explog.flush();
                }
            }
            //System.out.println("Compute average is: " + compute.getAverage() + " *** Overhead average is: " + overhead.getAverage()+ " requesting for " + numTasks + " tasks");
            output.writeInt(numTasks);
            //**********************************************************************************//

            try{
                String tempTemplate=worker.getTemplateQuery().getstring();                                
                output.writeUTF(tempTemplate);  //Javier - the isolated worker can specify the tasks that he is interested on
            }catch(Exception e){
                System.out.println("Task Tuple query template not found, a default one will be provided");
                output.writeUTF("NULL");
            }

            output.flush();

        } catch (Exception ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null,ex);
        }finally{
            try {
                if(output!=null){
                    //close connection after sending a task request to RequestHandlerProxy
                    output.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    

    
    private class ComputeTaskClass implements Runnable{
        DataInputStream in;
        public ComputeTaskClass(DataInputStream in){
            this.in=in;
        }
        public void run() {
            Object receivedObj = customizedReadStream(in); 
            
            //let the main loop to continue accepting commands
            synchronized (Lock) {                
                Lock.notify();
            }            
            //work with the task
            if (receivedObj != null) {                
                customizedProcess(receivedObj);
            }            
            synchronized(Lock){
                processingTask=false;
            }
            requestTask();
        }
    }

    @Override
    public Object customizedReadStream(DataInputStream in) {
        try {
            if (in.readInt() == ControlConstant.RHMSG_NOTASK) {
                in.close();
                try {
                    //System.out.println("waiting for a task.. try " + numTry);
                    Thread.sleep(taskWaitPeriod);
                } catch (InterruptedException ex) {
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }

            numTry = 0; //reset
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (Debug.isEnabled("IsolatedLogging")) {
            Log explog = new Log("IWoverhead.txt", true);
            explog.write("TaskReceivingTime " + (System.currentTimeMillis() - stime_task) + System.getProperty("line.separator"));
            explog.closeLog();
        }

        long e = System.currentTimeMillis() - stime_task;
        overhead.add((int) e);
        if (Debug.isEnabled("ScalableHandler")) {
            explog.write("TaskReceivingTime " + e + System.getProperty("line.separator"));
            explog.flush();
        }

        Object receivedObject = null;
        try {
            long stime = System.currentTimeMillis();
            int length = in.readInt(); //data length
            byte[] bytesdata = new byte[length];
            in.readFully(bytesdata);
            receivedObject = programming5.io.Serializer.deserialize(bytesdata);
                                   
            long etime = System.currentTimeMillis();
            this.runtime = etime - stime;    //data transfer in time
            
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{   
            String tupleString=in.readUTF();// template. 
            
            int taskId=-1;
            if(receivedObject!=null){
                //assume we a one tuple only. 
                XmlTuple taskTuple = (XmlTuple)((List)receivedObject).get(0);                                        
                taskId=Integer.parseInt((String) taskTuple.getTupleFields().get(CometConstants.TaskId));
            
            }
            Long startTime=in.readLong();

            System.setProperty("startTime"+taskId, startTime.toString());
            
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return receivedObject;
    }

    @Override
    public void customizedProcess(Object receivedObject) {
        List taskList;
        taskList = (List) receivedObject;
        if (Debug.isEnabled("ScalableHandler")) {
            explog.writeln("Tasks Received =\t" + taskList.size());
            explog.flush();
        }
        System.out.println("IsolatedWorker:size of task list: " +taskList.size());
        
        ListIterator taskCount = taskList.listIterator();
        XmlTuple task = null;
        while (taskCount.hasNext()) {
            task = (XmlTuple) taskCount.next();
//Moustafa partitionID is used for Isolated workers on supercomputers e.g. bluegene
//            if(partitionID!=null){
//                task.set("partitionID",partitionID);
//            }
            //if (task != null) task.PrintXMLTuple();            
            consumeTask(task);            
        }
        
        //To avoid block when no tasks received
        if (taskList.isEmpty()){
            System.out.println("IsolatedWorker: No Tasks returned in previous call MORE tasks");
            //ready for next task
            synchronized (this) {
                if (noTaskCount >= noTaskCountMax) {
                    ALIVE = false;
                } else {
                    noTaskCount++;
                }
                try {
                    this.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        ////////////
        
        //System.out.println("IsolatedWorker:end customizedprocess");
    }

    public void consumeTask(XmlTuple task) {
        try {
            //work with the task

            long stime, etime;
            Object obj = null;
            if (task.getData().length > 1) {
                obj = programming5.io.Serializer.deserialize(task.getData());
            }
            if (obj != null && obj.toString().equals("poisonpill") == true) {
                //notify master its leave
                System.out.println("get poison pill");
                Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO,"get poison pill");
                SendClient client = getClient();
                String MasterNetName = (String) task.getTupleFields().get(CometConstants.MasterNetName);
                if (MasterNetName == null) {
                    System.out.println("cannot send message to master : MasterNetName is null");
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "cannot send message to master : MasterNetName is null");
                } else {
                    client.sendMsgToMaster(myName, MessageType.Worker_leave, null, MasterNetName);
                    System.out.println("send master its leave");
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, "send master its leave");
                    this.runtime = 0;
                }
                //stop isolated worker
                ALIVE = false;
            } else {
                int taskid = new Integer((String) task.getTupleFields().get(CometConstants.TaskId));

                //compute task
                stime = System.currentTimeMillis();
                Object result = worker.computeTask(task); //pass the whole task

                //**** added smart task request comparing network overhead and computation time ****//
                compute_time = System.currentTimeMillis() - stime;
                compute.add((int) compute_time);
                //**********************************************************************************//

                //send result to master
                String MasterNetName = (String) task.getTupleFields().get(CometConstants.MasterNetName);
                if (MasterNetName == null) {
                    String comment="ERROR: cannot send message to master : MasterNetName is null";
                    System.err.println(comment);
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.WARNING, comment);
                } else {
                    etime_task = System.currentTimeMillis();
                    etime = System.currentTimeMillis();
                    String msg = "worker-task " + etime_task + " " + stime_task + " " + (etime_task - stime_task) + " " + (etime - stime);
                    worker.sendResultToMaster(taskid, result, msg, MasterNetName);
                }
                consumed_tasks++;
                if (consumed_tasks % 20 == 0) {
                    String comment="consumed number of tasks = " + consumed_tasks + " taskid = " + task.getTupleFields().get(CometConstants.TaskId);
                    System.out.println(comment);
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.INFO, comment);
                }
            }
            //ready for next task
            synchronized (this) {
                noTaskCount=0;
                
            }
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private SendClient getClient(){
	SendClient ret;
        if(System.getProperty("chord.NETWORK_CLASS", "TCP").equals("SSL")){
            ret = new SSLClient();
            //System.out.println("getClient: Created a SSL client");
        }
        else{
            ret = new TCPClient();
            //System.out.println("getClient: Created a TCP client");
        }
        return ret;
    }
    
    
    
}
