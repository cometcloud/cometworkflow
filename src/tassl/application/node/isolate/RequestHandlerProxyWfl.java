/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided 
 * that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and 
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its 
 * contributors may be used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
package tassl.application.node.isolate;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import programming5.io.ArgHandler;
import programming5.io.FileHandler;
import tassl.application.node.isolate.scalablehandler.RequestHandlerProxyMonitorWfl;
import tassl.automate.agent.CloudBridgingConstants;
import tassl.automate.application.node.isolate.scalablehandler.HistoryQueue;
import tassl.automate.comet.CometConstants;
import tassl.automate.comet.XmlTuple;
import tassl.automate.comet.tuplespace.TSConstant;
import tassl.automate.comet.xmltuplespace.XmlTupleService;
import tassl.automate.network.ControlConstant;
import tassl.automate.programmodel.masterworker.TaskFramework;
import tassl.automate.util.Log;

/**
 *
 * @author hyunjoo
 */
public class RequestHandlerProxyWfl extends Thread {

    private static InetAddress destAddr = null;
    private static int destPort;
    private static ServerSocket servSock = null;
    private static Socket clientSocket = null;
    private static DataOutputStream output = null;
    private static DataInputStream input = null;
    private static boolean FLAG = true;
    //public static String[] requestHandler;
    public static ArrayList requestHandler;
    //private static int[] requestHandlerTaskCount;
    public static int numReqHandler, indReqHandler = 0, totalReqHandler = 0;
    private static int msgcount = 0;
    private static String MasterNetName;
    public static HistoryQueue workerReqRateQueue = null;
    private static Hashtable<String, Integer> poisonpill = new Hashtable<String, Integer>();
    private static Log explog = null;
    private static RequestHandlerProxyMonitorWfl proxyMon = null;

    public void RequestHandlerProxy() {
    }

    @Override
    public void run() {
        destPort = Integer.parseInt(System.getProperty("REQUEST_HANDLER_PORT",ControlConstant.REQUEST_HANDLER_PORT).trim());        
        //load RequestHandlerList
        if (FileHandler.fileExists(ControlConstant.RequestHandlerList)) {
            try {
                FileHandler fh = new FileHandler(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);
                int count = 0;
                while (fh.readLine() != null) {
                    count++;
                }
                numReqHandler = count;
                requestHandler = new ArrayList(numReqHandler);
                if (numReqHandler < 1) {
                    System.out.println("Request handlers are not specified in the RequestHandlerList. Quitting proxy.");
                    System.exit(0);
                }
                CometConstants.loadProperties("comet.properties");
                if (System.getProperty("DynamicRequestHandler", "false").trim().equalsIgnoreCase("true")) {
                    numReqHandler = 1;
                    fh.setFile(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);
                    count = 0;
                    String rhName = null;
                    //read the first default RH
                    while ((rhName = fh.readLine()) != null && count == 0) {
                        requestHandler.add(rhName);
                        count++;
                    }
                    
                    proxyMon = new RequestHandlerProxyMonitorWfl(this);
                    proxyMon.start();
                    
                } else {
                    //read all RH in the file
                    count = 0;
                    String rhName = null;
                    fh.setFile(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);
                    while ((rhName = fh.readLine()) != null) {
                        requestHandler.add(rhName);
                        count++;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(RequestHandlerProxyWfl.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else { // if file does not exist.
            System.out.println("RequestHandlerList not specified. Create a file named RequestHandlerList and enter the names of the Request Handlers separated by a new line.\nThen runt he proxy again.");
            System.exit(0);
        }
       
        
        
        int proxyPort=ControlConstant.PROXY_PORT; //default port
        String tempPort=System.getProperty("IsolatedProxyPort");
        if (tempPort!=null && !tempPort.isEmpty()){
            proxyPort=Integer.parseInt(tempPort.trim());
        }
        
        //set server socket
        try {            
            servSock = new ServerSocket(proxyPort);
            servSock.setReuseAddress(true);
            System.out.println("Proxy is launching at " + InetAddress.getLocalHost().getHostName() + "(" + InetAddress.getLocalHost().getHostAddress() + "):" + proxyPort);
        } catch (IOException e) {
            System.err.println("Error listening on port " + proxyPort);
            e.printStackTrace();
            closeSockets();
            return;
        }

        explog = new Log("ProxyLog.csv", true);
        workerReqRateQueue = new HistoryQueue(10);
        long _currSec = System.currentTimeMillis() / 1000;
        int _currFreq = 0;

        while (FLAG) {
            try {
                try {
                    clientSocket = servSock.accept();                    
                    //calculate proxy request rate
                    long e = System.currentTimeMillis() / 1000;
                    if (_currSec == e) {
                        _currFreq++;
                    } else {
                        workerReqRateQueue.add(_currFreq);
                        explog.writeln(System.currentTimeMillis() + "\t***Current woker request rate is:\t" + _currFreq);
                        _currSec = e;
                        _currFreq = 1;
                    }

                } catch (IOException ex) {
                    Logger.getLogger(RequestHandlerProxyWfl.class.getName()).log(Level.SEVERE, null, ex);
                    if (FLAG == true) {
                        System.err.println("Error recieving connection on port " + proxyPort + ex.getMessage());
                    }
                    closeSockets();
                    return;
                }
                input = new DataInputStream(clientSocket.getInputStream());                
                String messageType = input.readUTF();
                String arg2 = input.readUTF();
                int arg3 = input.readInt();

                if (messageType.equals("WorkerReqTask")) {
                    explog.writeln("" + System.currentTimeMillis() + "\tReceived request for task from\t" + arg2 + ":" + arg3);
                    explog.flush();
                    int numTasks = input.readInt();
                    String taskTupleTemplate=input.readUTF();   
                    String worker=input.readUTF();
                    // received message from the isolated worker
                    forwardRequest(arg2, arg3, numTasks, taskTupleTemplate,worker);
                }

            } catch (IOException ex) {
                Logger.getLogger(RequestHandlerProxyWfl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }// while ends

        explog.closeLog();
    }

    private static void forwardRequest(String senderAddr, int senderPort, int numTasks, String taskTupleTemplate,String worker) {
        try {
            /* Put poisonpills into a specific cloud:
            Handling autonomic cloudbursts for multiple clouds:
            message from scheduling agent for poisonpill
            instead of putting poisonpills into comet space,
            proxy puts them to specified cloud
             */
            if (senderAddr.equals(CloudBridgingConstants.CloudBridgingScheduler) == true) {
                String shrinkCloudID = input.readUTF();
                int shrinkNodes = input.readInt();
                if (poisonpill.get(shrinkCloudID) == null) {
                    poisonpill.put(shrinkCloudID, shrinkNodes);
                } else {
                    poisonpill.put(shrinkCloudID, poisonpill.get(shrinkCloudID) + shrinkNodes);
                }
                MasterNetName = input.readUTF();
                input.close();
                System.out.println("get poisonpill-->" + shrinkCloudID + " " + shrinkNodes);
                return;
            }

            String[] sender = InetAddress.getByName(senderAddr).getHostAddress().split("\\.");
            //System.out.println("proxy] sender="+InetAddress.getByName(senderAddr).getHostName());
            if (poisonpill.get(sender[sender.length - 2]) != null
                    && poisonpill.get(sender[sender.length - 2]) > 0) {
                XmlTuple task = new XmlTupleService();
                byte[] DataArr = programming5.io.Serializer.serializeBytes("poisonpill");
                task.setData(DataArr);

                Class taskclass = Class.forName(System.getProperty("TaskClass"));
                TaskFramework tf = (TaskFramework) taskclass.newInstance();
                XmlTuple template = tf.getQuery();

                String[] tags = template.getkeys();
                for (int i = 1; i < tags.length; i += 2) {
                    if (tags[i].equals(CometConstants.TaskId)) {
                        tags[i + 1] = Integer.toString(poisonpill.get(sender[sender.length - 2]));
                    } else if (tags[i].equals(CometConstants.MasterNetName)) {
                        tags[i + 1] = MasterNetName;
                    } else if (tags[i].equals(CometConstants.TaskPriority)) {
                        tags[i + 1] = TSConstant.HighPriority;
                    } else {
                        tags[i + 1] = null;
                    }
                }
                task.createXMLtuple(tags);

                List taskList = new ArrayList();
                taskList.add(task);

                //reduce poisonpill count;
                poisonpill.put(sender[sender.length - 2], poisonpill.get(sender[sender.length - 2]) - 1);
                System.out.println("send poisonpill to " + sender[sender.length - 2] + " remaining= " + poisonpill.get(sender[sender.length - 2]) + " >>>>>>>>>>>>>>>>");

            } else {
                poisonpill.put(sender[sender.length - 2], 0);

                //forward the request to RequestHandler in RoundRobin
                destAddr = InetAddress.getByName((String) requestHandler.get((indReqHandler++) % numReqHandler));

                if (msgcount % 20 == 0) {
                    //System.out.println("sender="+senderAddr+" forward to "+destAddr.getHostAddress());
                    msgcount = 0;
                } else {
                    msgcount++;
                }

                //System.out.println("Proxy Forward request to: destAddr = "+destAddr);
                int rhFound = 0, numNotFound = 0;
                Socket sock = null;
                while (rhFound != 1) {
                    try {
                        sock = new Socket(destAddr, destPort);
                        output = new DataOutputStream(sock.getOutputStream());
                        output.writeUTF("ProxyRequestsTask");
                        output.writeUTF(senderAddr);
                        output.writeInt(senderPort);//Moustafa - send the task listener server port
                        output.writeInt(numTasks);
                        output.writeUTF(taskTupleTemplate); //Javier - the isolated worker can specify the tasks that he is interested on
                        output.writeUTF(worker);  //just pass it around to control who ask what
                        output.flush();
                        rhFound = 1;
                        closeProcess();
                        System.out.println("Sending request to: " + destAddr+". senderAddr:"+senderAddr+" senderPort:"+senderPort);
                    } catch (ConnectException ex) {
                        numNotFound++;
                        if (numNotFound >= 3 * numReqHandler) {
                            System.out.println("Cannot connect to any request handler. Stop trying.");
                            //JAVI: Proxy should not die. It may be that a Isolated worker is asking for tasks when he should not.
                            closeProcess();
                            break;                             
                            //System.exit(0);
                        }
                        System.out.println("Cannot connect to " + destAddr + ":"+destPort+ ". Trying the next Request Handler");
                        destAddr = InetAddress.getByName((String) requestHandler.get((indReqHandler++) % numReqHandler));
                    }
                }
            }
        } catch (UnknownHostException ex) {
            System.err.println("Error: Invalid client: " + clientSocket.getRemoteSocketAddress());
            ex.printStackTrace();
            closeProcess();
        } catch (IOException ex) {
            System.err.println("Error getting I/O from client: " + clientSocket.getRemoteSocketAddress());
            ex.printStackTrace();
            closeProcess();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RequestHandlerProxyWfl.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            closeProcess();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
            closeProcess();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
            closeProcess();
        }
    }

    private static void closeProcess() {
        try {            
            input.close();
        } catch (Exception ex) {
            System.err.println("Error: Failed to close input buffer reader: ");
        }
        try {            
            output.close();
        } catch (Exception ex) {
            System.err.println("Error: Failed to close output buffer reader: ");
        }
    }

    private static void closeSockets() {
        try {
            clientSocket.close();            
        } catch (Exception e) {
            System.err.println("Error closing clientSocket");
        }
        try {            
            servSock.close();
        } catch (Exception e) {
            System.err.println("Error closing servSocket");
        }
    }

    public void quit() {
        FLAG = false;
        closeSockets();
        System.out.println("RequestHandlerProxy: Quiting Request Handler Proxy Monitor");
        proxyMon.quit();
    }

    public static void main(String[] args) {
        ArgHandler argHandler = new ArgHandler(args);
        Vector<String> appPropertyFiles = argHandler.getMultipleStringArg("-propertyFile");
        for (String propertyFile : appPropertyFiles) {
            CometConstants.loadProperties(propertyFile);
        }
        new RequestHandlerProxyWfl().start();
    }
}