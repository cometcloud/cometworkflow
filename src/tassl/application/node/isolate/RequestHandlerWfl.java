/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided 
 * that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and 
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its 
 * contributors may be used to endorse or promote products derived from this software without specific prior 
 * written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */
package tassl.application.node.isolate;

import java.util.Hashtable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.automate.application.node.DrtsPeer;
import tassl.automate.comet.XmlTuple;
import tassl.automate.comet.tuplespace.TSConstant;
import tassl.automate.network.ControlConstant;
import tassl.automate.programmodel.masterworker.TaskFramework;
import tassl.automate.comet.CometConstants;
import java.util.Vector;
import java.util.logging.Handler;
import java.util.logging.SimpleFormatter;
import org.apache.commons.lang.StringUtils;
import programming5.io.ArgHandler;
import programming5.io.FileHandler;
import tassl.application.cometcloud.WorkflowMaster;
import tassl.application.node.isolate.scalablehandler.RequestHandlerMonitorWfl;
import tassl.automate.application.node.isolate.CustomizedTaskSelectionInterface;
import tassl.automate.application.node.isolate.scalablehandler.DefaultRHMonitor;
import tassl.automate.application.node.isolate.scalablehandler.HistoryQueue;
import tassl.automate.comet.xmltuplespace.XmlTupleService;
import tassl.automate.util.ConfigurationFileManager;
import tassl.automate.util.Log;

/**
 *
 * @author hyunjoo
 */
public class RequestHandlerWfl extends Thread {
    // Load properties file
    static {
        String propertiesFileName = System.getProperty("CometPropertiesFile", "comet.properties");
        Properties p = new Properties(System.getProperties());
        try {
            p.load(new FileInputStream(propertiesFileName));
            System.setProperties(p);
        } catch (FileNotFoundException fnf) {
            System.out.println("No Comet properties file");
        } catch (IOException ioe) {
            System.out.println("Bad Comet properties file");
        }
        CometConstants.enableDebugMessage("comet.DEBUG");
    }

    private ServerSocket servSock = null;
    private Socket clientSocket = null;
    private DataOutputStream output = null;
    private DataInputStream input = null;
    private boolean FLAG = true;
    private DrtsPeer peer = null;
    private static String myName = null;
    private CustomizedTaskSelectionInterface custTaskSel = null;
    private XmlTuple taskTemplate = null;
    private List <XmlTuple> taskList = new ArrayList();
    private Log rhLog = null;
    private long currSec;
    private int currSecFreq = 0;
    public HistoryQueue cinQueue = null;
    public HistoryQueue proxyReqRateQueue = null;
    public RequestHandlerMonitorWfl monitor = null;
    private boolean busy = false;
    private boolean skipPrintException = false;

    public RequestHandlerWfl() {
    }

    public ServerSocket getServSock(){
        return servSock;
    }
    public RequestHandlerWfl(DrtsPeer p) {
        try {
            peer = p;
            Class taskclass = Class.forName(System.getProperty("TaskClass"));
            TaskFramework tf = (TaskFramework) taskclass.newInstance();
            taskTemplate = tf.getQuery();

            if (System.getProperty("CustomizedTaskClass") != null) {
                Class customizedTaskClass = Class.forName(System.getProperty("CustomizedTaskClass"));
                custTaskSel = (CustomizedTaskSelectionInterface) customizedTaskClass.newInstance();
                custTaskSel.initialize();
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, ex);
            closeProcess();
        } catch (InstantiationException ex) {
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, ex);
            closeProcess();
        } catch (IllegalAccessException ex) {
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, ex);
            closeProcess();
        }

        try {
            String uri = System.getProperty("chord.LOCAL_URI", "//" + InetAddress.getLocalHost().getHostAddress());
            String[] localURI = uri.replace("//", "").split(":");
            myName = localURI[0];
        } catch (UnknownHostException ex) {
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void setLog(String filename){
        java.util.logging.FileHandler fh;
        try {
            fh=new java.util.logging.FileHandler(filename, true);
            
            Logger l = Logger.getLogger(""); //Everyone will print here. Create new logger if different file wanted
            
            for (Handler i:l.getHandlers()){
                l.removeHandler(i);
            }
            
            l.setUseParentHandlers(false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.ALL);  
            
        } catch (SecurityException ex){  
            System.out.println("ERROR: WorkflowMaster: Setting up log");
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "ERROR: WorkflowMaster: Setting up log", ex);
        }catch(IOException ex){
            System.out.println("ERROR: WorkflowMaster: Setting up log");
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "ERROR: WorkflowMaster: Setting up log", ex);
        }             
    }
    
    @Override
    public void run() {
        setLog(System.getProperty("logFileRH", "RequestHandler.log"));
        Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Starting Server in port "+Integer.parseInt(System.getProperty("REQUEST_HANDLER_PORT",ControlConstant.REQUEST_HANDLER_PORT).trim())); 
        
        try {
            servSock = new ServerSocket(Integer.parseInt(System.getProperty("REQUEST_HANDLER_PORT",ControlConstant.REQUEST_HANDLER_PORT).trim()));
            servSock.setReuseAddress(true);
        } catch (IOException e) {
            System.err.println("Error listening on port " + Integer.parseInt(System.getProperty("REQUEST_HANDLER_PORT",ControlConstant.REQUEST_HANDLER_PORT).trim()));
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error listening on port " + Integer.parseInt(System.getProperty("REQUEST_HANDLER_PORT",ControlConstant.REQUEST_HANDLER_PORT).trim()), e);
            closeSockets();
            return;
        }
        
        String logname = "RHLog_" + myName + ".csv";
        rhLog = new Log(logname, false);
        rhLog.writeln(System.currentTimeMillis() + "\tStarted request handler on the port");

        int hist = 10;
        
        if (System.getProperty("DynamicRequestHandler", "false").trim().equalsIgnoreCase("true")) {
            try {
                String t = System.getProperty("CometSpaceReadRateHistory");
                if (t == null) {
                    hist = 10;
                } else {
                    hist = Integer.parseInt(t);
                }
            } catch (NumberFormatException ex) {
                hist = 10; // assign 10 by default if value not mentioned or unparseable
            }
            System.out.println("RequestHandler: Starting RequestHandlerMonitor");
            monitor = new RequestHandlerMonitorWfl(this);
            monitor.start();
        }

        cinQueue = new HistoryQueue(hist);
        currSec = System.currentTimeMillis() / 1000;
        currSecFreq = 0;

        proxyReqRateQueue = new HistoryQueue(10);
        long _currSec = System.currentTimeMillis() / 1000;
        int _currFreq = 0;

        while (FLAG) {
            try {
                busy = false;                
                clientSocket = servSock.accept();                
                busy = true;
            } 
            catch (java.net.SocketException e) {
                if (!skipPrintException){
                   Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            catch (IOException ex) {
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (FLAG){                
                rhLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tAccepted connection processing packet");
                process();
                rhLog.flush();

                //calculate proxy request rate
                long e = System.currentTimeMillis() / 1000;
                if (_currSec == e) {
                    _currFreq++;
                } else {
                    proxyReqRateQueue.add(_currFreq);
                    rhLog.writeln(System.currentTimeMillis() + "\t" + myName + "\t^^^Current proxy request rate is:\t" + _currFreq + "\t per second");
                    _currSec = e;
                    _currFreq = 1;
                }
            }
        }
    }

    private void process() {
        try {
            //get incoming message
            input = new DataInputStream(clientSocket.getInputStream());
            String messageType = input.readUTF();
            String senderAddr = input.readUTF();
            int senderPort = input.readInt();
            int numTasks = input.readInt();
            
            if (messageType.equals("GetReqHandlerTaskCount")) {
                // if proxy sends a message to get the number of tasks in the request handler
                Socket proxySock = new Socket(senderAddr, senderPort); //connect to the proxy
                output = new DataOutputStream(proxySock.getOutputStream());
                int numOfTasks = taskList.size();
                //get myName
                //System.out.println("Sending task count from " + myName);
                output.writeUTF("RequestHandlerMessage");
                output.writeUTF(myName);
                output.writeInt(numOfTasks);
                output.flush();
                proxySock.close();
            } else if (messageType.equals("ProxyRequestsTask")) {
                //System.out.println("RequestHandlerWfl:received req from proxy");
                String taskTupleTemplate=input.readUTF();
                String worker=input.readUTF();
                //get customized message
                if (custTaskSel != null) {
                    //control message
                    if (!custTaskSel.receiveCustomizedStream(input, senderAddr)) {
                        return;
                    }
                }
                List tempList = new ArrayList();
                for (int i = 0; i < numTasks; i++) {
                    readTask(senderAddr,taskTupleTemplate);
                    if (taskList.isEmpty() != true) {
                        tempList.add(taskList.remove(0));
                    }
                }
                taskList = tempList;
                                
                //send out tasks to isolated worker
                try {
                    rhLog.writeln("" + System.currentTimeMillis() + "\t" + myName + "\t Sending task to " + senderAddr + ":" + senderPort);
                    rhLog.flush();
                    //System.out.println("Sending to " + senderAddr + " and " + senderPort);
                    Socket sock = new Socket(senderAddr, senderPort); //connect to the isolated worker on the given port
                    output = new DataOutputStream(sock.getOutputStream());
                    if(taskList.isEmpty()){
                        output.writeUTF("RHMSG_NOTASK");
                        output.writeInt(numTasks);
                        output.writeUTF(taskTupleTemplate);
                        output.writeUTF(worker);                        
                    }else{
                                                                        
                        List tempTasks=new ArrayList();
                        for(XmlTuple task:taskList) {                            
                            tempTasks.add(task.getTupleFields().get("TaskId"));  //GET TASKID               
                        }
                        
                        //TODO: store the list of tasks that agent has. They will be regenerated if agent is down
                        
                        byte[] forwardObj = programming5.io.Serializer.serializeBytes(taskList);
                        output.writeUTF("RHMSG_TASK");
                        output.writeInt(forwardObj.length);                        
                        output.write(forwardObj);                        
                        output.writeUTF(taskTupleTemplate);                        
                        output.writeUTF(worker);
                        output.writeUTF(StringUtils.join(tempTasks.toArray(),","));                        
                    }
                    //System.out.println("RequestHandlerWfl sends a task "+task.getTupleFields().get(MWConstants.TaskId)+" to " + senderAddr);
                    closeProcess();
                } catch (UnknownHostException e) {                    
                    Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error: Invalid host ", e);
                    closeProcess();
                    return;
                } catch (IOException e) {                    
                    Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error getting I/O ", e);
                    closeProcess();
                    return;
                }
            }else{                
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "RequestHandler:Unknown message: {0}", messageType);
            }
        }
        catch (java.net.SocketException e) {
            if (!skipPrintException){
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        catch (IOException ex) {
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /* template to get a random task    */
    private XmlTuple generateRHTaskTemplate(XmlTuple tup) {
        String tupStr = tup.getstring();
        String[] tags = tup.getkeys();
        if (!tupStr.contains(CometConstants.TaskPriority)) {
            String[] priorityTags = new String[tags.length + 2];
            System.arraycopy(tags, 0, priorityTags, 0, tags.length);
            priorityTags[tags.length] = CometConstants.TaskPriority;
            priorityTags[tags.length] = CometConstants.TaskPriority;
            priorityTags[tags.length + 1] = "*";
            tup.createXMLtuple(priorityTags);
        } else {
            for (int i = 0; i < tags.length; i++) {
                if (tags[i].equals(CometConstants.TaskPriority) == true) {
                    tags[i + 1] = "*";
                    break;
                }
            }
            tup.createXMLtuple(tags);
        }
        
        return tup;
    }

    /* template to get a random task where a cloud domain is specified */
    private XmlTuple generateRHTaskTemplate(XmlTuple tup, int cloudID) {
        String tupStr = tup.getstring();
        String[] tags = tup.getkeys();
        if (!tupStr.contains(CometConstants.TaskPriority)) {
            String[] priorityTags = new String[tags.length + 2];
            System.arraycopy(tags, 0, priorityTags, 0, tags.length);
            priorityTags[tags.length] = CometConstants.TaskPriority;
            priorityTags[tags.length + 1] = "*";
            tup.createXMLtuple(priorityTags);
        } else {
            for (int i = 0; i < tags.length; i++) {
                if (tags[i].equals(CometConstants.TaskPriority) == true) {
                    tags[i + 1] = "*";
                    break;
                }
            }
            tup.createXMLtuple(tags);
        }

        //set cloud type
        if (tupStr.contains("CloudType")) {
            for (int i = 0; i < tags.length; i++) {
                if (tags[i].equals("CloudType")) {
                    tags[i + 1] = Integer.toString(cloudID);
                    break;
                }
            }
            tup.createXMLtuple(tags);
        }
        return tup;
    }

    private void closeProcess() {
        try {
            input.close();            
        } catch (Exception ex) {
            if (!skipPrintException){
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error: Failed to close input buffer reader: ", ex);
            }
        }
        try {            
            output.close();
        } catch (Exception ex) {
            if (!skipPrintException){
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error: Failed to close output buffer reader: ", ex);
            }
        }        
    }

    private void closeSockets() {
        try {
            servSock.close();
        } catch (Exception e) {
            if (!skipPrintException){
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error closing server Socket", e);
            }
                  
        }
        try {
            clientSocket.close();
        } catch (Exception e) {
            if (!skipPrintException){
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.SEVERE, "Error closing client Socket", e);
            }
                
        }       
        
    }

    public DrtsPeer getPeer() {
        return peer;
    }

    public void setPeer(DrtsPeer peer) {
        this.peer = peer;
    }

    public void quit() {
        FLAG = false;
        //this prevents to print SocketException when the socket are close
        skipPrintException = true; 
        closeSockets();
        closeProcess();
        if (System.getProperty("DynamicRequestHandler", "false").trim().equalsIgnoreCase("true")){
            Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.INFO, "Terminating Dynamic Request Handlers");
            System.out.println("Terminating Dynamic Request Handlers");
            monitor.quit();
        }
        
    }

    private List readTask(String senderAddr, String taskTupleTemplate) {
        //'in' to the Comet space
        XmlTuple template=null;
        if (taskTupleTemplate.equals("NULL")){
            template = generateRHTaskTemplate(taskTemplate);
        }else{
            template = new XmlTupleService();
            template.createXMLquery(taskTupleTemplate);            
        }
        
        if (custTaskSel != null) {
            template = custTaskSel.getCustomizedTemplate(peer, template, senderAddr);
        }
        XmlTuple task = null;
        if (template != null && template.getData() == null) {
            taskList = peer.getCometSpace().in(CometConstants.spacename, template, TSConstant.GET_ANY, 0, peer.getLocalPeerID(), peer.getOverlay());
            long e = System.currentTimeMillis() / 1000;
            if (currSec == e) {
                currSecFreq++;
            } else {
                cinQueue.add(currSecFreq);
                rhLog.writeln("" + System.currentTimeMillis() + "\t" + myName + "\t***Current cin freq is:\t\t" + currSecFreq + "\t\taverage is:\t\t" + cinQueue.getAverage());
                currSecFreq = 1;
                currSec = e;
            }

            //JAVI: What is this code for??
            /*
            ListIterator taskCount = taskList.listIterator();
            int i = 0;
            while (taskCount.hasNext()) {                
                task = (XmlTuple) taskCount.next();  
                task.getid();
                i++;
            }
            */
        } else if (template != null && template.getData() != null) {
            System.out.println("inserting poison pill ");
            task = template;
            taskList.add(template);
        } else {
            if (template == null) {
                System.out.println("RequestHandler: template is null");
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.INFO, "RequestHandler: template is null");
            } else {
                System.out.println("RequestHandler: some other error");
                Logger.getLogger(RequestHandlerWfl.class.getName()).log(Level.INFO, "RequestHandler: some other error");
            }
        }
        return taskList;
    }

    public void killSelf() {
        while (busy == true) {
        }
        System.exit(0);
    }

    public static void main(String[] args) {
        ArgHandler argHandler = new ArgHandler(args);
        Vector<String> propertyFiles = argHandler.getMultipleStringArg("-propertyFile");
        String[] arrayPropertyFiles = propertyFiles.toArray(new String[]{});
        Hashtable<String, String> properties = ConfigurationFileManager.parsePropertyFiles(arrayPropertyFiles);

        System.out.println("Local bootstrap is: " + properties.get("chord.LOCAL_BOOTSTRAP"));
        String localBootStrap = properties.get("chord.LOCAL_BOOTSTRAP");
        if (localBootStrap == null) {
            //if local bootstrap not specified,
            //read the default RH from comet.properties
            //and set local bootstrap to that.
            if (FileHandler.fileExists(ControlConstant.RequestHandlerList)) {
                try {
                    FileHandler fh = new FileHandler(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);
                    int count = 0;
                    while (fh.readLine() != null) {
                        count++;
                    }
                    fh.setFile(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);

                    String rhName = null;
                    if ((rhName = fh.readLine()) != null) {
                        String defaultRH = rhName;
                        properties.put("chord.LOCAL_BOOTSTRAP", defaultRH);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(DefaultRHMonitor.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("Request handler list not specified. Exiting.");
                System.exit(0);
            }
        }

        DrtsPeer peer = null;
        peer = new DrtsPeer();

        RequestHandlerWfl rh = new RequestHandlerWfl(peer);
        rh.start();
    }
}
