/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.application.node.isolate.scalablehandler;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.node.isolate.RequestHandlerProxyWfl;
import tassl.automate.network.ControlConstant;

/**
 *
 * @author Nilu
 */
public class RequestHandlerProxyMonitorWfl extends Thread {

    RequestHandlerProxyWfl proxy = null;
    public boolean FLAG=true;
    Socket cliSock;
    ServerSocket serv = null;
    
    public RequestHandlerProxyMonitorWfl(RequestHandlerProxyWfl p) {
        proxy = p;
    }

    @Override
    public void run() {        
        try {
            serv = new ServerSocket(ControlConstant.PROXY_MONITOR_PORT);
        } catch (IOException ex) {
            Logger.getLogger(RequestHandlerProxyMonitorWfl.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (FLAG) {
            try {
                cliSock = serv.accept();
                DataInputStream input = new DataInputStream(cliSock.getInputStream());
                String messageType = input.readUTF();
                String arg2 = input.readUTF();
                int arg3 = input.readInt();
                if (messageType.equals("AddRequestHandler")) {
                    System.out.println("Adding request handler.");
                    synchronized (proxy) {
                        RequestHandlerProxyWfl.requestHandler.add(arg2);
                        RequestHandlerProxyWfl.numReqHandler++;
                    }
                    System.out.println("Current Request Handlers:");
                    for (int i = 0; i < RequestHandlerProxyWfl.numReqHandler; i++) {
                        System.out.println(RequestHandlerProxyWfl.requestHandler.get(i));
                    }
                } else if (messageType.equals("RemoveRequestHandler")) {
                    System.out.println("Removing request handler.");
                    synchronized (proxy) {
                        RequestHandlerProxyWfl.requestHandler.remove(arg2);
                        RequestHandlerProxyWfl.numReqHandler--;
                    }
                    System.out.println("Current Request Handlers:");
                    for (int i = 0; i < RequestHandlerProxyWfl.numReqHandler; i++) {
                        System.out.println(RequestHandlerProxyWfl.requestHandler.get(i));
                    }
                }
            } catch (IOException ex) {
                if (FLAG==true){
                    System.err.println("RequestHandlerProxyMonitor: "+ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }
    }

    public void closeSockets(){
        try {
            serv.close();         
        } catch (Exception e) {
            System.err.println("Error closing server Socket");
        }
        try {            
            cliSock.close();
        } catch (Exception e) {
            System.err.println("Error closing client Socket");
        }
    }   

        
    public void quit(){
        FLAG=false;
        closeSockets();
    }
    
}
