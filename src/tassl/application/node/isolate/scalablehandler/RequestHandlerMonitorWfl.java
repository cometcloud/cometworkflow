/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.application.node.isolate.scalablehandler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import programming5.io.FileHandler;
import tassl.application.node.isolate.RequestHandlerWfl;
import tassl.automate.application.node.isolate.scalablehandler.DefaultRHMonitor;
import tassl.automate.network.ControlConstant;
import tassl.automate.util.Log;

/**
 *
 * @author Nilu
 */
public class RequestHandlerMonitorWfl extends Thread {

    String defaultRH = null;
    RequestHandlerWfl myRH = null;
    private String myName = null;
    private ArrayList RHList = null;
    Log rhmLog = null;
    private boolean FLAG=true;
    DefaultRHMonitor rhmonitor=null;

    public RequestHandlerMonitorWfl(RequestHandlerWfl rh) {
        myRH = rh;
        try {
            myName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(RequestHandlerMonitorWfl.class.getName()).log(Level.SEVERE, null, ex);
        }

        //find default RH
        if (FileHandler.fileExists(ControlConstant.RequestHandlerList)) {
            try {
                FileHandler fh = new FileHandler(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);
                int count = 0;
                while (fh.readLine() != null) {
                    count++;
                }
                RHList = new ArrayList(count);
                fh.setFile(ControlConstant.RequestHandlerList, FileHandler.HandleMode.READ);
                String rhName = null;
                while ((rhName = fh.readLine()) != null) {
                    RHList.add(rhName);
                }
            } catch (IOException ex) {
                Logger.getLogger(DefaultRHMonitor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("Request handler list not specified. Exiting.");
            System.exit(0);
        }
        defaultRH = (String) RHList.get(0);

        System.out.println("Default rh is: " + defaultRH + "My name is : " + myName);

        //create log        
        String logname = "RHM_" + myName + ".csv";
        rhmLog = new Log(logname, true);
        rhmLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tReq handler monitor main done.");
    }

    @Override
    public void run() {

        rhmLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tStarting RequestHandler Monitor");

        //start default RH monitor if this is the default request handler
        if (myName.equals(defaultRH)) {
            rhmonitor = new DefaultRHMonitor();
            rhmonitor.start();
            System.out.println("\tStarting default RH monitor");
            rhmLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tStarting default RH monitor");
        }

        int upperThreshold = Integer.parseInt(System.getProperty("CometSpaceReadRateUpperThreshold"));
        int lowerThreshold = Integer.parseInt(System.getProperty("CometSpaceReadRateLowerThreshold"));
        rhmLog.flush();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(RequestHandlerMonitorWfl.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (FLAG) {
            try {                
                if (myRH.cinQueue.getAverage() >= upperThreshold) {
                    //System.out.println(myName + "  Detected above");
                    rhmLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tDetected above threshold");
                    rhmLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tSending Above threshold message to " + defaultRH);
                    rhmLog.flush();
                    //if above or below threshold send appropriate message to default monitor (on defaultRH)
                    Socket cliSock = new Socket(InetAddress.getByName(defaultRH), ControlConstant.DEFAULT_RH_MONITOR_PORT);
                    DataOutputStream out = new DataOutputStream(cliSock.getOutputStream());
                    out.writeUTF("AboveThreshold");
                    out.writeUTF(myName);
                    out.flush();
                    out.close();
                    cliSock.close();
                    rhmLog.writeln(System.currentTimeMillis() + "\t" + myName + "\tSent message to default monitor");
                    rhmLog.flush();

                }
                if (myRH.cinQueue.getAverage() <= lowerThreshold) {
                    if (!myName.equalsIgnoreCase(defaultRH)) {
                        //  System.out.println(myName + "  Detected below. ask kill?");
                        Socket cliSock = new Socket(InetAddress.getByName(defaultRH), ControlConstant.DEFAULT_RH_MONITOR_PORT);
                        DataOutputStream out = new DataOutputStream(cliSock.getOutputStream());
                        out.writeUTF("BelowThreshold");
                        out.writeUTF(myName);
                        out.flush();
                        out.close();
                        cliSock.close();
                        ServerSocket tempSock = new ServerSocket(ControlConstant.RH_MONITOR_PORT);
                        Socket sock = tempSock.accept();
                        DataInputStream in = new DataInputStream(sock.getInputStream());
                        String ans = in.readUTF();
                        if (ans.equalsIgnoreCase("yes") && !myName.equals(defaultRH)) {
                            myRH.killSelf();
                            System.exit(0);
                        } else {
                            //cannot kill request handler
                        }
                    }
                }
                //monitor RH parameters very X seconds
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println("RequestHandlerMonitor: " + ex.getMessage());                
                ex.printStackTrace();
            } catch (IOException ex) {
                System.err.println("RequestHandlerMonitor: " + ex.getMessage());                
                ex.printStackTrace();
            }
        }
    }
    public void quit(){
        FLAG=false;
        rhmonitor.quit();
    }
}
