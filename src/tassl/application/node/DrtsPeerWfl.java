/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * @author Javier Diaz-Montes
 */
package tassl.application.node;



import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.node.isolate.RequestHandlerWfl;
import tassl.automate.application.network.SSLServerModule;
import tassl.automate.application.network.ServerModule;
import tassl.automate.application.network.TCPServerModule;
import tassl.automate.application.node.DrtsPeer;
import tassl.automate.network.ControlConstant;
import tassl.automate.programmodel.masterworker.MasterFramework;
import tassl.automate.programmodel.masterworker.ResultCollectingMaster;
import tassl.automate.programmodel.masterworker.WorkerFramework;

public class DrtsPeerWfl extends DrtsPeer {

    RequestHandlerWfl requestHandler;
    
    /** Creates a new instance of DrtsPeerWfl */
    public DrtsPeerWfl(byte [] nodeFile, Hashtable<String, String> propertyTable, ServerModule myServerModule) {        
        super(nodeFile,propertyTable,myServerModule);
    }

    public DrtsPeerWfl(ServerModule myServerModule) {
        super(myServerModule);
    }

    public DrtsPeerWfl(Hashtable<String, String> propertyTable) {
        super(propertyTable);
    }

    public DrtsPeerWfl() {
        super();
    }

    @Override
    public void ConfigureApp() {
        if (this.getLocalPeerID() == null) {
            System.out.println("localPeerID is null");
            return;
        }
        
        try {
            if (this.masterId != -1) { //master
                if (getProperty("ResultCollectingMaster", null) != null) {
                    System.out.println("Start sub master at "+this.getLocalPeerID()+" masterid="+this.masterId);
                    master = new ResultCollectingMaster();
                    master.startMaster();
                    control_server.AddMasterModule(master);

                } else {
                    System.out.println("Start the master at "+this.getLocalPeerID()+" masterid="+this.masterId);
                    Class masterclass = Class.forName(peerProperties.get("MasterClass"));
                    master = (MasterFramework) masterclass.newInstance();
                    master.setCometEnv(this.getCometSpace(), this.masterId, this.getLocalPeerID(), this.getOverlay());
                    master.startMaster();
                    control_server.AddMasterModule(master);
                }
            }
            if (this.workerId != -1) {  //worker
                System.out.println("Start the worker at "+this.getLocalPeerID()+" workerid="+this.workerId);
                Class workerclass = Class.forName(peerProperties.get("WorkerClass"));
                worker = (WorkerFramework) workerclass.newInstance();
                worker.setCometEnv(this.getCometSpace(), workerId, this.getLocalPeerID(), this.getOverlay());
                worker.startWorker();
            }
            if (this.reqHandlerId != -1) { //request handler
                System.out.println("Start request handler at "+this.getLocalPeerID()+" reqHandlerid="+this.reqHandlerId);
                requestHandler = new RequestHandlerWfl(this);
                requestHandler.start();
                
            }
            

        } catch (InstantiationException ex) {
            Logger.getLogger(DrtsPeerWfl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DrtsPeerWfl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DrtsPeerWfl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static DrtsPeerWfl joinOverlay(Hashtable<String, String> propertyTable) {
        if (propertyTable.get("chord.LOCAL_BOOTSTRAP")==null) {
            System.err.println("chord.LOCAL_BOOTSTRAP should be set in chord.properties");
            System.exit(1);
        }

        System.out.println("chord.LOCAL_BOOTSTRAP="+propertyTable.get("chord.LOCAL_BOOTSTRAP"));
        DrtsPeerWfl peer = null;
		ServerModule serverModule;
		if(propertyTable.get("chord.NETWORK_CLASS").equals("SSL")){
			serverModule = new SSLServerModule(Integer.parseInt(System.getProperty("TCP_CONTROL_PORT",ControlConstant.TCP_CONTROL_PORT).trim()));
			System.out.println("joinOverlay: Creating SSLServerModule");
		}else{
			serverModule = new TCPServerModule(Integer.parseInt(System.getProperty("TCP_CONTROL_PORT",ControlConstant.TCP_CONTROL_PORT).trim()));
			System.out.println("joinOverlay: Creating TCPServerModule");
		}
        peer = new DrtsPeerWfl(serverModule);
        peer.setPeerProperties(propertyTable);
        return peer;
    }
    @Override
    public void terminateInstance() {
        
        System.out.println("DrtsPeer terminateInstance "+this.getOverlay().getLocalID());
        if (this.workerId!=-1)
            this.worker.quit();
        else if (this.masterId!=-1)
            this.master.quit();
        else if (this.reqHandlerId!=-1){
            System.out.println("Request Handler quit");
            requestHandler.quit();
        }
        try{
            this.getOverlay().leave();  //leave nicely
            //peerOverlay.terminate(); //terminates without worrying about rerouting.            
        }catch(java.lang.RuntimeException e){
            System.out.println("The peer already left the overlay.");
        }
        
        try{
            System.out.println("Quiting TCPServerModule");
            quitServer();
        }catch(java.lang.NullPointerException e){
            System.out.println("The TCPServerModule already quit.");            
        }
    }
}
