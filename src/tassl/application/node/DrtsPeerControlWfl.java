/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.application.node;

import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Properties;
import tassl.automate.network.ControlConstant;
import tassl.automate.application.network.ServerModule;
import tassl.automate.application.network.TCPServerModule;
import tassl.automate.application.network.SSLServerModule;
import tassl.automate.overlay.OverlayObjectControl;

/**
 *
 * @author hyunjoo
 */
public class DrtsPeerControlWfl implements OverlayObjectControl<DrtsPeerWfl> {

    private ServerModule control_server = null;

    public DrtsPeerWfl startInstance(byte[] nodeFile, Hashtable<String, String> properties) throws InstantiationException {
        populateSystemProperties(properties);
		if(control_server == null){
			if(System.getProperty("chord.NETWORK_CLASS","TCP").equals("SSL"))
				control_server = new SSLServerModule(Integer.parseInt(System.getProperty("TCP_CONTROL_PORT",ControlConstant.TCP_CONTROL_PORT).trim()));
			else
				control_server = new TCPServerModule(Integer.parseInt(System.getProperty("TCP_CONTROL_PORT",ControlConstant.TCP_CONTROL_PORT).trim()));
		}
        return new DrtsPeerWfl(nodeFile, properties, control_server);
    }

    public void terminateInstance(DrtsPeerWfl instance) {
        instance.terminateInstance();
    }

    private void populateSystemProperties(Hashtable<String, String> propertyTable) {
        Properties p = new Properties(System.getProperties());
        for (Entry<String, String> property : propertyTable.entrySet()) {
            if (!property.getKey().contains("comet") && !property.getKey().contains("squid") && !property.getKey().contains("chord"))
                p.setProperty(property.getKey(), property.getValue());
        }
        System.setProperties(p);
    }
}
