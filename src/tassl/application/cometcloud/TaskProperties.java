/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Javier Diaz-Montes
 */
public class TaskProperties implements Serializable{
    Integer taskId;
    //requierment value in the tuple 
    String requirement;
    //values needed to create tuple
    List taskTupleValues;
    //data associated to the tuple
    List taskParam;
    //input files
    List <FileProperties>inputs;
    //output files
    List <FileProperties>outputs;
    
    double minTime;
    double maxTime;
    
    public TaskProperties(){
        taskParam=new ArrayList();
        taskTupleValues=new ArrayList();
        inputs=new ArrayList();
        outputs=new ArrayList();
        minTime=1.0;
        maxTime=1.0;
    }

    public TaskProperties(Integer taskId, String requirement, List taskTupleValues, List taskParam, 
                            double minTime, double maxTime, List inputs, List outputs) {
        this.taskId = taskId;
        this.requirement = requirement;
        this.taskTupleValues = taskTupleValues;
        this.taskParam = taskParam;
        this.minTime = minTime;
        this.maxTime = maxTime;        
        this.inputs=inputs;
        this.outputs=outputs;
    }
   
     /**
     * Minimum time to execute in slowest proc
     * @return
     */
    public double getMinTime() {
        return minTime;
    }

    /**
     * Maximum time to execute in slowest proc
     * @return
     */
    public double getMaxTime() {
        return maxTime;
    }

    public List<FileProperties> getInputs() {
        return inputs;
    }

    public List<FileProperties> getOutputs() {
        return outputs;
    }
    
    public String getRequirement() {
        return requirement;
    }

    public List getTaskTupleValues() {
        return taskTupleValues;
    }

    public List getTaskParam() {
        return taskParam;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public void setTaskTupleValues(List taskTupleValues) {
        this.taskTupleValues = taskTupleValues;
    }

    public void setTaskParam(List taskParam) {
        this.taskParam = taskParam;
    }

               
    
    /**
     * Compare if two tasks are identical. 
     * We do not use equals, because we do not want to consider taskId, 
     *  this is used to check for duplicated tasks
     * @param obj 
     * @return
     */
    public boolean compareTask(TaskProperties obj) {
        boolean result = false;
        if (obj == null) {
            result = false;
        } else {            
            if (this.requirement.equals(obj.getRequirement()) 
                    && this.taskTupleValues.equals(obj.getTaskTupleValues())
                    && this.taskParam.equals(obj.getTaskParam())
                    && this.inputs.equals(obj.getInputs())
                    && this.outputs.equals(obj.getOutputs())
                    && this.minTime==obj.getMinTime()
                    && this.maxTime==obj.getMaxTime()) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "TaskProperties{" + "taskId=" + taskId + ", requirement=" + requirement + ", taskTupleValues=" + taskTupleValues.toString() + ", taskParam=" + taskParam.toString() + ", inputs=" + inputs.toString() + ", outputs=" + outputs.toString() + ", minTime=" + minTime + ", maxTime=" + maxTime + '}';
    }
    
    
    
}
