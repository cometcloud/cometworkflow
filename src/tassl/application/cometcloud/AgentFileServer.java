/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided 
 * that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and 
 * the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *  - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its 
 *  contributors may be used to endorse or promote products derived from this software without specific prior 
 *  written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 *  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY 
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 *  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 *  POSSIBILITY OF SUCH DAMAGE.
 *  
 */
package tassl.application.cometcloud;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;
import tassl.application.utils.CommonUtils;
import tassl.workflow.resources.OSGJobHandler;

/**
 *
 * @author Javier Diaz
 */
public class AgentFileServer extends Thread {

    private ServerSocket servSock = null;
    private Socket clientSocket = null;
    private boolean FLAG = true;
    private int serverPort;
    String stageFileDir;
    int proc_max;
    private long dataTransTime = 0;
    final Object lock = new Object();
    private String userInWorker;
    private long incomingDataSize = 0;
    private long incomingFileCount = 0;
    private long outcomingDataSize = 0;
    private long outcomingFileCount = 0;
    private long dataTransTimeglobal = 0;
    private long incomingDataSizeglobal = 0;
    private long incomingFileCountglobal = 0;
    private long outcomingDataSizeglobal = 0;
    private long outcomingFileCountglobal = 0;

    String AgentMetricsAddress;
    int AgentMetricsPort;
    String dbName;
    boolean useMeticsService;

    AgentFileServer(int port, String stageFileDir, int proc_max, String AgentMetricsAddress, int AgentMetricsPort, boolean useMeticsService, String dbName) {
        this.serverPort = port;
        this.stageFileDir = stageFileDir;
        this.proc_max = proc_max;
        this.userInWorker = "`whoami`";
        this.AgentMetricsAddress = AgentMetricsAddress;
        this.AgentMetricsPort = AgentMetricsPort;
        this.dbName = dbName;
        this.useMeticsService = useMeticsService;
    }

    @Override
    public void run() {
        startServer();
    }

    private void startServer() {
        //set server socket
        try {
            servSock = new ServerSocket(serverPort);
            servSock.setReuseAddress(true);
            System.out.println("Starting File Server in port " + serverPort);
            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "Starting File Server in port " + serverPort);
        } catch (IOException e) {
            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "ERROR listening on port " + serverPort, e);
            closeSockets();
            return;
        }

        List<Thread> proc_list = new ArrayList<Thread>();
        while (FLAG) {
            if (proc_list.size() == proc_max) {
                boolean full = true;
                while (full) {
                    for (int i = proc_list.size() - 1; i >= 0; i--) {
                        if (!proc_list.get(i).isAlive()) {
                            proc_list.remove(i);
                            full = false;
                        }
                    }
                    if (full) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            try {
                clientSocket = servSock.accept();
                Thread request = new Thread(new processRequest(clientSocket, stageFileDir));
                request.start();
                proc_list.add(request);
            } catch (IOException e) {
                if (FLAG == true) {
                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "Error recieving connection on port " + serverPort, e);
                }
            }
        }

    }

    private class processRequest implements Runnable {

        Socket cSocket;
        String oFilesDir;

        public processRequest(Socket clientSocket, String stageFileDir) {
            cSocket = clientSocket;
            oFilesDir = stageFileDir;
            new File(oFilesDir).mkdirs(); //directory final results
        }

        public void run() {
            long stime, etime;
            stime = System.currentTimeMillis();
            String message = "";
            DataInputStream input = null;
            DataOutputStream output = null;

            long TempincomingDataSize = 0;
            long TempincomingFileCount = 0;
            long TempoutcomingDataSize = 0;
            long TempoutcomingFileCount = 0;

            int maxFilesRsync = 500;

            try {
                output = new DataOutputStream(cSocket.getOutputStream());
                input = new DataInputStream(cSocket.getInputStream());

                boolean more = true;

                message = input.readUTF();
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, message);
                String[] inMsg = message.split("\\|");

                String workerAddress = input.readUTF(); //(cSocket.getInetAddress().getHostAddress();) does not work when forwarding 

                if (inMsg[0].equals("get")) {//pull task from remote site
                    List<Document> fromSites = new ArrayList<Document>();
                    List<Document> toWorkers = new ArrayList<Document>();

                    if (inMsg.length == 8) {
                        //get|true|remotedir|file1,file2,file3|dirInWorker|key|"worker"|fileTransferType 
                        //when "worker" string is there, it means that a worker requested the file. when "noworker" means that the fileserver is prefetching files
                        String localcache = inMsg[1];
                        String remotedir = inMsg[2];
                        List<String> files = new ArrayList<String>();
                        files.addAll(Arrays.asList(inMsg[3].split(",")));
                        String dirInWorker = inMsg[4];
                        String keyfile = inMsg[5]; //key to communicate with worker. "null" means no key needed
                        String worker = inMsg[6]; //this is "worker" or "noworker"
                        String fileTransferType = inMsg[7];// type of file transfer service (default is rsync)
                        String addkey = "";
                        int currentCounter = 0;
                        if (!keyfile.equals("null")) {
                            addkey = " -i " + keyfile;
                        }
                        if (localcache.equals("true")) {
                            int status = 1;
                            List<String> filesCacheList = new ArrayList<String>();
                            String filesCache = "";
                            for (int i = (files.size() - 1); i >= 0; i--) {
                                if (!files.get(i).trim().equals("")) {
                                    File test = new File(stageFileDir + "/" + files.get(i));
                                    if (test.exists()) {
                                        filesCache += " " + stageFileDir + "/" + files.get(i);
                                        files.remove(i);
                                    }
                                    currentCounter++;
                                    if (currentCounter >= maxFilesRsync) {
                                        filesCacheList.add(filesCache);
                                        filesCache = "";
                                        currentCounter = 0;
                                    }
                                }
                            }
                            if (currentCounter != 0) {
                                filesCacheList.add(filesCache);
                            }
                            if (worker.equals("worker")) {
                                for (String filesChunk : filesCacheList) {
                                    if (!filesChunk.equals("")) {
                                        if (workerAddress.equals("127.0.0.1") || workerAddress.equals("localhost")) {
                                            status = CommonUtils.execute("cp -f " + filesChunk + " " + dirInWorker + "/");
                                        } else {
                                            long startTime = System.currentTimeMillis();

                                            //send file to the worker
                                            String[] outputExec = null;
                                            if (fileTransferType.equals("default")) {
                                                outputExec = CommonUtils.execute("rsync -avz -e \"ssh " + addkey + " -o BatchMode=yes\" " + filesChunk + " "
                                                        + userInWorker + "@" + workerAddress + ":" + dirInWorker + "/", false);
                                            } else if (fileTransferType.contains("osg")) {
                                                outputExec = OSGJobHandler.transferFileToJob(workerAddress, filesChunk, dirInWorker, fileTransferType);
                                            } else {
                                                Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: Invalid fileTransferType");
                                            }
                                            status = Integer.parseInt(outputExec[0]);
                                            long endTime = System.currentTimeMillis();

                                            if (status == 0 && useMeticsService && outputExec != null) {
                                                try {
                                                    Document doc = createDocumentRsync(null, workerAddress, startTime, endTime, "local", outputExec[1]);
                                                    if (!doc.isEmpty()) {
                                                        toWorkers.add(doc);
                                                    }
                                                } catch (Exception e) {
                                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: creating Document for DB " + outputExec[1], e);
                                                }
                                            }
                                        }
                                        if (status != 0) {
                                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: sending files from cache to worker: " + filesChunk);
                                            // RETRY
                                        }
                                    }
                                }
                            }

                        }
                        currentCounter = 0;
                        if (files.size() > 0) {
                            int status = 1;
                            String[] part = remotedir.split(":");
                            String filesRemote = "";
                            List<String> filesCacheList = new ArrayList<String>();
                            String filesCache = "";
                            List<String> filesRemoteList = new ArrayList<String>();
                            for (int i = (files.size() - 1); i >= 0; i--) {
                                if (!files.get(i).trim().equals("")) {
                                    filesRemote += ":" + part[1] + "/" + files.get(i) + " ";
                                    filesCache += " " + stageFileDir + "/" + files.get(i);
                                    currentCounter++;
                                    if (currentCounter >= maxFilesRsync) {
                                        filesCacheList.add(filesCache);
                                        filesRemoteList.add(filesRemote);
                                        filesCache = "";
                                        filesRemote = "";
                                        currentCounter = 0;
                                    }
                                }
                                files.remove(i);
                            }
                            if (currentCounter != 0) {
                                filesCacheList.add(filesCache);
                                filesRemoteList.add(filesRemote);
                            }
                            for (int i = 0; i < filesCacheList.size(); i++) {
                                filesCache = filesCacheList.get(i).trim();
                                filesRemote = filesRemoteList.get(i).trim();

                                if (!filesCache.equals("")) {
                                    //bring file to local cache
                                    long startTime = System.currentTimeMillis();

                                    String[] outputExec = CommonUtils.execute("rsync -avz -e \"ssh -o BatchMode=yes\" " + part[0] + filesRemote + " "
                                            + " " + stageFileDir + "/", false);

                                    long endTime = System.currentTimeMillis();

                                    status = Integer.parseInt(outputExec[0]);

                                    if (status == 0 && useMeticsService) {
                                        String machine = part[0];
                                        if (part[0].contains("@")) {
                                            machine = part[0].split("@")[1];
                                        }
                                        try {
                                            Document doc = createDocumentRsync(machine, null, startTime, endTime, "remote", outputExec[1]);
                                            if (!doc.isEmpty()) {
                                                fromSites.add(doc);
                                            }
                                        } catch (Exception e) {
                                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: creating Document for DB " + outputExec[1], e);
                                        }
                                    }

                                    if (status != 0) {
                                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: bringing files to cache: " + part[0] + filesRemote);
                                        // RETRY
                                    }

                                    for (String tempFile : filesCache.split(" ")) {
                                        TempincomingFileCount++;
                                        TempincomingDataSize += new File(tempFile).length();
                                    }

                                    if (status == 0) {
                                        if (worker.equals("worker")) {
                                            if (workerAddress.equals("127.0.0.1") || workerAddress.equals("localhost")) {
                                                status = CommonUtils.execute("cp -f " + filesCache + " " + dirInWorker + "/");
                                            } else {
                                                //send file to the worker
                                                startTime = System.currentTimeMillis();
                                                if (fileTransferType.equals("default")) {
                                                    outputExec = CommonUtils.execute("rsync -avz -e \"ssh " + addkey + " -o BatchMode=yes\" " + filesCache + " "
                                                            + userInWorker + "@" + workerAddress + ":" + dirInWorker + "/", false);
                                                } else if (fileTransferType.contains("osg")) {
                                                    outputExec = OSGJobHandler.transferFileToJob(workerAddress, filesCache, dirInWorker, fileTransferType);
                                                } else {
                                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: Invalid fileTransferType");
                                                }
                                                status = Integer.parseInt(outputExec[0]);
                                                endTime = System.currentTimeMillis();

                                                if (status == 0 && useMeticsService) {
                                                    try {
                                                        Document doc = createDocumentRsync(null, workerAddress, startTime, endTime, "local", outputExec[1]);
                                                        if (!doc.isEmpty()) {
                                                            toWorkers.add(doc);
                                                        }
                                                    } catch (Exception e) {
                                                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: creating Document for DB " + outputExec[1], e);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (status != 0) {
                                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: sending files from cache to worker: " + filesCache);
                                        // RETRY
                                    }
                                } else {
                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: empty files: " + filesCache);
                                }
                            }
                        }
                        if (worker.equals("worker")) {
                            if (files.size() > 0) {
                                output.writeUTF("ERROR: Some files have not been transferred: " + Arrays.toString(files.toArray()));
                            } else {
                                output.writeUTF("OK");
                            }
                        }

                        if (useMeticsService) {
                            List param = new ArrayList();
                            param.add(fromSites);
                            param.add(toWorkers);
                            sendInfo(AgentMetricsAddress, AgentMetricsPort, "dbWriteAgentStoreTransferInfo", param);

                        }

                    } else {
                        String error = "ERROR: wrong number of parameters in GET command:" + Arrays.toString(inMsg);
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.WARNING, error);
                        output.writeUTF(error);
                    }

                } else if (inMsg[0].equals("put")) {
                    List<Document> toSites = new ArrayList<Document>();
                    List<Document> fromWorkers = new ArrayList<Document>();

                    if (inMsg.length == 6) {
                        //put|remotedir|file1,file2,file3|dirInWorker|fileTransferType
                        String remotedir = inMsg[1];
                        List<String> files = new ArrayList<String>();
                        files.addAll(Arrays.asList(inMsg[2].split(",")));
                        String dirInWorker = inMsg[3];
                        String keyfile = inMsg[4]; //key to communicate with worker. "null" means no key needed
                        String fileTransferType = inMsg[5];// type of file transfer service (default is rsync)
                        String addkey = "";
                        if (!keyfile.equals("null")) {
                            addkey = " -i " + keyfile;
                        }
                        int status = 1;
                        int currentCounter = 0;
                        List<String> filesRemoteList = new ArrayList<String>();
                        String filesRemote = "";
                        String filesLocalhost = "";
                        List<String> filesCacheList = new ArrayList<String>();
                        List<String> filesLocalhostList = new ArrayList<String>();
                        String filesCache = "";
                        for (int i = (files.size() - 1); i >= 0; i--) {
                            if (!files.get(i).trim().equals("")) {
                                filesLocalhost += " " + dirInWorker + "/" + files.get(i) + " ";
                                filesRemote += ":" + dirInWorker + "/" + files.get(i) + " ";
                                filesCache += " " + stageFileDir + "/" + files.get(i);
                                currentCounter++;
                                if (currentCounter >= maxFilesRsync) {
                                    filesRemoteList.add(filesRemote);
                                    filesCacheList.add(filesCache);
                                    filesLocalhostList.add(filesLocalhost);
                                    filesCache = "";
                                    filesRemote = "";
                                    filesLocalhost = "";
                                    currentCounter = 0;
                                }
                            }
                            files.remove(i);
                        }
                        if (currentCounter != 0) {
                            filesRemoteList.add(filesRemote);
                            filesCacheList.add(filesCache);
                            filesLocalhostList.add(filesLocalhost);
                        }
                        for (int i = 0; i < filesCacheList.size(); i++) {
                            filesCache = filesCacheList.get(i).trim();
                            filesRemote = filesRemoteList.get(i).trim();
                            filesLocalhost = filesLocalhostList.get(i).trim();

                            if (workerAddress.equals("127.0.0.1") || workerAddress.equals("localhost")) {
                                status = CommonUtils.execute("cp -f " + filesLocalhost
                                        + " " + stageFileDir + "/");
                            } else {
                                //bring file to local cache
                                long startTime = System.currentTimeMillis();
                                String[] outputExec = null;
                                if (fileTransferType.equals("default")) {
                                    outputExec = CommonUtils.execute("rsync -avz -e \"ssh " + addkey + " -o BatchMode=yes\" "
                                            + userInWorker + "@" + workerAddress + filesRemote
                                            + " " + stageFileDir + "/", false);
                                } else if (fileTransferType.contains("osg")) {
                                    outputExec = OSGJobHandler.transferFileFromJob(workerAddress, filesRemote, stageFileDir, fileTransferType);
                                } else {
                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: Invalid fileTransferType");
                                }
                                status = Integer.parseInt(outputExec[0]);
                                long endTime = System.currentTimeMillis();

                                if (status == 0 && useMeticsService) {
                                    try {
                                        Document doc = createDocumentRsync(workerAddress, null, startTime, endTime, "local", outputExec[1]);
                                        if (!doc.isEmpty()) {
                                            fromWorkers.add(doc);
                                        }

                                    } catch (Exception e) {
                                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: creating Document for DB " + outputExec[1], e);
                                    }
                                }
                                if (status != 0) {
                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: sending files from worker to cache: " + filesRemote);
                                }
                            }
                            if (status == 0) {
                                if (!remotedir.equals("")) {
                                    //send file to the central repository (remotedir is ip:dir)
                                    long startTime = System.currentTimeMillis();
                                    String[] outputExec = CommonUtils.execute("rsync -avz -e \"ssh -o BatchMode=yes\" " + filesCache + " "
                                            + remotedir + "/", false);

                                    long endTime = System.currentTimeMillis();

                                    status = Integer.parseInt(outputExec[0]);

                                    if (status == 0 && useMeticsService) {
                                        String[] part = remotedir.split(":");
                                        String machine = part[0];
                                        if (part[0].contains("@")) {
                                            machine = part[0].split("@")[1];
                                        }
                                        try {
                                            Document doc = createDocumentRsync(null, machine, startTime, endTime, "remote", outputExec[1]);
                                            if (!doc.isEmpty()) {
                                                toSites.add(doc);
                                            }

                                        } catch (Exception e) {
                                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: creating Document for DB " + outputExec[1], e);
                                        }
                                    }

                                    for (String tempFile : filesCache.split(" ")) {
                                        if (!tempFile.trim().equals("")) {
                                            TempoutcomingFileCount++;
                                            TempoutcomingDataSize += new File(tempFile).length();
                                        }
                                    }
                                } else {
                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "File in local cache only. We did not send it.");
                                }
                                if (status != 0) {
                                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, "ERROR: sending files from cache to destination: " + filesCache);
                                }  // RETRY                
                            }//else TODO:RETRY

                            if (files.size() > 0) {
                                output.writeUTF("ERROR: Some files have not been transferred: " + Arrays.toString(files.toArray()));
                            } else {
                                output.writeUTF("OK");
                            }

                        }
                        //store in DB
                        if (useMeticsService) {
                            List param = new ArrayList();
                            param.add(toSites);
                            param.add(fromWorkers);
                            sendInfo(AgentMetricsAddress, AgentMetricsPort, "dbWriteAgentStoreTransferInfo", param);
                        }
                    } else {
                        String error = "ERROR: wrong number of parameters in PUT command:" + Arrays.toString(inMsg);
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.WARNING, error);
                        output.writeUTF(error);
                    }
                } else {
                    String error = "ERROR: wrong operation command:" + inMsg[0] + " " + Arrays.toString(inMsg);
                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.WARNING, error);
                    output.writeUTF(error);
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (output != null) {
                    try {
                        output.close();
                    } catch (Exception e) {
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, e);
                    }
                }
                if (input != null) {
                    try {
                        input.close();
                    } catch (Exception e) {
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, e);
                    }
                }
            }
            etime = System.currentTimeMillis();

            synchronized (lock) {
                dataTransTime += (etime - stime) / 1000L;
                incomingDataSize += TempincomingDataSize;
                incomingFileCount += TempincomingFileCount;
                outcomingDataSize += TempoutcomingDataSize;
                outcomingFileCount += TempoutcomingFileCount;
                dataTransTimeglobal += (etime - stime) / 1000L;
                incomingDataSizeglobal += TempincomingDataSize;
                incomingFileCountglobal += TempincomingFileCount;
                outcomingDataSizeglobal += TempoutcomingDataSize;
                outcomingFileCountglobal += TempoutcomingFileCount;
            }

        }
    }

    //send information to outside
    private boolean sendInfo(String addess, int port, String command, Object param) {//sync happens outside this method
        boolean status = true;
        DataOutputStream conn = null;
        try {
            conn = CommonUtils.getOutput(addess, port);
            if (conn != null) {
                if (command.equals("dbWriteAgentStoreTransferInfo")) {
                    conn.writeUTF("dbWriteAgentStoreTransferInfo");
                    conn.writeUTF(dbName);

                    List<Document> sites = (List<Document>) ((List) param).get(0);
                    List<Document> workers = (List<Document>) ((List) param).get(1);
                    byte[] forwardObj;
                    //send sites
                    forwardObj = programming5.io.Serializer.serializeBytes(sites);
                    conn.writeInt(forwardObj.length);
                    conn.write(forwardObj);

                    //send workers
                    forwardObj = programming5.io.Serializer.serializeBytes(workers);
                    conn.writeInt(forwardObj.length);
                    conn.write(forwardObj);

                }

            } else {
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.WARNING, "Could not connect with " + addess + ":" + port);
                status = false;
            }
        } catch (IOException e) {
            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, e);
            status = false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ex) {
                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return status;
    }

    /**
     * Create Document to record each data transfer
     *
     * @param from
     * @param to
     * @param start
     * @param end
     * @param type
     * @param rsyncLog
     * @return Document with transfer information
     */
    private Document createDocumentRsync(String from, String to, long start, long end, String type, String rsyncLog) {

        Document doc = new Document();

        String[] lineBreakLog = rsyncLog.split("\n");
        String line;
        int numFiles = 0;
        for (int i = 1; i < lineBreakLog.length; i++) {//bc line 0 is "receiving incremental file list"
            line = lineBreakLog[i].trim();
            if (!line.isEmpty()) {
                if (line.startsWith("total size ")) {
                    String[] lastline = line.split(" ");
                    if (numFiles != 0) {
                        double bytes = 0;
                        if (from != null) {
                            doc.append("from", from); //get or pull

                        } else if (to != null) {
                            doc.append("to", to); //put or push
                        }

                        bytes = Double.parseDouble(lastline[3].replaceAll(",", ""));

                        if (bytes < 1000000) {//if it is less than 1MB we ignore it because the transfer rate would be very low
                            return new Document();
                        }
                        doc.append("bytes", bytes);
                        doc.append("startTime", start);
                        doc.append("endTime", end);
                        doc.append("duration", (end - start));
                        doc.append("type", type); //local or remote
                        doc.append("transferRate", (double) (bytes / (1024 * 1024)) / ((double) (((double) (end - start)) / 1000.0)));//Double.parseDouble(lastline[8].replaceAll(",", "")));
                        doc.append("protocol", "rsync");

                    }
                    break;
                }
                numFiles++;
            }
        }

        return doc;
    }

    public long getDataTransferTime() {
        return dataTransTime;
    }

    public void resetDataTransferTime() {
        synchronized (lock) {
            dataTransTime = 0;
        }
    }

    public void setUserInWorker(String userInWorker) {
        this.userInWorker = userInWorker;
    }

    private void closeSockets() {
        try {
            clientSocket.close();
        } catch (Exception e) {
            if (!FLAG) {
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        try {
            servSock.close();
        } catch (Exception e) {
            if (!FLAG) {
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, e);
            }

        }
    }

    public void quit() {
        FLAG = false;
        closeSockets();
    }

    public String getStatistics() {
        String statics = "DATATRANSFERTIME:" + dataTransTime + "\n"
                + "INCOMINGDATASIZE:" + incomingDataSize + "\n"
                + "INCOMINGFILECOUNT:" + incomingFileCount + "\n"
                + "OUTCOMINGDATASIZE:" + outcomingDataSize + "\n"
                + "OUTCOMINGFILECOUNT:" + outcomingFileCount + "\n"
                + "Global Values: \n"
                + "DATATRANSFERTIMEGLOBAL:" + dataTransTimeglobal + "\n"
                + "INCOMINGDATASIZEGLOBAL:" + incomingDataSizeglobal + "\n"
                + "INCOMINGFILECOUNTGLOBAL:" + incomingFileCountglobal + "\n"
                + "OUTCOMINGDATASIZEGLOBAL:" + outcomingDataSizeglobal + "\n"
                + "OUTCOMINGFILECOUNTGLOBAL:" + outcomingFileCountglobal + "\n";

        System.out.println("CWDEBUG: AgentFileServer: setting stage counters to 0");
        dataTransTime = 0;
        incomingDataSize = 0;
        incomingFileCount = 0;
        outcomingDataSize = 0;
        outcomingFileCount = 0;

        //System.out.println("CWDEBUG: AgentFileServer: removing stagged files");
        //CommonUtils.execute("rm -f "+stageFileDir+"/*");
        return statics;

    }
}
