package tassl.application.cometcloud.sampleLoop;

import tassl.application.cometcloud.sample.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.WorkflowMeteorGenericWorker;
import tassl.application.cometcloud.WorkflowTaskTuple;
import tassl.application.utils.CommonUtils;


/**
 *
 * @author Javier Diaz
 */

public class AppWorker extends WorkflowMeteorGenericWorker{

    
    /**
    * This method is called to compute the task    
    * 
    * @param dataobj It is the data associated to the task. The one send from the master
    * @return Object result data that will be attached to the task result. The object has to be serializable.
    */
    
    @Override
    public Object computeTaskSpecific(Object dataobj, WorkflowTaskTuple tasktuple) {
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO, "AppWorker "+this.getPeerIP()+" gets taskid " + tasktuple.getTaskid());
        
        List data = (List) dataobj;
        String stageId=(String)data.get(0);
        String operation=(String)data.get(1);
        FileProperties outputFP=(FileProperties)data.get(2);
        List <FileProperties> inputs=(List<FileProperties>)data.get(3);
        
        
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"stage:"+stageId);        
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"output:"+outputFP.getLocation());
        
        if(stageId.equals("stage1")){
            Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"jsec:"+data.get(5));
            Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"isec:"+data.get(6));
        }
        
        
        String workingdir=System.getProperty("WorkingDir");
        
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Getting input file");
        
        //GET INPUT
        long startGetInput=System.currentTimeMillis();
        //download all inputs
        List<String> inputFileList=this.downloadInputs(inputs, workingdir);
        
        String filenames=StringUtils.join(inputFileList.toArray()," ");
         
        long stopGetInput=System.currentTimeMillis();
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO, "Time in sec to get all INPUT:{0}", (stopGetInput-startGetInput)/1000L);
        
        //remove input files
        CommonUtils.execute("rm -f "+filenames);
        
        //EXECUTE
        double time=0.0;
        try {
            time=(Double)data.get(4)/Double.parseDouble(System.getProperty("performance"));
            Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Worker will take "+time+" seconds. task:"+data.get(4)+" perf:"+System.getProperty("performance"));
            Thread.sleep(((long)time)*1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(AppWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Generate OUTPUT FILE        
        
        //file size will be between 50MB and 100MB
        double minSize=52428800;
        double maxsize=104857600;
        long size= Math.round(minSize + (Math.random() * (maxsize - minSize)));
                
        String randomfile=tasktuple.getTaskid()+"_"+System.getProperty("Name")+".txt";
        File f=new File(workingdir+"/"+randomfile);
        if(!f.isFile()){
            try {
                RandomAccessFile r=new RandomAccessFile(workingdir+"/"+randomfile, "rw");                                    
                r.setLength(size);                                    
                r.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(AppWorker.class.getName()).log(Level.SEVERE, null, ex);
            }catch (IOException ex) {
                Logger.getLogger(AppWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        List<String> listFileNames=new ArrayList();
        listFileNames.add(randomfile);

        //SEND OUTPUT
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Uploading output file");
        
        long startSendOut=System.currentTimeMillis();
        
        //upload results and generate list of resuts
        List<FileProperties> resultFiles=this.uploadResults(listFileNames, workingdir, outputFP);
                
        long endSendOut=System.currentTimeMillis();
        
        
        //create file with times for logs      
        FileWriter fstream; 
        try {
            fstream = new FileWriter(workingdir+"/"+tasktuple.getTaskid()+".log", false); //true tells to append data.        
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(((stopGetInput-startGetInput)/1000L)+" "+(time)+" "+((endSendOut-startSendOut)/1000L)); //input exec output
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(AppWorker.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        List outfiles=new ArrayList();
        outfiles.add(tasktuple.getTaskid()+".log");
        
        this.putFile("", outfiles, workingdir);                
        
                
        boolean iterate=true;
        //for testing. This will only have effect if the stage below is end of a loop
        if(stageId.equals("S6_2")){
            iterate=false;
        }
        if(stageId.equals("S7_2")){
            iterate=false;
        }
        if(stageId.equals("S2_2")){
            iterate=false;
        }
        if(stageId.equals("S3_2")){
            iterate=false;
        }
        if(stageId.equals("S4_2")){
            iterate=false;
        }
        if(stageId.equals("S3_0_2")){
            iterate=false;
        }
        if(stageId.equals("S3_1_2")){
            iterate=false;
        }
        if(stageId.equals("S3_2_2")){
            iterate=false;
        }
        
        return new Object[]{"OK",resultFiles,iterate};
    }

    @Override
    public void cancelJob() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

}
