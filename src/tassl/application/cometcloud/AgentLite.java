/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.application.cometcloud;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.commons.lang.StringUtils;
import tassl.application.utils.CommonUtils;
import tassl.automate.application.network.SendSSL;
import tassl.automate.application.network.SendTCP;
import tassl.automate.application.network.Sender;
import tassl.automate.comet.CometConstants;
import tassl.automate.comet.XmlTuple;
import tassl.automate.network.CMessage;
import tassl.automate.network.ControlConstant;

/**
 *
 * @author Javier Diaz-Montes
 */
public class AgentLite extends Thread {

    //{"address:port":#retries,...}
    HashMap<String, Integer> workerRetries;
    //{"address:port":"template",...}
    HashMap<String, String> currentInterest;
    //{"address:port":{taskid,taskid,...},...}
    HashMap<String,List<String>> currentTasks;    
    int servPort; //Task Service port. Used by agents to retrieve tasks
    String servPublicIp;
    String IpAgentForWorkers; //when using ssh tunnel servPublicIp will be localhost. This tell local workers the machine they report to
    Properties properties;
    private int noTaskCountMax;
    boolean alive = true;

    String StageResultDir;
    AgentFileServer fileserver;
    boolean StartFileServer;
    int FileServerPort;
    int FileServerMaxThreads;
    String StageFileDir;
    boolean startManagement;
    int mgmtPort;
    String centralManagerServerAddress;
    int centralManagerServerPort;
    int monitorInterval;
    TaskHandler th;
    //store informations such as workers assigned to a workflow, free workers, machine status(running, stopped, failure)
    //This is only used if management is activated
    AgentResourceManager resManager;

    boolean agent_state = true;

    //time when the task enters the system
    HashMap<String, Long> taskTimes;

    //start SSH tunnels from Agent to worker
    boolean startSSHTunnelAgent;
    
    //Metric Service that calculates stats and stores info DB
    String AgentMetricsAddress;
    int AgentMetricsPort;   
    String dbName;
    //use metric service to record system info
    boolean useMeticsService;
          
            
    private AgentLite(Properties p) {

        properties = p;

        workerRetries = new HashMap<>();
        currentInterest= new HashMap<>();
        currentTasks= new HashMap<>();      
        taskTimes=new HashMap<>();
    }

    private void setLog(String filename) {
        FileHandler fh;
        try {
            fh = new FileHandler(filename, true);

            Logger l = Logger.getLogger(""); //Everyone will print here. Create new logger if different file wanted

            for (Handler i : l.getHandlers()) {
                l.removeHandler(i);
            }

            l.setUseParentHandlers(false);
            fh.setFormatter(new SimpleFormatter());
            l.addHandler(fh);
            l.setLevel(Level.ALL);

        } catch (SecurityException | IOException e) {
            System.out.println("ERROR: AgentLite: Setting up log");
            e.printStackTrace();
        }
    }

    public void loadConfig() {
        String portTemp = properties.getProperty("portAgent");
        if (portTemp != null) {
            servPort = Integer.parseInt(portTemp);
        } else {
            System.err.println("ERROR: AgentLite: You need to specify the portAgent parameter in the configuration file. ");
            System.exit(1);
        }

        servPublicIp = properties.getProperty("publicIpAgent");
        if (servPublicIp == null) {
            System.err.println("ERROR: AgentLite: You need to specify the public IP of this machine using the publicIpAgent parameter in the configuration file. ");
            System.exit(1);
        }

        IpAgentForWorkers = properties.getProperty("IpAgentForWorkers", properties.getProperty("publicIpAgent"));

        setLog(properties.getProperty("logFile", "AgentLite.log"));
        noTaskCountMax = Integer.parseInt(properties.getProperty("MaxNoTaskRetries", "10"));
        StartFileServer=properties.getProperty("StartFileServer","true").equals("true");
        if(StartFileServer){
            FileServerPort=Integer.parseInt(properties.getProperty("FileServerPort"));
            FileServerMaxThreads=Integer.parseInt(properties.getProperty("FileServerMaxThreads","10"));
            StageFileDir=properties.getProperty("StageFileDir");
        }
        startManagement=properties.getProperty("StartManager","true").equals("true");
        if(startManagement){
            mgmtPort=Integer.parseInt(properties.getProperty("MgmtPort"));
            String []cms=properties.getProperty("CentralManagerServer").split(":");
            centralManagerServerAddress=cms[0];
            centralManagerServerPort=Integer.parseInt(cms[1]);
            monitorInterval=Integer.parseInt(properties.getProperty("MonitorInterval","60"));            
            
        }
        
        useMeticsService=properties.getProperty("UseMeticsService","false").equals("true");
        //publish it to the whole java VM
        System.setProperty("UseMeticsService", properties.getProperty("useMeticsService","false"));        
        if(useMeticsService){            
            AgentMetricsAddress=properties.getProperty("AgentMetricsAddress");
            AgentMetricsPort=Integer.parseInt(properties.getProperty("AgentMetricsPort"));
            dbName=properties.getProperty("DBName");
        }
        
    }
    
    @Override
    public void run() {

        loadConfig();

        System.out.println("Starting AgentLite Server in " + servPort);

        Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "Starting AgentLite Server in " + servPort);

        //start Management thread (monitor and start/stop workers)
        if (startManagement) {
            //load information about the nodes
            String res=properties.getProperty("Resources");            
            resManager=new AgentResourceManager(mgmtPort,centralManagerServerAddress,centralManagerServerPort, this, AgentMetricsAddress, AgentMetricsPort, useMeticsService,dbName);
            resManager.setPublicIp(servPublicIp);
            resManager.setMonitorInterval(monitorInterval);
            //in case we need ssh tunnels for the worker to communicate with agent
            boolean startSSHTunnels = properties.getProperty("StartSshTunnelWorkers", "false").equals("true");
            resManager.setStartSSHtunnels(startSSHTunnels);

            //in case we need a ssh tunnel for the agent to communicate with workers
            startSSHTunnelAgent = properties.getProperty("StartSshTunnelAgent", "false").equals("true");
            resManager.setStartSSHtunnelAgent(startSSHTunnelAgent);
            if (startSSHTunnelAgent) {
                String portrange = properties.getProperty("SshTunnelAgentPortRange"); //,"9999:10999"
                if (portrange != null && !portrange.isEmpty()) {
                    String[] stringPortRange = portrange.split(":");
                    int[] agentPortRange = {Integer.parseInt(stringPortRange[0]), Integer.parseInt(stringPortRange[1])};
                    resManager.setSshTunnelAgentPortRange(agentPortRange);
                } else {
                    Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, "ERROR: loading Agent Properties. "
                            + "A port range has to be specified using the property SshTunnelAgentPortRange (e.g. SshTunnelAgentPortRange=9999:10999) "
                            + "when using StartSshTunnelAgent=true. Leaving now...");
                    System.err.println("ERROR: loading Agent Properties. "
                            + "A port range has to be specified using the property SshTunnelAgentPortRange (e.g. SshTunnelAgentPortRange=9999:10999) "
                            + "when using StartSshTunnelAgent=true"
                            + ". Leaving now...");
                    System.exit(1);
                }
            }

            File baseProperties = new File(".worker_base.properties");
            resManager.setWorkerBaseProperties(baseProperties.getAbsolutePath());

            if (!resManager.loadProperties(res)) {
                Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, "ERROR: loading Resources Properties");
                System.err.println("ERROR: loading Resources Properties. More details in the log file. Leaving now...");
                System.exit(1);
            }

            if (!createWorkerBaseProperties(baseProperties)) {
                Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, "ERROR: Creating worker base properties file");
                System.err.println("ERROR: Creating worker base properties file. Leaving now...");
                System.exit(1);
            } 
            
        }        
        
        //start FileServer
        if(StartFileServer){
            fileserver=new AgentFileServer(FileServerPort,StageFileDir,FileServerMaxThreads, AgentMetricsAddress, AgentMetricsPort, useMeticsService, dbName);
            
            if(startManagement){
                fileserver.setUserInWorker(resManager.getResources().getUserWorker());
            }

            fileserver.start();
        }

        //Optimization:
        //TODO: MAKE IT MULTITHREAD
        //Starting task handler thread.
        th = new TaskHandler();
        Thread taskHandlerThread = new Thread(th);
        taskHandlerThread.start();

        //we start this one at the end because it contacts with the autonomic scheduler and we need to make sure that
        // the rest of services in this agent are running OK
        if (startManagement) {
            System.out.println("Starting AgentLite Manager in " + mgmtPort);
            Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "Starting AgentLite Manager in " + mgmtPort);       
            resManager.start();            
        }    
        
    }

    public void quit() {
        //kill taskHandler
        th.quit();

        //kill fileserver
        fileserver.quit();                
    }

    public boolean createWorkerBaseProperties(File filebase) {
        boolean status = true;
        FileWriter fw;
        BufferedWriter bw = null;
        try {
            fw = new FileWriter(filebase.getAbsoluteFile());
            bw = new BufferedWriter(fw);
            String towrite = "Name=" + resManager.getResources().getIdentifier() + "\n"
                    + "Zone=" + resManager.getResources().getZone() + "\n"
                    + "IsolatedProxy=" + IpAgentForWorkers + "\n"
                    + "IsolatedProxyPort=" + servPort + "\n"
                    + "FileServerPort=" + FileServerPort + "\n"
                    + "TaskClass=" + properties.getProperty("TaskClass", "tassl.application.cometcloud.WorkflowTaskTuple") + "\n"
                    + "CloudburstNodeType=IsolatedWorkerWfl\n"
                    + "UnderAgent=true\n"
                    + "log=true\n"
                    + "siteFileCache=" + System.getProperty("user.name") + "@" + properties.getProperty("publicIPSite", servPublicIp) + ":" + StageFileDir + "\n";
            //"siteFileCache="+System.getProperty("user.name")+"@"+servPublicIp+":"+StageFileDir+"\n";
            bw.write(towrite);

        } catch (IOException ex) {
            Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
            status = false;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return status;
    }

    public HashMap<String, List<String>> getCurrentTasks() {
        return currentTasks;
    }

    //class to deal with tasks requests
    private class TaskHandler implements Runnable {

        ServerSocket servSock;
        Socket clientSocket;

        @Override
        public void run() {
            //set the address and port of the proxy to get tasks. This info is in comet.properties
            String proxyAddress = properties.getProperty("IsolatedProxy");
            int proxyPort = Integer.parseInt(properties.getProperty("IsolatedProxyPort", new Integer(ControlConstant.PROXY_PORT).toString()).trim());

            try {
                servSock = new ServerSocket(servPort);
                servSock.setReuseAddress(true);
                DataInputStream input = null;
                Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "Agent ready at "+new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
                while (alive) {
                    try {
                        clientSocket = servSock.accept();
                        input = new DataInputStream(clientSocket.getInputStream());

                        String command = input.readUTF();

                        Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "command: " + command);
                        //request task
                        if (command.equals("WorkerReqTask")) {
                            String address = input.readUTF();
                            int port = input.readInt();
                            int numtasks = input.readInt();
                            String template = input.readUTF();

                            System.out.println("CWDEBUG. agentlite. worker requesttask " + address + ":" + port);
                            //request task to proxy
                            List param = new ArrayList();
                            param.add("WorkerReqTask");
                            param.add(servPublicIp);
                            param.add(servPort);
                            param.add(numtasks);
                            param.add(template);
                            param.add(address + ":" + port);

                            System.out.println("CWDEBUG. agentlite. forwarding petition to proxy " + proxyAddress + ":" + proxyPort);

                            if (agent_state) {
                                sendInfo(proxyAddress, proxyPort, "WorkerReqTask", param);
                            }
                            String curInter = currentInterest.get(address + ":" + port);
                            if (curInter == null || !curInter.equals(template)) {//this can control if the worker changes its template.
                                currentInterest.put(address + ":" + port, template);
                            }

                            workerRetries.put(address + ":" + port, 0);

                            //does not get tasks 
                        } else if (command.equals("RHMSG_NOTASK")) {
                            int numTasks = input.readInt();
                            String taskTupleTemplate = input.readUTF();
                            String worker = input.readUTF();
                            Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: No tasks Received "
                                    + "for template:{0} worker:{1}", new Object[]{taskTupleTemplate, worker});

                            //check if workers is active
                            String[] workparts = worker.split(":");
                            // Moustafa - modified checking on the workers only if the resource manager is enabled
                            if (startManagement && resManager.getResources().checkWorkerStatus(workparts[0], Integer.parseInt(workparts[1])).equals("down")) {
                                Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: Worker {0} "
                                        + " is down ", new Object[]{worker});
                            } else {
                                String regInter = currentInterest.get(worker);
                                if (regInter.equals(taskTupleTemplate)) {
                                    int retries = workerRetries.get(worker);
                                    workerRetries.put(worker, ++retries);
                                    if (retries >= noTaskCountMax) {
                                        Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: Retries over "
                                                + "for template:{0} worker:{1}. SOME ACTION WILL BE TAKEN", new Object[]{taskTupleTemplate, worker});
                                        //do something. Kill workers, make them sleep, change their queries ...
                                        Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: STOPVM:" + worker.split(":")[0] + ":" + System.currentTimeMillis());

                                        //terminate nicely the worker
                                        String wkfStage = resManager.getResources().getWflStageWorker(worker);
                                        if (!wkfStage.equals("null")) {
                                            Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: releaseOneWorker. " + worker);
                                            String[] wkfStageParts = wkfStage.split("\\.");
                                            resManager.releaseOneDoneWorker(worker, wkfStageParts[0], wkfStageParts[1]);
                                        } else {
                                            Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: releaseOneWorker. workflow.stage NOT FOUND");
                                            System.out.println("JAVTEST. AgentLite: releaseOneWorker. workflow.stage NOT FOUND");
                                        }

                                        //tell worker to die. This will be treated as failure
                                        //                                        List param=new ArrayList();
                                        //                                        param.add("true");
                                        //                                        sendInfo(workparts[0], Integer.parseInt(workparts[1]), "TerminateWorker", param);
                                    } else {
                                        //resend the petition
                                        //request task to proxy
                                        List param = new ArrayList();
                                        param.add("WorkerReqTask");
                                        param.add(servPublicIp);
                                        param.add(servPort);
                                        param.add(numTasks);
                                        param.add(taskTupleTemplate);
                                        param.add(worker);
                                        sendInfo(proxyAddress, proxyPort, "WorkerReqTask", param);

                                    }
                                } else {//ignore it
                                    Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "AgentLite: Worker {0} "
                                            + "changed its interest to:{1}", new Object[]{worker, taskTupleTemplate});
                                }
                            }

                            //get tasks
                        } else if (command.equals("RHMSG_TASK")) {
                            int length = input.readInt(); //data length
                            byte[] bytesdata = new byte[length]; //tuples
                            input.readFully(bytesdata);
                            String taskTupleTemplate = input.readUTF();//template
                            String worker=input.readUTF();                            
                            String []workerParts=worker.split(":");
                            String tempTasks=input.readUTF();
                            
                            System.out.println("CWDEBUG. agenlite. temptasks "+tempTasks);
                            
                            //send task worker
                            List param = new ArrayList();
                            param.add("RHMSG_TASK");
                            param.add(ControlConstant.RHMSG_TASK);
                            param.add(length);
                            param.add(bytesdata);
                            param.add(taskTupleTemplate);
                            
                            Long startTime=System.currentTimeMillis();
                            param.add(startTime);
                                        
                            String addressWorker=workerParts[0];
                            int portWorker=Integer.parseInt(workerParts[1]);
                            
                            if(startSSHTunnelAgent){
                                Integer localport=resManager.getResources().getPortMapWorker(addressWorker,portWorker);
                                if(localport!=null){
                                    addressWorker="localhost";
                                    portWorker=localport.intValue();
                                }
                            }
                            
                            //Store DB
                            if(useMeticsService){
                                try {
                                    //this is a List of XmlTuple                                    
                                    List receivedObject = (List)programming5.io.Serializer.deserialize(bytesdata);
                                    if(!receivedObject.isEmpty()){
                                        //We assume we only get one tuple at a time (default CometCloud behaviour)                                        
                                        XmlTuple taskTuple = (XmlTuple)receivedObject.get(0);                                        
                                        int taskId=Integer.parseInt((String) taskTuple.getTupleFields().get(CometConstants.TaskId));
                                        String app=(String) taskTuple.getTupleFields().get("StageAppName");                                
                                        String workflowID=(String) taskTuple.getTupleFields().get("WorkflowID");
                                        String stageId=(String) taskTuple.getTupleFields().get("StageID");
                                        
                                        System.out.println("CWDEBUG. agentlite.startTimeTask. taskid="+taskId+" app="+app+" workflowid="+workflowID+" stageid="+stageId);
                                        
                                        List paramTemp=new ArrayList(Arrays.asList(taskId,app,startTime, workflowID, stageId));
                                        sendInfo(AgentMetricsAddress,AgentMetricsPort,"dbWriteAgentStartTimeTask",paramTemp);
                                                                                
                                    }
                                } catch (ClassNotFoundException ex) {
                                    Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (UnknownHostException ex) {
                                    Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);         
                                }
                                
                            }
                            
                            if(sendInfo(addressWorker, portWorker, "RHMSG_TASK", param)){

                                //store the tasks a worker is running. If the worker goes down, we tell Master to regenerate them                                
                                currentTasks.put(worker, new ArrayList(Arrays.asList(tempTasks.split(","))));

                                System.out.println("CWDEBUG. agenlite. tasks worker: " + StringUtils.join(currentTasks.get(worker).toArray(), ","));
                                taskTimes.put(StringUtils.join(currentTasks.get(worker).toArray(), ","), System.currentTimeMillis());

                            } else {
                                //TODO: tell master to regenerate TASKS (via proxy or autonomicscheduler??)
                                System.out.println("CWDEBUG. agentline.: worker "+worker+" down, request regenerate tasks  TODO  TODO");
                                //store the tasks a worker is running. If the worker goes down, we tell Master to regenerate them                                
                                currentTasks.put(worker, new ArrayList(Arrays.asList(tempTasks.split(","))));
                            }

                            //result
                        } else if (command.equals("Results")) {
                            if (agent_state) {
                                //get results from worker
                                String master = input.readUTF();
                                int tag = input.readInt();
                                int taskid = input.readInt();
                                int status = input.readInt();
                                String message = input.readUTF();
                                int size = input.readInt();
                                byte[] d;
                                Object receivedObject = null;
                                if (size > 0) {
                                    d = new byte[size];
                                    input.readFully(d);
                                    try {
                                        receivedObject = programming5.io.Serializer.deserialize(d);
                                    } catch (ClassNotFoundException ex) {
                                        Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            String appName=input.readUTF();
                            String machineType=input.readUTF(); //vm type in cloud , worker type in OSG or IP in cluster
                            String workflowId=input.readUTF();
                            Long startTime=input.readLong();
                            String getInputTime=input.readUTF(); //"start,end,size"
                            String putOutputTime=input.readUTF(); //"start,end,size"
                            
                            Long endTime=System.currentTimeMillis();
                            if(useMeticsService){
                                List param=new ArrayList(Arrays.asList(taskid,endTime, appName, machineType, workflowId, startTime, getInputTime, putOutputTime));
                                sendInfo(AgentMetricsAddress,AgentMetricsPort,"dbWriteAgentEndTimeTask",param);                                
                            }                            
                            
                            System.out.println("CWDEBUG. agenlite. DONE: "+taskid+" "+taskTimes.get(String.valueOf(taskid))+" "+endTime);
                            
                            //files are sent from the worker using the fileserver

                                System.out.println("CWDEBUG. agenlite. DONE: " + taskid + " " + taskTimes.get(String.valueOf(taskid)) + " " + System.currentTimeMillis());

                            //files are sent from the worker using the fileserver
                                //Send result to Master
                                Sender sender = getSender();
                                CMessage cmsg = new CMessage(tag, taskid, status);
                                cmsg.setPayLoad(receivedObject);
                                cmsg.setMsg(message);
                                sender.sendOut(cmsg, master, Integer.parseInt(properties.getProperty("TCP_CONTROL_PORT", ControlConstant.TCP_CONTROL_PORT).trim()));
                            }
                        } else {
                            Logger.getLogger(AgentLite.class.getName()).log(Level.INFO, "Unknown command: {0}", command);
                        }

                    } catch (IOException e) {
                        Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, e);
                    } finally {
                        if (input != null) {
                            input.close();
                        }
                    }
                }

            } catch (IOException ex) {
                System.out.println("ERROR: starting Server in " + servPort);
                Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public void quit() {
            alive = false;
            closeSockets();
        }

        private void closeSockets() {
            try {
                clientSocket.close();
            } catch (Exception e) {
                if (!alive) {
                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            try {
                servSock.close();
            } catch (Exception e) {
                if (!alive) {
                    Logger.getLogger(AgentFileServer.class.getName()).log(Level.SEVERE, null, e);
                }

            }
        }

    }

    private boolean sendInfo(String addess, int port, String command, List param) {
        boolean status = true;
        DataOutputStream conn = null;
        try {
            conn = CommonUtils.getOutput(addess, port);            
            if (conn!=null){                
                if(command.equals("WorkerReqTask")){
                    conn.writeUTF((String)param.get(0));
                    conn.writeUTF((String)param.get(1));
                    conn.writeInt((Integer)param.get(2));
                    conn.writeInt((Integer)param.get(3));
                    conn.writeUTF((String)param.get(4));
                    conn.writeUTF((String)param.get(5));
                }else if (command.equals("RHMSG_NOTASK")) {
                    
                }else if (command.equals("RHMSG_TASK")) {
                    conn.writeUTF((String)param.get(0));
                    conn.writeInt((Integer)param.get(1));
                    conn.writeInt((Integer)param.get(2));                        
                    conn.write((byte[])param.get(3));                        
                    conn.writeUTF((String)param.get(4));
                    conn.writeLong((Long)param.get(5));
                }else if (command.equals("RequestTask")) {
                    conn.writeUTF("RequestTask");
                } else if (command.equals("TerminateWorker")) {
                    conn.writeUTF("TerminateWorker");
                    conn.writeUTF((String)param.get(0));
                }else if(command.equals("dbWriteAgentStartTimeTask")){                    
                    conn.writeUTF(command);
                    conn.writeUTF(dbName);
                    conn.writeInt((Integer)param.get(0));
                    conn.writeUTF((String)param.get(1));
                    conn.writeLong((Long)param.get(2));
                    conn.writeUTF((String)param.get(3));
                    conn.writeUTF((String)param.get(4));
                    
                }else if(command.equals("dbWriteAgentEndTimeTask")){
                    conn.writeUTF(command);
                    conn.writeUTF(dbName);
                    
                    conn.writeInt((Integer)param.get(0));
                    conn.writeLong((Long)param.get(1));
                    conn.writeUTF((String)param.get(2));
                    conn.writeUTF((String)param.get(3));
                    conn.writeUTF((String)param.get(4));
                    conn.writeLong((Long)param.get(5));
                    conn.writeUTF((String)param.get(6));
                    conn.writeUTF((String)param.get(7));
                                        
                }
            } else {
                Logger.getLogger(AgentLite.class.getName()).log(Level.WARNING, "Could not connect with " + addess + ":" + port);
                status = false;
            }
        } catch (IOException e) {
            Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, e);
            status = false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ex) {
                    Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return status;
    }

    private Sender getSender() {
        Sender ret;
        if (System.getProperty("chord.NETWORK_CLASS", "TCP").equals("SSL")) { //chord.NETWORK_CLASS is not in the environment variables
            ret = new SendSSL();
            //System.out.println("getSender: Created a SSL sender");
        } else {
            ret = new SendTCP();
            //System.out.println("getSender: Created a TCP sender");
        }
        return ret;
    }

    public static void main(String[] args) {
        if (args != null) {
            try {
                
                List<String> appPropertyFiles = CommonUtils.getMultipleStringArg(args,"-propertyFile");
                Properties p = new Properties();
                for (String propertyFile : appPropertyFiles) {
                    p.load(new FileInputStream(propertyFile));
                }

                AgentLite wfM = new AgentLite(p);
                wfM.start();

            } catch (IOException e) {
                System.err.println("ERROR: Check loading property files. " + e.getMessage());
            }
        } else {
            System.err.println("ERROR: AgentLite: At least You need to specify the property file with agent configuration and comet.properties.");
            System.exit(1);
        }

    }

    public void removeWorker(String worker) {
        workerRetries.remove(worker);
        currentInterest.remove(worker);
        currentTasks.remove(worker);
    }

    public AgentFileServer getFileserver() {
        return fileserver;
    }

    public int getFileServerPort() {
        return FileServerPort;
    }

    public int getServPort() {
        return servPort;
    }

    protected void enableAgent() {
        agent_state = true;
    }

    protected void disableAgent() {
        agent_state = false;
    }

    protected boolean getAgentState() {
        return agent_state;
    }

}
