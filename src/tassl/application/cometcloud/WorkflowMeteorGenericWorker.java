/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * MeteorAppWorker.java
 *
 * Created on Nov 26, 2010 9:39:30 PM
 */
package tassl.application.cometcloud;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

import programming5.io.Debug;
import tassl.automate.network.CMessage;
import tassl.automate.network.ControlConstant;
import tassl.automate.network.MessageType;
import tassl.automate.application.network.Sender;
import tassl.automate.application.network.SendTCP;
import tassl.automate.application.network.SendSSL;
import tassl.automate.application.network.SendClient;
import tassl.automate.application.network.TCPClient;
import tassl.automate.application.network.SSLClient;
import tassl.automate.comet.CometConstants;
import tassl.automate.comet.CometSpace;
import tassl.automate.comet.XmlTuple;
import tassl.automate.comet.xmltuplespace.XmlTupleService;
import tassl.automate.meteor.Meteor;
import tassl.automate.meteor.MeteorNotifier;
import tassl.automate.meteor.application.MeteorWorkerFramework;
import tassl.application.cometcloud.WorkflowTaskTuple;
import tassl.application.node.isolate.IsolatedWorker;
import tassl.application.utils.CommonUtils;
import tassl.automate.overlay.OverlayService;
import tassl.automate.programmodel.masterworker.WorkerFramework;

/**
 * @author Samprita Hegde created on Nov 26, 2010 9:39:30 PM
 *
 */
public abstract class WorkflowMeteorGenericWorker implements MeteorWorkerFramework, MeteorNotifier {
    
    private Meteor mSpace;
    private int workerid;
    private String peerIP;
    private OverlayService overlays;
    private boolean workerAlive = true;
    public Sender sender = getSender();
    private String fileTransferType;//default for rsync, osgSCP for OSG scp or osgRsync for OSG rsync

    //these are to refresh the subscription    
    long srefreshtime = 0;
    Thread refreshSubscription = null;
    long monSubPeriod = 0;
    boolean subscribed = false;
    private final Object lock = new Object();
    
    private int ttl;
    private int numSubscriptions;
    
    private int initialTaskCount = 1;
    
    String taskids = "*"; //if passed it will be "task|task|task"
    String appName = "*";
    String requirement = "*";
    private String wfManagerName = "*";
    private String workflowID = "*";
    private String stageID = "*";
    String resultAddress;   //this is the address of the agent. 
    int resultPort = Integer.parseInt(System.getProperty("TCP_CONTROL_PORT", ControlConstant.TCP_CONTROL_PORT).trim()); //the port where we send the results. It has a default one that need to be 
    //  changed when using the AgentLite
    
    boolean underAgent = true;//Isolated return result to Agent (listening in IsolatedProxy:IsolatedProxyPort) instead of to the Master.
    
    int fileServerPort;
    
    int currentTaskId;
    HashMap<Integer, String> getInputTime = new HashMap(); //taskId:"timeStart,timeEnd,size"
    HashMap<Integer, String> putOutputTime = new HashMap(); //taskId:"timeStart,timeEnd,size"
    
    public String getResultAddress() {
        return resultAddress;
    }

    public int getMasterPort() {
        return resultPort;
    }

    public void setResultAddress(String address) {
        this.resultAddress = address;
    }

    public void setResultPort(int port) {
        this.resultPort = port;
    }    
    
    public String getWfManagerName() {
        return wfManagerName;
    }

    public String getWorkflowID() {
        return workflowID;
    }

    public String getStageID() {
        return stageID;
    }

    public void setWfManagerName(String wfManagerName) {
        this.wfManagerName = wfManagerName;
    }

    public void setWorkflowID(String workflowID) {
        this.workflowID = workflowID;
    }

    public void setStageID(String stageID) {
        this.stageID = stageID;
    }

    /**
     * Get Application Name
     *
     * @return String
     */
    public String getAppName() {
        return appName;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setAppName(String name) {
        this.appName = name;
    }

    public void setRequirement(String req) {
        this.requirement = req;
    }
    
    public void setTaskids(String taskids) {
        this.taskids = taskids;
    }
    
    public int getInitialTaskCount() {
        return initialTaskCount;
    }

    public void setInitialTaskCount(int value) {
        initialTaskCount = value;
    }

    /**
     * Get working directory
     *
     * @return String
     */
    public String getWorkingDirectory() {
        return System.getProperty("WorkingDir");
    }
    
    public void MeteorGenericWorker() {        
    }

    public void loadParams() {
        appName = System.getProperty("appName");
        if (appName == null) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, "There is no Application Name. Leaving...");
            System.exit(1);
        }
        taskids = System.getProperty("taskids", "*");
        requirement = System.getProperty("requirement", "*");        
        wfManagerName = System.getProperty("wfManagerName", "*");        
        workflowID = System.getProperty("workflowID", "*");        
        stageID = System.getProperty("stageID", "*");
        fileTransferType = System.getProperty("fileTransferType", "default");//default uses rsync
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.WARNING, "fileTransferType for this worker is."+fileTransferType);
        String agent = System.getProperty("UnderAgent");
        if (agent == null) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.WARNING, "UnderAgent not provided. Assuming it is true and therefore the Worker will send results to an Agent.");
            underAgent = true;
        } else {
            if (agent.equals("true")) {
                underAgent = true;
            } else {
                underAgent = false;
            }
        }
        //Agent Information
        resultAddress = System.getProperty("IsolatedProxy");
        if (resultAddress == null) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, "IsolatedProxy not provided. There is no Agent Address. Leaving...");
            System.exit(1);
        }
        String port = System.getProperty("IsolatedProxyPort");
        if (port == null) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.WARNING, "IsolatedProxyPort not provided. The port for the agent will be " + Integer.parseInt(System.getProperty("TCP_CONTROL_PORT", ControlConstant.TCP_CONTROL_PORT).trim()));
        } else {
            resultPort = Integer.parseInt(port.trim());
        }
        String fsPort = System.getProperty("FileServerPort");
        if (port == null) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.WARNING, "fileServerPort not provided.");
        } else {
            fileServerPort = Integer.parseInt(fsPort);
        }
    }
    
    public String getPeerIP() {
        return peerIP;
    }
    
    public int getWorkerid() {
        return workerid;
    }
    
    public void setPeerIP(String publicAddress) {
        peerIP = publicAddress;
    }

    /* (non-Javadoc)
     * @see tassl.automate.meteor.application.MeteorWorkerFramework#setMeteorEnv(tassl.automate.meteor.Meteor, int, java.lang.String, tassl.automate.overlay.OverlayService)
     */
    @Override
    public void setMeteorEnv(Meteor space, int workerID, String peerIPaddr, OverlayService overlay) { //NOT USED
        mSpace = space;
        workerid = workerID;
        peerIP = peerIPaddr;
        overlays = overlay;

        //important for communication with master
        sender.SetWorkerHandle((WorkerFramework) this);
    }

    /* (non-Javadoc)
     * @see tassl.automate.programmodel.masterworker.WorkerFramework#computeTask(java.lang.Object)
     */
    @Override
    public Object computeTask(Object obj) {
        WorkflowTaskTuple taskTup = null;
        Object ret = null;
        try {
            Class taskclass = Class.forName(System.getProperty("TaskClass"));            
            taskTup = (WorkflowTaskTuple) taskclass.newInstance();
            
            taskTup.getTaskTuple((XmlTuple) obj);

            //set taskId of task being executed
            this.currentTaskId = taskTup.getTaskid();
            
            Object dataobj = programming5.io.Serializer.deserialize(((XmlTuple) obj).getData());
            ret = computeTaskSpecific(dataobj, taskTup);
            
        } catch (IOException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("AppWorker " + peerIP + " consumes taskid " + taskTup.getTaskid());
        return ret;
    }

    /* (non-Javadoc)
     * @see tassl.automate.programmodel.masterworker.WorkerFramework#quit()
     */
    @Override
    public void quit() {
        workerAlive = false;
        if (refreshSubscription != null) {
            refreshSubscription.interrupt();
        }
    }

    /* (non-Javadoc)
     * @see tassl.automate.programmodel.masterworker.WorkerFramework#sendResultToMaster(int, java.lang.Object, java.lang.String, java.lang.String)
     */
    @Override
    public void sendResultToMaster(int taskid, Object data, String message, String masterName) {
        
        if (!underAgent) {
            //send result to the master indicated in the tasktuple (regular IsolatedWorker)
            CMessage cmsg = new CMessage(MessageType.Block_result, taskid, 0);
            cmsg.setPayLoad(data);
            cmsg.setMsg(message);            
            sender.sendOut(cmsg, masterName, Integer.parseInt(System.getProperty("TCP_CONTROL_PORT", ControlConstant.TCP_CONTROL_PORT).trim()));
        } else {
            //send result to the agent
            DataOutputStream output = CommonUtils.getOutput(resultAddress, resultPort);
            if (output != null) {
                try {
                    output.writeUTF("Results");
                    output.writeUTF(masterName);
                    output.writeInt(MessageType.Block_result);
                    output.writeInt(taskid);
                    output.writeInt(0);
                    output.writeUTF(message);
                    byte[] d;
                    if (data != null) {
                        d = programming5.io.Serializer.serializeBytes(data);
                        output.writeInt(d.length);
                        output.write(d);
                        output.flush();
                    } else {
                        output.writeInt(0);
                        output.flush();
                    }
                    output.writeUTF(System.getProperty("appName"));                    
                    output.writeUTF(System.getProperty("machineType"));
                    output.writeUTF(System.getProperty("workflowID"));
                    output.writeLong(new Long(System.getProperty("startTime" + taskid)));
                    if (getInputTime.containsKey(taskid)) {
                        output.writeUTF(getInputTime.get(taskid)); //"start,end"
                    } else {
                        output.writeUTF("-1,-1"); //"start,end"
                    }
                    if (putOutputTime.containsKey(taskid)) {
                        output.writeUTF(putOutputTime.get(taskid)); //"start,end"
                    } else {
                        output.writeUTF("-1,-1"); //"start,end"
                    }
                    
                    getInputTime.remove(taskid);
                    putOutputTime.remove(taskid);
                    
                } catch (IOException e) {
                    Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, e);
                } finally {
                    if (output != null) {
                        try {
                            output.close();
                        } catch (IOException ex) {
                            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                
            }
        }
    }
    /*moved to utils
     private DataOutputStream getOutput(String address, int port) {
     DataOutputStream out = null;
     try {
     Socket clientSocket = new Socket(address, port);
     out = new DataOutputStream(clientSocket.getOutputStream());
     } catch (UnknownHostException ex) {
     Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
     } catch (IOException ex) {
     Logger.getLogger(AgentLite.class.getName()).log(Level.SEVERE, null, ex);
     }
     return out;
     }
     */

    /* (non-Javadoc)
     * @see tassl.automate.programmodel.masterworker.WorkerFramework#startWorker()
     */
    @Override
    public void startWorker() {//NOT USED
        Debug.println("Meteor space has been initialized", "Meteor.app");
        System.out.println("StartWorker " + Thread.currentThread().getName());
        monSubPeriod = Long.parseLong(System.getProperty("meteor.RefreshSubscriptionPeriod", "0").trim());
        if (monSubPeriod != 0) {
            //Monitor enabled, the subscription is refreshed every X milliseconds
            //This is useful when peers, that insert tasks, join the overlay after the initial subscription is done.
            //Once the subscription is refreshed (unsuscribe and subcribe) everyone in the overlay konws about it.
            RefresSubscriptionThread rfs = new RefresSubscriptionThread();
            refreshSubscription = new Thread(rfs);
            refreshSubscription.start();
        }
        
        numSubscriptions = Integer.parseInt(System.getProperty("meteor.NumberSubscription", "1").trim());
        ttl = numSubscriptions;
        meteorSubscribe(numSubscriptions);        
        subscribed = true;
    }
    
    public void meteorSubscribe(int ttl) {//NOT USED
        //get XmlTuple for query
        XmlTuple template;        
        template = getTemplateQuery();
        //The last value is the number of tasks to be consumed before the subscription expires. 0 means infinite.
        mSpace.meteorSubscribe(CometConstants.spacename, template, peerIP, this, ttl);
        
    }
    
    public void meteorUnsubscribe() {//NOT USED
        //get XmlTuple for query
        XmlTuple template;
        template = getTemplateQuery();
        mSpace.meteorUnsubscribe(CometConstants.spacename, template, peerIP, this);
        
    }

    /* (non-Javadoc)
     * @see tassl.automate.meteor.MeteorNotifier#meteorNotifyNode(tassl.automate.comet.XmlTuple)
     */
    @Override
    public synchronized void meteorNotifyNode(XmlTuple tup) {//NOT USED
        
        consumeTask(tup);
        
        synchronized (lock) {  //to prevent executing several tasks at the same time when the subscription is for several tasks     
            if (workerAlive && subscribed && ttl == 0) {// && monSubPeriod==0){
                //request for nect task
                System.out.println("ReSubscribed after consuming Task");
                ttl = numSubscriptions;
                meteorSubscribe(numSubscriptions);
            }            
        }
    }
    
    @Override
    public void consumeTask(XmlTuple tup) {
        synchronized (lock) {  //to prevent executing several tasks at the same time when the subscription is for several tasks
            Object result;
            WorkflowTaskTuple taskTup = null;
            try {
                Class taskclass = Class.forName(System.getProperty("TaskClass"));                
                taskTup = (WorkflowTaskTuple) taskclass.newInstance();                
                
                taskTup.getTaskTuple(tup);
                
                ttl--;
                srefreshtime = System.currentTimeMillis();
                if (taskTup.data[0] == 0) {
                    System.out.println("data is null??");
                } else {
                    try {
                        
                        System.out.println("Start consuming a task " + taskTup.getTaskid());                        
                        
                        Object obj = programming5.io.Serializer.deserialize(tup.getData());
                        if (obj != null && obj.toString().equals("poisonpill") == true) {
                            System.out.println("worker " + workerid + " get poison pill");

                            //send a notification to master for terminating instances
                            String myName = System.getProperty("chord.LOCAL_URI", "//" + InetAddress.getLocalHost().getHostAddress());
                            myName = myName.substring(2);
                            SendClient client = getClient();
                            client.sendMsgToMaster(myName, MessageType.Worker_leave, null, taskTup.getMasterName());
                            System.out.println("worker " + workerid + " sends master its leave");

                            /*
                             //check if local space is empty
                             while (mSpace.getRequestQueue().getNumTuples(template)> 0) {
                             try {
                             Thread.sleep(100);
                             } catch (InterruptedException ex) {
                             Logger.getLogger(AppWorker.class.getName()).log(Level.SEVERE, null, ex);
                             }
                             }
                             */
                            //leave the overlay
                            //overlays.leave();
                            System.out.println("worker " + workerid + " overlay leave");
                            quit();
                            
                        } else {
                            long stime = System.currentTimeMillis();

                            //do something with task
                            //  memMon.reset();        
                            result = computeTask(tup);
                            //memMon.getUsedMem();
                            long etime = System.currentTimeMillis();
                            String msg = "task " + taskTup.getTaskid() + " " + (etime - stime);

                            //send the calculated result back to the master using TCP
                            sendResultToMaster(taskTup.getTaskid(), result, msg, taskTup.getMasterName());
                        }
                        
                    } catch (IOException ex) {
                        System.out.println("MeteorAppWorker: IOException");
                        Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        System.out.println("MeteorAppWorker: ClassNotFoundException");
                        Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                    
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /* (non-Javadoc)
     * @see tassl.automate.programmodel.masterworker.WorkerFramework#setCometEnv(tassl.automate.comet.CometSpace, int, java.lang.String, tassl.automate.overlay.OverlayService)
     */
    @Override
    public void setCometEnv(CometSpace space, int workerID, String peerIPaddr, OverlayService overlay) {//NOT USED
        throw new UnsupportedOperationException("Not supported yet.");
        
    }
    
    private SendClient getClient() {
        SendClient ret;
        if (System.getProperty("chord.NETWORK_CLASS", "TCP").equals("SSL")) {
            ret = new SSLClient();
            //System.out.println("getClient: Created a SSL client");
        } else {
            ret = new TCPClient();
            //System.out.println("getClient: Created a TCP client");
        }
        return ret;
    }
    
    private Sender getSender() {
        Sender ret;
        if (System.getProperty("chord.NETWORK_CLASS", "TCP").equals("SSL")) {
            ret = new SendSSL();
            //System.out.println("getSender: Created a SSL sender");
        } else {
            ret = new SendTCP();
            //System.out.println("getSender: Created a TCP sender");
        }
        return ret;
    }
    
    private class RefresSubscriptionThread implements Runnable {//NOT USED

        public void run() {            
            srefreshtime = System.currentTimeMillis();
            long etime;
            while (workerAlive) {                
                etime = System.currentTimeMillis();                
                if (monSubPeriod < (etime - srefreshtime)) {
                    System.out.println("MeteorAppWorker: refreshing subscription");
                    subscribed = false;                    
                    synchronized (lock) {                        
                        meteorUnsubscribe();                        
                        meteorSubscribe(numSubscriptions);                        
                        ttl = numSubscriptions;
                        subscribed = true;
                    }
                    srefreshtime = System.currentTimeMillis();
                }
                try {
                    Thread.sleep(monSubPeriod);
                } catch (InterruptedException ex) {
                    if (workerAlive) {
                        Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    //method to overrite if we customize the TaskTuple by extending GenericTaskTuple class
    @Override
    public XmlTuple getTemplateQuery() {
        XmlTuple template;
        WorkflowTaskTuple taskTup = new WorkflowTaskTuple();        
        template = taskTup.getQuery(taskids, wfManagerName, workflowID, stageID, appName, requirement);
        
        return template;
    }

    /**
     * Retrieve input files to the local worker.
     *
     * @param useLocalCache Whether we check in the Agent local storage for the
     * a copy of the file or not.
     * @param remotedir Address and Directory where the original file is stored.
     * @param files List of files to be retrieved.
     * @param dirInWorker Directory where we want to have the file in our local
     * worker.
     * @return String with OK or ERROR:message
     */
    public String getFile(Boolean useLocalCache, String remotedir, List files, String dirInWorker) {
        Socket clientSocket;
        DataOutputStream output = null;
        DataInputStream input = null;
        String valueToReturn = "ERROR";
        try {   //connect with the agent file server to get files
            clientSocket = new Socket(resultAddress, fileServerPort);
            output = new DataOutputStream(clientSocket.getOutputStream());            
            input = new DataInputStream(clientSocket.getInputStream());
            //get|true|remotedir|file1,file2,file3|dirInWorker|key|"worker"|fileTransferType
            output.writeUTF(StringUtils.join(new Object[]{"get", useLocalCache, remotedir,
                StringUtils.join(files.toArray(), ","), dirInWorker, System.getProperty("keyfile", "null"), "worker", fileTransferType}, "|"));
            if (fileTransferType.equals("default")) {
                if (peerIP == null) {
                    output.writeUTF(InetAddress.getLocalHost().getHostAddress());
                } else {
                    output.writeUTF(peerIP);
                }
            } else if (fileTransferType.contains("osg")) {
                String jobID = System.getenv("CONDOR_JOB_ID");
                Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.WARNING, "the jobID for this worker is."+jobID);
                output.writeUTF(jobID);
            }
            valueToReturn = input.readUTF();
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, resultAddress + ":" + fileServerPort, ex);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }        
        return valueToReturn;
    }

    /**
     * Upload a file to a specific machine and directory.
     *
     * @param remotedir Address and Directory to store the file.
     * @param files List of files to be stored.
     * @param dirInWorker Directory where the files are in our local worker.
     * @return String with OK or ERROR:message
     */
    public String putFile(String remotedir, List files, String dirInWorker) {
        Socket clientSocket;
        DataOutputStream output = null;
        DataInputStream input = null;
        String valueToReturn = "ERROR";
        try {//connect with the agent file server to put the file
            clientSocket = new Socket(resultAddress, fileServerPort);
            output = new DataOutputStream(clientSocket.getOutputStream());            
            input = new DataInputStream(clientSocket.getInputStream());
            ///put|remotedir|file1,file2,file3|dirInWorker 
            output.writeUTF(StringUtils.join(new Object[]{"put", remotedir, StringUtils.join(files.toArray(), ","), dirInWorker, System.getProperty("keyfile", "null"),fileTransferType}, "|"));
            if (fileTransferType.equals("default")) {
                if (peerIP == null) {
                    output.writeUTF(InetAddress.getLocalHost().getHostAddress());
                } else {
                    output.writeUTF(peerIP);
                }
            } else if (fileTransferType.contains("osg")) {
                String jobID = System.getenv("CONDOR_JOB_ID");
                Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.WARNING, "the jobID for this worker is."+jobID);
                output.writeUTF(jobID);
            }
            valueToReturn = input.readUTF();
        } catch (IOException ex) {
            Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, resultAddress + ":" + fileServerPort, ex);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    Logger.getLogger(IsolatedWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }        
        return valueToReturn;
    }

    /**
     * Retrieve input files and create a list of file names. Use this when
     * interested on collecting transfer metrics.
     *
     * @param inputs List of FileProperties files
     * @param workingdir Path to the working dir where files will be downloaded
     * @return List<String> of files downloaded
     */
    protected List<String> downloadInputs(List<FileProperties> inputs, String workingdir) {
        double size = 0;
        
        long startGetInput = System.currentTimeMillis();
        int numberOfFiles = 0;
        //it will have site:{file,file,},..
        HashMap<String, List> inputsHash = new HashMap();
        List<String> filenames = new ArrayList();
        for (FileProperties fp : inputs) {            
            List temp = inputsHash.get(fp.getLocation());
            if (temp == null) {
                temp = new ArrayList();
                inputsHash.put(fp.getLocation(), temp);
            }
            temp.add(fp.getName());
            
            size += fp.getSize();//calculate overall size
            numberOfFiles++;
            
            filenames.add(workingdir + "/" + fp.getName());
        }        
        for (String site : inputsHash.keySet()) {
            String status = this.getFile(true, site, inputsHash.get(site), workingdir);
            Logger.getLogger(WorkflowMeteorGenericWorker.class.getName()).log(Level.INFO, "Getting {0} from {1}: {2}",
                    new Object[]{StringUtils.join(inputsHash.get(site).toArray(), ","), site, status});
        }
        
        long stopGetInput = System.currentTimeMillis();
        
        getInputTime.put(currentTaskId, startGetInput + "," + stopGetInput + "," + size + "," + numberOfFiles);
        
        return filenames;
    }

    /**
     * Upload results to the destination and create the list of result files.
     * Use this when interested on collecting transfer metrics.
     *
     * @param outputfiles Name of generated files
     * @param workingdir Directory where files are located
     * @param outputFP Destination of the results. If location is empty then
     * they are left in the staging area of the site.
     * @return List of result FileProperty
     */
    protected List<FileProperties> uploadResults(List<String> outputfiles, String workingdir, FileProperties outputFP) {
        List outfiles = new ArrayList();
        List<FileProperties> resultFiles = new ArrayList();
        
        long startPutOutput = System.currentTimeMillis();
        
        double sizeFiles = 0;

        //Obtain information for results	
        String outputlocation = ""; //this should be machine:dir
        String nameout = "";
        String zoneout = "";
        //if our Result field is empty in the workflow description
        if (outputFP.getLocation().trim().equals("")) {
            outputlocation = System.getProperty("siteFileCache"); //Site cache directory that can be use as data repository (siteIp:dir)
            nameout = System.getProperty("Name");
            zoneout = System.getProperty("Zone");
        } else {
            outputlocation = outputFP.getLocation();
            zoneout = outputFP.getZone();
            nameout = outputFP.getSitename();
        }
        
        String filesToRemove = "";
        int numberOfFiles = 0;
        for (String outfile : outputfiles) {
            
            outfiles.add(outfile);            
            filesToRemove += " " + workingdir + "/" + outfile;
            
            File fp = new File(workingdir + "/" + outfile);
            long size = fp.getTotalSpace();
            FileProperties resultfile = new FileProperties(outfile, outputlocation, (double) size,
                    zoneout, nameout, outputFP.getConstraints());            
            resultFiles.add(resultfile);
            
            sizeFiles += size;
            numberOfFiles++;
        }
        //send output files
        String status = this.putFile(outputFP.getLocation(), outfiles, workingdir);
        
        long stopPutOutput = System.currentTimeMillis();
        
        putOutputTime.put(currentTaskId, startPutOutput + "," + stopPutOutput + "," + sizeFiles + "," + numberOfFiles);

        //remove output files from worker
        CommonUtils.execute("rm -f " + filesToRemove);
        
        return resultFiles;
    }

    //
    /**
     * Process the tasks. It is called after computeTask retrieve the data
     * associated with the task
     *
     * @param dataobj Data Object with the data associated to the tuple. It is
     * typically a List and it was created during task creation.
     * @param tasktupl TaskTuple object with the properties of the task.
     * @return Object to return to the master once the task is done.
     */
    public abstract Object computeTaskSpecific(Object dataobj, WorkflowTaskTuple tasktupl);

    public abstract void cancelJob();
}
