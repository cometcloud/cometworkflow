/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Information about files or directories
 * 
 * @author Javier Diaz-Montes
 */
public class FileProperties  implements Serializable{
    String name;  //filename. it can be empty if FileProperties refers to a directory
    String location; //it can be user@machine:dir
    double size;  // it can be 0 if it refers to a directory

    //dataset info
    String zone;  // zone that it is part of (a zone can contain multiple sites). 
    String sitename;   //site it is part of. 
    List <String> constraints;  //list of zones and sites where the data can be moved. It can be empty
    
    //NOTE: if we modify the properties of this class, we need to include the changes in the duplicate() method

    public FileProperties(String name, String location, double size, String zone, String sitename, List<String> constraints) {
        this.name = name;
        this.location = location;
        this.size = size;
        this.zone = zone;
        this.sitename = sitename;
        this.constraints = constraints;
    }
    
    public FileProperties(String name, String location, double size, String zone, String sitename) {
        this.name = name;
        this.location = location;
        this.size = size;
        this.zone = zone;
        this.sitename = sitename;
        this.constraints = new ArrayList();
    }
    
    /**
     * Zone where the dataset is located
     * @return
     */
    public String getZone() {
        return zone;
    }

    /**
     * Name of the site where the dataset is located
     * @return
     */
    public String getSitename() {
        return sitename;
    }

    /**
     * List of sitenames and zones where the dataset can be moved
     * @return
     */
    public List<String> getConstraints() {
        return constraints;
    }
    
    /**
     * When the dataset refers to a file, this is the name of the file
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * URI of the dataset. machine:/path/
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     * When the dataset refers to a file, this is its size
     * @return
     */
    public double getSize() {
        return size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public void setConstraints(List<String> constraints) {
        this.constraints = constraints;
    }

    @Override
    public String toString() {
        return "FileProperties{" + "name=" + name + ", location=" + location + ", size=" + size + ", zone=" + zone + ", sitename=" + sitename + ", constraints=" + Arrays.toString(constraints.toArray()) + '}';
    }
   
    /**
     * Return a new Object that is a deep copy of the current one
     *  This is used to avoid clone.
     * 
     * @return
     */
    public FileProperties duplicate(){
        //String name, String location, double size, String zone, String sitename, List<String> constraints                
        List<String> constraintsNew=new ArrayList();
        for(String st:this.getConstraints()){
            constraintsNew.add(st);
        }
        return new FileProperties(this.getName(),this.getLocation(),this.getSize(),this.getZone(),this.getSitename(),constraintsNew);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        if(this.name!=null)
            hash = 7 * hash + this.name.hashCode();
        if(this.location!=null)
            hash = 7 * hash + this.location.hashCode();
        //size cannot be null
        hash = 7 * hash + ((Double)this.size).hashCode();
        if(this.zone!=null)
            hash = 7 * hash + this.zone.hashCode();
        if(this.sitename!=null)
            hash = 7 * hash + this.sitename.hashCode();
        if(this.constraints!=null)
            hash = 7 * hash + this.constraints.hashCode();        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != getClass()) {
            result = false;
        } else {
            FileProperties fp = (FileProperties) obj;
            if (this.name.equals(fp.getName()) 
                    && this.location.equals(fp.getLocation())
                    && this.size==fp.getSize()
                    && this.zone.equals(fp.getZone())
                    && this.sitename.equals(fp.getSitename())
                    && this.constraints.equals(fp.getConstraints())) {
                result = true;
            }
        }
        return result;
    }
    
    
    
}
