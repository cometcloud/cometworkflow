/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud.sampleNonBlocking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.GenerateTasksAbstract;
import tassl.application.cometcloud.GenerateTasksObject;


/**
 * This code is an application that has multiple stages. 
 * The first stage reads files from a directory and generate a task per file.
 * The second stage generates a task per result of previous stage
 * The third stage generates a task per result of previous stage
 * The fourth stage generates a task per result of previous stage
 * 
 * @author Javier Diaz-Montes
 */
public class GenerateTasks extends GenerateTasksAbstract{
    
    public GenerateTasks(){
        super();
    }
    
    //this one is done by the user
    @Override
    public GenerateTasksObject createTasks(String stageId, List<FileProperties> input, FileProperties output, String propertyFileValues, List dependencies, String method){ 
        GenerateTasksObject taskObj=null;
        
        //load properties from the file specified in the workflow XML (it can be different for each stage)
        this.loadProperties(propertyFileValues);
        
        if(method.equals("step1")){
            taskObj=step1(input,output,propertyFileValues, stageId);
        }else if(method.equals("step2")){            
            HashMap <String,FileProperties>previousFiles=this.generatePreviousResultFiles(stageId, dependencies);            
            taskObj=step2(input,output,propertyFileValues,previousFiles,stageId);            
        }else if(method.equals("step3")){
            HashMap <String,FileProperties>previousFiles=this.generatePreviousResultFiles(stageId, dependencies);            
            taskObj=step3(input,output,propertyFileValues,previousFiles,stageId);
        }else if(method.equals("step4")){
            HashMap <String,FileProperties>previousFiles=this.generatePreviousResultFiles(stageId, dependencies);            
            taskObj=step4(input,output,propertyFileValues,previousFiles,stageId);
        }
        return taskObj;
        
        
    }
    
    //example where each input file creates a task
    public GenerateTasksObject step1(List<FileProperties> input, FileProperties output, String propertyFile,String stageId){
        //input and output are address://dir. We get all using SSH
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the slowest machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        double minTimeVal=Double.parseDouble(getProperty("minTimeStep1","7"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTimeStep1","10"));
        
        String dimensions = getProperty("dimensions","2");
        String linearity = getProperty("linearity","0");
        String testCode = getProperty("testCode","0");
        String scale = getProperty("scale","0");
        
        List <List<FileProperties>>inputList=new ArrayList();
        
        
        //Each dataset is composed by two input directories
        for(int j=0;j<input.size();j+=2){
            
            FileProperties inputFP=input.get(j);
            FileProperties inputFP_mov=input.get(j+1);
                          
            String inputS=inputFP.getLocation();            
            String []parts=inputS.split(":");
            String returnSsh=executeSsh(parts[0]," ls -l "+parts[1]+" | awk '{print $5, $9}'");            
            String [] files=returnSsh.split("\n"); //returns size and name 
            
            String inputS_mov=inputFP_mov.getLocation();            
            String []parts_mov=inputS_mov.split(":");
            String returnSsh_mov=executeSsh(parts_mov[0]," ls -l "+parts_mov[1]+" | awk '{print $5, $9}'");            
            String [] files_mov=returnSsh_mov.split("\n"); //returns size and name
            
            //check //files.length==files_mov.length
            
            //lets assume that each file is the input of a task
            for(int i=0;i<files.length;i++){
                if(!files[i].trim().isEmpty()){
                    System.out.println(files[i]);                   
                    
                    //here each task only has one input, but the API requries a list
                    List <FileProperties>inputs=new ArrayList();
                    String [] fileParts=files[i].split(" ");//size and name
                    String [] fileParts_mov=files_mov[i].split(" ");//size and name

                    inputs.add(new FileProperties(fileParts[1],inputS,Double.parseDouble(fileParts[0]), inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));
                    inputs.add(new FileProperties(fileParts_mov[1],inputS_mov,Double.parseDouble(fileParts_mov[0]), inputFP_mov.getZone(), inputFP_mov.getSitename(), inputFP_mov.getConstraints()));
                    
                    taskParams.add(Arrays.asList(
                                                 "step1",              //0                                                        
                                                 output,               //1                                                        
                                                 inputs,               //2 
                                                 stageId,              //3
                                                 dimensions,           //4
                                                 linearity,            //5
                                                 testCode,             //6
                                                 scale                 //7
                                    ));
                    taskRequirement.add("large"); //Requirement
                    minTime.add(minTimeVal);
                    maxTime.add(maxTimeVal);
                    System.out.println("TaskId:"+taskid+" Images: "+ files[i]+" and "+files_mov[i]);
                    System.out.println("minTimeVal:"+minTimeVal+" maxTimeVal:"+maxTimeVal);                        
                    taskid++;
                    inputList.add(inputs);
                    
                }
            }            
            
        }
                
        printTest("step1",taskParams,inputList);
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
        
    }
    
    public void printTest(String method,List <List>taskParams,List <List<FileProperties>>inputList){
        
        System.out.println("CWDEBUG. generateTasks. method="+method+"  taskParam=");
        for(int i=0;i<taskParams.size();i++){
            List temp=taskParams.get(i);
            System.out.println("    "+temp.toString());
        }
        System.out.println(" inputlist=");
        for(int i=0;i<inputList.size();i++){
            List temp=inputList.get(i);
            System.out.println("    "+temp.toString());
            
        }
        
    }
    
    //this step simply gets previous result and generate a task
    public GenerateTasksObject step2(List<FileProperties> input, FileProperties output, String propertyFile, HashMap <String,FileProperties>previousResults,String stageId){
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List<FileProperties>>inputList=new ArrayList();
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the slowest machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        String dimensions = getProperty("dimensions","2");
        String linearity = getProperty("linearity","0");
        String testCode = getProperty("testCode","0");
        String scale = getProperty("scale","0");
        
        double minTimeVal=Double.parseDouble(getProperty("minTimeStep2","0.5"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTimeStep2","1"));
        
        for (String key:previousResults.keySet()){
            List <FileProperties>inputs=new ArrayList();
            inputs.add(previousResults.get(key));
    
            inputList.add(inputs);
            
            taskParams.add(Arrays.asList(
                                         "step2",              //0                                     
                                         output,               //1                                                                          
                                         inputs,               //2                                         
                                         stageId,              //3
                                         dimensions,           //4
                                         linearity,            //5
                                         testCode,             //6
                                         scale                 //7
                            ));
            taskRequirement.add("large"); //Requirement
            minTime.add(minTimeVal);
            maxTime.add(maxTimeVal);
            System.out.println("TaskId:"+taskid+" Input: "+ previousResults.get(key).getName());
            System.out.println("minTimeVal:"+minTimeVal+" maxTimeVal:"+maxTimeVal);                        
            taskid++;
            
        
        }
        printTest("step2",taskParams,inputList);
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
    }
    
    //this step simply gets previous result and generate a task
    public GenerateTasksObject step3(List<FileProperties> input, FileProperties output, String propertyFile, HashMap <String,FileProperties>previousResults,String stageId){
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List<FileProperties>>inputList=new ArrayList();
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the slowest machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        String dimensions = getProperty("dimensions","2");
        String linearity = getProperty("linearity","0");
        String testCode = getProperty("testCode","0");
        String scale = getProperty("scale","0");
        
        double minTimeVal=Double.parseDouble(getProperty("minTimeStep3","7"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTimeStep3","10"));
        
        for (String key:previousResults.keySet()){
            List <FileProperties>inputs=new ArrayList();
            inputs.add(previousResults.get(key));
    
            inputList.add(inputs);
            
            taskParams.add(Arrays.asList(
                                         "step3",              //0                                     
                                         output,               //1                                                                          
                                         inputs,               //2                                         
                                         stageId,              //3
                                         dimensions,           //4
                                         linearity,            //5
                                         testCode,             //6
                                         scale                 //7
                            ));
            taskRequirement.add("large"); //Requirement
            minTime.add(minTimeVal);
            maxTime.add(maxTimeVal);
            System.out.println("TaskId:"+taskid+" Input: "+ previousResults.get(key).getName());
            System.out.println("minTimeVal:"+minTimeVal+" maxTimeVal:"+maxTimeVal);                        
            taskid++;
            
        
        }
        
        printTest("step3",taskParams,inputList);
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
    }
    
    //this step simply gets previous result and generate a task
    public GenerateTasksObject step4(List<FileProperties> input, FileProperties output, String propertyFile, HashMap <String,FileProperties>previousResults,String stageId){
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List<FileProperties>>inputList=new ArrayList();
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the slowest machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        String dimensions = getProperty("dimensions","2");
        String linearity = getProperty("linearity","0");
        String testCode = getProperty("testCode","0");
        String scale = getProperty("scale","0");
        
        double minTimeVal=Double.parseDouble(getProperty("minTimeStep4","0.5"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTimeStep4","1"));
        
        for (String key:previousResults.keySet()){
            List <FileProperties>inputs=new ArrayList();
            inputs.add(previousResults.get(key));
    
            inputList.add(inputs);
            
            taskParams.add(Arrays.asList(
                                         "step4",              //0                                     
                                         output,               //1                                                                          
                                         inputs,               //2                                         
                                         stageId,              //3
                                         dimensions,           //4
                                         linearity,            //5
                                         testCode,             //6
                                         scale                 //7
                            ));
            taskRequirement.add("large"); //Requirement
            minTime.add(minTimeVal);
            maxTime.add(maxTimeVal);
            System.out.println("TaskId:"+taskid+" Input: "+ previousResults.get(key).getName());
            System.out.println("minTimeVal:"+minTimeVal+" maxTimeVal:"+maxTimeVal);                        
            taskid++;
            
        
        }
        
        printTest("step4",taskParams,inputList);
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
    }
   
}
