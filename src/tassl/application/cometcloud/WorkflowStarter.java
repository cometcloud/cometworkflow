/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud;

/**
 *
 * @author Javier Diaz-Montes
 */
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import programming5.io.Debug;
import programming5.net.IncompleteResultException;
import tassl.application.InternalStarter;
import tassl.application.utils.CommonUtils;
import tassl.automate.network.ControlConstant;
import tassl.automate.programmodel.masterworker.simpleapi.GenericMaster;
/**
 *
 * @author Javier Diaz
 */
public class WorkflowStarter{

    public InternalStarter cometStarter;

    public void initialize(String nodeFile,String portFile,List<String> propertyFiles,List<String> exceptionFiles,int overlayPort) {
        Debug.enableDefault();
        
        InternalStarter cs = new InternalStarter(nodeFile, portFile, propertyFiles.toArray(new String[]{}), exceptionFiles.toArray(new String[]{}), overlayPort);

        try {
            //setup comet infrastructure
            cs.initComet();

            //wait for overlay stabilized
            Thread.sleep(Long.parseLong(System.getProperty("OVERLAY_SETUP_WAITING", "0")));

        } catch (InterruptedException ex) {
            Logger.getLogger(WorkflowStarter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            System.out.println("Invalid node names in node file");
        } catch (IncompleteResultException ex) {
            System.out.println("Timed out on ring create");
        }

        appSpecificStartup(cs);

        cs.startApp();

        appSpecificEndup();
        
    }


    public void appSpecificStartup(InternalStarter cs) {
        System.out.println("specific startup");
        cometStarter = cs;
    }
    
    public void appSpecificEndup() { 
        System.out.println("Specific endup");
        boolean done=false;
        System.out.println("CS, Master, Proxy ready"+new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss.SSS").format(Calendar.getInstance().getTime()));
        while (!done){
            done=isJobDone();
            if (!done){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(tassl.automate.programmodel.masterworker.simpleapi.GenericStarter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                System.out.println("Job DONE");
            }
        }
                
        System.out.println("Waiting for moinitoring thread");
        ((GenericMaster)cometStarter.getDrtsPeer().getMaster()).waitMonitoring(); //waits 3 seconds and then interrupts the thread
        
        System.out.println("Waiting for Master thread");
        ((GenericMaster)cometStarter.getDrtsPeer().getMaster()).waitMaster();
       
        System.out.println("Cleaning Overlay");
        cometStarter.terminateAll();
        
        System.out.println("The application is ready to leave.");
        //System.exit(0);
        
    }
    
    public boolean isJobDone() {
        Object value;
        value = cometStarter.getDrtsPeer().getMaster().isJobDone();
        if (value == null)
            return false;
        else
            return (Boolean)value;
        
    }

    public static void main(String[] args) {
        System.out.println("Starting Application");
        
        String nodeFile = CommonUtils.getStringArg(args,"-nodeFile");
        String portFile = CommonUtils.getStringArg(args,"-portFile");
        List<String> propertyFiles = CommonUtils.getMultipleStringArg(args,"-propertyFile");
        List<String> exceptionFiles = CommonUtils.getMultipleStringArg(args,"-exceptionFile");
        
        int overlayPort = ControlConstant.OVERLAY_CONTROL_PORT;        
        String tempOverlayPort= CommonUtils.getStringArg(args,"-overlayPort");
        if(!tempOverlayPort.isEmpty()){
            overlayPort=Integer.parseInt(tempOverlayPort);
        }
        
        WorkflowStarter workflowStarter = new WorkflowStarter();
        workflowStarter.initialize(nodeFile,portFile,propertyFiles,exceptionFiles,overlayPort);
    }
}

