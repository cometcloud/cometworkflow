/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Diaz-Montes
 */
public abstract class GenerateTasksAbstract {
    Properties properties;      //property object
    String propertyFileValues; //content of the original propertyFile specified by client.
    HashMap<String, List <Results>> previousResults;//results from previous stages of this workflow
    
    //results of related workflows
    //wkfid:{stageid:{results,results ..},...}, where results has taskid and result data
    HashMap <String, HashMap<String, List <Results>>>relatedWorkflowResults;
    
    boolean lastCall;// to know if this is the last time createTask is called for the present stage.
                     // this is important for non-blocking transitions bc createTask is called multiple times for the same stage, for blocking transitions is always the last call.
    
    public GenerateTasksAbstract(){
        lastCall=false;
        properties=new Properties();
        propertyFileValues="";
        previousResults=new HashMap();
        relatedWorkflowResults= new HashMap<>();
    }

    /**
     * Get results from previous stages
     * @return HashMap  {stageId:{result1,result2,...},stageId:{result1,result2,...},...}
     */
    public HashMap<String, List<Results>> getPreviousResults() {
        return previousResults;
    }

    /**
     * Get result files of the previous stages that had dependencies with the current one
     * This is assuming that ALL Results of previous stages are FileProperties objects
     * 
     * @param stageId
     * @param dependencies
     * @return a hashmap with {"filename":FileProperties,"filename":FileProperties,...}
     */
    public HashMap<String,FileProperties> generatePreviousResultFiles(String stageId, List dependencies){
        //we need this to learn where the files are. Format will be something like filename;location;size|filename;location;size|...
        HashMap <String,FileProperties>previousFiles=new HashMap();
        HashMap<String,List<Results>> results=this.getPreviousResults();
        
        for(String i:(List<String>)dependencies){
            List<Results> res=results.get(i);
            for(int j=0;j<res.size();j++){
                System.out.println("CWDEBUG: Stage "+stageId+" has as dependency Stage "+i+" result:"+res.get(j).getResult());
                
                //this should return a list of FileProperties
                List <FileProperties>files=(List<FileProperties>)res.get(j).getResult();
                
                for(int z=0;z<files.size();z++){   
                    System.out.println("CWDEBUG: file:"+files.get(z).getName());                    
                    previousFiles.put(files.get(z).getName(),files.get(z));                    
                }
            }
        }
        return previousFiles;           
    }
    
    /**
     * Get results of all stages of a related workflow id
     *  
     * @param workflowId     
     * @return a hashmap with {stageid:{results,results ..},...}
     */
    public HashMap<String, List<Results>> getRelatedWorkflowsResults(String workflowId) {
        return relatedWorkflowResults.get(workflowId);
    } 
    
    /**
     * Get result files of a specific previous stage 
     * This is assuming that ALL Results of previous stages are FileProperties objects
     * 
     * @param previousStage
     * @return a hashmap with {"filename":FileProperties,"filename":FileProperties,...}
     */
    public HashMap<String,FileProperties> generatePreviousResultFilesStageId(String previousStage){
        HashMap <String,FileProperties>previousFiles=new HashMap();
        HashMap<String,List<Results>> results=this.getPreviousResults();
        if(results.containsKey(previousStage)){
            List<Results> res=results.get(previousStage);
            for(int j=0;j<res.size();j++){
                System.out.println("CWDEBUG: Stage "+previousStage+" has result:"+res.get(j).getResult());

                //this should return a list of FileProperties
                List <FileProperties>files=(List<FileProperties>)res.get(j).getResult();

                for(int z=0;z<files.size();z++){   
                    System.out.println("CWDEBUG: file:"+files.get(z).getName());                    
                    previousFiles.put(files.get(z).getName(),files.get(z));                    
                }
            }
        }else{
            System.out.println("CWDEBUG: Stage "+previousStage+" has no results yet");
        }
        return previousFiles;
    }
    
    public void setPreviousResults(HashMap<String, List <Results>> previousResults) {
        this.previousResults = previousResults;
    }
        
    public void addRelatedWorkflowResults(HashMap<String, List <Results>> relatedResultsResults, String workflowId){
        relatedWorkflowResults.put(workflowId, relatedResultsResults);
    }
    public void setLastCall(Boolean lastCall){
        this.lastCall=lastCall.booleanValue();
    }
    
    /**
     * Identify if this is createTask is called for the present stage.
     * This is important for non-blocking transitions because createTask is called multiple times for the same stage
     * In the case of blocking transitions, there is only one call to createTask for each stage, so lastCall is always true.
     * 
     * @return boolean
     */
    public boolean getLastCall(){
        return lastCall;
    }
    public String getProperty(String value, String defvalue){
        return properties.getProperty(value,defvalue);
    }
    public String getProperty(String value){
        return properties.getProperty(value);
    }
    
    public void setProperty(String variable, String value){
        properties.setProperty(variable, value);
    }
    
    /** 
    * Load properties contained in the file specified in the argument.
    *  After this command properties should be accessible using the
    *  function getProperty
    */
    public void loadProperties(String propertyFileValues){
         if(propertyFileValues != null && !propertyFileValues.isEmpty()){
            Properties p = new Properties();        
            try {
                p.load(new StringReader(propertyFileValues.replaceAll("&#10;", "\n")));
            } catch (IOException ex) {
                Logger.getLogger(GenerateTasksAbstract.class.getName()).log(Level.SEVERE, null, ex);
            }

            this.properties=p;
            this.propertyFileValues=propertyFileValues;
            
            System.out.println(p.toString());
            
         }
    }
    
    /*
    public void loadProperties(String propertyFile){
        if(propertyFile != null && !propertyFile.equals(this.propertyFile)){   
            if(propertyFile.split(":").length==1){
                Properties p = new Properties();        
                try {
                    p.load(new FileInputStream(propertyFile));
                } catch (IOException ex) {
                    Logger.getLogger(GenerateTasksAbstract.class.getName()).log(Level.SEVERE, null, ex);
                }

                this.properties=p;
                this.propertyFile=propertyFile;
            }else{//assume that is a remote file
                String []parts=propertyFile.split(":");
                String returnSsh=executeSsh(parts[0], " cat "+parts[1]);            
                
                Properties p = new Properties();        
                try {
                    p.load(new ByteArrayInputStream(returnSsh.getBytes()));
                } catch (IOException ex) {
                    Logger.getLogger(GenerateTasksAbstract.class.getName()).log(Level.SEVERE, null, ex);
                }

                this.properties=p;
                this.propertyFile=propertyFile;
            }
        }
    }
    */
    
    /**
     * Execute a command in a remote machine using ssh
     * @param address. Address of the remote machine
     * @param command. Command to execute in the remote machine
     * @return. Stdout if success, Stderr if error.
     */
    public static String executeSsh(String address,String command){
        int exitValue=0;
        String stdout="";
        String stderr="";        
        String cmdSsh="ssh -oBatchMode=yes "+address+" " + command;
        try 
        { 
            String [] cmd={"/bin/bash", "-c", cmdSsh};
            Process p;             
            p = Runtime.getRuntime().exec(cmd);           
            p.waitFor();
            exitValue=p.exitValue();
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())); 
            if (exitValue==0){
                String line=reader.readLine();  
                stdout=line+"\n";;
                while(line!=null) 
                {                      
                    line=reader.readLine(); 
                    if (line!=null)
                        stdout+=line+"\n";
                } 
            }else{
                BufferedReader readerE=new BufferedReader(new InputStreamReader(p.getErrorStream())); 
                String lineE=readerE.readLine(); 
                stderr=lineE+"\n";;
                while(lineE!=null) 
                {                     
                    lineE=readerE.readLine(); 
                    if (lineE!=null)
                        stderr+=lineE+"\n";
                } 
            }
            
        } catch (IOException ex) {
            Logger.getLogger(GenerateTasksAbstract.class.getName()).log(Level.SEVERE, null, ex);
        }catch (InterruptedException ex) {
            Logger.getLogger(GenerateTasksAbstract.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (exitValue==0){
            return stdout;
        }else{
            return stderr;
        }        
    } 
    
    
    /**
     *  Create Tasks for the specified stage of the workflow.
     * @param stage
     * @param input list of input FileProperties, which include URI (address:/dir/path/) where you can find input files, and constraints. Correspond to InputData in the input xml
     * @param output a FileProperties object, which includes URI (address:/dir/path/) that should be placed in the task to know where to upload the results. Correspond to Results in the input xml.
     * @param propertyFileValues Values of the propertyFile specified in the XML workflow description
     * @param dependencies Previous stages. This can be use to get results values from those previous stages
     * @param method. Method to use for task generation
     * @return return an object with requirements and task parameters
     */    
    public abstract GenerateTasksObject createTasks(String stageId,  List<FileProperties> input, FileProperties output, String propertyFileValues, List dependencies, String method);
    
}
