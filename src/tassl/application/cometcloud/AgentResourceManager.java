/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.application.cometcloud;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.application.utils.CommonUtils;
import tassl.workflow.resources.NodeAcct;
import tassl.workflow.resources.Resource;
import tassl.workflow.resources.ResourceCloud;
import tassl.workflow.resources.ResourceCluster;
import tassl.workflow.resources.ResourceOSG;

/**
 *
 * @author Javier Diaz-Montes
 */
public class AgentResourceManager extends Thread {

    final Object synresource = new Object();

    Resource resources;

    AgentLite agent; //pointer to the main agent. Use synchronize when reading objects that change over time.

    int mgmtPort;
    String centralManagerServerAddress;
    int centralManagerServerPort;
    String workerBaseProperties;
    boolean alive = true;
    String publicIp;
    int monitorInterval;

    int provisioning; //indicates if provisioning resources. This is used to prevent the monitor system going off while provisioning (dummyIp in clouds).
    //if > 0 we skip monitoring

    //this is to keep track of what resources were started and when (accounting). When a node fails we look up the ip or type to notify scheduler
    //{wkfid.stageid:{nodeacct,nodeacct},...}
    HashMap<String, List<NodeAcct>> accountingNodes;

    //start ssh tunnels (reverse) for the workers to talk with the Agent 
    boolean startSSHtunnels;

    //start ssh tunnels for the Agent to talk with workers
    boolean startSSHtunnelAgent;
    int [] agentSSHTunnelPortRange;
    
    String AgentMetricsAddress; 
    int AgentMetricsPort;
    String dbName;
    boolean useMeticsService;
    
    
    public AgentResourceManager(int port,String cmsaddress, int cmsport, AgentLite agent, String AgentMetricsAddress, int AgentMetricsPort, boolean useMeticsService, String DBName){                
        mgmtPort=port;
        centralManagerServerAddress=cmsaddress;
        centralManagerServerPort=cmsport;
        this.agent=agent;
        accountingNodes=new HashMap<>();
        startSSHtunnels=false;
        startSSHtunnelAgent=false;        
        
        this.AgentMetricsAddress=AgentMetricsAddress;
        this.AgentMetricsPort=AgentMetricsPort;
        this.dbName=DBName;
        this.useMeticsService=useMeticsService;
        
    }
    
    public boolean loadProperties(String res){
        boolean status=true;
        
        String []parts=res.split(":");
        if(parts.length==2){
            if (parts[0].equals("cloud")){                                   
                this.resources=new ResourceCloud(publicIp, AgentMetricsAddress, AgentMetricsPort,useMeticsService,dbName);
                status=((ResourceCloud)this.resources).loadConfig(parts[1]);                                               
            }else if (parts[0].equals("cluster")){                   
                this.resources=new ResourceCluster(publicIp, AgentMetricsAddress, AgentMetricsPort,useMeticsService,dbName);
                status=((ResourceCluster)this.resources).loadConfig(parts[1]);       
            }else if (parts[0].equals("osg")){                   
                this.resources=new ResourceOSG(publicIp, AgentMetricsAddress, AgentMetricsPort,useMeticsService,dbName);
                status=((ResourceOSG)this.resources).loadConfig(parts[1]);       
            }else{
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, "Unknown resource type: "+parts[0]);
                status=false;                    
            }
            if (!status) {
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, "ERROR loading property files or unknown resource type.");
            } else {
                this.resources.setWorkerBasePropertyFile(workerBaseProperties);
                if (startSSHtunnels) {
                    this.resources.setStartSSHtunnels(true);
                    this.resources.setPorts(agent.getServPort(), mgmtPort, agent.getFileServerPort());
                }
                if (startSSHtunnelAgent) {
                    this.resources.setStartSSHtunnelAgent(true);
                    this.resources.setAgentSSHTunnelPortRange(agentSSHTunnelPortRange);
                }
            }
        } else {
            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, "Wrong input format: " + res + ". The format should be <cloud|cluster|osg>:file");
            status = false;
        }

        return status;
    }

    @Override
    public void run() {

        provisioning = 0;

        //send all info to start          
        if (!sendInfo(centralManagerServerAddress, centralManagerServerPort, "SendAllInfo", "SendAllInfo")) {
            System.err.println("ERROR: Contacting with the Central Manager. Leaving now...");
            System.exit(1);
        }

        Monitor mon = new Monitor();
        Thread monitoring = new Thread(mon);
        monitoring.start();

        ServerSocket servSock;
        try {
            servSock = new ServerSocket(mgmtPort);
            servSock.setReuseAddress(true);
            DataInputStream input = null;
            DataOutputStream out = null;
//            boolean enabled_state = true;// Moustafa added this to report the state of the agent (enabled or disabled) used with the command CheckSiteEnabled
            while (alive) {
                try {
                    Socket clientSocket = servSock.accept();
                    input = new DataInputStream(clientSocket.getInputStream());
                    out = new DataOutputStream(clientSocket.getOutputStream());
                    String command = input.readUTF();

                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "command: " + command);

                    //provision new worker. It may involve provisioning machine too
                    if (command.equals("scheduleStage")){ 
                        List <String>operations=new ArrayList<String>();
                        String wflId=input.readUTF();
                        String stageId=input.readUTF();
                        String MachineWorkers=input.readUTF();
                        String Tasks=input.readUTF();                        
                        String properties=input.readUTF();
                        String appname=input.readUTF();
                        long starttimestamp=input.readLong();
                        
                        double refBenchScore=input.readDouble();//this can be used for testing and demo
                        
                        long startRequest=System.currentTimeMillis();
                        
                        //List of inputs files for prefetching: "sierra.futuregrid.org:/file/path/,sierra.futuregrid.org:/file/path/|file1,file2" two positional lists                        
                        int length = input.readInt();
                        byte[] data = new byte[length];
                        input.readFully(data);
                        String inputFiles = new String(data, "UTF-8");

                        prefetchFiles(inputFiles); //prefetch inputs files that tasks will need

                        //allocate stage in resources. Return list of nodes to provision
                        //cluster= {ip:port,ip:port,ip:port}
                        //cloud = {nodetype:port port, nodetype:port} 
                        //osg = {osgtype:port port, osgtype:port}
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "Allocate Stage " + stageId + " of workflow " + wflId);

                        //separate the resources to allocate from the resources to release
                        List <String> toAllocate=new ArrayList<String>();
                        List <String> toRelease=new ArrayList<String>();
                        String [] allMachines=MachineWorkers.split(";");
                        for(String oneMach:allMachines){
                            if(!(oneMach.trim()).isEmpty()){
                                System.out.println("CWDEBUG. agentresourcemanager "+oneMach);
                                if(oneMach.split(":")[1].equals("-1")){                                
                                    toRelease.add(oneMach.split(":")[0]);
                                } else {
                                    toAllocate.add(oneMach);
                                }
                            }
                        }
                        boolean resourceReleased=false;
                        String tasksToReinsert="";
                        //release resources
                        if(!toRelease.isEmpty()){
                            operations.add("deallocate");
                            
                            synchronized(synresource){//not only prevents concurrent modification, but also ensures atomicity of operations
                                                                
                                if(resources instanceof ResourceCloud){  
                                    toRelease=((ResourceCloud)resources).getVMAddressbyType(wflId, stageId, toRelease);
                                }
                                if(resources instanceof ResourceOSG){
                                    toRelease=((ResourceOSG)resources).getOSGJobIDbyType(wflId, stageId, toRelease);
                                }
                                //release workers
                                List <String>releasedWorkers=resources.releaseSomeWorkersbyMachineIp(wflId, stageId, toRelease, 
                                        accountingNodes.get(wflId+"."+stageId), false);
                                //remove from accounting -- all accounting is kept in central manager
                                List<NodeAcct> na=accountingNodes.get(wflId+"."+stageId);
                                for(String machine:toRelease){                                    
                                    for(NodeAcct naObj:na){
                                        if(naObj.getIp().equals(machine)){
                                            na.remove(naObj);
                                            break;
                                        }
                                    }
                                }
                                
                                //tasks Assigned to releasedWorkers need to be reinserted                                
                                HashMap <String,List<String>> tasksHash=agent.getCurrentTasks();
                                if(tasksHash!=null){                                    
                                    for(String oneworker:releasedWorkers){
                                        if(tasksHash.containsKey(oneworker)){
                                          tasksToReinsert+=StringUtils.join(tasksHash.get(oneworker).toArray(),",")+",";
                                        }
                                    }                                    
                                }
                                
                                if(resources instanceof ResourceCloud){                                        
                                    //workers have been already disabled and terminated
                                    //we terminate VMs now
                                    ((ResourceCloud)resources).terminateVMs(toRelease);                                

                                } else if (resources instanceof ResourceOSG){
                                    //workers have been already disabled and terminated
                                    //we terminate jobs now
                                    ((ResourceOSG)resources).terminateOSGJobs(toRelease);
                                }
                                //else if(resources instanceof ResourceCluster){
                                //   here we would release allocation from queue system when supported
                                //}
                                else{
                                    System.out.println("CWDEBUG:agentresourcemanager. unknown classtype:"+resources.getClass());
                                }

                                //release ports from variables if SSH tunnels from Agent to worker are actuvated
                                if(this.startSSHtunnelAgent){
                                    for (int i=0;i<releasedWorkers.size();i++){
                                        String []work=releasedWorkers.get(i).split(":");
                                        
                                        String addressWorker=work[0];
                                        int portWorker=Integer.parseInt(work[1]);
                                    
                                        resources.releasePortMapWorker(addressWorker,portWorker);
                                    }
                                }
                                //release reverse tunnels from Agent to Worker VM
                                if(this.startSSHtunnels){
                                    for (int i=0;i<releasedWorkers.size();i++){
                                        String []work=releasedWorkers.get(i).split(":");
                                        
                                        String addressWorker=work[0];
                                        resources.releaseTunnelsWorker(addressWorker);
                                    }
                                }

                            }
                            resourceReleased=true;
                            
                            long endDeallocate=System.currentTimeMillis();
                            //if DB is active then we store info
                            if(useMeticsService && !operations.isEmpty()){
                                                                        
                                List param=new ArrayList(Arrays.asList(startRequest, endDeallocate, toRelease, appname, wflId, stageId, resources.getClass().getSimpleName()));
                                sendInfo(AgentMetricsAddress, AgentMetricsPort, "dbWriteAgentProvisioningDeallocate", param);                               
                            }
                            
                        }

                        //if stage has been already allocated, then update Query of all workers. This WAKEs UP ALL ALLOCATED WORKERS too
                        updateWorkersTasksQuery(wflId, stageId, Tasks);

                        //if toAllocate is empty. we skip this.
                        if(!toAllocate.isEmpty()){
                            operations.add("allocate");
                            System.out.println("CWDEBUG:agentresourcemanager. allocate stage");
                            List<String> nodes;
                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "Locking Resources in scheduleStage");
                            synchronized (synresource) {

                                provisioning++;

                                Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "Resources locked in scheduleStage. prov=" + provisioning);
                                nodes = resources.allocateStage(wflId, stageId, toAllocate, properties);
                            }

                            System.out.println("CWDEBUG:agentresourcemanager. stage allocated");
                            for (String i : nodes) {
                                System.out.println("CWDEBUG:agentresourcemanager. " + i);
                            }
                            if (nodes.isEmpty()) {
                                out.writeUTF("ERROR");
                            } else {
                                out.writeUTF("OK");
                                //return new resource status
                                byte[] forwardObj;

                                synchronized (synresource) {
                                    forwardObj = programming5.io.Serializer.serializeBytes(resources);

                                    out.writeInt(forwardObj.length);                                                        
                                    out.write(forwardObj);       
                                    if(resourceReleased){
                                        out.writeUTF("tasks");
                                        out.writeUTF(tasksToReinsert);
                                    }else{
                                        out.writeUTF("nothing");
                                    }
                                }
                                                                
                                System.out.println("CWDEBUG:launching provisioning thread");                                
                                                                
                                //provision resources in a new Thread (remeber to put syn in all the global variables modifications)
                                Thread launchWorkers=new Thread(new LaunchWorkers(wflId, stageId, nodes,appname, properties, Tasks, starttimestamp, startRequest, toAllocate, refBenchScore));
                                launchWorkers.start();

                            }
                        } else if (resourceReleased) {
                            out.writeUTF("OK");
                            //return new resource status
                            byte[] forwardObj;

                            synchronized (synresource) {
                                forwardObj = programming5.io.Serializer.serializeBytes(resources);

                                out.writeInt(forwardObj.length);
                                out.write(forwardObj);
                                out.writeUTF("tasks");
                                out.writeUTF(tasksToReinsert);
                            }
                        } else {
                            out.writeUTF("Unchanged");
                        }
                        
                        
                        
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "Resources released in scheduleStage");
                                
                        long endRequest=System.currentTimeMillis(); //time when we finished processing request (this server response time)
                        
                        //if DB is active then we store info
                        if(useMeticsService && !operations.isEmpty()){
                            List param=new ArrayList(Arrays.asList(startRequest, endRequest, allMachines, operations, appname, wflId, stageId, resources.getClass().getSimpleName()));
                            sendInfo(AgentMetricsAddress, AgentMetricsPort, "dbWriteAgentProvisioningRequest", param);                               
                        }
                        
                    //update the workers(start,stop,change query)
                    }else if (command.equals("UpdateWorker")) {
                        //send operation to worker
                        //update status
                        //if error, report back
                        //if no error, then status should be in sync

                        //ask for information (running workers, ping, available resources, supported apps)
                    } else if (command.equals("Info")) {

                        //send all info
                    } else if (command.equals("SendAllInfo")) {
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Locking Resources to SendAllInfo");
                        synchronized (synresource) {
                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources locked to SendAllInfo");
                            sendInfo(centralManagerServerAddress, centralManagerServerPort, "SendAllInfo", "SendAllInfo");
                        }
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources released after SendAllInfo");

                        //send all info if this agent is register to the centralmanager/autonomicscheduler that ask for it
                    } else if (command.equals("SendAllInfoIfRegisteredToMe")) {
                        String centralManagerAddress = input.readUTF();
                        int centralManagerPort = input.readInt();
                        if (centralManagerServerAddress.equals(centralManagerAddress) && centralManagerServerPort == centralManagerPort) {
                            synchronized (synresource) {
                                sendInfo(centralManagerServerAddress, centralManagerServerPort, "SendAllInfo", "SendAllInfo");
                            }
                        }

                        //update resource information and workers.
                    } else if (command.equals("UpdateInfo")) {
                        System.out.println("CWDEBUG. agentresoutcemanager. updateinfo");
                        String cmd = input.readUTF();
                        System.out.println("CWDEBUG. agentresoutcemanager. updateinfo cmd:" + cmd);
                        String wkfstage = input.readUTF();
                        String[] args = wkfstage.split("\\.");
                        if (cmd.equals("removeAll")) { //terminate workers. If it is a cloud, we terminate the VMs directly
                            List indentifiers;
                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Locking Resources in UpdateInfo");
                            synchronized (synresource) {
                                Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources locked in UpdateInfo");
                                indentifiers=resources.releaseWorkers(args[0],args[1], accountingNodes.get(wkfstage), false);
                                
                                accountingNodes.remove(wkfstage);
                                
                                if(indentifiers!=null){
                                    if(resources instanceof ResourceCloud){
                                        List VMIps=new ArrayList();
                                        for(int i=0;i<indentifiers.size();i++){
                                            String ipVM=((String)indentifiers.get(i)).split(":")[0];
                                            if(!VMIps.contains(ipVM)){
                                                VMIps.add(ipVM);
                                            }
                                        }
                                        //indentifiers has VM address so we call terminate to terminate VMs                                    
                                        ((ResourceCloud)resources).terminateVMs(VMIps);                                

                                    } else if (resources instanceof ResourceOSG){
                                        List jobIDs=new ArrayList();
                                        for(int i=0;i<indentifiers.size();i++){
                                            String jobID=((String)indentifiers.get(i)).split(":")[0];
                                            if(!jobIDs.contains(jobID)){
                                                jobIDs.add(jobID);
                                            }
                                        }
                                        //indentifiers has jobID so we call terminate to terminate job                                    
                                        ((ResourceOSG)resources).terminateOSGJobs(jobIDs);
                                        
                                    } else if(resources instanceof ResourceCluster){                                         
                                          //release machines (e.g. terminate queue job that holds our machines). For now we do not need to do anything
//                                        //identifiers is workersNames is ip:port. So call each one and terminate it
//                                        for (int i = 0; i < indentifiers.size(); i++) {
//                                            String[] work = ((String) indentifiers.get(i)).split(":");
//
//                                            String addressWorker = work[0];
//                                            int portWorker = Integer.parseInt(work[1]);
//                                            if (this.startSSHtunnelAgent) {
//                                                Integer localport = resources.getPortMapWorker(addressWorker, portWorker);
//                                                if (localport != null) {
//                                                    addressWorker = "localhost";
//                                                    portWorker = localport.intValue();
//                                                }
//                                            }
//
//                                            sendInfo(addressWorker, portWorker, "TerminateWorker", "true");
//
//                                            if (this.startSSHtunnelAgent) {
//                                                resources.releasePortMapWorker(addressWorker, portWorker);
//                                            }
//                                        }
                                        
                                    }else{
                                        System.out.println("CWDEBUG:agentresourcemanager. unknown classtype:"+resources.getClass());
                                    }

                                    //release ports from variables if SSH tunnels from Agent to worker are actuvated
                                    if (this.startSSHtunnelAgent) {
                                        for (int i = 0; i < indentifiers.size(); i++) {
                                            String[] work = ((String) indentifiers.get(i)).split(":");

                                            String addressWorker = work[0];
                                            int portWorker = Integer.parseInt(work[1]);

                                            resources.releasePortMapWorker(addressWorker, portWorker);
                                        }
                                    }

                                    //release reverse tunnels from Agent to Worker VM
                                    if (this.startSSHtunnels) {
                                        for (int i = 0; i < indentifiers.size(); i++) {
                                            String[] work = ((String) indentifiers.get(i)).split(":");

                                            String addressWorker = work[0];
                                            resources.releaseTunnelsWorker(addressWorker);
                                        }
                                    }
                                    
                                    
                                }else{
                                    System.out.println("CWDEBUG:agentresourcemanager. IDENTIFIERS NULL");
                                }
                                //return new resource status
                                byte[] forwardObj;

                                forwardObj = programming5.io.Serializer.serializeBytes(resources);

                                out.writeInt(forwardObj.length);
                                out.write(forwardObj);

                                //print Agent Statistics for stage
                                Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "StatisticsStage\n" + agent.getFileserver().getStatistics());

                            }
                            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources released in UpdateInfo");

                        }
                    }else if(command.equals("DisableSite")){
                        if (agent.getAgentState()) {
                            out.writeUTF("OK");
                            agent.disableAgent();
                            //set available resources to 0
                            synchronized (synresource) {
                                resources.disableSite();
                            }
                            //terminate every resource running (this will contact central resource manager and notify it as a failure)
//                            this.dealDownNodes(resources.getActiveResourcesWorkflow(), System.currentTimeMillis());
                        this.dealDownNodes(resources.getActiveResourcesWorkflow(),System.currentTimeMillis(), false);
                        } else {
                            out.writeUTF("ERROR: Agent is already disabled");

//                        enabled_state = false;
                        //set available resources to 0
//                        resources.disableSite();
                        //terminate every resource running (this will contact central resource manager and notify it as a failure)
                        }
                    } else if (command.equals("RestoreSite")) {
                        if (!agent.getAgentState()) {
                            out.writeUTF("OK");
                            agent.enableAgent();
                            //restore available resources
                            resources.restoreSite();
                            //tell central resource manager my current info
                            synchronized (synresource) {
                                sendInfo(centralManagerServerAddress, centralManagerServerPort, "SendAllInfo", "SendAllInfo");
                            }

                        } else {
                            out.writeUTF("ERROR: Agent is already enabled");
                        }
                    } else if (command.equals("CheckStatus")) {
                        out.writeUTF("OK");
                    } else if (command.equals("CheckSiteEnabled")) {//Moustafa reports if this agent at this site is enabled or not
                        if (agent.getAgentState()) {
                            out.writeUTF("OK");
                        } else {
                            out.writeUTF("NO");
                        }
                    } else if (command.equals("TerminateAgent")) {
                        //send info to someone???
                        out.writeUTF("OK");
                        //Moustafa QUESTION: should this set available resources to 0 and terminate every resources (similar to Disable Site)?
                        //terminate this service
                        alive = false;
                        //terminate the main one
                        agent.quit();

                    } else {
                        Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "Unknown command: {0}", command);
                    }

                } catch (IOException e) {
                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, e);
                } finally {
                    if (input != null) {
                        input.close();
                    }
                    if (out != null) {
                        out.close();
                    }

                }
            }

        } catch (IOException ex) {
            System.out.println("ERROR: starting Server in " + mgmtPort);
            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Stopping Monitor");
            mon.setActive(false);
            monitoring.interrupt();

            System.out.println("Leaving now...");
            System.exit(1);
        }
    }
    
    //"sierra.futuregrid.org:/file/path/,sierra.futuregrid.org:/file/path/|file1,file2" two positional lists
    private void prefetchFiles(String inputFiles){
        try{
            
            //System.out.println("CWDEBUG: prefetchFiles="+inputFiles);
            
            if(!inputFiles.trim().equals("|") && !inputFiles.trim().equals("")){            
                String []parts=inputFiles.split("\\|");
                String [] locationList=parts[0].split(","); //locations
                String [] fileNameList=parts[1].split(","); //filenames
                HashMap <String,String> locationFilesnames=new HashMap<String,String>();
                for(int i=0; i< locationList.length; i++){
                    if(locationFilesnames.containsKey(locationList[i])){                        
                        locationFilesnames.put(locationList[i], (locationFilesnames.get(locationList[i])+","+fileNameList[i]));                        
                    }else{
                        locationFilesnames.put(locationList[i], fileNameList[i]);
                    }
                }
                if (locationList.length != 0) {
                    Socket clientSocket;
                    DataOutputStream output = null;
                    DataInputStream input = null;
                    //call get for each location
                    for (String loc : locationFilesnames.keySet()) { // we call to retrieve files in background
                        try {
                            clientSocket = new Socket(publicIp, agent.getFileServerPort());//fileserver is in the same machine
                            output = new DataOutputStream(clientSocket.getOutputStream());
                            input = new DataInputStream(clientSocket.getInputStream());
                            //get|true|remotedir|file1,file2,file3|dirInWorker|key|"worker"|fileTransferType                     
                            output.writeUTF(StringUtils.join(new Object[]{"get", true, loc,
                                locationFilesnames.get(loc), "null", "null", "noworker",resources.getFileTransferType()}, "|"));
                            output.writeUTF("null");
                        } catch (IOException ex) {
                            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, "Prefetching files: " + publicIp + ":" + agent.getFileServerPort(), ex);
                        } finally {
                            if (output != null) {
                                try {
                                    output.close();
                                } catch (IOException ex) {
                                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            if (input != null) {
                                try {
                                    input.close();
                                } catch (IOException ex) {
                                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }

                    }
                }

            }
        } catch (Exception ex) {
            System.out.println("ERROR: Prefetching files");
            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Update the list of tasks that workers can request
     *
     * @param wflId
     * @param stageId
     * @param Tasks
     */
    public void updateWorkersTasksQuery(String wflId, String stageId, String Tasks){
        List <String>existingWorkers=resources.getWorkersWorkflowStage(wflId, stageId);
        if(!existingWorkers.isEmpty()){
            String []work;
            List <String>params=new ArrayList<String>();
            //We update the list of tasksIds for the query. This is what it can change for now
            params.add("ChangeQuery");
            params.add("taskids");
            params.add(Tasks);
            for (String worker : existingWorkers) {
                work = worker.split(":");
                sendInfo(work[0], Integer.parseInt(work[1]), "ChangeQuery", params);
            }
        }
    }

    public void setMonitorInterval(int monitorInterval) {
        this.monitorInterval = monitorInterval;
    }

    public void setPublicIp(String publicIp) {
        this.publicIp = publicIp;
    }

    public void setStartSSHtunnels(boolean value) {
        startSSHtunnels = value;
    }

    public void setStartSSHtunnelAgent(boolean value) {
        startSSHtunnelAgent = value;
    }

    public void setSshTunnelAgentPortRange(int[] portRange) {
        agentSSHTunnelPortRange = portRange;
    }

    //send information to outside
    private boolean sendInfo(String addess, int port, String command, Object param){//sync happens outside this method
        boolean status=true;
        DataOutputStream conn = null;       
        DataInputStream in=null;
        try {
            Object[] both = CommonUtils.getOutputInput(addess, port);            
            if (both[0]!=null && both[1]!=null){
                in=(DataInputStream)both[0];
                conn=(DataOutputStream)both[1];
                
                if(command.equals("SendAllInfo")){   
                    System.out.println("CWDEBUG. agentresourcemanger. Send info to autonomic scheduler");
                    byte[] forwardObj;
                    forwardObj = programming5.io.Serializer.serializeBytes(resources);
                    conn.writeUTF((String) param);
                    conn.writeUTF(publicIp);
                    conn.writeInt(mgmtPort);
                    conn.writeInt(forwardObj.length);
                    conn.write(forwardObj);
                    
                }else if (command.equals("UpdateInfo")) {
                    
                }else if (command.equals("ChangeQuery")) {                    
                    conn.writeUTF((String)((List)param).get(0));                                   
                    conn.writeUTF((String)((List)param).get(1));
                    conn.writeUTF((String)((List)param).get(2));
                
                }else if (command.equals("Failure")){
                    //param is the hashmap from Monitor (wflStage_workerTasks)

                    System.out.println("CWDEBUG. agentresourcemanger. Send Failure info to autonomic scheduler");
                    conn.writeUTF("WorkerFailure");

                    //Send agent info
                    conn.writeUTF(publicIp);
                    conn.writeInt(mgmtPort);

                    //send HashMap with wflStage_workerTasks
                    byte[] wkflObj;
                    wkflObj = programming5.io.Serializer.serializeBytes(param);
                    conn.writeInt(wkflObj.length);
                    conn.write(wkflObj);

                    //send new Resource
                    byte[] forwardObj;
                    forwardObj = programming5.io.Serializer.serializeBytes(resources);
                    conn.writeInt(forwardObj.length);
                    conn.write(forwardObj);

                } else if (command.equals("releaseOneWorker")) {
                    System.out.println("CWDEBUG. agentresourcemanger. Releasing worker done");
                    conn.writeUTF("releaseOneWorker");

                    //Send agent info
                    conn.writeUTF(publicIp);
                    conn.writeInt(mgmtPort);

                    //send HashMap with wflStage_workerTasks
                    byte[] wkflObj;
                    wkflObj = programming5.io.Serializer.serializeBytes(param);
                    conn.writeInt(wkflObj.length);
                    conn.write(wkflObj);

                    //send new Resource
                    byte[] forwardObj;
                    forwardObj = programming5.io.Serializer.serializeBytes(resources);
                    conn.writeInt(forwardObj.length);
                    conn.write(forwardObj);
                    
                }else if (command.equals("dbWriteAgentProvisioningRequest")){
                    
                    List paramList=(List)param;
                    
                    conn.writeUTF(command);
                    conn.writeUTF(dbName);
                    
                    conn.writeLong((Long)paramList.get(0));
                    conn.writeLong((Long)paramList.get(1));
                    
//                    byte[] forwardObj;                                                        
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(2));         
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(2));
                    
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(3));         
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(3));
                    
                    
                    conn.writeUTF((String)paramList.get(4));
                    conn.writeUTF((String)paramList.get(5));
                    conn.writeUTF((String)paramList.get(6));
                    conn.writeUTF((String)paramList.get(7));
                                        
                }else if(command.equals("dbWriteAgentProvisioningDeallocate")){
                    List paramList=(List)param;
             
                    conn.writeUTF(command);
                    conn.writeUTF(dbName);
                    
                    conn.writeLong((Long)paramList.get(0));
                    conn.writeLong((Long)paramList.get(1));
                    
//                    byte[] forwardObj;                                                        
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(2));         
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(2));
                    
                    conn.writeUTF((String)paramList.get(3));
                    conn.writeUTF((String)paramList.get(4));
                    conn.writeUTF((String)paramList.get(5));
                    conn.writeUTF((String)paramList.get(6));
                    
                }else if(command.equals("dbWriteAgentProvisioningAllocate")){
                    List paramList=(List)param;
             
                    conn.writeUTF(command);
                    conn.writeUTF(dbName);
                    
                    conn.writeLong((Long)paramList.get(0));
                    conn.writeLong((Long)paramList.get(1));
                    
//                    byte[] forwardObj;                                                        
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(2));         
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(2));
                    
                    conn.writeUTF((String)paramList.get(3));
                    conn.writeUTF((String)paramList.get(4));
                    conn.writeUTF((String)paramList.get(5));
                    conn.writeUTF((String)paramList.get(6));
                    conn.writeLong((Long)paramList.get(7));
                    conn.writeLong((Long)paramList.get(8));
                    conn.writeLong((Long)paramList.get(9));
                    conn.writeLong((Long)paramList.get(10));
                    conn.writeInt((Integer)paramList.get(11));
                    
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(12));         
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(12));
                    
                    conn.writeInt((Integer)paramList.get(13));
                    
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(14));
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(14));
//                    forwardObj= programming5.io.Serializer.serializeBytes(paramList.get(15));
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);
                    CommonUtils.sendObject(conn, paramList.get(15));
                    
                }
            } else {
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.WARNING, "Could not connect with " + addess + ":" + port);
                status = false;
            }
        } catch (IOException e) {
            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, e);
            status = false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ex) {
                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return status;
    }

    public Resource getResources() {
        return resources;
    }

    public void setResources(Resource resources) {
        this.resources = resources;
    }

    public void setWorkerBaseProperties(String workerBaseProperties) {
        this.workerBaseProperties = workerBaseProperties;
    }

    public NodeAcct removeNodeAcct(String ip, List<NodeAcct> nodeAcctList) {
        if (nodeAcctList != null && !nodeAcctList.isEmpty()) {
            for (int i = 0; i < nodeAcctList.size(); i++) {
                if (nodeAcctList.get(i).getIp().equals(ip)) {
                    return nodeAcctList.remove(i);
                }
            }
        }
        return null;
    }

    /**
     * Notify Autonomic Scheduler/Resource Manager about failed workers and updates the resources object
     * 
     * sync outside because it modifies resources & agent (agentLite object) -- order is important for consistency
     * 
     * @param downnodes
     * @param currentTimeStamp this is used only when there is a failure in provisioning
     * @param provisioningB is this method called during the provisioning of new resources?
     */
    public void dealDownNodes(HashMap <String,List<String>> downnodes, long currentTimeStamp, boolean provisioningB){
        //downnodes is {"wflId.stageId":{"ip:port",..},"wflId.stageId":{"ip:port",..},...}
        //currentTasks is {"address:port":{taskid,taskid,...},...}
        if (!downnodes.isEmpty()) {// do something with down nodes

            //to convert nodeAcctListRemove in ip:timestamp or vmtype:timestamp separated by commas
            String nodeAcctListString = "";

            ////worker is ip or type of VM. No port
            //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp_CommaSeparated","wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp_CommaSeparated",...}
            HashMap <String, String> wflStage_workerTasks= new HashMap<String, String>();                    
            for(String wflStage:downnodes.keySet()){
                String allTasks="";
                List <String>workersNames=downnodes.get(wflStage);                        
                String []parts=wflStage.split("\\.");
                
                List<NodeAcct> nodeAcctList=accountingNodes.get(wflStage);
                List<NodeAcct> nodeAcctListRemove= new ArrayList<NodeAcct>();
                
                
                //this is the list of VM types (cloud), OSG job types (OSG) or list of machine ips(cluster) corresponding with the workers 
                List <String>workersNamesforAS=new ArrayList<String>();
                for(String oneworker:workersNames){
                    System.out.println("CWDEBUG. agentresourcemanager. workerdown: "+oneworker);
                    if(oneworker.startsWith("fail")){//nothing to do. it was a provisioning failure. Currently this only happen with Clouds.
                        workersNamesforAS.add(oneworker.split(":")[1]); //this is type for VMs.                          
                        nodeAcctListString += oneworker.split(":")[1] + ":" + currentTimeStamp + ":-1,";
                    } else {
                        //releaseResource
                        releaseResource(oneworker, parts[0], parts[1], nodeAcctList, workersNamesforAS, nodeAcctListRemove, true,provisioningB);
                        
                        //identify tasks of each node - organize them by workflow
                        HashMap <String,List<String>> tasksHash=agent.getCurrentTasks();
                        if(tasksHash!=null && tasksHash.containsKey(oneworker)){
                            for(String i:tasksHash.get(oneworker)){
                                allTasks+=i+",";
                            }
                            agent.removeWorker(oneworker);
                        }
                    }
                }

                //convert nodeAcctListRemove in ip:timestamp or vmtype:timestamp or osgtype:timestamp separated by commas                
                if(resources instanceof ResourceCloud||resources instanceof ResourceOSG){
                    for(NodeAcct na:nodeAcctListRemove){
                        nodeAcctListString+=na.getType()+":"+na.getStartTime()+":0,";
                    }                    
                } else if(resources instanceof ResourceCluster){ 
                    for(NodeAcct na:nodeAcctListRemove){
                        nodeAcctListString+=na.getIp()+":"+na.getStartTime()+":0,";
                    }                
                }
                if (!nodeAcctListRemove.isEmpty()) {
                    nodeAcctListString = nodeAcctListString.substring(0, nodeAcctListString.length() - 1);//remove last comma               
                }

                if (nodeAcctListString.isEmpty()) {
                    nodeAcctListString = "null";
                }

                if (allTasks.isEmpty()) {
                    allTasks = "null";
                }

                System.out.println("CWDEBUG: dealDownWorkers. " + StringUtils.join(workersNamesforAS.toArray(), ",") + ";" + allTasks + ";" + nodeAcctListString);

                wflStage_workerTasks.put(wflStage, StringUtils.join(workersNamesforAS.toArray(), ",") + ";" + allTasks + ";" + nodeAcctListString);
            }

            //send wflStage_workerTasks to the ResourceManager
            //send resources object
            //return new resource status
            sendInfo(centralManagerServerAddress, centralManagerServerPort, "Failure", wflStage_workerTasks);

        }
    }
    
    /**
     * This method is called when a worker makes to many unsuccessful attempts to retrieve tasks
     * @param worker
     * @param workflowId
     * @param stageId
     */
    public void releaseOneDoneWorker(String worker, String workflowId,String stageId){
        List<NodeAcct> nodeAcctList=accountingNodes.get(workflowId+"."+stageId);
        List<NodeAcct> nodeAcctListRemove= new ArrayList<NodeAcct>();
        //this is the list of VM types (cloud) or OSG job types (osg) or list of machine ips(cluster) corresponding with the workers 
        List <String>workersNamesforAS=new ArrayList<String>();
        
        synchronized(synresource){                                        
            releaseResource(worker,workflowId,stageId,nodeAcctList,workersNamesforAS,nodeAcctListRemove, false, false); 
        }

        //to convert nodeAcctListRemove in ip:timestamp or vmtype:timestamp or osgtype:timestamp separated by commas
        String nodeAcctListString = "";

        //convert nodeAcctListRemove in ip:timestamp or vmtype:timestamp or osgtype:timestamp separated by commas                
        if(resources instanceof ResourceCloud||resources instanceof ResourceOSG){
            for(NodeAcct na:nodeAcctListRemove){
                nodeAcctListString+=na.getType()+":"+na.getStartTime()+":0,";
            }                    
        } else if(resources instanceof ResourceCluster){ 
            for(NodeAcct na:nodeAcctListRemove){
                nodeAcctListString+=na.getIp()+":"+na.getStartTime()+":0,";
            }                
        }
        if (!nodeAcctListRemove.isEmpty()) {
            nodeAcctListString = nodeAcctListString.substring(0, nodeAcctListString.length() - 1);//remove last comma               
        }

        if (nodeAcctListString.isEmpty()) {
            nodeAcctListString = "null";
        }

        String allTasks = null;//no task failed here
        System.out.println("CWDEBUG: releaseOneDoneWorker. " + StringUtils.join(workersNamesforAS.toArray(), ",") + ";" + allTasks + ";" + nodeAcctListString);

        //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp_CommaSeparated","wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp_CommaSeparated",...}
        HashMap <String, String> wflStage_workerTasks= new HashMap<String, String>();  
        wflStage_workerTasks.put(workflowId+"."+stageId, StringUtils.join(workersNamesforAS.toArray(),",")+";"+allTasks+";"+nodeAcctListString);
                
        sendInfo(centralManagerServerAddress, centralManagerServerPort, "releaseOneWorker", wflStage_workerTasks);

    }
    

    
    public void releaseResource(String oneworker, String workflowId,String stageId, List<NodeAcct> nodeAcctList,List <String>workersNamesforAS, 
            List<NodeAcct> nodeAcctListRemove, boolean failure, boolean provisioningB){
        
        String addressWorker=oneworker.split(":")[0];
        int portWorker=Integer.parseInt(oneworker.split(":")[1]);
        //ip of the machine that has the worker
        String wokIp=addressWorker;       
        
        boolean machineActive=true;
        //remove node from active ones
        List temp=new ArrayList();
        temp.add(oneworker);               //this disable worker in data structures and ask worker to finish
        
        if(!resources.isActive(wokIp)){//if machine is down, then it is not a worker failure
            failure=false;
            machineActive=false;
        }
        workersNamesforAS.addAll(resources.releaseSomeWorkers(workflowId,stageId,temp, nodeAcctList, failure, provisioningB));

                     
        //check if node or VM is active (ssh)
        //WARNING: if there is a queue system and I do not have a job running, the ssh call will fail. (currently we do not deal with this)
        if(machineActive){
            System.out.println("CWDEBUG: releaseResource. "+wokIp+" is active");
            //check if there are other workers, if not take node down/shutdownVM and report number of nodes                                
            if(resources instanceof ResourceCloud){                                      
                //Currently, we take down a VM that is unused. In the future we may change it to reduce overheads of starting VMs
                if (((ResourceCloud) resources).numberWorkersinVM(workflowId, stageId, wokIp) == 0) {
                    //remove VM from VMActive 
                    //call terminate because no workers active -- no failure here.                                
                   
                    resources.releaseMachine(wokIp, nodeAcctList, false);
                    List temp1=new ArrayList();
                    temp1.add(wokIp);
                    ((ResourceCloud)resources).terminateVMs(temp1);

                    //remove machine from accounting and tell AS
                    NodeAcct tempna = removeNodeAcct(wokIp, nodeAcctList);
                    if (tempna != null) {
                        nodeAcctListRemove.add(tempna);
                    } else {
                        System.out.println("CWDEBUG: releaseResource. removeNodeAcct was NULL");
                    }

                }else{                    
                    System.out.println("CWDEBUG: releaseResource. VM "+wokIp+" has more workers running");                    
                }                                    
            } else if(resources instanceof ResourceOSG){                                      
                //Currently, we take down a job that is unused. In the future we may change it to reduce overheads of starting osg jobs
                if (((ResourceOSG) resources).numberWorkersinJob(workflowId, stageId, wokIp) == 0) {
                    //remove job from osgActive 
                    //call terminate because no workers active -- no failure here.                                
                   
                    resources.releaseMachine(wokIp, nodeAcctList, false);
                    List temp1=new ArrayList();
                    temp1.add(wokIp);
                    ((ResourceOSG)resources).terminateOSGJobs(temp1);

                    //remove machine from accounting and tell AS
                    NodeAcct tempna = removeNodeAcct(wokIp, nodeAcctList);
                    if (tempna != null) {
                        nodeAcctListRemove.add(tempna);
                    } else {
                        System.out.println("CWDEBUG: releaseResource. removeNodeAcct was NULL");
                    }

                }else{                    
                    System.out.println("CWDEBUG: releaseResource. VM "+wokIp+" has more workers running");                    
                }                                    
            }else if(resources instanceof ResourceCluster){ 
                if(((ResourceCluster)resources).getNumberActiveWorkers(wokIp)==0){
                    //remove machine from accounting and tell AS
                    NodeAcct tempna = removeNodeAcct(wokIp, nodeAcctList);
                    if (tempna != null) {
                        nodeAcctListRemove.add(tempna);
                    }

//                }
//
//                if (this.startSSHtunnelAgent) {
//                    Integer localport = resources.getPortMapWorker(addressWorker, portWorker);
//                    if (localport != null) {
//                        addressWorker = "localhost";
//                        portWorker = localport.intValue();
//                    }
//                }
//
//                //terminate worker                
//                sendInfo(addressWorker, portWorker, "TerminateWorker", "true");
                }
            }
        } else {//all workers should be in the list, do not worry about that. only disable nodes
            System.out.println("CWDEBUG: releaseResource. " + wokIp + " is inactive");
            //it can happen that the machine has been disabled/terminated already
            if (resources.isStatusActive(wokIp)) {
                //if it is down, then update resource status
                //add worker to the down list
                
                //release Machine -- make it unavailable
                resources.releaseMachine(wokIp, nodeAcctList, true);
                
                if(resources instanceof ResourceCloud){
                    //remove VM from VMActive                                                                 
                    List temp1=new ArrayList();
                    temp1.add(wokIp);
                    //call terminate
                    ((ResourceCloud)resources).terminateVMs(temp1);
                }
                
                if(resources instanceof ResourceOSG){
                    //remove job from osgActive                                                                 
                    List temp1=new ArrayList();
                    temp1.add(wokIp);
                    //call terminate
                    ((ResourceOSG)resources).terminateOSGJobs(temp1);
                }
                //remove machine from accounting and tell AS
                NodeAcct tempna=removeNodeAcct(wokIp,nodeAcctList);
                if(tempna!=null){
                    nodeAcctListRemove.add(tempna);
                }else{
                    System.out.println("CWDEBUG: releaseResource. removeNodeAcct was NULL");
                } 
                
            }else{
                System.out.println("CWDEBUG: releaseResource. resources.isStatusActive("+wokIp+") is false");
            }                            
        }

        if (this.startSSHtunnelAgent) {
            resources.releasePortMapWorker(addressWorker, portWorker);
        }

        //release reverse tunnels from Agent to Worker VM
        if (this.startSSHtunnels) {
            resources.releaseTunnelsWorker(addressWorker);
        }

    }

    //check status of resources...
    private class Monitor implements Runnable {

        boolean active = true;

        public void run() {

            //Although taskhandler can modify it in agentlite, it should be ok to read it.
            //syncronize operations with resources because it can be modified by another thread
            System.out.println("TaskMonitoring starts");
            HashMap <String,List<String>> downnodes;
            while(active) {
                try {
                    Thread.sleep(monitorInterval * 1000);
                } catch (InterruptedException ex) {
                    if (active) {
                        Logger.getLogger(AgentResourceManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:Locking Resources in Monitor");
                synchronized(synresource){
                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:Resources locked in Monitor");            
                    if (provisioning==0){                        
                        downnodes=resources.checkAllWorkerStatus();                                        
                        dealDownNodes(downnodes,0, false);
                    }else{
                        Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Skipping worker check due to provisioning");                    
                    }

                }
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources released in Monitor");

            }

            System.out.println("TaskMonitoring terminates");
        }

        public void setActive(boolean active) {
            this.active = active;
        }
    }

    //to launch VMs and start workers
    private class LaunchWorkers implements Runnable {

        //nodeInfo is:
        //If cluster - {"ip:port","ip:port","ip:port"}
        //If cloud - {"nodetype:port port", "nodetype:port"} 
        //If osg - {"osgtype:port port", "osgtype:port"} 
        List<String> nodeInfo;
        String wflId;
        String stageId;
        String appname;
        String properties;
        String tasks;
        long starttimestamp; //for accounting and to be in syn with AS
        long startRequest; //used as key for DB entry
        List <String> toAllocate;//used to summarized provisioned resources in Db
        double refBenchScore;
        
        public LaunchWorkers(String wflId, String stageId, List <String> nodes, String appname, String properties, String tasks, long starttimestamp, long startRequest, List <String> toAllocate, double refBenchScore){
            nodeInfo=nodes;
            this.wflId=wflId;
            this.stageId=stageId;
            this.appname=appname;
            this.properties=properties;
            this.tasks=tasks;
            this.starttimestamp=starttimestamp;
            this.startRequest=startRequest;
            this.toAllocate=toAllocate;
            this.refBenchScore=refBenchScore;
        }

        public void run() {
            List<String> failedWorkers=new ArrayList();  
            HashMap<String,String> additionalValues=new HashMap<String,String>(); //values custom to different resources
            
            long startMachineprovisioning=0, endMachineprovisioning=0;
            long startWorkerprovisioning=0, endWorkerprovisioning=0;
            
            List<String> failedMachines=new ArrayList<String>(); //machines that failed to start/allocate 
                                                         //to identify VM types that failed to start (used for DB logging)
            int numReqMachines=toAllocate.size();
            int numReqWorkers=0;
            if(resources instanceof ResourceCloud){
                //startVMs
                System.out.println("CWDEBUG:agentresourcemanager. deployVMs");
                
                List<String> ips;                
                List <Double>performance=new ArrayList<Double>();
                List <String>types=new ArrayList<String>();                
                
                startMachineprovisioning = System.currentTimeMillis();
                //synchronized(synresource){ //moved inside deployVMs
                //nodeInfo (list) = {m1.small:7777,m1.medium:7777 7778} (deploy two VMs)
                ips=((ResourceCloud)resources).deployVMs(wflId, stageId, nodeInfo, appname, synresource, failedMachines);
                //ips={"ip:port","ip:port","ip:port","fail:type:port"} //one fail per worker that was supposed to start                
                endMachineprovisioning = System.currentTimeMillis();
                
                numReqWorkers=ips.size();
                
                System.out.println("CWDEBUG:agentresourcemanager. seconds STARTVM:"+ (endMachineprovisioning-startMachineprovisioning)/1000.0);
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:agentresourcemanager. seconds STARTVM:"+ (endMachineprovisioning-startMachineprovisioning)/1000.0);
                //get estimated performance of workers   
                synchronized(synresource){
                    for(String s:nodeInfo){
                        String []part=s.split(":");
                        for(int i=0;i<part[1].split(" ").length;i++){
                            performance.add(resources.getNodeList().get(part[0]).getBenchScore()/refBenchScore); //This is just for testing and demo. 
                            types.add(part[0]); //add type of VM
                        }
                    }

                    //add accounting before start workers just in case they fail
                    //CHECK if EXISTS to add only the new ones.
                    //generate the accounting info for this stage. In this way we have types and ips. The AS only knows types in the case of cloud
                    if (accountingNodes.containsKey(wflId + "." + stageId)) {
                        //add
                        List<NodeAcct> newAdditions=resources.generateAccountingInfo(wflId,stageId,starttimestamp,accountingNodes.get(wflId+"."+stageId),appname);
                        accountingNodes.get(wflId+"."+stageId).addAll(newAdditions);

                    } else {
                        //this is to keep track of what resources were started and when (accounting). When a node fails we look up the ip or type to notify scheduler    
                        accountingNodes.put(wflId+"."+stageId,resources.generateAccountingInfo(wflId,stageId,starttimestamp, new ArrayList<NodeAcct>(),appname));
                    }

                }
                //}
                System.out.println("CWDEBUG:agentresourcemanager. start workers");
                //start Workers (includes creating the input file)
                startWorkerprovisioning = System.currentTimeMillis();
                failedWorkers=resources.startWorkers(ips, wflId, stageId, appname, properties,performance, tasks, types);  //does not modify resources
                endWorkerprovisioning = System.currentTimeMillis();
                System.out.println("CWDEBUG:agentresourcemanager. seconds STARTWORKERS:"+ (endWorkerprovisioning-startWorkerprovisioning)/1000.0);
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:agentresourcemanager. "+
                        "seconds STARTWORKERS:{0}", (endWorkerprovisioning-startWorkerprovisioning)/1000.0);
                
                additionalValues.put("imageId", ((ResourceCloud)resources).getImageId(appname));
                
            } else if(resources instanceof ResourceOSG){
                //startjobs
                System.out.println("CWDEBUG:agentresourcemanager. deployOSGJobs");
                
                List<String> jobIDs;                
                List <Double>performance=new ArrayList<Double>();
                List <String>types=new ArrayList<String>();                
                
                startMachineprovisioning = System.currentTimeMillis();
                //synchronized(synresource){ //moved inside deployVMs
                //nodeInfo (list) = {osg.small:7777,osg.medium:7777 7778} (deploy two jobs)
                jobIDs=((ResourceOSG)resources).deployOSGJobs(wflId, stageId, nodeInfo, appname, synresource, failedMachines);
                //jobIDs={"jobID:port","jobID:port","jobID:port","fail:type:port"} //one fail per worker that was supposed to start                
                endMachineprovisioning = System.currentTimeMillis();
                
                numReqWorkers=jobIDs.size();
                
                System.out.println("CWDEBUG:agentresourcemanager. seconds STARTOSGJOB:"+ (endMachineprovisioning-startMachineprovisioning)/1000.0);
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:agentresourcemanager. seconds STARTOSGJOB:"+ (endMachineprovisioning-startMachineprovisioning)/1000.0);
                //get estimated performance of workers   
                synchronized(synresource){
                    for(String s:nodeInfo){
                        String []part=s.split(":");
                        for(int i=0;i<part[1].split(" ").length;i++){
                            performance.add(resources.getNodeList().get(part[0]).getBenchScore()/refBenchScore); //This is just for testing and demo. 
                            types.add(part[0]); //add type of job
                        }
                    }

                    //add accounting before start workers just in case they fail
                    //CHECK if EXISTS to add only the new ones.
                    //generate the accounting info for this stage. In this way we have types and ips. The AS only knows types in the case of cloud
                    if (accountingNodes.containsKey(wflId + "." + stageId)) {
                        //add
                        List<NodeAcct> newAdditions=resources.generateAccountingInfo(wflId,stageId,starttimestamp,accountingNodes.get(wflId+"."+stageId),appname);
                        accountingNodes.get(wflId+"."+stageId).addAll(newAdditions);

                    } else {
                        //this is to keep track of what resources were started and when (accounting). When a node fails we look up the ip or type to notify scheduler    
                        accountingNodes.put(wflId+"."+stageId,resources.generateAccountingInfo(wflId,stageId,starttimestamp, new ArrayList<NodeAcct>(),appname));
                    }

                }
                //}
                System.out.println("CWDEBUG:agentresourcemanager. start workers");
                //start Workers (includes creating the input file)
                startWorkerprovisioning = System.currentTimeMillis();
                failedWorkers=resources.startWorkers(jobIDs, wflId, stageId, appname, properties,performance, tasks, types);  //does not modify resources
                endWorkerprovisioning = System.currentTimeMillis();
                System.out.println("CWDEBUG:agentresourcemanager. seconds STARTWORKERS:"+ (endWorkerprovisioning-startWorkerprovisioning)/1000.0);
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:agentresourcemanager. "+
                        "seconds STARTWORKERS:{0}", (endWorkerprovisioning-startWorkerprovisioning)/1000.0);
            } else if(resources instanceof ResourceCluster){  
                //add accounting before start workers just in case they fail
                synchronized (synresource) {
                    //CHECK if EXISTS to add only the new ones.
                    //generate the accounting info for this stage. In this way we have types and ips. The AS only knows types in the case of cloud or osg
                    if (accountingNodes.containsKey(wflId + "." + stageId)) {
                        //add
                        List<NodeAcct> newAdditions=resources.generateAccountingInfo(wflId,stageId,starttimestamp,accountingNodes.get(wflId+"."+stageId),appname);
                        accountingNodes.get(wflId+"."+stageId).addAll(newAdditions);

                    } else {
                        //this is to keep track of what resources were started and when (accounting). When a node fails we look up the ip or type to notify scheduler    
                        accountingNodes.put(wflId+"."+stageId,resources.generateAccountingInfo(wflId,stageId,starttimestamp, new ArrayList<NodeAcct>(),appname));
                    }

                }
                
                startMachineprovisioning = System.currentTimeMillis();                
                //provisioning              
                endMachineprovisioning = System.currentTimeMillis()+1000;
                
                numReqWorkers=nodeInfo.size();
                
                //get estimated performance of workers
                List <Double>performance=new ArrayList<Double>();
                for(String s:nodeInfo){
                    //System.out.println("Score "+ resources.getNodeList().get(s.split(":")[0]).getBenchScore() +"  ref "+refBenchScore +"  speedup="+(resources.getNodeList().get(s.split(":")[0]).getBenchScore()/refBenchScore));
                    performance.add(resources.getNodeList().get(s.split(":")[0]).getBenchScore()/refBenchScore);//This is just for testing and demo. 
                }
                //start Workers(includes creating the input file)
                //(the info should be right in resources)
                startWorkerprovisioning = System.currentTimeMillis();
                failedWorkers=resources.startWorkers(nodeInfo, wflId, stageId, appname ,properties,performance,tasks, null);
                endWorkerprovisioning = System.currentTimeMillis();
                System.out.println("CWDEBUG:agentresourcemanager. seconds STARTWORKERS:"+ (endWorkerprovisioning-startWorkerprovisioning)/1000.0);
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:agentresourcemanager. "+ 
                        "seconds STARTWORKERS:{0}", (endWorkerprovisioning-startWorkerprovisioning)/1000.0);              
                
                additionalValues.put("queueName", ((ResourceCluster)resources).getQueueName());
                additionalValues.put("queueType", ((ResourceCluster)resources).getQueueType());
                
            } else{
                System.out.println("CWDEBUG:agentresourcemanager. unknown classtype:"+resources.getClass());
            }

            //CWDEBUG
            System.out.println("CWDEBUG. agentresourcemanager. accounting after VM provisioning and before considering failed workers");
            printAllAccounting();

            if (!failedWorkers.isEmpty()) {
                //Notify that there are nodes that failed
                //create downnodes hashmap
                //downnodes is {"wflId.stageId":{"ip:port",..},"wflId.stageId":{"ip:port",..},...}
                HashMap <String,List<String>> downnodes= new HashMap<String,List<String>>();            
                downnodes.put(wflId+"."+stageId, failedWorkers); 
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:Locking Resources in LaunchWorker worker failed");
                synchronized(synresource){
                    Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:Resources locked in LaunchWorker worker failed");   
                    dealDownNodes(downnodes, starttimestamp, true);
                }
                Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources released in LaunchWorker worker failed");
            }

            Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:Locking Resources in LaunchWorker provision--");
            synchronized(synresource){      
                
                //CWDEBUG
                printAllAccounting();

                provisioning--;
                Logger.getLogger(AgentResourceManager.class.getName()).log(Level.INFO, "CWDEBUG:Resources locked in LaunchWorker provision--. prov=" + provisioning);
            }
            Logger.getLogger(AgentFileServer.class.getName()).log(Level.INFO, "CWDEBUG:Resources released in LaunchWorker provision--");    
            
            
            //if DB is active then we store info
            if(useMeticsService){                
                //List param=new ArrayList(Arrays.asList(wflId, stageId, startRequest, startMachineprovisioning, endMachineprovisioning, 
                //        startWorkerprovisioning, endWorkerprovisioning, failedMachines, failedWorkers, additionalValues));
                //sendInfo(AgentMetricsAddress, AgentMetricsPort, "dbWriteAgentProvisioningDone", param);
                                                
                long endAllocate=System.currentTimeMillis();
                List param=new ArrayList(Arrays.asList(startRequest, endAllocate, toAllocate, appname, wflId, stageId, resources.getClass().getSimpleName(),
                        startMachineprovisioning, endMachineprovisioning, startWorkerprovisioning, endWorkerprovisioning, numReqMachines, failedMachines, numReqWorkers, failedWorkers, additionalValues));
                sendInfo(AgentMetricsAddress, AgentMetricsPort, "dbWriteAgentProvisioningAllocate", param);                               
             
                    
                
            }
        }
    }

    private void printAllAccounting() {
        //{wkf.stage:{site:{nodeacct, nodeacct},...},...}
        //HashMap <String,HashMap<String,List<NodeAcct>>> accountingGlobal;

        System.out.println("CWDEBUG: autonomicscheduler. CURRENT ACCOUNTING");
        for (String wkfstage : accountingNodes.keySet()) {
            System.out.println("  stage: " + wkfstage);
            for (NodeAcct naObj : accountingNodes.get(wkfstage)) {
                System.out.println("      " + naObj.toString());
            }

        }

    }
}
