package tassl.application.cometcloud.sampleMontage;

import tassl.application.cometcloud.sample.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.WorkflowMeteorGenericWorker;
import tassl.application.cometcloud.WorkflowTaskTuple;
import tassl.application.utils.CommonUtils;


/**
 *
 * @author Javier Diaz
 */

public class AppWorkerMontage extends WorkflowMeteorGenericWorker{

    
    /**
    * This method is called to compute the task    
    * 
    * @param dataobj It is the data associated to the task. The one send from the master
    * @return Object result data that will be attached to the task result. The object has to be serializable.
    */    
    @Override
    public Object computeTaskSpecific(Object dataobj, WorkflowTaskTuple tasktuple) {
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO, "AppWorker "+this.getPeerIP()+" gets taskid " + tasktuple.getTaskid());
        
        List data = (List) dataobj;
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"stage:"+data.get(0));
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"input:"+data.get(1));
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"output:"+data.get(2));
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"inputs:"+StringUtils.join(((List)data.get(3)).toArray(), "|"));
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"outputs:"+StringUtils.join(((List)data.get(4)).toArray(), "|"));
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"runtime:"+data.get(5));
        
        //outputs is the list of files, but location, zone, sitename and constraints should be the same as output 
        FileProperties output=((FileProperties)data.get(2));
        
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Getting input file");
        
        String workingdir=System.getProperty("WorkingDir");
        
        //GET INPUT
        long startGetInput=System.currentTimeMillis();
        
        //download all inputs
        List<String> inputFileList=this.downloadInputs((List<FileProperties>)data.get(3), workingdir);
        
        String filenames=StringUtils.join(inputFileList.toArray()," ");
        
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO, "Time in sec to get all INPUT:{0}", (System.currentTimeMillis()-startGetInput)/1000L);
        
        //GENERATE OUTPUT and remove input
        long startGenOutput=System.currentTimeMillis();
        
        //remove files
        CommonUtils.execute("rm -f "+filenames);
        
        //generate output        
        List outfiles=new ArrayList(); 
        
        List<FileProperties> resultFiles=new ArrayList();
        
        
        String outputnames="";
        for (FileProperties fp:(List<FileProperties>)data.get(4)){
            createRandomFile(System.getProperty("WorkingDir"), fp);
            outfiles.add(fp.getName());
            String location;
            if(output.getLocation().equals("")){
                location=System.getProperty("siteFileCache"); //Site cache directory that can be use as data repository (siteIp:dir)
                fp.setLocation(location);
                fp.setSitename(System.getProperty("Name"));
                fp.setZone(System.getProperty("Zone"));
            }            
            
            //since all fields are the same as in output and outputs, then we should be ok with just changing fp.location
            //FileProperties resultfile=new FileProperties(fp.getName(),location,fp.getSize(),
            //   output.getZone(), output.getSitename(), output.getConstraints());
                 
            resultFiles.add(fp);
                    
            outputnames+=" "+workingdir+"/"+fp.getName();
        }                        
                
        double totalGenOutput=(System.currentTimeMillis()-startGenOutput)/1000D;
        
        //COMPUTE
        double time=0.0;
        try {
            time=(Double)data.get(5)/Double.parseDouble(System.getProperty("performance"))-totalGenOutput;
            Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Worker will take "+time+" seconds. task:"+data.get(5)+" perf:"+System.getProperty("performance")+" minus gen data:"+totalGenOutput);
            
            Thread.sleep(((long)time)/8*1000);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(AppWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO, "Time in sec to EXECUTE:{0}", time);
                
        //SEND OUTPUT                
        long startPutOutput=System.currentTimeMillis();
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Uploading output file");
        
        String status=this.putFile(((FileProperties)data.get(2)).getLocation(), outfiles, System.getProperty("WorkingDir"));
        
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO,"Exit status: "+status);
        Logger.getLogger(AppWorker.class.getName()).log(Level.INFO, "Time in sec to put all OUTPUT:{0}", (System.currentTimeMillis()-startPutOutput)/1000L);
        
        CommonUtils.execute("rm -f "+outputnames);
        
        return new Object[]{"OK",resultFiles};
        
    }
    
    public void createRandomFile(String dir, FileProperties fp){
        File f=new File(dir+"/"+fp.getName());                                    
        try {
            RandomAccessFile r=new RandomAccessFile(dir+"/"+fp.getName(), "rw");                                    
            r.setLength((long)fp.getSize());
            r.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
                Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    @Override
    public void cancelJob() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
