/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud.sampleMontage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tassl.application.cometcloud.GenerateTasksAbstract;
import tassl.application.cometcloud.GenerateTasksObject;
import tassl.application.cometcloud.Results;
import tassl.application.cometcloud.FileProperties;

/**
 *
 * @author Javier Diaz-Montes
 */
public class GenerateTasksMontage extends GenerateTasksAbstract{
    
    public GenerateTasksMontage(){
        super();
    }
    
    //this one is done by the user
    @Override
    public GenerateTasksObject createTasks(String stageId, List<FileProperties> input, FileProperties output, String propertyFileValues, List dependencies, String method){ //example where each input file creates a task
        GenerateTasksObject taskObj=null;
        
        //load properties from the file specified in the workflow XML (it can be different for each stage)
        this.loadProperties(propertyFileValues);
        
        if(method.equals("mProjectPP")){
            
            taskObj=genStage(input,output,propertyFileValues, null, stageId,"mProjectPP");
        }else if(method.equals("mDiffFit")){     
            
            //we need this to learn where the files are. Format will be something like filename;location;size
            HashMap <String,FileProperties>previousFiles=generatePreviousResultFiles(stageId, dependencies);
            
            taskObj=genStage(input,output,propertyFileValues, previousFiles, stageId,"mDiffFit");
        }else if(method.equals("mConcatFit")){
            HashMap <String,FileProperties>previousFiles=generatePreviousResultFiles(stageId, dependencies);
            
            //mConcatFit and mBgModel
            taskObj=genStageConcatModel(input,output,propertyFileValues, previousFiles, stageId,"mConcatFit");
        }else if(method.equals("mBackground")){
            HashMap <String,FileProperties>previousFiles=generatePreviousResultFiles(stageId, dependencies);
                        
            taskObj=genStage(input,output,propertyFileValues, previousFiles, stageId,"mBackground");
        }else if(method.equals("mmImgTbl")){
            HashMap <String,FileProperties>previousFiles=generatePreviousResultFiles(stageId, dependencies);
            //mImgTbl, mAdd, mShrink,mJPEG
            taskObj=genStageFinal(input,output,propertyFileValues, previousFiles, stageId,"mmImgTbl");
        }
        return taskObj;
        
        
    }
       
    
    public GenerateTasksObject genStage(List<FileProperties> input, FileProperties output, String propertyFileValues, HashMap previousResults, String stageid, String nameCode){
        //input and output are address://dir. We get all using SSH
        
        //ASSUMING ALL IS IN THE FIRST INPUT DIRECTORY
        FileProperties inputFP=input.get(0);
        String inputS=inputFP.getLocation();
                       
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the reference machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        List <List<FileProperties>>inputList=new ArrayList();
        List <List<FileProperties>>outputList=new ArrayList();
        
        String montagexml=getProperty("montage_xml");
        
        Document montagedoc = XMLParser(new File(montagexml));        
        NodeList stageList = montagedoc.getElementsByTagName("job");
        for (int i=0; i<stageList.getLength(); i++) {
            Element el = (Element)stageList.item(i);
            String name = el.getAttribute("name");
            if (name.equals(nameCode)){
                double runtime=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                List <FileProperties>inputs=new ArrayList();
                List <FileProperties>outputs=new ArrayList();
                
                NodeList jobList = el.getElementsByTagName("uses");
                for (int j=0; j<jobList.getLength(); j++) {
                    Element elu = (Element)jobList.item(j);                    
                    if (elu.getAttribute("link").equals("input")){                        
                        
                        if(previousResults!=null && previousResults.containsKey(elu.getAttribute("file"))){
                            FileProperties tempF=(FileProperties)previousResults.get(elu.getAttribute("file"));
                            //tempF.getSize(); should the the same as elu.getAttribute("size") 
                            //we select as input the place where the task was executed before to exploit data locality                            
                            if(tempF!=null){
                                inputs.add(tempF);                                
                            }
                        }else{                                                        
                            inputs.add(new FileProperties(elu.getAttribute("file"),inputS,Double.parseDouble(elu.getAttribute("size")), inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));
                        }                        
                        
                        
                        //create dummy files 
                        if(name.equals("mProjectPP")){
                            String dir=inputS.split(":")[1];
                            File f=new File(dir+"/"+elu.getAttribute("file"));                            
                            if(!f.isFile()){
                                try {
                                    RandomAccessFile r=new RandomAccessFile(dir+"/"+elu.getAttribute("file"), "rw");                                    
                                    r.setLength(Long.parseLong(elu.getAttribute("size")));                                    
                                    r.close();
                                } catch (FileNotFoundException ex) {
                                    Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
                                }catch (IOException ex) {
                                        Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }else if (elu.getAttribute("link").equals("output")){
                        String filename=elu.getAttribute("file");
                        if (nameCode.equals("mDiffFit")){
                            filename=filename+taskid;
                        }
                        outputs.add(new FileProperties(filename,output.getLocation(),Double.parseDouble(elu.getAttribute("size")),output.getZone(), output.getSitename(), output.getConstraints()));                                           
                    }                    
                }
                //this is the object that will be associated to the tuple. Worker will get it as is.
                taskParams.add(taskid, Arrays.asList(
                                        stageid,               //0
                                        inputFP,               //1 //FileProperties, the first datasource entry only
                                        output,                //2 //FileProperties
                                        inputs,                //3
                                        outputs,               //4                                        
                                        runtime                //5
                           ));
                taskRequirement.add("something"); //Requirement (TODO: fix it so it can be blank)
                
                //Interval to estimate task duration. (for scheduler)
                minTime.add(runtime);
                maxTime.add(runtime);
                //input and output files with sizes and locations (for scheduler)
                inputList.add(inputs);
                outputList.add(outputs);
                
                System.out.println("Task:"+taskid+" Inputs: "+ StringUtils.join(inputs.toArray(), ",") +" Outputs: "+ 
                        StringUtils.join(outputs.toArray(), ",") +" Runtime:"+runtime); 
                taskid++;
                
            }
        }
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime, inputList, outputList);
        
    }
    
    public GenerateTasksObject genStageConcatModel(List<FileProperties> input, FileProperties output, String propertyFileValues, HashMap previousResults, String stageid, String nameCode){
        //input and output are address://dir. We get all using SSH
        
        //ASSUMING ALL IS IN THE FIRST INPUT DIRECTORY
        FileProperties inputFP=input.get(0);
        String inputS=inputFP.getLocation();
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the reference machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
                
        List <List<FileProperties>>inputList=new ArrayList();
        List <List<FileProperties>>outputList=new ArrayList();
        
        String montagexml=getProperty("montage_xml");
        
        Document montagedoc = XMLParser(new File(montagexml));        
        NodeList stageList = montagedoc.getElementsByTagName("job");
        boolean concat=false;
        boolean model=false;
        double runtime=0.0;
        List <FileProperties>inputs=new ArrayList();
        List <FileProperties>outputs=new ArrayList();
        
        for (int i=0; i<stageList.getLength(); i++) {
            Element el = (Element)stageList.item(i);
            String name = el.getAttribute("name");            
            
            if (name.equals("mConcatFit")){
                runtime+=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                NodeList jobList = el.getElementsByTagName("uses");
                for (int j=0; j<jobList.getLength(); j++) {
                    Element elu = (Element)jobList.item(j);                    
                    if (elu.getAttribute("link").equals("input")){
                        //inputs are known
                        
                        //get N fit.txt<num> and diff.txt<num>
                        boolean good=false;
                        for (String keyi:(Set<String>)previousResults.keySet()){
                            
                            if(keyi.startsWith("fit.txt")){
                                good=true;
                            }else if (keyi.startsWith("diff.txt")){
                                good=true;
                            }
                            if(good){
                                FileProperties tempF=(FileProperties)previousResults.get(keyi);                                
                                if(tempF!=null){                                    
                                    inputs.add(tempF);
                                }                             
                                good=false;
                            }
                        }
                    }               
                }
                concat=true;
            }
            if(name.equals("mBgModel")){
                runtime+=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                NodeList jobList = el.getElementsByTagName("uses");
                for (int j=0; j<jobList.getLength(); j++) {
                    Element elu = (Element)jobList.item(j); 
                    if (elu.getAttribute("link").equals("input")){
                        if(elu.getAttribute("file").equals("pimages.tbl")){
                            inputs.add(new FileProperties(elu.getAttribute("file"), inputS, Double.parseDouble(elu.getAttribute("size")),inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));                        
                            
                            String dir=inputS.split(":")[1];
                            File f=new File(dir+"/"+elu.getAttribute("file"));                            
                            if(!f.isFile()){
                                try {
                                    RandomAccessFile r=new RandomAccessFile(dir+"/"+elu.getAttribute("file"), "rw");                                    
                                    r.setLength(Long.parseLong(elu.getAttribute("size")));                                    
                                    r.close();
                                } catch (FileNotFoundException ex) {
                                    Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
                                }catch (IOException ex) {
                                        Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }
                    if (elu.getAttribute("link").equals("output")){
                        String filename=elu.getAttribute("file");
                        
                        outputs.add(new FileProperties(filename,output.getLocation(),Double.parseDouble(elu.getAttribute("size")),output.getZone(), output.getSitename(), output.getConstraints())); 
                    }   
                }
                model=true;
            }
            if(concat && model){
                //this is the object that will be associated to the tuple. Worker will get it as is.
                taskParams.add(taskid, Arrays.asList(
                                        stageid,               //0
                                        input,                 //1
                                        output,                //2
                                        inputs,                //3
                                        outputs,               //4                                        
                                        runtime                //5
                           ));
                taskRequirement.add("something"); //Requirement (TODO: fix it so it can be blank)
                
                //Interval to estimate task duration.
                minTime.add(runtime);
                maxTime.add(runtime);
                
                //input and output files with sizes and locations (for scheduler)
                inputList.add(inputs);
                outputList.add(outputs);
                
                System.out.println("Task:"+taskid+" Inputs: "+ StringUtils.join(inputs.toArray(), ",") +" Outputs: "+ 
                        StringUtils.join(outputs.toArray(), ",") +" Runtime:"+runtime); 
                taskid++;
                
                break;
            }
                
        }
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime, inputList, outputList);
        
    }
    
    public GenerateTasksObject genStageFinal(List<FileProperties> input, FileProperties output, String propertyFileValues, HashMap previousResults, String stageid, String nameCode){
        //input and output are address://dir. We get all using SSH
        
        //ASSUMING ALL IS IN THE FIRST INPUT DIRECTORY
        FileProperties inputFP=input.get(0);
        String inputS=inputFP.getLocation();
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the reference machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        List <List<FileProperties>>inputList=new ArrayList();
        List <List<FileProperties>>outputList=new ArrayList();
        
        String montagexml=getProperty("montage_xml");
        
        Document montagedoc = XMLParser(new File(montagexml));        
        NodeList stageList = montagedoc.getElementsByTagName("job");
        boolean imgtbl=false;
        boolean madd=false;
        boolean mshrink=false;
        boolean mjpeg=false;
        double runtime=0.0;
        
        List <FileProperties>inputs=new ArrayList();
        List <FileProperties>outputs=new ArrayList();
            
        for (int i=0; i<stageList.getLength(); i++) {
            Element el = (Element)stageList.item(i);
            String name = el.getAttribute("name");       
            if (name.equals("mImgTbl")){
                runtime+=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                NodeList jobList = el.getElementsByTagName("uses");
                for (int j=0; j<jobList.getLength(); j++) {
                    Element elu = (Element)jobList.item(j);                    
                    if (elu.getAttribute("link").equals("input")){
                        
                        if(previousResults!=null && previousResults.containsKey(elu.getAttribute("file"))){
                            FileProperties tempF=(FileProperties)previousResults.get(elu.getAttribute("file"));
                            //tempF.getSize(); should the the same as elu.getAttribute("size") 
                            //we select as input the place where the task was executed before to exploit data locality                            
                            if(tempF!=null){
                                inputs.add(tempF);                                
                            }
                        }else{                                                        
                            inputs.add(new FileProperties(elu.getAttribute("file"),inputS,Double.parseDouble(elu.getAttribute("size")), inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));
                        }  
                        
                        
                        if(elu.getAttribute("file").equals("cimages.tbl")){
                            String dir=inputS.split(":")[1];
                            File f=new File(dir+"/"+elu.getAttribute("file"));                            
                            if(!f.isFile()){
                                try {
                                    RandomAccessFile r=new RandomAccessFile(dir+"/"+elu.getAttribute("file"), "rw");                                    
                                    r.setLength(Long.parseLong(elu.getAttribute("size")));                                    
                                    r.close();
                                } catch (FileNotFoundException ex) {
                                    Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
                                }catch (IOException ex) {
                                        Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }                
                }
                imgtbl=true;
            }
            if(name.equals("mAdd")){
                runtime+=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                NodeList jobList = el.getElementsByTagName("uses");
                for (int j=0; j<jobList.getLength(); j++) {
                    Element elu = (Element)jobList.item(j); 
                    if (elu.getAttribute("link").equals("input")){
                        if(elu.getAttribute("file").equals("region.hdr")){
                            inputs.add(new FileProperties(elu.getAttribute("file"), inputFP.getLocation(), Double.parseDouble(elu.getAttribute("size")), 
                                    inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));
                        }
                    }                   
                }
                madd=true;
            }
            if(name.equals("mShrink")){
                runtime+=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                
                mshrink=true;
            }
            if(name.equals("mJPEG")){
                runtime+=Double.parseDouble(el.getAttribute("runtime"))*8; //because the time in the xml was done in 8 machines and we will use only one.
                NodeList jobList = el.getElementsByTagName("uses");
                for (int j=0; j<jobList.getLength(); j++) {
                    Element elu = (Element)jobList.item(j); 
                    
                    if (elu.getAttribute("link").equals("output")){
                        String filename=elu.getAttribute("file");                        
                        outputs.add(new FileProperties(filename,output.getLocation(),Double.parseDouble(elu.getAttribute("size")),
                                output.getZone(), output.getSitename(), output.getConstraints()));                                           
                    }   
                }
                mjpeg=true;
            }
            if(imgtbl && madd && mshrink && mjpeg){
                //this is the object that will be associated to the tuple. Worker will get it as is.
                taskParams.add(taskid, Arrays.asList(
                                        stageid,              //0
                                        input,                 //1
                                        output,                //2
                                        inputs,                //3
                                        outputs,               //4                                        
                                        runtime                //5
                           ));
                taskRequirement.add("something"); //Requirement (it can be blank)
                
                //Interval to estimate task duration.
                minTime.add(runtime);
                maxTime.add(runtime);
                
                //input and output files with sizes and locations (for scheduler)
                inputList.add(inputs);
                outputList.add(outputs);
                
                System.out.println("Task:"+taskid+" Inputs: "+ StringUtils.join(inputs.toArray(), ",") +" Outputs: "+ 
                        StringUtils.join(outputs.toArray(), ",") +" Runtime:"+runtime); 
                taskid++;
                
                break;
            }
                
        }
        
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime, inputList, outputList);
        
    }
    
    
    public static Document XMLParser(File xmlFile){        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document doc=null;
        
        try {
            db = dbf.newDocumentBuilder();
            doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);            
            ex.printStackTrace();        
        } catch (SAXException ex) {
            Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();        
        } catch (IOException ex) {
            Logger.getLogger(GenerateTasksMontage.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();        
        }         
        return doc;
    }

    
    public static void main(String []args){
        GenerateTasksMontage gt=new GenerateTasksMontage();
        List<FileProperties> input=new ArrayList();
        input.add(new FileProperties("","jdiaz@sierra.futuregrid.org:/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/inputs/",0,"zoneA","siteSierra",new ArrayList()));
        
        gt.createTasks("S1",input, new FileProperties("","localhost:/tmp/",0,"","",new ArrayList()), "/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/sample.properties",null, "mProjectPP");
        System.out.println("======================================");
        gt.createTasks("S2",input, new FileProperties("","localhost:/tmp/",0,"","",new ArrayList()), "/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/sample.properties",null, "mDiffFit");
        System.out.println("======================================");
        gt.createTasks("S3",input, new FileProperties("","localhost:/tmp/",0,"","",new ArrayList()), "/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/sample.properties",null, "mConcatFit");
        System.out.println("======================================");
        gt.createTasks("S4",input, new FileProperties("","localhost:/tmp/",0,"","",new ArrayList()), "/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/sample.properties",null, "mBackground");
        System.out.println("======================================");
        gt.createTasks("S5",input, new FileProperties("","localhost:/tmp/",0,"","",new ArrayList()), "/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/sample.properties",null, "mmImgTbl");
    }
}
