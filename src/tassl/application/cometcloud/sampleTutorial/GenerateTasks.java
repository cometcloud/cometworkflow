/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud.sampleTutorial;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.GenerateTasksAbstract;
import tassl.application.cometcloud.GenerateTasksObject;

/**
 * This code is a simple map/reduce application. 
 *  The map is going to generate PR tasks per file found in the input directories.
 *  The reduce will get all results and perform an operation. 
 *  Note: no binaries are needed, just dummy files and the computation is emulated using the sleep function.
 * 
 * 
 * @author Javier Diaz-Montes
 */
public class GenerateTasks extends GenerateTasksAbstract{
    
    public GenerateTasks(){
        super();
    }
    
    //this one is done by the user
    @Override
    public GenerateTasksObject createTasks(String stageId, List<FileProperties> input, FileProperties output, String propertyFileValues, List dependencies, String method){ 
        GenerateTasksObject taskObj=null;
        
        //load properties into the environment so we can obtain them using the getProperty(varName) method.
        this.loadProperties(propertyFileValues);
        if(method.equals("map")){
            //call method that generate tasks
            taskObj=map(input,output,propertyFileValues);
        }else if(method.equals("reduce")){
            //obtain results of previous stages. We can use them as input of the following stage.
            HashMap <String,FileProperties>previousFiles=this.generatePreviousResultFiles(stageId, dependencies);
            //call method that generate tasks
            taskObj=reduce(input,output,propertyFileValues,previousFiles);
        }            
        return taskObj;
        
        
    }
    
    //example where each input file creates a task
    public GenerateTasksObject map(List<FileProperties> input, FileProperties output, String propertyFile){
            //input and output are address://dir. We get all using SSH
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)

        //we create lists to keep properties of each task
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        List <List<FileProperties>>inputList=new ArrayList();

        //getProperty get values from the property files specified in the workflow
        //the property file has two variables to indicate the execution time interval of tasks.
        double minTimeVal=Double.parseDouble(getProperty("minTime"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTime"));
        //obtain the PR value
        int PR=Integer.parseInt(getProperty("PR"));
        //iterate on InputData sources
        for(FileProperties inputFP: input){
            //obtain information of each InputData source
            String inputS=inputFP.getLocation();
            String []parts=inputS.split(":");
            //obtain list of files and size
            String returnSsh=executeSsh(parts[0]," ls -l "+parts[1]+" | awk '{print $5, $9}'");
            String [] files=returnSsh.split("\n"); //returns size and name
            //for each file we generate PR tasks
            for(int i=0;i<files.length;i++){
                if(!files[i].trim().isEmpty()){
                    for (int j=0; j< PR;j++){
                            //Here each task only has one input, but the API requries a list
                            List <FileProperties>inputs=new ArrayList();
                            String [] fileParts=files[i].split(" ");//size and name
                            //create input file of task and add it to the list of input files of this task
                            inputs.add(new FileProperties(fileParts[1],inputS,Double.parseDouble(fileParts[0]),
                                                inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));
                            //generate random duration for this task, which is used by the worker to emulate the execution of the task
                            double taskDuration=minTimeVal + (Math.random() * (maxTimeVal - minTimeVal));
                            //Define task's parameters. This is used to pass information to the worker.
                            //Thus, these parameters will be received for the worker in the same order.
                            taskParams.add(taskid, Arrays.asList(
                                                         "map",                 //0
                                                         output,                //1
                                                         inputs,                //2
                                                         taskDuration,          //3
                                                         Integer.toString(j+1)  //4
                                            ));
                            //we can add one custom value as requirements to the task tuple.
                            taskRequirement.add("large"); //Requirement
                            //we add the execution interval of task (this is for initial scheduling when no information of tasks exist)
                            minTime.add(minTimeVal);
                            maxTime.add(maxTimeVal);
                            //add input files of task
                            inputList.add(inputs);
                            taskid++;
                    }
                }
            }

        }
        //return object with the information of all tasks
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
        
    }
    
    //second stage
    //we aggregate all previous results and produce a single final output.
    public GenerateTasksObject reduce(List<FileProperties> input, FileProperties output, String propertyFile, HashMap <String,FileProperties>previousResults){
        
            int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
            //we create lists to keep properties of each task
            List <Double> minTime= new ArrayList();
            List <Double> maxTime=new ArrayList();
            List <List>taskParams=new ArrayList();
            List <String>taskRequirement=new ArrayList<String>();
            List <List<FileProperties>>inputList=new ArrayList();

            //create one task that aggregates all previous results
            List <FileProperties>inputs=new ArrayList();
            for (String key:previousResults.keySet()){
                inputs.add(previousResults.get(key));
            }
            inputList.add(inputs);
            //getProperty get values from the property files specified in the workflow
            //the property file has two variables to indicate the execution time interval of tasks.
            double minTimeVal=Double.parseDouble(getProperty("minTime"));
            double maxTimeVal=Double.parseDouble(getProperty("maxTime"));
            //generate random duration for this task, which is used by the worker to emulate the execution of the task
            double taskDuration=minTimeVal + (Math.random() * (maxTimeVal - minTimeVal));
            //Define task's parameters. This is used to pass information to the worker.
            //Thus, these parameters will be received for the worker in the same order.
            taskParams.add(taskid, Arrays.asList(
                                         "reduce",              //0
                                         output,                //1
                                         inputs,                //2
                                         taskDuration           //3
                            ));
            //we can add one custom value as requirements to the task tuple.
            taskRequirement.add("large"); //Requirement
            //we add the execution interval of task (this is for initial scheduling when no information of tasks exist)
            minTime.add(minTimeVal);
            maxTime.add(maxTimeVal);
            //add input files of task
            inputList.add(inputs);
            taskid++;

            return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
    }
    
}
