/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Javier Diaz-Montes
 */
public class GenerateTasksObject {
    List <List> taskParams;
    List <String> taskRequirement;
    List <Double> minTime;
    List <Double> maxTime;
    List <List<FileProperties>>inputList;
    List <List<FileProperties>>outputList;
    
    public GenerateTasksObject(){
        taskParams=new ArrayList();
        taskRequirement=new ArrayList();
    }

    public GenerateTasksObject(List<List> taskParams, List<String> taskRequirement, List <Double> minTime, 
            List <Double> maxTime, List <List<FileProperties>>inputList, List <List<FileProperties>>outputList) {
        this.taskParams = taskParams;
        this.taskRequirement = taskRequirement;
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.inputList=inputList;
        this.outputList=outputList;
    }

    /**
     * Minimum time to execute in slowest
     * @return
     */
    public List <Double> getMinTime() {
        return minTime;
    }

    /**
     * Maximum time to execute in slowest proc
     * @return
     */
    public List <Double> getMaxTime() {
        return maxTime;
    }

    /**
     * List of input files as objects of FileProperties
     * @return 
     */
    public List<List<FileProperties>> getInputList() {
        return inputList;
    }

    /**
     * List of output files as objects of FileProperties
     * @return
     */
    public List<List<FileProperties>> getOutputList() {
        return outputList;
    }

    public List <List> getTaskParams(){
        return taskParams;
    }
    public List <String> getTaskRequirement(){
        return taskRequirement;
    }
}
