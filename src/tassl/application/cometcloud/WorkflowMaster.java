package tassl.application.cometcloud;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.automate.comet.XmlTuple;
import tassl.automate.comet.xmltuplespace.XmlTupleService;
import tassl.automate.programmodel.masterworker.simpleapi.GenericMaster;
import tassl.workflow.WorkflowStage;

/**
 * Task Manager
 *
 * @author Javier Diaz
 */
public class WorkflowMaster extends GenericMaster {

    final String DEFAULT_SERVER_PORT = "7777";

    int currentTaskId = 0;
    boolean alive = true;

    HashMap<Integer, TaskProperties> taskProp = new HashMap<Integer, TaskProperties>();

    //Store done tasks to clean up once stage is done
    // workflowId.stageId:{taskid,taskid,..}
    HashMap<String, List<Integer>> doneTasksWorkflow = new HashMap();
    //Store all tasks of a stage
    //wkfid.stageid:{taskid,taskid} 
    HashMap<String, List<Integer>> taskWorkflowStage = new HashMap();

    //wkfid:{stageid:{results,results ..},...}, where results has taskid and result data
    HashMap<String, HashMap<String, List<Results>>> resultsHash
            = new HashMap<String, HashMap<String, List<Results>>>();

    //stageId and the following stages that can start before this stage finishes (for now it will be just one)
    //"workflowId.stageId":{"workflowId.stageId","workflowId.stageId","workflowId.stageId"}
    HashMap<String, List<String>> activeNonBlockingStages = new HashMap();

    //control number of times a task has failed. We only try 3 times each task.
    //if it cannot be completed, its status get changed to done
    HashMap<Integer, Integer> resubmitted = new HashMap();

    //Non-blocking transition - Store done tasks, this is collected, sent to AS to generate following tasks, and cleaned periodically.
    // {workflowId.stageId,workflowId.stageId,workflowId.stageId,...}
    List<String> NBTransitionsWithNewResults = new ArrayList();

    //stages are added here when new tasks need to be generated because a task from a previous stage finished
    //{wkfid:{stageid,stageid},...}
    HashMap<String, List<String>> NBStagestoSend = new HashMap();

    String workflowManager = null;

    //create a monitor thread just in case there is non-blocking transitions        
    Thread NBTmonitoring = null;
    NonBlockingTransitionMonitor collector;
    //to sync NBTransitionsWithNewResults with NBTmonitoring thread
    final Object syncNonBlocking = new Object();

    //to sync activeNonBlockingStages between geenrateTasks and setResults
    final Object syncActiveNonBlockingStages = new Object();

    /**
     * This method is called to read the application properties and initiate the
     * tasks creation.
     */
    @Override
    public void generateTasks() {

        //Accept connections and have conditions to choose operation
        //get stage options
        //create object of the generate task class
        //get hashtables with tasks
        //insert tasks into space
        //tell autonomic scheduler how many tasks for this application we have, and the info from the workflow
        int servPort = Integer.parseInt(System.getProperty("workflowMasterPort", DEFAULT_SERVER_PORT));

        System.out.println("Starting Server in port " + servPort);

        Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Starting Server in port " + servPort);

        ServerSocket servSock;
        try {
            servSock = new ServerSocket(servPort);
            servSock.setReuseAddress(true);

            while (alive) {
                try {
                    Socket clientSocket = servSock.accept();
                    DataInputStream input = new DataInputStream(clientSocket.getInputStream());
                    DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
                    String command = input.readUTF();

                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "command: " + command);
                    if (command.equals("generateTasks")) {
                        String newManager = input.readUTF();
                        if (newManager.equals("newWorkflowManager")) {
                            int port = input.readInt();
                            System.out.println("CWDEBUG. workflowmaster Waiting for address");
                            String address = input.readUTF();
                            System.out.println("The workflow manager is: " + address + ":" + port);
                            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "The workflow manager is: " + address + ":" + port);
                            //we only allow one workflowmanager at this time
                            //TODO: Fix this. We need to have a hashMap of workflowManagers and workflow stages.
                            workflowManager = address + ":" + port;
                        }

                        String wkflId = input.readUTF();
                        List<WorkflowStage> stages = (List) readObject(input);
                        String stagesInfoReturn = "";
                        //to return with tasks properties for each stage
                        HashMap<String, HashMap<Integer, TaskProperties>> stageTasksProp = new HashMap();
                        for (WorkflowStage tempS : stages) {
                            HashMap<String, Object> properties = tempS.getProperties();
                            String stageId = tempS.getId();
                            System.out.println("CWDEBUG: Generate tasks for stage " + stageId);

                            //check if this stage has non-blocking transitions (currently only allow a pipeline, so the list can only have one element)
                            List<String> stageNonBlockTrans = tempS.getNonBlockingTransitions();
                            if (!stageNonBlockTrans.isEmpty()) {
                                synchronized (syncActiveNonBlockingStages) {
                                    if (!activeNonBlockingStages.containsKey(wkflId + "." + stageId)) {
                                        activeNonBlockingStages.put(wkflId + "." + stageId, addWorkflowIdtoStageId(stageNonBlockTrans, wkflId));
                                        //check if monitor is running and start it otherwise                                    
                                        if (NBTmonitoring == null) {
                                            collector = new NonBlockingTransitionMonitor();
                                            NBTmonitoring = new Thread(collector);
                                            NBTmonitoring.start();
                                        } else if (!NBTmonitoring.isAlive()) {
                                            collector.setActive(true);
                                            NBTmonitoring = new Thread(collector);
                                            NBTmonitoring.start();
                                        } else if (!collector.getActive()) {
                                            collector.setActive(true);
                                        }
                                    }
                                }
                            }

                            Object[] originalWorkfStage;//[<String>OriginalWkflStageId, <double>%tasksReleased]
                            synchronized (syncActiveNonBlockingStages) {
                                //get stageId of the first stage that is not the result of a non-blocking transition
                                originalWorkfStage = findOriginalStageNonBlocking(wkflId + "." + stageId);
                            }

                            String stageIdforTasks;//bc when non-blocking they should have original stage in the tuple
                            boolean lastCall;// to know if this is the last time createTask is called for the present stage.
                                             // this is important for non-blocking transitions bc createTask is called multiple times for the same stage, for blocking transitions is always the last call.
                            if(originalWorkfStage==null){
                                stageIdforTasks=stageId;
                                lastCall=true;
                            }else{
                                stageIdforTasks=((String)originalWorkfStage[0]).split("\\.")[1];
                                if(((Double)originalWorkfStage[1]).doubleValue()==1.0){
                                   lastCall=true; 
                                }else{
                                   lastCall=false;
                                }
                            }
                                    
                            String []classTask_method=((String)properties.get("AppGenerateClass")).split(":");//in the XMLParser we make sure that it has this structure.
                            String classTask=classTask_method[0];
                            String methodTask=classTask_method[1];
                            String propertyFile=(String)properties.get("PropertyFile"); //returns the properties contained in the original property file
                            String appName=(String)properties.get("Application");
                            List<FileProperties> stageIn=(List<FileProperties>)properties.get("InputData");
                            FileProperties stageOut=(FileProperties)properties.get("Results");                  
                            Class generateTaskClass = null;
                            int newtasksAdded = 0;
                            try {
                                generateTaskClass = Class.forName(classTask);
                                Object genObj = generateTaskClass.newInstance();

                                HashMap tempHash = resultsHash.get(wkflId);
                                if (tempHash != null) {//set results of previous stages so they are available to be used in next generation
                                    System.out.println("CWDEBUG: Previous Results are not empty");
                                    Class[] arg = new Class[]{HashMap.class};
                                    Method methodSet = GenerateTasksAbstract.class.getDeclaredMethod("setPreviousResults", arg);
                                    methodSet.invoke(genObj, tempHash);
                                }
                                //if we want to use results from other related workflows
                                if (propertyFile.contains("RelatedWorkflowIds=")) {
                                    String RelatedWorkflowIds_temp1 = propertyFile.split("RelatedWorkflowIds=")[1]; //split by our variable of interest
                                    String RelatedWorkflowIds_temp2 = RelatedWorkflowIds_temp1.split("&#10;")[0]; //split by XML \n character
                                    String[] RelatedWorkflowIds = RelatedWorkflowIds_temp2.split(","); //we would have a comma separated list

                                    for (int i = 0; i < RelatedWorkflowIds.length; i++) {
                                        String relatedWorkflow = RelatedWorkflowIds[i].trim();
                                        HashMap tempHash1 = resultsHash.get(relatedWorkflow);
                                        if (tempHash != null) {//set results of previous stages so they are available to be used in next generation
                                            System.out.println("CWDEBUG: Results from related workflow " + relatedWorkflow + " are not empty");
                                            Class[] arg = new Class[]{HashMap.class, String.class};
                                            Method methodSet = GenerateTasksAbstract.class.getDeclaredMethod("addRelatedWorkflowResults", arg);
                                            methodSet.invoke(genObj, tempHash1, relatedWorkflow);
                                        }
                                    }
                                }
                                
                                
                                Class[] arg = new Class[]{Boolean.class};
                                Method methodSet = GenerateTasksAbstract.class.getDeclaredMethod("setLastCall", arg);                                    
                                methodSet.invoke(genObj,lastCall);
                                                                
                                Class[] argTypes = new Class[]{String.class,List.class,FileProperties.class,String.class, List.class, String.class};
                                Method method = generateTaskClass.getDeclaredMethod("createTasks", argTypes);                                   
                                GenerateTasksObject stageTasks=(GenerateTasksObject) method.invoke(genObj, 
                                        stageId,stageIn,stageOut,propertyFile, tempS.getDependencies(), methodTask);

                                //test
                                System.out.println("CWDEBUG: TestGenerated for stage " + stageId);

                                List params = stageTasks.getTaskParams();
                                List requir = stageTasks.getTaskRequirement();
                                List<Double> minTimes = stageTasks.getMinTime();
                                List<Double> maxTimes = stageTasks.getMaxTime();
                                List inputs = stageTasks.getInputList(); //input files information
                                List outputs = stageTasks.getOutputList(); //output files information

                                HashMap<Integer, TaskProperties> tempTasksProps = new HashMap();//to return only tasks from stages
                                TaskProperties task;
                                String requirement = "";
                                //MOUSTAFA - INSERTED A CHECK TO SEE IF THE PARAMETERS BEING PASSED ARE NULL
                                Double minTime = 1.0;
                                Double maxTime = 10000.0;
                                List outputListGen = new ArrayList();
                                List inputListGen = new ArrayList();
                                int initialTaskId = currentTaskId;

                                List<Integer> tasksOfStage;
                                synchronized (syncActiveNonBlockingStages) {//sync bc cleanStage
                                    //get existing tasks of stage                               
                                    tasksOfStage = this.taskWorkflowStage.get(wkflId + "." + stageId);
                                    if (tasksOfStage == null) {
                                        tasksOfStage = new ArrayList();
                                        this.taskWorkflowStage.put(wkflId + "." + stageId, tasksOfStage);
                                    }
                                }
                                //list of tasks to add to existing ones
                                List<Integer> tasksToAdd = new ArrayList();
                                if (params == null) {
                                    params = new ArrayList();
                                }
                                for (int i = 0; i < params.size(); i++) {

                                    //System.out.println("WorkflowMaster: "+Arrays.toString(stageTasks.getTaskParams().get(i).toArray()));
                                    if (requir != null) {
                                        requirement = (String) requir.get(i);
                                    }
                                    //MOUSTAFA - INSERTED A CHECK TO SEE IF THE PARAMETERS BEING PASSED ARE NULL
                                    if (minTimes != null) {
                                        minTime = minTimes.get(i);
                                    }
                                    if (maxTimes != null) {
                                        maxTime = maxTimes.get(i);
                                    }
                                    if (inputs != null) {
                                        inputListGen = (List) inputs.get(i);
                                    }
                                    if (outputs != null) {
                                        outputListGen = (List) outputs.get(i);
                                    }
                                    task = new TaskProperties(currentTaskId, requirement, Arrays.asList(workflowManager,
                                            wkflId, stageIdforTasks, appName, requirement), (List) params.get(i), minTime, maxTime, inputListGen, outputListGen);

                                    //check if duplicated
                                    if (tasksOfStage.isEmpty() || !checkIfDuplicatedTask(tasksOfStage, task)) {

                                        synchronized (syncActiveNonBlockingStages) {//sync bc cleanStage
                                            taskProp.put(currentTaskId, task);
                                        }

                                        tempTasksProps.put(currentTaskId, task);

                                        tasksToAdd.add(currentTaskId);

                                        this.insertTask(currentTaskId);
                                        currentTaskId++;
                                        newtasksAdded++;
                                        System.out.println("CWDEBUG: Task Added");
                                    } else {
                                        System.out.println("CWDEBUG: Task Ignored - Already in System");
                                    }
                                }
                                synchronized (syncActiveNonBlockingStages) {//sync bc cleanStage
                                    tasksOfStage.addAll(tasksToAdd);
                                }
                                setNumOfTasks(this.getNumOfTasks() + newtasksAdded);

                                //11/26/2014- initialTaskId is not used anymore
                                stagesInfoReturn += stageId + "-" + initialTaskId + ",";

                                //tasks
                                stageTasksProp.put(stageId, tempTasksProps);

                            } catch (IllegalArgumentException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break; //workflow would be incomplete. It goes into error status
                            } catch (InvocationTargetException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, "Cause", ex.getCause());
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break;
                            } catch (NoSuchMethodException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break;
                            } catch (SecurityException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break;
                            } catch (InstantiationException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break;
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break;
                            } catch (ClassNotFoundException ex) {
                                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                                stagesInfoReturn = "ERROR: in Master stage stageId" + stagesInfoReturn + ";";
                                break;
                            } finally {

                                String originalStageId;
                                if (originalWorkfStage == null) {//this stage is not part of a non-blocking transition

                                    stagesInfoReturn += newtasksAdded + ";";
                                } else { //this stage follows a non-blocking transition
                                    originalStageId = ((String) originalWorkfStage[0]).split("\\.")[1];
                                    //% objective to release based on % completed tasks
                                    double percentageToRelease = ((Double) originalWorkfStage[1]).doubleValue();
                                    stagesInfoReturn += newtasksAdded + "," + originalStageId + "," + percentageToRelease + ";";
                                }
                            }
                        }
                        //send back stage1-firsttaskid,#tasks;stage2-firsttaskid,#tasks; //when non-blocking transitions not involved
                        //or
                        //send back stage2-firsttaskid,#tasks,stage1,0.3; //here stage2 follows a non-blocking transition originated by stage1, and stage1 is completed at 30%
                        out.writeUTF(stagesInfoReturn);

                        byte[] taskObj;
                        taskObj = programming5.io.Serializer.serializeBytes(stageTasksProp);
                        out.writeInt(taskObj.length);
                        out.flush();
                        out.write(taskObj);
                        out.flush();

                    } else if (command.equals("failedTasks")) { //regenerate tasks from existing scheduled stages, notify which ones are done
                        //to return
                        HashMap<String, String> wflStage_doneTasks = new HashMap();

                        //receive info about workflow, workers and tasks
                        int length = input.readInt(); //data length
                        byte[] bytesdata = new byte[length];
                        input.readFully(bytesdata);

                        //{"wflId.stageId":"workersCommaSeparated;tasksCommaSeparated;worker:starttimestamp:0_CommaSeparated","wflId.stageId":"workersCommaSeparated;tasks;worker:starttimestamp:-1_CommaSeparated",...}
                        // 0 in the accounting part means that we need to stop the clock and -1 means that there was an error in provisioning and we remove this from accounting
                        HashMap<String, String> wflStage_workerTasks = new HashMap();
                        try {
                            wflStage_workerTasks = (HashMap) programming5.io.Serializer.deserialize(bytesdata);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        System.out.println("CWDEBUG: failedTasks. wflStage_workerTasks: " + wflStage_workerTasks.toString());

                        String[] valuePart;
                        for (String keys : wflStage_workerTasks.keySet()) {
                            valuePart = ((String) wflStage_workerTasks.get(keys)).split(";");
                            System.out.println("CWDEBUG: failedTasks. all " + wflStage_workerTasks.get(keys));
                            System.out.println("CWDEBUG: failedTasks. wkflid.stageid " + keys);
                            System.out.println("CWDEBUG: failedTasks. workersDown " + valuePart[0]);
                            System.out.println("CWDEBUG: failedTasks. tasks " + valuePart[1]);
                            System.out.println("CWDEBUG: failedTasks. accounting " + valuePart[2]);

                            if (!valuePart[1].equals("null")) {
                                //reinsert missing tasks
                                for (String task : valuePart[1].split(",")) {//commaSeparated failed tasks   
                                    if (!task.trim().isEmpty()) {
                                        if (this.getTaskStatus(Integer.parseInt(task)) == 0) {//if task is not done
                                            this.reinsertTask(Integer.parseInt(task));
                                        }
                                    }
                                }
                            }
                            //get done tasks for stage
                            //in case of non-blocking transitions, this "keys" will be always the original stage 
                            //  because is the only one scheduled.So getAllDoneTasks return tasks of this stage plus 
                            //  following stages after non-blocking transitions
                            synchronized (syncActiveNonBlockingStages) {
                                wflStage_doneTasks.put(keys, this.getAllDoneTasks(keys));
                            }
                        }

                        //send back HashMap with wflStage_doneTasks
                        byte[] wkflObj;
                        wkflObj = programming5.io.Serializer.serializeBytes(wflStage_doneTasks);
                        out.writeInt(wkflObj.length);
                        out.write(wkflObj);
                                               
                    }else if(command.equals("reinsertTasks")){                        
                        String tasks=input.readUTF();
                        for(String task:tasks.split(",")){
                            if(!task.trim().isEmpty()){
                                if(this.getTaskStatus(Integer.parseInt(task))==0){//if task is not done
                                    this.reinsertTask(Integer.parseInt(task));
                                }
                            }
                        }
                    }else if(command.equals("checkStageDoneNoNewTask")){                        
                        String wfkStageId=input.readUTF();
                        System.out.println("CWDEBUG: checkStageDoneNoNewTask. for "+wfkStageId);
                        this.checkStageDoneNoNewTask(wfkStageId);

//                    } else if (command.equals("reinsertTasks")) {
//                        String tasks = input.readUTF();
//                        for (String task : tasks.split(",")) {
//                            if (!task.trim().isEmpty()) {
//
//                                if (this.getTaskStatus(Integer.parseInt(task)) == 0) {//if task is not done
//
//                                    this.reinsertTask(Integer.parseInt(task));
//                                }
//                            }
//                        }
                    } else {
                        Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Unknown command: {0}", command);
                    }

                    input.close();
                    out.close();
                } catch (IOException e) {
                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, e);
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private List<String> addWorkflowIdtoStageId(List<String> stageList, String workflowId) {
        List<String> toReturn = new ArrayList();
        for (String st : stageList) {
            toReturn.add(workflowId + "." + st);
        }
        return toReturn;
    }

    public boolean checkIfDuplicatedTask(List<Integer> tasksOfStage, TaskProperties newTask) {
        boolean result = false;

        //System.out.println("**********************************************");
        //System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
        //System.out.println("COMPARE NEW TASK "+newTask.toString());
        for (int taskid : tasksOfStage) {
            //System.out.println("----------------------------------------------");
            //System.out.println(" WITH            "+taskProp.get(taskid).toString());
            //System.out.println("----------------------------------------------");
            if (taskProp.get(taskid).compareTask(newTask)) {
                //System.out.println("  DUPLICATED");
                result = true;
                break;
            }//else{
            //System.out.println("  NOT duplicated");
            //}
        }
        //System.out.println("----------------------------------------------");

        //System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
        //System.out.println("**********************************************");
        return result;
    }

    public Object readObject(DataInputStream in) {
        Object receivedObject = null;
        try {
            long stime = System.currentTimeMillis();

            int length = in.readInt(); //data length             
            byte[] bytesdata = new byte[length];
            in.readFully(bytesdata);
            receivedObject = programming5.io.Serializer.deserialize(bytesdata);

            long etime = System.currentTimeMillis();
        } catch (IOException ex) {
            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
        return receivedObject;
    }

    /**
     * This method is called from insertTask to create the data associated to
     * the task
     *
     * @param taskid Task identifier that can be use as index to create its
     * data.
     * @return Object input data that will be attached to the task. The object
     * has to be serializable.
     */
    @Override
    public Object createTaskData(int taskid) {

        return (Object) taskProp.get(taskid).getTaskParam();

    }

    /**
     * This method is called when a worker return a result
     *
     * @param taskid Task identifier that can be use as index to create its
     * data.
     * @param data Object with the returned data
     * @param message Additional information send by the worker
     * @param sender Name of the worker
     */
    @Override
    public boolean setResultSpecific(int taskid, Object data, String message, String sender) {

        boolean jobok = true;
        //do something with the returned result

        //result[0] is a String with the status; result[1] is an Object with the actual result; 
        // results[2], if it is there, it is a boolean that indicates whether we should iterate or not.
        //    For now we assume that a meaningful results[2] comes from an aggregation stage with a single task, hence there is a unique value that decides
        //    if we should do another iteration or not
        Object[] result = (Object[]) data;

        if (getTaskStatus(taskid) != 0) { //the task is already done. It is a duplicated and we do not care about its status.
            return true;
        }

        if (((String) result[0]).startsWith("ERROR")) {
            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.WARNING, " Result taskid={0} received with ERROR. Resubmitting task: {1}", new Object[]{taskid, result[0]});
            jobok = false;
            if (resubmitted.get(taskid) == null) {
                resubmitted.put(taskid, 1);
                insertTask(taskid);
            } else {
                int n = resubmitted.get(taskid);
                if (n < 3) {
                    resubmitted.put(taskid, ++n);
                    insertTask(taskid);
                } else {
                    resubmitted.put(taskid, ++n);
                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.WARNING,
                            "Taskid={0} cannot be completed. Stop trying.", taskid);
                    jobok = true;
                    //Set task as done and increment completed tasks
                    setTaskStatus(taskid);
                    String wfkStageId=getWkfStageId(taskid);
                    List <Integer>doneTasks=doneTasksWorkflow.get(wfkStageId);
                    if(doneTasks==null){
                        doneTasks=new ArrayList();
                        doneTasksWorkflow.put(wfkStageId, doneTasks);
                    }
                    doneTasks.add(taskid);
                }       
            }
        } else {//task is done
            //Set task as done and increment completed tasks
            setTaskStatus(taskid);

            //get id of the workflow.stage that has this task
            String wfkStageId=getWkfStageId(taskid);
            

            List <Integer>doneTasks=doneTasksWorkflow.get(wfkStageId);
            if(doneTasks==null){
                doneTasks=new ArrayList();
                doneTasksWorkflow.put(wfkStageId, doneTasks);
            }
            doneTasks.add(taskid);

            int totalTasks = taskWorkflowStage.get(wfkStageId).size();
            int doneTask = doneTasks.size();

            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Result taskid={0} "
                    + "received {1}/{2} tasks from workflow.stage {3}", new Object[]{taskid, doneTask, totalTasks, wfkStageId});

            System.out.println("CWDEBUG. Result taskid=" + taskid + " received " + doneTask + "/" + totalTasks + " tasks from workflow.stage " + wfkStageId);

            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Taskid={0}. "
                    + "Returned String: {1}, Result Object: {2}", new Object[]{taskid, result[0], result[1].toString()});

            //store the returned object.           
            try {
                String[] wfk_stage = wfkStageId.split("\\.");
                HashMap stagesRes = resultsHash.get(wfk_stage[0]);
                if (stagesRes == null) {
                    stagesRes = new HashMap();
                    List temp = new ArrayList();
                    temp.add(new Results(taskid, result[1]));
                    stagesRes.put(wfk_stage[1], temp);
                    resultsHash.put(wfk_stage[0], stagesRes);
                } else {
                    List res = (List) stagesRes.get(wfk_stage[1]);
                    if (res == null) {
                        List temp = new ArrayList();
                        temp.add(new Results(taskid, result[1]));
                        stagesRes.put(wfk_stage[1], temp);
                    } else {
                        res.add(new Results(taskid, result[1]));
                    }
                }

            } catch (NullPointerException e) {
                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, "ERROR storing result", e);
            }

            //we check all tasks completed before and following non-blocking stages completed.
            //  in this way we can support multiple contiguous non-blocking stages as we can do recursive calls
            if (checkStageDone(wfkStageId)) {//stage DONE
                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Workflow.Stage: {0} DONE", wfkStageId);

                //this is the id that we will use. When non-blocking transitions involved, 
                //  this id is the one of the original stage that started the first non-blocking transition
                String wkfStageId_toOperate;
                Object[] original;
                synchronized (syncActiveNonBlockingStages) {
                    //clean stages' information
                    original = findOriginalStageNonBlocking(wfkStageId);
                }
                if (original == null) {//it has no non-blocking transitions
                    wkfStageId_toOperate = wfkStageId;
                } else {
                    wkfStageId_toOperate = (String) original[0];
                }

                boolean iterate = false;
                if (result.length == 3) {//iterate parameter is optional and it should be in the third position
                    iterate = (Boolean) result[2];
                }

                System.out.println("CWDEBUG. workflowmaster. Iterate? " + iterate);
                String[] managerParts = workflowManager.split(":");
                Socket clientSocket;
                try {
                    clientSocket = new Socket(managerParts[0], Integer.parseInt(managerParts[1]));
                    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                    out.println("stageDone");
                    out.println(wkfStageId_toOperate); //workflowId.stageId
                    out.println(wfkStageId.split("\\.")[1]);//stageId of the latest stage that finished (when non blocking stages). It it is a regular stage then it should be the same as before.
                    out.println(iterate); //true or false.                     
                    out.close();
                } catch (UnknownHostException ex) {
                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                }

                synchronized (syncActiveNonBlockingStages) {
                    //clean all stages involved. (it can be more than one if non-blocking transitions involved)
                    cleanStages(wkfStageId_toOperate);

                    //stop thread if there is no active non-blocking transitions
                    if (activeNonBlockingStages.isEmpty()) {
                        if (collector != null) {
                            collector.setActive(false);
                        }
                    }
                }

            } else {
                boolean isThere;
                synchronized (syncActiveNonBlockingStages) {
                    isThere = activeNonBlockingStages.containsKey(wfkStageId);
                }
                if (isThere) {//if it is "from" in a non-blocking transition, we add it to generate following tasks                        
                    // a monitor will collect them periodically and notify workflow manager
                    synchronized (syncNonBlocking) {//sync with the monitor
                        if (!NBTransitionsWithNewResults.contains(wfkStageId)) {
                            NBTransitionsWithNewResults.add(wfkStageId);
                        }
                    }
                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Workflow.Stage: Stage{0} is \"from\" in a non-blocking transition. Task added for collector ", wfkStageId);
                } else {
                    Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Workflow.Stage: Not done {0}/{1}", new Object[]{doneTask, totalTasks});
                }
            }
        }

        return jobok;
    }

    /**
     * Check if a stage is done when a task does not generate tasks of the
     * following stage. (non-blocking stages) e.g., a task from S1 does not
     * generate new task of S2 (S2 has non-blocking dependency with S1). So we
     * need to check if S2 is done
     */
    public void checkStageDoneNoNewTask(String wfkStageId) {
        System.out.println("CWDEBUG: Check if stage is done when a task from previous stage didn't generate tasks of this stage");
        if (checkStageDone(wfkStageId)) {//stage DONE
            System.out.println("CWDEBUG: Stage is done");

            Logger.getLogger(WorkflowMaster.class.getName()).log(Level.INFO, "Workflow.Stage: {0} DONE", wfkStageId);

            //this is the id that we will use. When non-blocking transitions involved, 
            //  this id is the one of the original stage that started the first non-blocking transition
            String wkfStageId_toOperate;
            Object[] original;
            synchronized (syncActiveNonBlockingStages) {
                //clean stages' information
                original = findOriginalStageNonBlocking(wfkStageId);
            }
            if (original == null) {//it has no non-blocking transitions
                wkfStageId_toOperate = wfkStageId;
            } else {
                wkfStageId_toOperate = (String) original[0];
            }

            String[] managerParts = workflowManager.split(":");
            Socket clientSocket;
            try {
                clientSocket = new Socket(managerParts[0], Integer.parseInt(managerParts[1]));
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                out.println("stageDone");
                out.println(wkfStageId_toOperate); //workflowId.stageId
                out.println(wfkStageId.split("\\.")[1]);//stageId of the latest stage that finished (when non blocking stages). It it is a regular stage then it should be the same as before.
                out.println(false); //true or false.  Note: If this gives problems, then we need to store the iterate value of the last stage of the loop.
                out.close();
            } catch (UnknownHostException ex) {
                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
            }

            synchronized (syncActiveNonBlockingStages) {
                //clean all stages involved. (it can be more than one if non-blocking transitions involved)
                cleanStages(wkfStageId_toOperate);

                //stop thread if there is no active non-blocking transitions
                if (activeNonBlockingStages.isEmpty()) {
                    if (collector != null) {
                        collector.setActive(false);
                    }
                }
            }

        }
    }

    /**
     * Find stage that originated the non-blocking transitions. This is required
     * because scheduling is done by adding tasks to the original one to reuse
     * resources It also returns the % of objective this stage can release. 100%
     * means that previous stages are completed and this stage should be able to
     * generate all its tasks e.g., S1-S2 is non-blocking, S2-S3 is
     * non-blocking, S4-S5 is nonblocking calling with S1, return S1 calling
     * with S2, return S1 calling with S3, return S1 calling with S4, return S4
     * calling with S5, return S4
     *
     * @param wfkStageId
     * @return [<String>OriginalWkflStageId, <double>%tasksReleased]
     */
    protected Object[] findOriginalStageNonBlocking(String wfkStageId) {
        Object[] toReturn;
        Set<String> keys = activeNonBlockingStages.keySet();
        for (String stage : keys) {
            if (activeNonBlockingStages.get(stage).contains(wfkStageId)) {
                toReturn = findOriginalStageNonBlocking(stage);
                if (toReturn == null) {//original
                    toReturn = new Object[2];
                    toReturn[0] = stage;
                    double totalTasks = taskWorkflowStage.get(stage).size();
                    double doneTasks = doneTasksWorkflow.get(stage).size();
                    toReturn[1] = new Double(doneTasks / totalTasks);

                    return toReturn;
                } else {
                    double totalTasks = taskWorkflowStage.get(stage).size();
                    double doneTasks = doneTasksWorkflow.get(stage).size();
                    //calculate the percentage of completed tasks of previous stages to know later how much of the objective can be released
                    toReturn[1] = (((Double) toReturn[1]).doubleValue()) * (doneTasks / totalTasks);

                    return toReturn;
                }
            }
        }
        return null; //case-base. this stage must be the original
    }

    /**
     * Find stage that a task is part of
     *
     * @param taskid
     * @return workflowId.stageId
     */
    public String getWkfStageId(int taskid) {
        String stageIdtoReturn = "";
        Set<String> workflowStages = taskWorkflowStage.keySet();
        for (String stageId : workflowStages) {
            if (taskWorkflowStage.get(stageId).contains(taskid)) {
                stageIdtoReturn = stageId;
                break;
            }
        }
        return stageIdtoReturn;
    }
    
    private void cleanStage(String wfkStageId){
        
        List <Integer>tasksDone=doneTasksWorkflow.remove(wfkStageId);
        if(tasksDone !=null){
            for(int i=0; i<tasksDone.size();i++){
                int taskid=tasksDone.get(i);
                //delete values of that task            
                taskProp.remove(taskid); 
            }        
        }
        //remove values of stage
        taskWorkflowStage.remove(wfkStageId);
        doneTasksWorkflow.remove(wfkStageId);

    }

    /**
     * Remove information of stages. If Non-blocking transition involved, then
     * it removes all stages involved in the non-blocking transitions.
     *
     * @param wfkStageId
     */
    protected void cleanStages(String wfkStageId) {
        cleanStage(wfkStageId);
        if (activeNonBlockingStages.containsKey(wfkStageId)) {
            for (String st : activeNonBlockingStages.get(wfkStageId)) {
                cleanStages(st);
            }
            activeNonBlockingStages.remove(wfkStageId);
        }

    }

    /**
     * Check if a stage is done. A stage is done when all its tasks are
     * completed and If the stage has a non-blocking transition, the stage
     * following the non-blocking transition has to be done
     *
     * if non-blocking transition involved call findOriginalStageNonBlocking to
     * 1. find first stage that was not originated from non-blocking transitions
     * 2. Know if I am 100% completed. If I am completed and I do not have a
     * non-blocking transition after me, and all following Non-blocking stages
     * are done, then original stage is notified as done. 3. We assume that in
     * the workflowManager and AutonomicScheduler will set to done all stages
     * after this one whose transitions are non-blocking
     *
     * @param stageId it is workflowId.stageId
     * @return
     */
    public boolean checkStageDone(String wfkStageId) {
        boolean done = false;
        boolean isfrom;

        synchronized (syncActiveNonBlockingStages) {
            isfrom = activeNonBlockingStages.containsKey(wfkStageId);
        }
        if (isfrom) {//it is from in a non-blocking transition, then it 
            //   cannot be done bc next stages generate tasks after the ones on this stage finish
            done = false;
        } else {
            boolean skip = false;
            synchronized (syncNonBlocking) {
                //to prevent that we have stages waiting to be generated while a task finishes in the last stage and the workflow is considered done
                if (NBStagestoSend.containsKey(wfkStageId) && !NBStagestoSend.get(wfkStageId).isEmpty()) {
                    done = false;
                    skip = true;
                }
            }
            if (!skip) {
                Object[] originalWorkfStage;
                //find origin stage and get the % completed
                synchronized (syncActiveNonBlockingStages) {
                    originalWorkfStage = findOriginalStageNonBlocking(wfkStageId);
                }
                double tasksReleased = 0.0;
                if (originalWorkfStage != null) {
                    tasksReleased = ((Double) originalWorkfStage[1]).doubleValue();
                }
                System.out.println("CWDEBUG. workflowMaster.checkstageDone. tasksReleased? " + tasksReleased);

                if (originalWorkfStage == null) { //regular stage, it is not involved in a non-blocking transition.

                    done = tasksCompleted(wfkStageId);

                } else if (tasksReleased == 1.0) {//all done before this stage and this stage has released all tasks
                    System.out.println("CWDEBUG. workflowMaster.checkstageDone. tasksCompleted stage " + wfkStageId + "=" + tasksReleased);
                    //check if this stage has all its tasks completed            
                    done = tasksCompleted(wfkStageId);
                } else {
                    System.out.println("CWDEBUG. workflowMaster.checkstageDone. tasksCompleted stage " + wfkStageId + "=false");
                }
            }
        }

        System.out.println("CWDEBUG. workflowMaster.checkstageDone. is Done? " + done);
        return done;
    }

    /**
     * Check if this stage has all tasks completed
     *
     * @param wfkStageId
     * @return
     */
    public boolean tasksCompleted(String wfkStageId) {
        int totalTasks = 0;
        List<Integer> taskList = taskWorkflowStage.get(wfkStageId);
        if (taskList != null) {
            totalTasks = taskList.size();
        } else {
            return false;
        }

        int doneTasks = 0;
        List<Integer> donetasksList = doneTasksWorkflow.get(wfkStageId);
        if (donetasksList != null) {
            doneTasks = donetasksList.size();
        }

        if (doneTasks == totalTasks) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check recursively if following Non-blocking stages are done
     *
     * @param wfkStageId
     * @return
     */
    /* NEEDED??public boolean checkFollowingStageDone(String wfkStageId){
     boolean done=true;
     if(activeNonBlockingStages.containsKey(wfkStageId)){          
     for(String st:activeNonBlockingStages.get(wfkStageId)){
     if(tasksCompleted(st)){//if this stage is completed, check if the following are
     done=checkFollowingStageDone(st);
     System.out.println("CWDEBUG. workflowMaster.checkFollowingStageDone. status returned from stage "+st+"="+done);
     if(!done){                        
     break;
     }
     }else{
     done=false;
     break;
     }
     }            
     }else{//there is no non-blocking transitions after this stage
     done=true;
     }
     return done;
     }*/
    //Method override due to the change in the TaskTuple. This method is called inside insertTasks
    @Override
    public XmlTuple createTaskTuple(int taskid) {
        XmlTuple task = new XmlTupleService();
        String taskstr;

        WorkflowTaskTuple tasktuple = new WorkflowTaskTuple();
        List<String> values = taskProp.get(taskid).getTaskTupleValues();

        System.out.println("CWDEBUG.workflowMaster.createTaskTuple. " + values.toString());

        String wfManagerName = values.get(0);
        String workflowID = values.get(1);
        String stageID = values.get(2);
        String appName = values.get(3);
        String requirement = values.get(4);

        taskstr = tasktuple.setTaskStr(taskid, masterNetName, wfManagerName, workflowID, stageID, appName, requirement, String.valueOf(taskid));
        task.createXMLtuple(taskstr);

        return task;
    }

    //Method override due to the change in the TaskTuple
    //this method is to create poison pills to kill workes. I could define value depending of the id
    //if I know that there are certain nodes waiting for a specific value.
    @Override
    public XmlTuple createPoisonTaskTuple(int id) { //NOT USED
        XmlTuple task = new XmlTupleService();
        WorkflowTaskTuple tasktuple = new WorkflowTaskTuple();
        //String value="somevalue";

        String valuepoison;

        //task.createXMLtuple(tasktuple.createPoisonpillTag(masterNetName, id,valuepoison));        
        return task;
    }

    /**
     * Auxiliary method to get done tasks of a single stage
     *
     * @param wkflId
     * @param stageId
     * @return
     */
    private String getDoneTasks(String wkflId, String stageId) {
        List donetasks = new ArrayList();
        //wkfid:{stageid:{results,results ..},...}, where results has taskid and result data
        // HashMap <String, HashMap<String, List <Results>>>resultsHash
        if (resultsHash.containsKey(wkflId)) {
            HashMap<String, List<Results>> stage = resultsHash.get(wkflId);
            if (stage.containsKey(stageId)) {
                for (Results r : stage.get(stageId)) {
                    donetasks.add(r.getTaskId());
                }
            }
        }

        return StringUtils.join(donetasks.toArray(), ",");

    }

    /**
     * Retrieve the ids of the tasks that have been completed. if non-blocking
     * transitions are involve, we return status of following stages too
     *
     * @param wkflId.stageId
     * @return
     */
    public String getAllDoneTasks(String wfkStageId) {
        String toReturn = "";

        String[] partWkfStage = wfkStageId.split("\\.");

        toReturn = getDoneTasks(partWkfStage[0], partWkfStage[1]);

        if (activeNonBlockingStages.containsKey(wfkStageId)) {
            for (String st : activeNonBlockingStages.get(wfkStageId)) {

                toReturn += "," + getAllDoneTasks(st);

            }
        }

        return toReturn;
    }

    private class NonBlockingTransitionMonitor implements Runnable {

        boolean active = true;
        int monitorInterval = 10;//each 10 seconds

        public void run() {

            System.out.println("NonBlockingTransitionMonitor starts");
            Logger.getLogger(NonBlockingTransitionMonitor.class.getName()).log(Level.INFO, "NonBlockingTransitionMonitor starts");

            while (active) {
                try {
                    Thread.sleep(monitorInterval * 1000);
                } catch (InterruptedException ex) {
                    if (active) {
                        Logger.getLogger(NonBlockingTransitionMonitor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                String stringToSend = "";
                synchronized (syncNonBlocking) {
                    //send "to" stages to schedule some tasks
                    for (String st : NBTransitionsWithNewResults) {
                        synchronized (syncActiveNonBlockingStages) {
                            if (activeNonBlockingStages.get(st) != null && !activeNonBlockingStages.get(st).isEmpty()) {
                                String wkfId = st.split("\\.")[0];
                                List<String> tempWkfStages = NBStagestoSend.get(wkfId); //NBStagesToSend={wkfid:{stageid,stageid},...}
                                if (tempWkfStages == null) {
                                    tempWkfStages = new ArrayList();
                                    NBStagestoSend.put(wkfId, tempWkfStages);
                                }
                                for (String stNB : activeNonBlockingStages.get(st)) {
                                    tempWkfStages.add(stNB.split("\\.")[1]);//add stageId
                                }
                            }
                        }
                    }
                    NBTransitionsWithNewResults.clear();
                    //construct the string to send
                    if (!NBStagestoSend.isEmpty()) {
                        for (String st : NBStagestoSend.keySet()) {
                            stringToSend += st + ":" + StringUtils.join(NBStagestoSend.get(st).toArray(), ",") + ";";
                        }
                        NBStagestoSend.clear();
                    }
                }
                //send info to workflowManager
                if (!stringToSend.isEmpty()) {
                    String[] managerParts = workflowManager.split(":");
                    Socket clientSocket;
                    try {
                        clientSocket = new Socket(managerParts[0], Integer.parseInt(managerParts[1]));
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println("scheduleStagesNBT");
                        out.println(stringToSend); //workflowId:stageId,stageId;workflowId:stageId,stageId
                        out.close();
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(WorkflowMaster.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

            System.out.println("NonBlockingTransitionMonitor terminates");
            Logger.getLogger(NonBlockingTransitionMonitor.class.getName()).log(Level.INFO, "NonBlockingTransitionMonitor terminates");
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public boolean getActive() {
            return this.active;
        }
    }

}
