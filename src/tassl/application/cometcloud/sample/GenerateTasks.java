/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.application.cometcloud.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.GenerateTasksAbstract;
import tassl.application.cometcloud.GenerateTasksObject;
import tassl.application.cometcloud.Results;

/**
 * This code is a simple map/reduce application. 
 *  The map is going to generate PR tasks per file found in the input directories.
 *  The reduce will get all results and perform an operation. 
 *  Note: no binaries are needed, just dummy files and the computation is emulated using the sleep function.
 * 
 * 
 * @author Javier Diaz-Montes
 */
public class GenerateTasks extends GenerateTasksAbstract{
    
    public GenerateTasks(){
        super();
    }
    
    //this one is done by the user
    @Override
    public GenerateTasksObject createTasks(String stageId, List<FileProperties> input, FileProperties output, String propertyFileValues, List dependencies, String method){ 
        GenerateTasksObject taskObj=null;
        
        //load properties from the file specified in the workflow XML (it can be different for each stage)
        this.loadProperties(propertyFileValues);
        
        if(method.equals("map")){
            taskObj=map(input,output,propertyFileValues);
        }else if(method.equals("reduce")){
            
            HashMap <String,FileProperties>previousFiles=this.generatePreviousResultFiles(stageId, dependencies);
            
            taskObj=reduce(input,output,propertyFileValues,previousFiles);
            
        }
        
        return taskObj;
        
        
    }
    
    //example where each input file creates a task
    public GenerateTasksObject map(List<FileProperties> input, FileProperties output, String propertyFile){
        //input and output are address://dir. We get all using SSH
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the slowest machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        double minTimeVal=Double.parseDouble(getProperty("minTime"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTime"));
        
        List <List<FileProperties>>inputList=new ArrayList();
        
        
        int PR=Integer.parseInt(getProperty("PR")); //getProperty get values from the property files specified in the workflow
        for(FileProperties inputFP: input){
                        
            String inputS=inputFP.getLocation();            
            String []parts=inputS.split(":");
            String returnSsh=executeSsh(parts[0]," ls -l "+parts[1]+" | awk '{print $5, $9}'");            
            String [] files=returnSsh.split("\n"); //returns size and name 
            
            //lets assume that each file is the input of a task
            for(int i=0;i<files.length;i++){
                if(!files[i].trim().isEmpty()){
                    System.out.println(files[i]);
                    
                    //this loops generate PR*PR tasks for each input file.
                    for (int j=0; j< PR;j++){
                        for (int z=0;z<PR;z++){
                            //here each task only has one input, but the API requries a list
                            List <FileProperties>inputs=new ArrayList();
                            String [] fileParts=files[i].split(" ");//size and name
                            
                            inputs.add(new FileProperties(fileParts[1],inputS,Double.parseDouble(fileParts[0]), inputFP.getZone(), inputFP.getSitename(), inputFP.getConstraints()));
                            
                            double taskDuration=minTimeVal + (Math.random() * (maxTimeVal - minTimeVal));
                            taskParams.add(taskid, Arrays.asList(
                                                         "stage1",              //0                                                        
                                                         output,                //1                                                        
                                                         inputs,                //2
                                                         taskDuration,          //3
                                                         Integer.toString(j+1), //4
                                                         Integer.toString(z+1)  //5
                                            ));
                            taskRequirement.add("large"); //Requirement
                            minTime.add(minTimeVal);
                            maxTime.add(maxTimeVal);
                            System.out.println("TaskId:"+taskid+" Image: "+ files[i]);
                            System.out.println("minTimeVal:"+minTimeVal+" maxTimeVal:"+maxTimeVal);                        
                            taskid++;
                            inputList.add(inputs);
                        }
                    }
                }
            }            
            
        }
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
        
    }
    
    //second stage
    //we aggregate all previous results and produce a single final output.
    public GenerateTasksObject reduce(List<FileProperties> input, FileProperties output, String propertyFile, HashMap <String,FileProperties>previousResults){
        
        int taskid=0;//NOT THE REAL TASKID(this is not used outside here)
        
        List <List<FileProperties>>inputList=new ArrayList();
        List <FileProperties>inputs=new ArrayList();
        
        //create one task that aggregates all previous results
        for (String key:previousResults.keySet()){            
            inputs.add(previousResults.get(key));
        }
        
        inputList.add(inputs);
        
        
        List <List>taskParams=new ArrayList();
        List <String>taskRequirement=new ArrayList<String>();
        //execution time interval for the slowest machine 
        List <Double> minTime= new ArrayList();
        List <Double> maxTime=new ArrayList();
        
        double minTimeVal=Double.parseDouble(getProperty("minTime"));
        double maxTimeVal=Double.parseDouble(getProperty("maxTime"));
        
        double taskDuration=minTimeVal + (Math.random() * (maxTimeVal - minTimeVal));
        taskParams.add(taskid, Arrays.asList(
                                     "stage2",              //0                                     
                                     output,                //1                                                                          
                                     inputs,                //2
                                     taskDuration           //3
                        ));
        taskRequirement.add("large"); //Requirement
        minTime.add(minTimeVal);
        maxTime.add(maxTimeVal);
        System.out.println("TaskId:"+taskid+" Number of Images: "+ inputs.size());
        System.out.println("minTimeVal:"+minTimeVal+" maxTimeVal:"+maxTimeVal);                        
        taskid++;
        inputList.add(inputs);
                
        return new GenerateTasksObject(taskParams,taskRequirement, minTime, maxTime,inputList,null);
    }
    
    public static void main(String []args){
        GenerateTasks gt=new GenerateTasks();
        List<FileProperties> input=new ArrayList();
        input.add(new FileProperties("","jd851@spring.rutgers.edu:/cac/u01/jd851/Molecular3D/spring/init_data/",0,"zoneA","siteSierra",new ArrayList()));
        
        gt.createTasks("S1",input, new FileProperties("","localhost:/tmp/",0,"","",new ArrayList()), "/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/sample.properties",null, "map");
    }
}
