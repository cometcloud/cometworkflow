/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tassl.application.cometcloud;

import tassl.automate.comet.CometConstants;
import tassl.automate.comet.XmlTuple;
import tassl.automate.comet.tuplespace.TSConstant;
import tassl.automate.comet.xmltuplespace.XmlTupleService;
import tassl.automate.programmodel.masterworker.simpleapi.GenericTaskTuple;

/**
 *
 * @author Javi
 */
public class WorkflowTaskTuple extends GenericTaskTuple{
    
    private String wfManagerName;
    private String workflowID;
    private String stageID;
    private String appName;
    private String requirement;
    private String taskidS;
//    private String partitionID;

    public String getWfManagerName() {
        return wfManagerName;
    }

    public String getWorkflowID() {
        return workflowID;
    }

    public String getStageID() {
        return stageID;
    }

    public String getAppName() {
        return appName;
    }

    public String getRequirement() {
        return requirement;
    }

    public String getTaskidS() {
        return taskidS;
    }
//    public String getPartitionID(){
//        return partitionID;
//    }
    
    public String setTaskStr(int taskid, String masterNetName, String wfManagerName, String workflowID, String stageID, 
            String stageAppName, String requirement, String taskidS) {
        String s = new String(); 
        s = "<WorkflowTask>" +
                "<WorkflowManagerName>"+ wfManagerName + "</WorkflowManagerName>" + 
                "<"+CometConstants.MasterNetName+">"+ masterNetName + "</"+CometConstants.MasterNetName+">" + 
                "<"+CometConstants.TaskId+">"+ taskid +"</"+CometConstants.TaskId+">" +                          
                "<WorkflowID>"+workflowID+"</WorkflowID>" +
                "<StageID>"+ stageID +"</StageID>" +
                "<StageAppName>"+ stageAppName +"</StageAppName>" +                                
                "<requirement>"+requirement+"</requirement>"+
                "<taskidS>"+taskidS+"</taskidS>"+
//                "<partitionID>*</partitionID>"+
             "</WorkflowTask>";
        
        this.taskid = taskid;
        this.masterNetName = masterNetName;
        
        this.wfManagerName=wfManagerName;
        this.workflowID=workflowID;
        this.stageID=stageID;
        this.appName=stageAppName;
        this.requirement=requirement;
        this.taskidS=taskidS;
        return s;
    }
    
    /*
     * Returns query to get any task in the space
     */
    @Override
    public XmlTuple getQuery(){
        XmlTuple template = new XmlTupleService();
        String s = new String();
        s = "<WorkflowTask>" +
                "<WorkflowManagerName>*</WorkflowManagerName>" +
                "<"+CometConstants.MasterNetName+">*</"+CometConstants.MasterNetName+">" +    
                "<"+CometConstants.TaskId+">*</"+CometConstants.TaskId+">" +        
                "<"+CometConstants.TaskPriority+">*</"+CometConstants.TaskPriority+">" +      //mandatory tag
                "<WorkflowID>*</WorkflowID>" +
                "<StageID>*</StageID>" +
                "<StageAppName>*</StageAppName>" +                                
                "<requirement>*</requirement>"+
                "<taskidS>*</taskidS>"+
//                "<partitionID>*</partitionID>"+
             "</WorkflowTask>";                
        template.createXMLquery(s);
        return template;
    }
    
     /*
     * Returns query to get only those tasks that match with the specified values
     */    
    public XmlTuple getQuery(String wfManagerName, String workflowID, String stageID, String stageAppName, String requirement){
        XmlTuple template = new XmlTupleService();
        String s = new String();
        s = "<WorkflowTask>" +
                "<WorkflowManagerName>"+ wfManagerName + "</WorkflowManagerName>" +
                "<"+CometConstants.MasterNetName+">*</"+CometConstants.MasterNetName+">" + 
                "<"+CometConstants.TaskId+">*</"+CometConstants.TaskId+">" +               
                "<"+CometConstants.TaskPriority+">*</"+CometConstants.TaskPriority+">" +      //mandatory tag
                "<WorkflowID>"+workflowID+"</WorkflowID>" +
                "<StageID>"+ stageID +"</StageID>" +
                "<StageAppName>"+ stageAppName +"</StageAppName>" +                                
                "<requirement>"+requirement+"</requirement>"+
                "<taskidS>*</taskidS>"+
//                "<partitionID>*</partitionID>"+
             "</WorkflowTask>";
        template.createXMLquery(s);
        return template;
    }
    /*
     * Returns query to get only those tasks that match with the specified values
     */    
    /**
     *
     * @param taskids List of tasksIds that I am interested on. Each taskid is separated by | 
     * @param wfManagerName
     * @param workflowID
     * @param stageID
     * @param stageAppName
     * @param requirement
     * @param partitionID
     * @return
     */
    public XmlTuple getQuery(String taskidS, String wfManagerName, String workflowID, String stageID, String stageAppName, String requirement){
        XmlTuple template = new XmlTupleService();
        String s = new String();
        s = "<WorkflowTask>" +
                "<WorkflowManagerName>"+ wfManagerName + "</WorkflowManagerName>" +
                "<"+CometConstants.MasterNetName+">*</"+CometConstants.MasterNetName+">" + 
                "<"+CometConstants.TaskId+">*</"+CometConstants.TaskId+">" +     
                "<"+CometConstants.TaskPriority+">*</"+CometConstants.TaskPriority+">" +      //mandatory tag
                "<WorkflowID>"+workflowID+"</WorkflowID>" +
                "<StageID>"+ stageID +"</StageID>" +
                "<StageAppName>"+ stageAppName +"</StageAppName>" +                                
                "<requirement>"+requirement+"</requirement>"+
                "<taskidS>"+taskidS+"</taskidS>"+
//                "<partitionID>*</partitionID>"+
             "</WorkflowTask>";
        template.createXMLquery(s);
        return template;
    }
    
    @Override
    public void getTaskTuple(XmlTuple tup){
        String[] s = tup.getkeys();

        //get tags
        for (int i=0;i<s.length;i++){
            if(s[i].equals(CometConstants.TaskId))
                taskid = new Integer(s[i+1]).intValue();
            else  if(s[i].equals(CometConstants.MasterNetName))
                masterNetName = s[i+1];
            else if (s[i].equals("WorkflowManagerName"))
                wfManagerName = s[i+1];
            else if (s[i].equals("WorkflowID"))
                workflowID = s[i+1];
            else if (s[i].equals("StageID"))
                stageID = s[i+1];
            else if (s[i].equals("StageAppName"))
                appName = s[i+1];
            else if (s[i].equals("requirement"))
                requirement = s[i+1];
            else if (s[i].equals("taskidS"))
                taskidS = s[i+1];
//            else if (s[i].equals("partitionID"))
//                partitionID = (String) tup.get("partitionID");
            }  
        //get data
        data = new byte [tup.getData().length];
        data = tup.getData();
    }
    
    public String createPoisonpillTag(String mastername, int poisonId, String wfManagerName, 
            String workflowID, String stageID, String stageAppName, String requirement) {
        String s = new String();
        s = "<WorkflowTask>" +
                "<TaskPriority>"+ TSConstant.HighPriority + "</TaskPriority>" +
                "<WorkflowManagerName>"+ wfManagerName + "</WorkflowManagerName>" +
                "<"+CometConstants.MasterNetName+">"+ masterNetName + "</"+CometConstants.MasterNetName+">" +    
                "<"+CometConstants.TaskId+">"+ taskid +"</"+CometConstants.TaskId+">" +                          
                "<WorkflowID>"+workflowID+"</WorkflowID>" +
                "<StageID>"+ stageID +"</StageID>" +
                "<StageAppName>"+ stageAppName +"</StageAppName>" +                                
                "<requirement>"+requirement+"</requirement>"+
                "<taskidS>*</taskidS>"+
//                "<partitionID>*</partitionID>"+
             "</WorkflowTask>";                
        return s;
    }
}
