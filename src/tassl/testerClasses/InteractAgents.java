/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tassl.testerClasses;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier Diaz-Montes
 */
public class InteractAgents {
    DataOutputStream out=null;
    
    
    public boolean connect(String address, int port){
        boolean status=true;
        Socket clientSocket = null;
        try {
            clientSocket = new Socket(address, port);
            out = new DataOutputStream(clientSocket.getOutputStream());
            
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: "+address+":"+port);
            status=false;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                               + "the connection to: "+address+":"+port);
            status=false;
        }
        return status;
    }
    public void disconnect(){
        try {
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(InteractAgents.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void disableSite(String address, int port){
        connect(address,port);
        try {
            out.writeUTF("DisableSite");
            disconnect();
        } catch (IOException ex) {
            Logger.getLogger(InteractAgents.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void restoreSite(String address, int port){
        connect(address,port);
        try {
            out.writeUTF("RestoreSite");
            disconnect();
        } catch (IOException ex) {
            Logger.getLogger(InteractAgents.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(args.length==3){
            InteractAgents ag=new InteractAgents();
            if(args[0].equals("disable")){
                ag.disableSite(args[1], Integer.parseInt(args[2]));
            }else if(args[0].equals("restore")){
                ag.restoreSite(args[1], Integer.parseInt(args[2]));
            }
            
        }else{
            System.out.println("USAGE: InteractAgents <disable|restore> <address> <port>");
        }
        
       
        
    }
}
