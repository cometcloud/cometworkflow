/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import tassl.application.cometcloud.FileProperties;
import tassl.application.cometcloud.TaskProperties;

/**
 *
 * @author Javier Diaz-Montes
 */
public class WorkflowStage implements Serializable{
    String id;
    String wflId;
    List <String> dependencies;
    HashMap<String, Object> properties; //All properties are Strings except InputData (List<FileProperties>) and Results(FileProperties)
    HashMap<String, String> objectives;
    HashMap<Integer,TaskProperties> taskProp;
    List <Integer> doneTasks;
    List <String>nonBlockingTransitions; //if a stage that depends on this can be started before this one finishes. That stage need to be prepared to allow incremental task generation.
    HashMap<String,String> objectiveAddOnFromNonBlockingT;
    
    //Ending stage that identify loop:Iteration number (to support nested loops)
    HashMap<String,Integer> loopsInvolved;//identify which loops involve this stage. Each one will add _<iter> to the stageId
    
    //NOTE: if we modify the properties of this class, we need to include the changes in the duplicate() method
    
    long startTime;
    long endTime;
    
    /* Not USED. Set to remove in 11/20/2014
    int firstTask;
    int numTasks;
    */
    
    public WorkflowStage(String id){
        this.id=id;
        properties=new HashMap();
        objectives=new HashMap();
        taskProp=new HashMap();
        doneTasks=new ArrayList();
        loopsInvolved=new HashMap();
        startTime=0;
        nonBlockingTransitions=new ArrayList();
        objectiveAddOnFromNonBlockingT=new HashMap();
    }
    
    public void addDonetasks(List <Integer>tasks){
        for(Integer tak:tasks){
            if(!doneTasks.contains(tak)){
                doneTasks.add(tak);
            }
        }
        
    }
    
    /**
     * Value of the Application field in the workflow XML for this stage
     * @return Application name
     */
    public String getApplication(){
        return (String)this.getProperties().get("Application");
    }

    public List<Integer> getDoneTasks() {
        return doneTasks;
    }
    
    /**
     * Update loop or add new one
     * @param id of the loop
     * @param iteration number
     */
    public void updateLoop(String id,int iteration){
        loopsInvolved.put(id, iteration);
    }

    /**
     * Return the index number of the loop identified by idLoop
     * @param idLoop
     * @return
     */
    public int getIterNumLoop(String idLoop){
        int value=-1;
        for(String liS:loopsInvolved.keySet()){
            if(liS.split("_")[0].equals(idLoop.split("_")[0])){//if stageId without loopIndexes are the same
                return loopsInvolved.get(liS);
            }
        }
        return value;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
//    public String getNewId(List<String>loopsOrder){
//        String idnew=id.split("_")[0];
//        for (String s:loopsOrder){
//            if(loopsInvolved.containsKey(s)){
//                idnew+="_"+loopsInvolved.get(s);
//            }
//        }        
//        return idnew;
//    }
    
    /**
     * Generate Stage Id based on participant loops
     *    We assume that loopsOrder is only renamed at the beginning 
     *    of workflow creation
     * @param loopsOrder
     * @return
     */
    public String getNewId(List<String>loopsOrder){
        System.out.println("NewIdStage "+id);
        System.out.println("LoopOrder:"+loopsOrder.toString());
        
        String idnew=id.split("_")[0];
        for (String s:loopsOrder){
            System.out.println("Loop :"+s);
            for(String liS:loopsInvolved.keySet()){
                System.out.println("    "+liS+":"+loopsInvolved.get(liS));
                if(liS.split("_")[0].equals(s.split("_")[0])){//if stageId without loopIndexes are the same
                    System.out.println("    Get THIS");
                    idnew+="_"+loopsInvolved.get(liS);
                    break;
                }
            }
        }    
        System.out.println("Newid is:"+idnew);
        
        return idnew;
    }


    public void setLoopsInvolved(HashMap<String, Integer> loopsInvolved) {
        this.loopsInvolved = loopsInvolved;
    }
    public void renameLoopInvolved(String oldId, String newId){
        int value=loopsInvolved.get(oldId);
        loopsInvolved.put(newId, value);
        loopsInvolved.remove(oldId);
    }
    
    public void setWflId(String id){
        wflId=id;
    }
    
    /* Not USED. Set to remove in 11/20/2014
    public void setTasksInfo(String taskInfo){
        String [] temp=taskInfo.split(",");
        firstTask=Integer.parseInt(temp[0]);
        numTasks=Integer.parseInt(temp[1]);
    }
    */
    
    /**
     * Set the tasks properties. If it is not empty, we add the tasks
     * @param taskProp
     */
    public void setTaskProp(HashMap<Integer, TaskProperties> taskProp) {
        if(this.taskProp.isEmpty()){
            this.taskProp = taskProp;
        }else{
            this.taskProp.putAll(taskProp);
        }
        
    }

    /**
     * Replace existing taskProp
     * @param taskProp
     */
    public void replaceTaskProp(HashMap<Integer, TaskProperties> taskProp){
        this.taskProp=taskProp;
    }
    
    public void setProperties(HashMap sourceInfo) {
        properties = sourceInfo;
    }
    public void setObjectivesInfo(HashMap objectiveInfo) {
        objectives = objectiveInfo;
    }
    public void setDependenciesInfo(List dependenciesInfo){
        dependencies=dependenciesInfo;
    }

    public void setNonBlockingTransitions(List<String> nonBlockingTransitions) {
        this.nonBlockingTransitions = nonBlockingTransitions;
    }
    
    /* Not USED. Set to remove in 11/20/2014
    public int getFirstTask() {
        return firstTask;
    }*/

    public HashMap<String, Integer> getLoopsInvolved() {
        return loopsInvolved;
    }


    public String getWflId() {
        return wflId;
    }

    /**
     * Time in milliseconds
     * @return
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Time in milliseconds
     * @return
     */
    public long getEndTime() {
        return endTime;
    }
    
    /* Not USED. Set to remove in 11/20/2014
    public int getNumTasks() {
        return numTasks;
    }*/
    
    public HashMap<String, Object> getProperties(){
        return properties;
    }
    
    
    /**
     * Return the objective type of this stage.
     * We are assuming that we can only have one at this moment.
     * 
     * @return
     */
    public String getObjectiveType(){
        List it=new ArrayList(objectives.keySet());
        String objectiveType=(String)it.get(0);
        return objectiveType;
    }
    
    /**
     * Get the objective value. The parameter type should not be needed as there is only one, but 
     * in this way it is ready for future if we allow multiple objectives. If there is non-blocking
     * transitions, then it will return the aggregated of all scheduled stages
     * @param objectiveType
     * @return
     */
    public double getObjectiveValue(String objectiveType){
        
        double originalValue=Double.parseDouble(objectives.get(objectiveType));
        double finalValue=originalValue;
        
        if(!objectiveAddOnFromNonBlockingT.isEmpty()){
            for(String val:objectiveAddOnFromNonBlockingT.values()){
                if(!val.trim().isEmpty()){
                    finalValue+=Double.parseDouble(val.trim());
                }
            }
        }
        
        return finalValue;
    }
    
    /**
     * Get the original objective value. This is only useful if using non-blocking stages and
     * you want to know what is the original value, instead of the aggregated. For example,
     * this can be used if you select a limit in the number of machines you want to use
     * @param objectiveType
     * @return
     */
    public double getObjectiveValueOriginal(String objectiveType){        
        return Double.parseDouble(objectives.get(objectiveType));        
    }
    
    public List<String> getDependencies(){
        return dependencies;
    }

    public List<String> getNonBlockingTransitions() {
        return nonBlockingTransitions;
    }
    
    /**
     * Add part of the objective of another stage to this one.
     * This is used because we schedule stages within non-blocking transitions
     * all together.
     * 
     * @param stageId
     * @param value
     */
    public void addObjectiveAddOnFromNonBlockingT(String stageId,String value){
        objectiveAddOnFromNonBlockingT.put(stageId, value);
    }

    /**
     * Get objectives to be added due to Non-blocking transitions
     * @return
     */
    public HashMap<String, String> getObjectiveAddOnFromNonBlockingT() {
        return objectiveAddOnFromNonBlockingT;
    }

    public void setObjectiveAddOnFromNonBlockingT(HashMap<String, String> objectiveAddOnFromNonBlockingT) {
        this.objectiveAddOnFromNonBlockingT = objectiveAddOnFromNonBlockingT;
    }
    
    
    
    public String getId(){
        return id;
    }

    /**
     * Return the properties of those tasks that need to be scheduled.
     * 
     * @return 
     */
    public HashMap<Integer, TaskProperties> getTaskProp() {
        HashMap<Integer,TaskProperties> schedTaskProp;
        
        schedTaskProp=(HashMap)taskProp.clone();
        
        //remove done tasks to return only the pending ones.
        for(int taskid:doneTasks){
            schedTaskProp.remove(taskid);
        }
        
        return schedTaskProp;
    }
    
    /**
     * Return the properties of all tasks composing a stage
     * @return
     */
    public HashMap<Integer,TaskProperties> getAllTaskProp(){
        return taskProp;
    }
    
    public void printWorkflowStage() {
        System.out.println("stageID= "+id);
        System.out.println("Stage ");
        for(String prop:properties.keySet()){
            System.out.println("  "+prop+":"+properties.get(prop));
        }
        System.out.println("Objectives ");
        for(String prop:objectives.keySet()){
            System.out.println("  "+prop+":"+objectives.get(prop));
        }
        System.out.println("Dependencies ");
        System.out.println(Arrays.toString(dependencies.toArray()));                    
    }
    public String toString(){
        return id;
    }
    
    public void setStartTime() {
        if(this.startTime==0){
            this.startTime = System.currentTimeMillis();
        }
    }

    public void setEndTime() {
        this.endTime = System.currentTimeMillis();
    }
    
    /**
     * Duration of the stage since it was ready to execute until it was completed
     * @return time in milliseconds
     */
    public long getDurationStage(){
        return this.endTime-this.startTime;
    }
    
    
    /**
     * Return a new Object that is a deep copy of the current one
     *  This is used to avoid clone.
     * @return
     */
    public WorkflowStage duplicate(){        
        //assuming that clone is broken and doing everything step by step

        WorkflowStage wfNew=new WorkflowStage(this.id);
        wfNew.setWflId(this.getWflId());

        //get dependencies
        List <String>dependenciesNew=new ArrayList();
        for(String s:this.getDependencies()){
            dependenciesNew.add(s);
        }
        wfNew.setDependenciesInfo(dependenciesNew);

        //get properties
        HashMap<String, Object> propertiesNew=new HashMap(); //All properties are Strings except InputData (List<FileProperties>) and Results(FileProperties)
        
        for(String s:this.getProperties().keySet()){
            if(s.equals("InputData")){
                List<FileProperties> fpLNew=new ArrayList();
                for(FileProperties fpL:(List<FileProperties>)this.getProperties().get(s)){                
                    fpLNew.add(fpL.duplicate());
                }                
                propertiesNew.put(s, fpLNew);
            }else if(s.equals("Results")){
                propertiesNew.put(s,((FileProperties)this.getProperties().get(s)).duplicate());
            }else{
                propertiesNew.put(s, this.getProperties().get(s));
            }
        }
        wfNew.setProperties(propertiesNew);
        
        //get objectives
        
        HashMap<String,String> objectivesNew=new HashMap();
        for(String s:this.objectives.keySet()){
            objectivesNew.put(s, this.objectives.get(s));
        }
        wfNew.setObjectivesInfo(objectivesNew);

        //get loopsinvolved
        
        HashMap<String,Integer> loopsInvolvedNew=new HashMap();
        for(String s:this.getLoopsInvolved().keySet()){
            loopsInvolvedNew.put(s, this.getLoopsInvolved().get(s));
        }
        wfNew.setLoopsInvolved(loopsInvolvedNew);


        //non-blocking transitions
        List <String>nonBlockingTransitionsNew=new ArrayList();
        if(!nonBlockingTransitions.isEmpty()){
            for(String s:nonBlockingTransitions){
                nonBlockingTransitionsNew.add(s);
            }            
        }        
        wfNew.setNonBlockingTransitions(nonBlockingTransitionsNew);
                
        return wfNew;
    }
    
    
}
