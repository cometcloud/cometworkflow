/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.workflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import programming5.io.Debug;
import tassl.application.cometcloud.FileProperties;
/**
 *
 * @author Javier Diaz-Montes, hyunjoo
 */
public class WorkflowXMLParser {
    public static Workflow XMLParser(String xmlString){        
        Workflow wf=null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document doc;
        try {
            db = dbf.newDocumentBuilder();            
            doc = db.parse(new InputSource(new StringReader(xmlString)));
            doc.getDocumentElement().normalize();
            wf=docToWorkflow(doc);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);            
            ex.printStackTrace();        
        } catch (SAXException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();        
        } catch (IOException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();        
        }        
        return wf;        
    }
    
    public static Workflow XMLParser(File xmlFile){        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document doc;
        Workflow wf=null;
        try {
            db = dbf.newDocumentBuilder();
            doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            wf=docToWorkflow(doc);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);            
            ex.printStackTrace();        
        } catch (SAXException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();        
        } catch (IOException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();        
        }         
        return wf;
    }
    
    public static Workflow docToWorkflow(Document doc){
        
        HashMap <String, HashMap<String, Object>>stages=new HashMap<String, HashMap<String, Object>>();
        HashMap <String, HashMap<String, String>>objectives=new HashMap<String, HashMap<String, String>>();
        HashMap<String, List<String>> dependencies = new HashMap<String, List<String>>();
        HashMap<String, List<String>> forwardList = new HashMap<String, List<String>>();
        HashMap<String, List<String>> loops = new HashMap<String, List<String>>();
        List<String> loopsOrder=new ArrayList(); //to keep order of loops when renaming stages
        
        HashMap<String, List<String>> nonBlockingTransitions = new HashMap<String, List<String>>(); //nonblocking transitions
        
        HashMap<String, WorkflowStage> wflStages=new HashMap<String, WorkflowStage>(); 
        
        //Debug.enable("WorkflowXMLParser");//CWDEBUG
        
        Debug.println("stages", "WorkflowXMLParser");
        //get stages information
        NodeList stageList = doc.getElementsByTagName("stage");
        for (int i=0; i<stageList.getLength(); i++) {
            Element el = (Element)stageList.item(i);
            String id = el.getAttribute("id");
            if (!(id.equals("StartNode")||id.equals("EndNode"))) {
                HashMap stage = stages.get(id);
                if (stage == null) {
                    stage = new HashMap();
                }
                
                if (el.getAttribute("type").equals("InputData")){
                    List datasets=new ArrayList();
                    NodeList childs=el.getChildNodes();
                    for(int j=0;j<childs.getLength();j++){
                        if(childs.item(j).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE){
                            Element elt = (Element)childs.item(j);            
                            List constraints=new ArrayList();
                            if(!elt.getAttribute("constraint").trim().isEmpty()){
                                constraints.addAll(Arrays.asList(elt.getAttribute("constraint").split(",")));
                            }
                            String valuetemp=elt.getAttribute("value");
                            String filename="";
                            if(!valuetemp.endsWith("/")){
                                String []tempparts=valuetemp.split("/");
                                filename=tempparts[tempparts.length-1];
                                valuetemp="";
                                for(int it=0;it<tempparts.length-1;it++){
                                    valuetemp+=tempparts[it]+"/";
                                }                                
                            }                            
                            FileProperties fp=new FileProperties(filename,valuetemp,(double)0, elt.getAttribute("zone"),
                                    elt.getAttribute("site"), constraints);
                            datasets.add(fp);
                        }                        
                    } 
                    //CREATE LIST OF FILE PROPERTIES
                    stage.put(el.getAttribute("type"), datasets);
                    
                }else if (el.getAttribute("type").equals("Results")){                    
                    List constraints=new ArrayList();
                    if(!el.getAttribute("constraint").trim().isEmpty()){
                        constraints.addAll(Arrays.asList(el.getAttribute("constraint").split(",")));
                    }
                    //CREATE FILE PROPERTIES
                    FileProperties fp=new FileProperties("",el.getAttribute("value"),(double)0, el.getAttribute("zone"),
                            el.getAttribute("site"), constraints);
                    stage.put(el.getAttribute("type"), fp);               
                    
                }else if (el.getAttribute("type").equals("AppGenerateClass")){
                    String parameter="";
                    if(!el.getAttribute("value").trim().isEmpty()){
                        parameter=el.getAttribute("value").trim();
                        if(!el.getAttribute("method").trim().isEmpty()){
                            parameter+=":"+el.getAttribute("method").trim();
                            stage.put(el.getAttribute("type"), parameter);
                            
                        }else{
                            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, "ERROR: AppGenerateClass has to "
                                    + "have a \"value\" attribute with the classpath of the class that generates tasks and a \"method\" with the"
                                    + "method to use inside of that class");
                            return null;
                        }
                    }else{
                        Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, "ERROR: AppGenerateClass has to "
                                    + "have a \"value\" attribute with the classpath of the class that generates tasks and a \"method\" with the"
                                    + "method to use inside of that class");
                        return null;
                    }
                    
                
                }else{                
                    stage.put(el.getAttribute("type"), el.getAttribute("value"));
                }                
                stages.put(id, stage);
                
                Debug.println("  "+el.getAttribute("id")+" "+el.getAttribute("type")+","+el.getAttribute("value"), "WorkflowXMLParser");
            }
        }

        //get objective information
        Debug.println("objectives", "WorkflowXMLParser");
        NodeList objLst = doc.getElementsByTagName("objective");
        for (int i=0; i<objLst.getLength(); i++) {
            Element el = (Element)objLst.item(i);
            String id = el.getAttribute("id");
            if (!(id.equals("StartNode")||id.equals("EndNode"))) {
                HashMap objective = objectives.get(id);
                if (objective == null) {
                    objective = new HashMap();
                }
                objective.put(el.getAttribute("type"), el.getAttribute("value"));
                objectives.put(id, objective);
                Debug.println("  "+el.getAttribute("id")+" "+el.getAttribute("type")+","+el.getAttribute("value"), "WorkflowXMLParser");
            }
        }

        //transform transitions into dependencies
        Debug.println("transitions", "WorkflowXMLParser");
        //each workflowStage has a list of dependencies (stages that need to finish before this one)
        //the workflow has the same information but in a forward List (i.e., what stages depend on the current stage)- this was needed for the loop support.
        NodeList transLst = doc.getElementsByTagName("transition");
        for (int i=0; i<transLst.getLength(); i++) {
            Element el = (Element)transLst.item(i);
            String key = el.getAttribute("from");
            String value = el.getAttribute("to");
            String blockingStr=el.getAttribute("blocking"); //Optional attribute. By default is true. false means that the following stage can ejecute tasks
            //if a stage that depends on this one can be started before this one finishes. That stage need to be prepared to allow incremental task generation.
            if(!blockingStr.isEmpty() && blockingStr.equalsIgnoreCase("false")){//only add non-blocking transitions
                List nextNonBlock=nonBlockingTransitions.get(key);
                if(nextNonBlock==null){
                    nextNonBlock=new ArrayList<String>();
                }
                nextNonBlock.add(value);
                nonBlockingTransitions.put(key, nextNonBlock);
                if(nextNonBlock.size()>1){
                    System.out.println("WARNING: Currently a non-blocking transition can only have one "
                            + "destination (e.g.,you cannot have non-blocking in S1->S2 and S1->S3). "
                            + "Your workflow may not be executed properly.");
                }
            }            
            
            //TODO: Check that stages invovled in non-blocking transitions have the same objective
            
            Debug.println("  from "+key+" to "+value + " - "+value+" has to wait for "+key+ " to be completed.", "WorkflowXMLParser");                
            List vec = dependencies.get(value);
            if (vec == null) {
                vec = new ArrayList<String>();
            }
            vec.add(key);
            dependencies.put(value, vec);
            
            List nextVec = forwardList.get(key);
            if (nextVec == null) {
                nextVec = new ArrayList<String>();
            }
            nextVec.add(value);
            
            //forward list for Loops
            forwardList.put(key,nextVec);
            
        }
                
        //identify loops and organize them in hashtable by last stage of iteration
        Debug.println("loops", "WorkflowXMLParser");
        NodeList loop = doc.getElementsByTagName("loop");
        for (int i=0; i<loop.getLength(); i++) {
            Element el = (Element)loop.item(i);
            String key = el.getAttribute("from");
            String value = el.getAttribute("to");
            Debug.println("  Loop from "+key+" to "+value, "WorkflowXMLParser");                
            //create a list
            List stagesLoop=findLoopPath(key,value,forwardList);
            
            //store that list by last stage
            loops.put(value, stagesLoop);
            loopsOrder.add(value);
        }
                       
        
         //Generate stages                  
        for (String id:stages.keySet()){   
            WorkflowStage temp=new WorkflowStage(id);                
            if (stages.get(id)!=null && !stages.get(id).isEmpty()) {
                temp.setProperties(stages.get(id));
            }
            if (objectives.get(id)!=null && !objectives.get(id).isEmpty()) {
                temp.setObjectivesInfo(objectives.get(id));
            }else{
                HashMap <String, String> tmp = new HashMap<String, String>();
                tmp.put("MinRunningTime", "0"); //by default Minimum Completion Time scheduling.
                temp.setObjectivesInfo(tmp);
            }
            if (dependencies.get(id)!=null && !dependencies.get(id).isEmpty()) {
                temp.setDependenciesInfo(dependencies.get(id));
            }else{
                temp.setDependenciesInfo(new ArrayList());
            }
            if (nonBlockingTransitions.get(id)!=null && !nonBlockingTransitions.get(id).isEmpty()) {
                temp.setNonBlockingTransitions(nonBlockingTransitions.get(id));
            }else{
                temp.setNonBlockingTransitions(new ArrayList());
            }
            
            for(String s:loops.keySet()){
                if(loops.get(s).contains(id)){
                    temp.updateLoop(s, 0);
                }
            }
            
            wflStages.put(id,temp);
        }
        
        
        
        Workflow wf = null;
        try{
            wf=new Workflow(wflStages);
            wf.setForwardList(forwardList);
            wf.setLoops(loops);
            wf.setLoopsOrder(loopsOrder);            
            
            wf.renameStages(wflStages, loops, false);
                        
            wf.setNestedLoops(createNestedLoopInfo(wf.getLoops()));
            
            System.out.println("Workflow: "+ wf.toString());
            
            for(String wfs:wflStages.keySet()){
                System.out.println("Stage="+wfs);
                System.out.println("   Dependencies="+wflStages.get(wfs).getDependencies().toString());
                
            }
            
            for(String sl:wf.getLoops().keySet()){
                System.out.println("loop "+sl);
                System.out.println("    "+wf.getLoops().get(sl));
                System.out.println("Loops involved "+wflStages.get(sl).getLoopsInvolved().toString());
            }
            
            for(String nl:wf.getNestedLoops().keySet()){
                System.out.println("Outer loop "+nl);
                System.out.println("    "+wf.getNestedLoops().get(nl).toString());
            }
            
            System.out.println("loopOrder: "+wf.getLoopsOrder().toString());
            
        }catch(IllegalArgumentException e){
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, e); 
        }
               
        
        return wf;
    } 
    
    public static HashMap<String, List<String>> createNestedLoopInfo(HashMap<String, List<String>> loops){
        //Identify nested loops and create a hashmap {loopid:{loopid_nested, loopid_nested},loopid:{loopid_nested, loopid_nested}}
        // what if there are more than one level?? - we add it as nested loop too. In this way all of them are reseted without recursion.
        // a loop A is inside of a loop B if all stages of A are in the path of loop B
        
        HashMap<String, List<String>> nestedLoops = new HashMap<String, List<String>>(); //identify loops inside loops {loopid:{loopid_nested, loopid_nested},loopid:{loopid_nested, loopid_nested}}
        
        for(String loopIdOuter:loops.keySet()){
            for(String loopIdInner:loops.keySet()){
                List<String>innerMayBeOuter=nestedLoops.get(loopIdInner);
                //if it is different loop && the loopIdInner is not outer loop of loopIdOuter
                if(!loopIdOuter.equals(loopIdInner) && (innerMayBeOuter==null || !innerMayBeOuter.contains(loopIdOuter))){
                    if(checkIfInnerLoop(loopIdInner,loopIdOuter, loops)){
                        List<String> innerLoops=nestedLoops.get(loopIdOuter);
                        if(innerLoops==null){
                            innerLoops=new ArrayList();
                            innerLoops.add(loopIdInner);
                            nestedLoops.put(loopIdOuter, innerLoops);
                        }else{
                            innerLoops.add(loopIdInner);
                        }
                    }
                }
            }
        }
        return nestedLoops;
    }
    
    /**
     * Check if a loop is inside another. Loop A (loopIdInner) is inside 
     *       of a loop B (loopIdOuter) if all stages of A are in the path of loop B
     * @param loopIdInner
     * @param loopIdOuter
     * @param loops
     * @return
     */
    public static boolean checkIfInnerLoop(String loopIdInner, String loopIdOuter, HashMap<String, List<String>> loops){
                
        List<String> stagesOuter=loops.get(loopIdOuter);
        List<String> stagesInner=loops.get(loopIdInner);
        
        return stagesOuter.containsAll(stagesInner);
    }
    
    public static List<String> findLoopPath(String from, String to, HashMap<String, List<String>> forwardList){
        List <String> stages=new ArrayList(); //we add the visited ones to our solution
        List <String> toExplore=new ArrayList();
        List <String> candidate=new ArrayList();
        System.out.println("CWDEBUG. findLoopPath Start");
        toExplore.add(from);
        while (!toExplore.isEmpty()){
            String obj=toExplore.remove(0);
            System.out.println(obj);
            if(obj.equals(to)){
                //we only add it
                stages.add(obj);
            }else{
                if(obj.equals(from)){
                    stages.add(obj);
                }else{
                    candidate.add(obj);//it is a candidate, we need to make sure this is in the loop path
                }
                List <String> tempList=forwardList.get(obj);
                if(tempList!=null){
                    for(String s:tempList){
                        System.out.println("  "+s);                    
                        if(!toExplore.contains(s) && !stages.contains(s)){
                            toExplore.add(s);
                        }
                        if(s.equals(to) && !stages.contains(s)){//if following stage is "to" then it is in the path for sure        
                            if(!stages.contains(obj)){
                                stages.add(obj);
                            }
                            candidate.remove(obj);
                        }
                    }                
                }else{
                    System.out.println("CWDEBUG. findLoopPath: forward list empy");
                }
            }
        }
        //check if candidate follower is in stages. If so, include. Do, while no new stages can be included
        boolean end;
        do{
            end=true;
            for(int i=candidate.size()-1;i>=0;i--){
                String candStage=candidate.get(i);
                System.out.println("Candidate  "+candStage);
                List <String> tempList=forwardList.get(candStage);
                if(tempList!=null){
                    for(String s:tempList){
                        System.out.println("  follower "+s);
                        if(stages.contains(s)){//if this candidate has as follower someone in the path, then it is in the path
                            if(!stages.contains(candStage)){
                                stages.add(candStage);
                            }
                            candidate.remove(i);
                            end=false;
                        }
                    }
                }
            }
        }while(!end);
        
        System.out.println("CWDEBUG. findLoopPath: "+stages.toString());
        return stages;
    }
    
    public static void main(String[] args) {
        Debug.enable("WorkflowXMLParser");
//        File xmlFile = new File("/home/javi/cometcloud/WithNewAPI/cometWorkflow/workflowSample");
//        Workflow wf = WorkflowXMLParser.XMLParser(xmlFile);
//        
//        System.out.println(wf.getExecutableStages());
//        wf.setStageDone("1");
//        System.out.println(wf.getExecutableStages());
//        wf.setStageDone("2");
//        System.out.println(wf.getExecutableStages());
//        wf.setStageDone("3");
//        System.out.println(wf.getExecutableStages());
//        wf.setStageDone("4");
//        System.out.println(wf.getExecutableStages());
//        wf.setStageDone("5");
//        System.out.println(wf.getExecutableStages());
        
        Workflow wf;
        BufferedReader br;
        String temp;
        String xmlstring="";
        try {
            br = new BufferedReader(new FileReader("/home/javi/cometcloud/WithNewAPI/cometWorkflow/simple_run/workflow/workflowSample.xml"));
            while ((temp = br.readLine()) != null) {
                xmlstring+=temp;
            }
            br.close();
            
            wf = WorkflowXMLParser.XMLParser(xmlstring);
        
            System.out.println(wf.getExecutableStages());
            wf.setStageDone("1");
            System.out.println(wf.getExecutableStages());
            wf.setStageDone("2");

            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(WorkflowXMLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
