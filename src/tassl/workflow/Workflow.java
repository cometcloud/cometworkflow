/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import tassl.application.cometcloud.FileProperties;

/**
 *
 * @author Javier Diaz-Montes
 */
public class Workflow {
    private String id="";
    private HashMap <String, WorkflowStage> workflowStages; //id:stage
    private List <String> completedStages; //id
    private List <String> scheduledStages; //id

    private HashMap<String, List<String>> forwardList; //id:{id,id,id},...
    private HashMap<String, List<String>> originalFollowStagesofLoop;// to know how to reconnect when nested loops
    private HashMap<String, List<String>> loops; //lastStageId:{id,id,id},...
    private HashMap<String, List<String>> loopsOrigName; //since loops is being renamed we need to have this to reset when nested loops..
    private List<String> loopsOrder; //to keep order of loops when renaming stages
    private HashMap<String, List<String>> nestedLoops; //identify loops inside loops {loopid:{loopid_nested, loopid_nested},loopid:{loopid_nested, loopid_nested}}
    
    private boolean workflowCanceled;
    
    public Workflow(HashMap <String, WorkflowStage> stages) throws IllegalArgumentException {
        IllegalArgumentException tooFewArguments = new IllegalArgumentException("Cannot create workflow: Workflow must have at least one stage");
        if (stages == null || stages.size()<=0) {            
            throw tooFewArguments;
        }else{
            workflowStages=stages;            
        }
        completedStages=new ArrayList();
        scheduledStages=new ArrayList();
        forwardList = new HashMap<String, List<String>>();
        loops = new HashMap<String, List<String>>();
        loopsOrigName = new HashMap<String, List<String>>();
        loopsOrder=new ArrayList();
        originalFollowStagesofLoop=new HashMap();
        nestedLoops = new HashMap<String, List<String>>();
        workflowCanceled=false;
    }
    
    /**
     * Set Workflow id and propagate it to the stages
     * @param id
     */
    public void setId(String id){
        this.id=id;
        for (String idS:workflowStages.keySet()){            
            WorkflowStage temp=workflowStages.get(idS);
            temp.setWflId(id);
        }        
        
    }
    
    public void cancelWorkflow(){
        workflowCanceled=true;
    }
    
    /**
     * Return true if workflow is canceled and false otherwise
     * @return
     */
    public boolean isWorkflowCanceled(){
        return workflowCanceled;
    }
    
    public void setForwardList(HashMap<String, List<String>> forwardList) {
        this.forwardList = forwardList;
    }

    public void setLoops(HashMap<String, List<String>> loops) {
        this.loops = loops;
    }
    public void setLoopsOrder(List<String> loopsOrder) {
        this.loopsOrder = loopsOrder;
    }

    public void setNestedLoops(HashMap<String, List<String>> nestedLoops) {
        this.nestedLoops = nestedLoops;
    }

    public HashMap<String, List<String>> getLoopsOrigName() {
        return loopsOrigName;
    }

    public HashMap<String, List<String>> getNestedLoops() {
        return nestedLoops;
    }
    
    public List<String> getLoopsOrder() {
        return loopsOrder;
    }
    
    public String getId(){
        return id;
    }
    public void setStageDone(String stageId){
        if(!completedStages.contains(stageId)){
            completedStages.add(stageId);
        }
        scheduledStages.remove(stageId);
    }
    
    /**
     * Check if stage id is the end of a loop
     * @param stageId
     * @return
     */
    public boolean isEndLoop(String stageId){
        return loops.containsKey(stageId);
    }

    public HashMap<String, List<String>> getLoops() {
        return loops;
    }
    
    public boolean isDone(){
        return workflowStages.size()==completedStages.size();
    }
          
    public WorkflowStage getStage(String stageId){
        return workflowStages.get(stageId);
    }
    public int getNumStages(){
        return workflowStages.size();
    }
    public int getNumCompletedStages(){
        return completedStages.size();
    }
    public List<String> getCompletedStages(){
        return completedStages;
    }
    // get stages that can be executed (dependencies are completed)
    public List <WorkflowStage> getExecutableStages(){
        List <WorkflowStage> ready=new ArrayList();
        
        for (String id:workflowStages.keySet()){
            if (!completedStages.contains(id)){
                boolean unresolved=false;
                WorkflowStage temp=workflowStages.get(id);
                for (String depId:temp.getDependencies()){
                    if (!completedStages.contains(depId)){
                        unresolved=true;
                        break;
                    }
                }
                if (!unresolved && !scheduledStages.contains(temp.getId())){
                    ready.add(temp);
                    scheduledStages.add(temp.getId());
                }
            }
        }                
        return ready;
    }   

    public String getResultList(){
        String resultList="";
        for(String stageId:workflowStages.keySet()){
            FileProperties fp=(FileProperties)workflowStages.get(stageId).getProperties().get("Results");
            String location=fp.getLocation();
            if(!location.trim().isEmpty()){
                if(location.contains("@")){
                    location=location.split("@")[1];
                }
                resultList+=location+";";
            }
        }
        
        return resultList;
        
    }
    
    /**
     * Get stages that compose the workflow
     * @return Stages
     */
    public HashMap<String, WorkflowStage> getWorkflowStages() {
        return workflowStages;
    }
    
    
    /**
     * Add stages to the workflow
     * @param stages
     * @return true or false. False status is reached when there are some stage with the same id of existing ones.
     */
    public boolean addStages(HashMap <String, WorkflowStage> stages){
        boolean success=true;
        //check if stages can be added without overwriting previous info.
        //we add them all or non
        for(String stageId:stages.keySet()){
            if(workflowStages.containsKey(stageId)){
                success=false;
                break;
            }
        }
        if(success){
            //add all stages
            workflowStages.putAll(stages);
        }
        
        return success;
    }
    
    /**
     * Create new iteration of a loop
     * @param stageIdendLoop  Identifies the loop, but the name of the last stage of last iteration until now
     */
    public void newIteration(String stageIdendLoop){ 
        
        //if this is an outer loop then
        //TODO: call prepareForOuterLoop for each inner loop        
        for(String nestedLoopStages:this.nestedLoops.keySet()){
            if(nestedLoopStages.split("_")[0].equals(stageIdendLoop.split("_")[0])){
                System.out.println("New Iter of "+stageIdendLoop+"; nested info is "+nestedLoopStages+": "+nestedLoops.get(nestedLoopStages).toString());
                for(String stageToPrepare:nestedLoops.get(nestedLoopStages)){
                    prepareForOuterLoop(stageToPrepare);
                }
            }               
        }
        
        System.out.println(workflowStages.get(stageIdendLoop).getLoopsInvolved().toString());
        int nextIter=(workflowStages.get(stageIdendLoop).getIterNumLoop(stageIdendLoop)+1);
        System.out.println("Next Iter id of "+stageIdendLoop+" is "+nextIter);
        List<String> stagesToDuplicate=loops.get(stageIdendLoop);
        HashMap<String,String> oldnewStageId=new HashMap();
        String firstStageLoop=stagesToDuplicate.get(0);
        
        List<String> forwardListForNewLastStage=forwardList.get(stageIdendLoop);
        
        HashMap <String, WorkflowStage> workflowStagesToAdd=new HashMap();
        System.out.println("Workflow.newIteration. StageToDuplicate:"+stagesToDuplicate.toString());
        System.out.println("Workflow.newIteration. StageToDuplicate firstStageLoop:"+firstStageLoop);
        for(String s:stagesToDuplicate){            
            WorkflowStage wfStageNew=(workflowStages.get(s)).duplicate();
            wfStageNew.updateLoop(stageIdendLoop, nextIter);//always stageId bc identifies loop
            String newId=wfStageNew.getNewId(this.getLoopsOrder());//get newId, but do not set yet. It is done in renameStages
            System.out.println("Workflow.newIteration. SToDupl:"+s);
            
            workflowStagesToAdd.put(s, wfStageNew);                        
        }
        
        HashMap<String, List<String>> loopToUpdate=new HashMap();
        loopToUpdate.put(stageIdendLoop, stagesToDuplicate);
        
        //rename all newly created stages
        //forwardList need to add all new stages
        //add stages to workflow and link all
        renameStages(workflowStagesToAdd,loopToUpdate, true);
        
    }
    
    
    
    /**
     * Rename all newly created stages,
     *  create forwardList for new stages
     *  add stages to workflow and link all
     * 
     * @param wflStages
     * @param loopsToUpdate
     * @param newIteration
     */
    public void renameStages(HashMap <String, WorkflowStage> wflStages, HashMap<String, List<String>> loopsToUpdate, boolean newIteration){
        //Rename stages with loops as well as its dependencies
        List <String> visited=new ArrayList();//to only rename once
        HashMap<String,String> oldnewNames=new HashMap();
        HashMap <String,List<String>> loopsNew=new HashMap();
        for(String s:loopsToUpdate.keySet()){//for each loop (s is stageId that identifies loop)
            System.out.println("Loop "+s);
            List<String> tempLoop=loopsToUpdate.get(s);       
            String firstStageLoop=tempLoop.get(0);
            List<String> newListLoop=new ArrayList();
            for(String st:tempLoop){//for each stage in a loop                     
                if(!visited.contains(st)){//if not already renamed
                    System.out.println("   oldName: "+st);
                    visited.add(st);
                    WorkflowStage stageTemp=wflStages.get(st);
                    String newId=stageTemp.getNewId(loopsOrder);
                    stageTemp.setId(newId);
                    oldnewNames.put(st, newId);
                    List<String> stagesAfter=forwardList.get(st);
                    
                    if(stagesAfter!=null){                        
                        for(String sFL:stagesAfter){//for each stage following a stage in a loop
                            String stagesFL=sFL;
                            if(oldnewNames.containsKey(sFL)){
                                stagesFL=oldnewNames.get(sFL);
                            }
                            System.out.println("   sFL "+stagesFL);
                            WorkflowStage tempStage=wflStages.get(stagesFL);
                            if(tempStage!=null){
                                List depend=tempStage.getDependencies();
                                depend.remove(st);
                                depend.add(newId);      
                            }else{// stage not in the loop path
                                if(!st.equals(firstStageLoop)){//if it is not the first stage, it will only be the last one. So we update it.
                                    List depend=workflowStages.get(stagesFL).getDependencies();
                                    depend.remove(st);
                                    depend.add(newId);
                                }//if it is first stage we do nothing as we do not want to change its dependency bc depends only from the original initial stage
                            }                            
                        }
                    }               
                    //Update workflowId in wflStages hashtable  
                    wflStages.remove(st);
                    wflStages.put(newId, stageTemp);
                    
                    newListLoop.add(newId);
                }else{
                    newListLoop.add(oldnewNames.get(st));
                }                
            }
            //update loopinvolved for each stage 
            for(String newListLoopS:newListLoop){
                System.out.println("RenameLoopInvolved "+s+" with "+oldnewNames.get(s)); //rename loop identifier in loopInvovled for each stage
                wflStages.get(newListLoopS).renameLoopInvolved(s, oldnewNames.get(s));
            }
            
            
            loopsNew.put(oldnewNames.get(s), newListLoop);
            
            //rename non-blocking transitions
            for(WorkflowStage wsO:wflStages.values()){
                //update non-blocking transitions for each stage
                List<String> nonBlocking=wsO.getNonBlockingTransitions();                        
                if(!nonBlocking.isEmpty()){
                    List<String> newnonBlockingList=new ArrayList();
                    System.out.println("NBtransitions of "+wsO.getId());
                    for(String snb:nonBlocking){
                        System.out.println("    renameNBT "+snb+" with "+oldnewNames.get(snb));
                        newnonBlockingList.add(oldnewNames.get(snb));
                    }
                    wsO.setNonBlockingTransitions(newnonBlockingList);
                }
            }
      
        }
        System.out.println("LoopsNew: "+loopsNew.keySet().toString());
        for(String s:loops.keySet()){  //add existing loops that have not been changed
            String key=oldnewNames.get(s);
            if(key==null){
                if(!loopsNew.containsKey(s)){
                    System.out.println("Add unmodified loop "+s);
                    loopsNew.put(s,loops.get(s));
                }
            }else{
                if(!loopsNew.containsKey(key)){//this will happen when inner loop
                    System.out.println("Add unmodified loop "+key);
                    List <String>newNames=new ArrayList();
                    for(String oldSname:loops.get(s)){
                        newNames.add(oldnewNames.get(oldSname));                        
                    }
                    //update loopinvolved for each stage 
                    for(String newListLoopS:newNames){
                        System.out.println("RenameLoopInvolved "+s+" with "+oldnewNames.get(s)); //rename loop identifier in loopInvovled for each stage
                        wflStages.get(newListLoopS).renameLoopInvolved(s, oldnewNames.get(s));
                    }
                    
                    loopsNew.put(key,newNames);
                }                
            }
        }
        loops=loopsNew;
        System.out.println("LoopsNew after: "+loopsNew.keySet().toString());
        
        if(!newIteration){//for initial renaming
            
            loopsOrigName=loops; //the first time we set this pointer. Since renaming loops consists of creating new hashmaps, this should work
            
            System.out.println("Loop Order before: "+loopsOrder.toString());
            //UPDATE loopsOrder. Only in the first renaming
            List<String> loopsOrderNew=new ArrayList();
            for(String s:loopsOrder){
                String newId=oldnewNames.get(s);
                if(newId==null){
                    newId=s;
                }
                loopsOrderNew.add(newId); 
            }
            loopsOrder=loopsOrderNew;
            System.out.println("Loop Order after: "+loopsOrderNew.toString());
            
            //UPDATE ALL forwardList: 
            HashMap<String, List<String>> newforwardList = new HashMap<String, List<String>>();
            for(String s:forwardList.keySet()){
                List<String> newStagesAfter=new ArrayList();
                List<String> stagesAfter=forwardList.get(s);
                if(stagesAfter!=null){
                    for(String st:stagesAfter){
                        String newId=oldnewNames.get(st);
                        if(newId==null){
                            newId=st;
                        }
                        newStagesAfter.add(newId);                    
                    }                
                }
                String newId=oldnewNames.get(s);
                if(newId==null){
                    newId=s;
                }
                newforwardList.put(newId, newStagesAfter);
            }
            forwardList=newforwardList;
        }else{
            //add newly create stages based on the previous ones
            for(String s:loopsToUpdate.keySet()){//it should be only one loop
                System.out.println("Loop "+s);
                List<String> tempLoop=loopsToUpdate.get(s);
                String firstStageLoop=tempLoop.get(0);//first stage of loop
                //the key s knows the forward list of the last new iteration. 
                //  It is needed bc the entry of the forwardList was overwritten 
                //  when creating the new stage of the following iteration
                for(String st:tempLoop){//for each stage in a loop
                    System.out.println(" st "+st);
                    if(st.equals(s) && originalFollowStagesofLoop.get(st)!=null){//if it is null, it may be that it is the last stage
                        
                        forwardList.put(oldnewNames.get(st), originalFollowStagesofLoop.get(st));
                        for(String sFl:originalFollowStagesofLoop.get(st)){
                            WorkflowStage wstemp=workflowStages.get(sFl);//we need to take it from here bc this stages will be outside of the loop
                            List<String>dep=wstemp.getDependencies();
                            dep.remove(s);
                            dep.add(oldnewNames.get(s));
                        }
                        
                    }else{
                        List<String> newStagesAfter=new ArrayList();
                        List<String> stagesAfter=forwardList.get(st);//still have old names
                        if(stagesAfter!=null){
                            for(String stageaf:stagesAfter){
                                System.out.println("     stageaf "+stageaf);
                                String newId=oldnewNames.get(stageaf);
                                if(newId==null){
                                    newId=stageaf;
                                }
                                System.out.println("     stageaf-newId "+newId);
                                if(!st.equals(firstStageLoop) ||  tempLoop.contains(stageaf)){//if it is not the first stage, then add follower                                    
                                    newStagesAfter.add(newId);                                //if it is in the loop path, then add follower
                                }                                                             //if it is the first stage and not in the loop path, do nothing
                            }                
                        }
                        String newId=oldnewNames.get(st);
                        if(newId==null){
                            newId=st;
                        }
                        forwardList.put(newId, newStagesAfter);
                    }
                }
                
                //now that all is renamed in this loop, we connect the last stage of previous iter with the first of the current                
                String stageIdendLoop=s;//last stage of previous iter
                for(String st:tempLoop){//for each stage in a loop
                    if(st.equals(firstStageLoop)){//conect previous iter with this new
                        List<String> dependencies=new ArrayList();
                        dependencies.add(stageIdendLoop);//add as a depenendency the last stage of previous loop iter

                        wflStages.get(oldnewNames.get(st)).setDependenciesInfo(dependencies);//this should NOT be renamed later

                        System.out.println("Workflow.newIteration. addOriginal?");
                        boolean found=false;
                        for(String ofsl:originalFollowStagesofLoop.keySet()){
                            System.out.println("Workflow.newIteration. addOriginal: "+ofsl);
                            if(ofsl.split("_")[0].equals(stageIdendLoop.split("_")[0])){
                                System.out.println("Workflow.newIteration. addOriginal: "+stageIdendLoop);
                                System.out.println("Workflow.newIteration. addOriginal: TRUE");
                                found=true;
                            }
                        }
                        if(!found){
                            if(forwardList.get(stageIdendLoop)==null){
                                originalFollowStagesofLoop.put(stageIdendLoop,new ArrayList());//keep original forwardList item of the last stage of first iter of the loop
                            }else{
                                originalFollowStagesofLoop.put(stageIdendLoop,forwardList.get(stageIdendLoop));//keep original forwardList item of the last stage of first iter of the loop
                            }
                            
                        }

                        List<String> forward=new ArrayList();
                        forward.add(oldnewNames.get(st)); 
                        forwardList.put(stageIdendLoop, forward);//connect last stage of previous iter to the first of current iter

                        break;
                    }
                }
                
                
            }
            //add all new stages
            workflowStages.putAll(wflStages);
        }
    }
    
    
    
    /**
     * Prepare inner loop for the next iteration of its outer loop
     *   
     * @param stageIdendLoop identify loop, (original name)
     */
    public void prepareForOuterLoop(String stageIdendLoop){
        //No need to set it to 0, bc outer loop would know only original loop, so it starts in 0
        //for(String s:loops.get(stageIdendLoop)){
        //    workflowStages.get(s).updateLoop(stageIdendLoop, 0);
        //}
        
        //Reconnect end of first iteration with the stage after the end of the whole unrolled loop
        
        
        //this assumed that stageIdendLoop is the name of the last stage of the last iteration of the loop.
//        if(originalFollowStagesofLoop!=null){
//            for(String anOriginalForwardStage:originalFollowStagesofLoop.keySet()){//for each original loop stored
//                if(anOriginalForwardStage.split("_")[0].equals(stageIdendLoop.split("_")[0])){
//                    //put in dependencies of each following stage, the original ending stage of the loop (fist iteration)
//                    
//                    for(String s:originalFollowStagesofLoop.get(anOriginalForwardStage)){//get all forward stages of the original loop we want
//                        List<String> dep=workflowStages.get(s).getDependencies();
//                        for(String st:dep){//it has the id of the last iteration of the loop. Replace it with anOriginalForwardStage
//                            if(st.split("_")[0].equals(anOriginalForwardStage.split("_")[0])){//compare core stageid (e.g., S1)
//                                dep.remove(st);//remove last stage of last iteration
//                                dep.add(anOriginalForwardStage);//add last stage of first iteration
//                            }
//                        }
//                    }
//                    //put as follower of original ending stage of the loop (fist iteration) all this
//                    forwardList.put(anOriginalForwardStage, originalFollowStagesofLoop.get(anOriginalForwardStage));//this breaks the loop,but we do not need it anymore, do we??? delete all stages when confirm this
//
//                }
//            }
//        }
        
        //this code is fo stageIdendLoop to be the name of the last stage of the first iteration of the loop
        if(originalFollowStagesofLoop!=null){                                
            for(String s:originalFollowStagesofLoop.get(stageIdendLoop)){//get all forward stages of the original loop we want
                List<String> dep=workflowStages.get(s).getDependencies();
                for(String st:dep){//it has the id of the last iteration of the loop. Replace it with anOriginalForwardStage
                    if(st.split("_")[0].equals(stageIdendLoop.split("_")[0])){//compare core stageid (e.g., S1)
                        dep.remove(st);//remove last stage of last iteration
                        dep.add(stageIdendLoop);//add last stage of first iteration
                    }
                }
            }
            //put as follower of original ending stage of the loop (fist iteration) all this
            forwardList.put(stageIdendLoop, originalFollowStagesofLoop.get(stageIdendLoop));//this breaks the loop,but we do not need it anymore, do we??? delete all stages when confirm this
            
        }
        
        //restore loops
        for(String ls:loops.keySet()){
            if(ls.split("_")[0].equals(stageIdendLoop.split("_")[0])){ //stageIdendLoop will be the original name, as it is called from the nestedLoop hashmap that does not get updated
                loops.put(stageIdendLoop, loopsOrigName.get(stageIdendLoop));
                loops.remove(ls);
            }
        }
        
        //Do nothing with loopOrder bc does not change anymore. 
        
    }    
    
    
    
    @Override
    public String toString(){
        String str="";
        for(String s:forwardList.keySet()){            
            str+=s+"\n";
            str+="   -> "+forwardList.get(s).toString()+"\n";
        }
        
        return str;        
    }
    
}
