/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.workflow.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Javier Diaz-Montes
 */
public class Node implements Serializable{
    boolean isUp;
    double benchScore;
    double cost;
    int totalWorkers;    
    int activeWorkers;
    String name; //address if cluster node; VM type if cloud node
    int minPort;
    int maxPort;
    List usedPorts;
    
    public Node (int worker, double c,double benchScore){
        this.totalWorkers=worker;        
        this.cost=c;
        this.benchScore=benchScore;
        activeWorkers=0;
        usedPorts=new ArrayList();
    }
    
    public String getName() {
        return name;
    }

    public boolean getIsUp() {
        return isUp;
    }

    /**
     * Get score of a benchmark. It will be used to calculate speedup(performance)
     * @return double score
     */
    public double getBenchScore() {
        return benchScore;
    }

    public double getCost() {
        return cost;
    }

    public int getTotalWorkers() {
        return totalWorkers;
    }

    public int getMinPort() {
        return minPort;
    }

    public int getMaxPort() {
        return maxPort;
    }

    public int getActiveWorkers() {
        return activeWorkers;
    }

    public void setMinPort(int minPort) {
        this.minPort = minPort;
    }

    public void setMaxPort(int maxPort) {
        this.maxPort = maxPort;
    }
    
    public void setIsUp(boolean status) {
        this.isUp = status;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setTotalWorkers(int totalWorkers) {
        this.totalWorkers = totalWorkers;
    }

    public void setActiveWorkers(int activeWorkers) {
        this.activeWorkers = activeWorkers;
    }
    
    public void decrementActiveWorkers(int numWorkers){
        this.activeWorkers=this.activeWorkers-numWorkers;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    //this cannot be used for VMs because Node is the type of VM, not the specific instance
    public int getFreePort(){
        boolean found=false;
        int port=-1;
        for(int i=minPort;i<=maxPort&&!found;i++){
            if(!usedPorts.contains(i)){
                port=i;
                found=true;
            }
        }
        usedPorts.add(port);
        return port;
    }
    
    public void releasePort(Integer port){
        usedPorts.remove(port);        
    }

    @Override
    public String toString() {
        return "Node{" + "isUp=" + isUp + ", benchScore=" + benchScore + ", cost=" + cost + ", totalWorkers=" + totalWorkers + ", activeWorkers=" + 
                activeWorkers + ", name=" + name + ", minPort=" + minPort + ", maxPort=" + maxPort + ", usedPorts=" + usedPorts + '}';
    }
    
    
}
