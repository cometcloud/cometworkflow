package tassl.workflow.resources;


import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.utils.CommonUtils;

/**
 *
 * @author moustafa
 */
public class OSGJobHandler {

    /*
     submit a job to OSG that is SSH accessible 
     currently this function is used sequential 
     e.g. allocate one job, wait for it to start, test ssh before allocating the next job
    TODO: make this run in parallel by taking N (total number of jobs to run) then start and monitor them all at the same time 
    possibly using multithreading or by submitting all jobs first before waiting/checking for them to start
    */
    public static String submitSSHJob(String workDir, String filename, int period){
        String jobID=null;
        boolean flag = true;
        while (flag){
            jobID = submitJob(workDir, filename);
            waitForJobRun(jobID, period);
            int status = canSSH(jobID);
            if(status==0){
                flag=false;
            }else if (status==1){
                cancelJob(jobID);
            }else{
                System.out.println("Trying condor_ssh again");
                try {
                        Thread.sleep(period);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(OSGJobHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                if(canSSH(jobID)==0){
                    flag =false;
                }else{
                    cancelJob(jobID);
                }
            }
        }
        return jobID;
    }
    /*
     input is the dir containing OSG job submission script (e.g. comet worker dir) 
     and the name of the script (e.g. job.sub)
     returns the jobID for a submitted job or null if there was a failure
     */
    public static String submitJob(String workDir, String filename) {
        String jobID = null;
        String temp = CommonUtils.execute("cd " + workDir + ";condor_submit " + filename, false)[1];
        try {
            jobID = temp.substring(temp.lastIndexOf(" "), temp.lastIndexOf(".")).trim();
            System.out.println("JobID is: " + jobID);
        } catch (Exception e) {
            System.out.println("Failed to parse jobID, output was: " + temp);
            e.printStackTrace();
        }
        return jobID;
    }

    /*
     cancel an existing job using a JobID
     Automatically closes any existing tunnels to the job
     */
    public static void cancelJob(String jobID) {
        CommonUtils.execute("condor_rm " + jobID, false);
    }

    /*
     input jobID returns job status based on JobStatus codes
     returns null if no code was generated
     0	Unexpanded	U
     1	Idle            I
     2	Running         R
     3	Removed         X
     4	Completed       C
     5	Held            H
     6	Submission_err	E
     */
    public static String checkJobStatus(String jobID) {
        String state = CommonUtils.execute("condor_q -l " + jobID + " | grep JobStatus", false)[1];
        if (state != null && state.length() > 0) {
            if (state.startsWith("JobStatus = 0")) {
                return "Unexpanded";
            }
            if (state.startsWith("JobStatus = 1")) {
                return "Idle";
            }
            if (state.startsWith("JobStatus = 2")) {
                return "Running";
            }
            if (state.startsWith("JobStatus = 3")) {
                return "Removed";
            }
            if (state.startsWith("JobStatus = 4")) {
                return "Completed";
            }
            if (state.startsWith("JobStatus = 5")) {
                return "Held";
            }
            if (state.startsWith("JobStatus = 6")) {
                return "Submission_error";
            }

        }
        return null;
    }

    /*
     function to wait for job to run before doing anything else
     (e.g. before setting up ssh tunnels)
     input is jobID and period of time in milliseconds 
     (e.g. check every 3 seconds would be period=3000)
     returns true if the job is running 
     false if the job status could not be found
     */
    public static boolean waitForJobRun(String jobID, int period) {
        String state;
        while (true) {
            state = checkJobStatus(jobID);
            System.out.println("State is: " + state);
            if (state != null) {
                if (state.equals("Running")) {
                    try {
                        Thread.sleep(period);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(OSGJobHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                } else {
                    try {
                        Thread.sleep(period);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(OSGJobHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /*
     function to check if you can SSH to job or not
     Note - on some nodes you can setup tunnel even if you can't directly SSH to machine
     */
    public static int canSSH(String jobID) {
        return decodeSSHTunnelCode(CommonUtils.execute("condor_ssh_to_job -ssh \"ssh -o ConnectTimeout=10 -o BatchMode=yes -o StrictHostKeyChecking=no\" " + jobID + " uname", false));
        
    }

    /*
     function to execute remote SSH commands on job
     */

    public static int executeSSHCmd(String jobID, String cmd){
        return decodeSSHTunnelCode(CommonUtils.execute("condor_ssh_to_job -ssh \"ssh -o ConnectTimeout=10 -o BatchMode=yes -o StrictHostKeyChecking=no\" " + jobID + cmd, false));
    }
    /*
     returns 0 if successful
     returns 1 if permanently failed to initiate tunnel (need a new job)
     returns 2 if temporarily failed to initiate tunnel (resubmit tunnel)
     returns 3 if failed to execute ssh tunnel (resubmit tunnel)
     */
    public static int decodeSSHTunnelCode(String[] output) {
        System.out.println("exit code is:" + output[0]);
        System.out.println("std out is: " + output[1]);
        System.out.println("std err is: " + output[2]);
        if (output[0].equals("0")) {
            return 0;
        } else {
            if (output[2].contains("Connection closed by remote host") || output[2].contains("This account is currently not available")) {
                return 1;
            }else if (output[2].contains("the job execution environment is not yet ready") || output[2].contains("Failed to connect to starter")) {
                System.out.println("the job execution environment is not yet ready or failed to connect to starter");
                return 2;
            }else{
                System.out.println("failed to execute condor_ssh");
                return 3;
            }
        }
    }
    /*
     setup a tunnel to an OSG job (if outbound communication from submission node to job is blocked)
     SSH is not guaranteed to work on all OSG nodes  
     resubmit another job to try another node
     */

    public static int setupTunnel(String jobID, int local_port, String remote_port) {
        return decodeSSHTunnelCode(CommonUtils.execute("condor_ssh_to_job -ssh \"ssh -fN -L " + local_port + ":localhost:" + remote_port + "\" " + jobID, false));
    }

    /*
     setup a reverse tunnel to an OSG job (if inbound communication from job to submission node is blocked)
     SSH is not guaranteed to work on all OSG nodes  
     resubmit another job to try another node
     */
    public static int setupReverseTunnel(String jobID, int local_port, int remote_port) {
        return decodeSSHTunnelCode(CommonUtils.execute("condor_ssh_to_job -ssh \"ssh -fN -R " + local_port + ":localhost:" + remote_port + "\" " + jobID, false));
    }

    /*
     function to transfer a file from the login node to a job using scp or rsync
     transfer_type = osgSCP for scp or = osgRsync for rsync
     filename = "file1 file2 file3 file4"
     */
    public static String[] transferFileToJob(String jobID, String filename, String remote_dir, String transfer_type) {
        if (transfer_type.equals("osgSCP")) {
            return CommonUtils.execute("condor_ssh_to_job -ssh scp "+jobID+" "+filename+" remote:"+remote_dir, false);
        } else if (transfer_type.equals("osgRsync")) {
            return CommonUtils.execute("rsync -avz -e \"condor_ssh_to_job\" "+filename+" "+jobID+ ":"+remote_dir, false);
        } else {
            System.out.println("Invalid transfer type for OSG");
            String [] result = {"-1","","Invalid transfer type for OSG"};
            return result;
        }
    }

    /*
     function to transfer a file from a job to the login node using scp or rsync
     transfer_type = osgSCP for scp or = osgRsync for rsync
     filename = ":file1:file2:file3:file4"
     */
    public static String[] transferFileFromJob(String jobID, String filename, String local_dir, String transfer_type) {
        if(local_dir.length()==0){
            local_dir=".";
        }
        if (transfer_type.equals("osgSCP")) {
            return CommonUtils.execute("condor_ssh_to_job -ssh scp "+jobID+" remote"+filename+" "+local_dir, false);
        } else if (transfer_type.equals("osgRsync")) {
            return CommonUtils.execute("rsync -avz -e \"condor_ssh_to_job\" "+jobID+filename+" "+local_dir, false);
        } else {
            System.out.println("Invalid transfer type for OSG");
            String [] result = {"-1","","Invalid transfer type for OSG"};
            return result;
        }
    }

}
