/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.workflow.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.application.utils.CommonUtils;
import tassl.application.utils.RunningStat;
import tassl.application.workflow.WorkerForScheduler;

/**
 *
 * @author Javier Diaz-Montes
 */
public class ResourceCloud extends Resource{
        
    String providerType; //Openstack
    String providerConfigFile; //novarc        
    String region;
    String cloudScript;    
    
    String defaultImageId;
    //custom VMs. We can specify a VM per application
    HashMap<String,String> imageIdApps;
    
    HashMap <String,Integer>VMLimits; //{m1.small:10,m1.medium:5;m1.large:3}
    HashMap <String,Integer>VMLimits_backup; //Used to restore after disabled {m1.small:10,m1.medium:5;m1.large:3}
    boolean disabled=false; //flag to prevent disabling this several times and loosing the backup
    HashMap <String,List>VMActive; //{m1.small:{ip,ip},m1.medium:{ip,ip},m1.large:{ip,ip})
    HashMap <String,String>VMIds; //{ip:id,ip:id)
    HashMap <String,Integer> VMFails; //to control number of fails of a VM type. After 3 fails we reduce VMLimits: {m1.small:0,m1.medium:1;m1.large:1}
    
    //nodeList is to know the properties of each VM type

    public ResourceCloud(String publicIp, String AgentMetricsAddress, int AgentMetricsPort, boolean useMeticsService, String dbName){
        super(publicIp, AgentMetricsAddress, AgentMetricsPort, useMeticsService, dbName);
        VMActive=new HashMap();
        VMIds=new HashMap();
        VMFails=new HashMap();
        imageIdApps=new HashMap();
        VMLimits=new HashMap();
        VMLimits_backup=new HashMap();
    }
    
    @Override
    public void disableSite(){
        //if the site is active
        if(!disabled){
            //clean backup just in case
            VMLimits_backup.clear();
            //create backup and set to 0
            for(String keyS:VMLimits.keySet()){                
                //create backup
                VMLimits_backup.put(keyS,VMLimits.get(keyS));
                //set to 0
                VMLimits.put(keyS, 0);
            }
            disabled=true;
        }      
    }
    
    
    @Override
    public void restoreSite(){
        if(disabled){
            for(String keyS:VMLimits.keySet()){
                //restore value
                VMLimits.put(keyS, VMLimits_backup.get(keyS));
            }
        }
    }

    
    @Override
    public List getAvailableSlotsScheduling(String siteAddress, String [] allocatedWorkers, double refBenchScore, HashMap<String,RunningStat>defaultAppExec,double defaultReferenceBenchmark){
        //workers is {worker:1, worker:1}, the number could be greater than 1
        
        //ADD to each slot
            //DONE HashMap<String,RunningStat>defaultAppExec,double defaultReferenceBenchmark
        
        //DONE calculate Performance using the reference
        //DONE remove getPerformance from WorkerForScheduler (slot) and from Node
        //DONE add getExecTime(appname,time) (slot)
        
        
        List nodesForSched=new ArrayList();
        
        //this is to store the number of slots allocated of each type. This is used  to initialize i in the totalAvail loop
        HashMap <String,Integer> slotsType=new HashMap();
        
        String [] wkPart;
        //add first the allocated workers
        for(String wk:allocatedWorkers){
            if(!wk.trim().isEmpty()){
                wkPart=wk.split(":");
            //FOR SIMPLICITY WE DO NOT RESTART WORKERS THAT FAILED IN A SLOT i.e. if workerVM is = 2 for a particular type, and there is only 1 in the 
                //list of allocated nodes, then only one slot is created.
                int allocated=0;
                if (slotsType.containsKey(wkPart[0])){
                    allocated=slotsType.get(wkPart[0]).intValue();
                    allocated++;
                    slotsType.put(wkPart[0], allocated);                
                }else{
                    slotsType.put(wkPart[0], 0);
                }

                for (int j=0;j<Integer.parseInt(wkPart[1]);j++){     
                    double benchScore=this.nodeList.get(wkPart[0]).getBenchScore();
                    WorkerForScheduler wfs=new WorkerForScheduler(siteAddress,this.identifier, this.zone,wkPart[0], allocated, j, 0, 0,defaultAppExec,defaultReferenceBenchmark,benchScore);
                    wfs.setCost(this.nodeList.get(wkPart[0]).getCost());
                    
                    wfs.setPerformance(benchScore/refBenchScore);
                    
                    nodesForSched.add(wfs);   
                    System.out.println("CWDEBUG: getAvailableSlotsScheduling - previously allocated - Cloud SLOT:"+wfs.toString());
                }        
            }
        }
        
        System.out.println("CWDEBUG:resourcecloud. nodelist= "+nodeList.keySet().toString());
        System.out.println("CWDEBUG:resourcecloud. VMLimit= "+VMLimits.toString());
        System.out.println("CWDEBUG:resourcecloud. VMActive= "+VMActive.toString());
        
        //ip/name:node or vmtype:node
        for(String type:this.nodeList.keySet()){
            int max=VMLimits.get(type);
            int used=0;
            List active=VMActive.get(type);
            if (active!=null){
                used=active.size();
            }
            int totalAvail=max-used;
            int workerVM=this.nodeList.get(type).getTotalWorkers();
            
            int addTotalAvail=0;
            if (slotsType.containsKey(type)){
                addTotalAvail=slotsType.get(type)+1;//because it starts in 0. then if it is 1, it means that there are slot 0 and slot 1.
            }
            
            for(int i=addTotalAvail;i<totalAvail+addTotalAvail;i++){
                for(int j=0;j<workerVM;j++){
                    double benchScore=this.nodeList.get(type).getBenchScore();
                    WorkerForScheduler wfs=new WorkerForScheduler(siteAddress,this.identifier,this.zone,type, i, j, this.getOverhead(), this.getOverhead(),defaultAppExec,defaultReferenceBenchmark,benchScore);
                    wfs.setCost(this.nodeList.get(type).getCost());
                    wfs.setPerformance(benchScore/refBenchScore);
                    nodesForSched.add(wfs);   
                    System.out.println("CWDEBUG: getAvailableSlotsScheduling - Cloud SLOT:"+wfs.toString());
                }
            }
            
        }            
            
        return nodesForSched;
    }
    
    
    
    public boolean loadConfig(String file){
        boolean status=true;
        Properties p = new Properties();                        
        try {
            p.load(new FileInputStream(file));                                    
            providerType=p.getProperty("ProviderType");
            providerConfigFile=p.getProperty("ProviderConfigFile");                    
            VMLimits=string2HashMapInt(p.getProperty("VMLimits"));                                                       
            defaultImageId=p.getProperty("defaultImageId");
            cloudScript=p.getProperty("CloudScript");
            region=p.getProperty("Region");
            if(defaultImageId==null || cloudScript==null || region==null){
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Please make sure that the parameters "
                        + "CloudScript (to interact with cloud), Region (cloud region) and defaultImageId (image in cloud) are defined.");
                return false;
            }            
            fileTransferType = p.getProperty("fileTransferType", "default");//default is rsync change to support different file transfers e.g. SCP gridFTP, OSG

            //load common info and create nodelist
            status=super.loadConfig(file,"cloud");
            //previous command fill out the supportedApps object
            //now we identify if there are custom VM images for each application (this is optional)
            for(String sa:supportedApps.keySet()){
                String value=p.getProperty(sa+"ImageId");//the VM image for an app is called <appname>ImageId
                if(value!=null){
                    imageIdApps.put(sa, value);
                }
            }
            
            if (VMLimits.size()!=this.nodeList.size()){
                Logger.getLogger(Resource.class.getName()).log(Level.WARNING, "The number of fields in VMLimits has to be the same that in "                        
                        + "WorkerLimit, Cost, Perf are different. Please check the syntaxis.");
                return false;
            }

        } catch (IOException ex) {
            Logger.getLogger(ResourceCloud.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }               
        return status;
    }
    
    

    public String getProviderType() {
        return providerType;
    }

    public String getProviderConfigFile() {
        return providerConfigFile;
    }

    public String getDefaultImageId() {
        return defaultImageId;
    }

    /**
     * Get the imageId of the custom VM image of an Application
     * @param app
     * @return imageId 
     */
    public String getImageId(String app) {        
        if(imageIdApps.containsKey(app)){
            return imageIdApps.get(app);
        }else{        
            return defaultImageId;
        }
    }
    
    public String getRegion() {
        return region;
    }

    public String getCloudScript() {
        return cloudScript;
    }

    public HashMap<String, Integer> getVMLimits() {
        return VMLimits;
    }

    
    public void setProviderType(String ProviderType) {
        this.providerType = ProviderType;
    }

    public void setProviderConfigFile(String ProviderConfigFile) {
        this.providerConfigFile = ProviderConfigFile;
    }

    public void setVMLimits(HashMap<String, Integer> VMLimits) {
        this.VMLimits = VMLimits;
    }

    public void setDefaultImageId(String imageId) {
        this.defaultImageId = imageId;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCloudScript(String cloudScript) {
        this.cloudScript = cloudScript;
    }

    /**
     * Return the type of VM that corresponds to the ip or null if not found
     * @param ipVM
     * @return
     */
    public String getTypebyIp(String ipVM){
        for(String key:VMActive.keySet()){
            if(VMActive.get(key).contains(ipVM)){
                return key;
            }
        }
        return null;
    }
    
    /**
     * Return list of addresses corresponding with VMs from those allocated to the specific stage
     * 
     * @param workflowId
     * @param stageId
     * @param vmTypes
     * @return List of ips
     */
    public List getVMAddressbyType(String workflowId, String stageId, List <String> vmTypes){
        //{address,address}
        List <String>ips=new ArrayList();
        
        List <String>allocatedWorkers=wflNode.get(workflowId).get(stageId);
        for (String type:vmTypes){
            //VMActive.get(type)
            if(allocatedWorkers!=null){
                for(int i=0;i<allocatedWorkers.size();i++){
                    String ip=allocatedWorkers.get(i).split(":")[0];
                    if(VMActive.get(type).contains(ip)){
                        ips.add(ip);
                        break;
                    }
                }            
            }
        }
        if(vmTypes.size() != ips.size()){
            System.out.println("CWDEBUG: ERROR: the number of IPs is different to the number of VM types");
            Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "ERROR: the number of IPs is different to the number of VM types");
        }
        
        return ips;
    }
    
    /**
     * Remove stage from wflnode. 
     *   Assumption: We release all workers of a VM because we do not share VM across stages
     * 
     * @param workflowId
     * @param stageId
     * @param nodeAcctList 
     * @param failure 
     * @return List of Workers to terminate them later
     */
    @Override
    public List releaseWorkers(String workflowId, String stageId, List<NodeAcct> nodeAcctList, boolean failure) {
        List allworkers=null;
        List terminatedworkers=new ArrayList();
        HashMap stages=wflNode.get(workflowId);
        System.out.println("CWDEBUG. resourcecloud. Before release");
        printWflNode();
        printVMactive();
        if(stages!=null){
            allworkers=(List)stages.get(stageId);
            if(allworkers!=null){
                System.out.println("CWDEBUG: resourcecloud. stage size:"+allworkers.size());
            
                for(int i=0;i<allworkers.size();i++){
                    System.out.println("CWDEBUG: resourcecloud. nodename:"+allworkers.get(i));
                    String [] parts=((String)allworkers.get(i)).split(":");
                    String addr=parts[0];
                    int port=Integer.parseInt(parts[1]);
                    
                    terminatedworkers.add((String)allworkers.get(i));
                    
                    //ask worker to end                        
                    terminateWorker(addr, port, true, nodeAcctList, failure, false); //releaseWorkers is never called when provisioning new resources
                    
                    //remove it from VMActive                    
                    releaseMachine(addr, nodeAcctList, failure);                    
                }
            }
            stages.remove(stageId); //remove stage info
            if(stages.isEmpty()){
                wflNode.remove(workflowId);
            }
        }else{
            System.out.println("CWDEBUG: resourcecloud. workflow not registered");
        }
        
        System.out.println("CWDEBUG. resourcecloud. after release");
        
        return terminatedworkers;
        
    }
    
    @Override
    public void releaseMachine(String ipVM, List<NodeAcct> nodeAcctList, boolean failure){
        String VMType="unknown";
        boolean found=false;
        for(String key:VMActive.keySet()){
            if(VMActive.get(key).contains(ipVM)){
                VMActive.get(key).remove(ipVM);
                VMType=key;
                found=true;
                break;
            }
        }
        if(found && (useMeticsService)){
            NodeAcct na=findNodeAcctIP(nodeAcctList,ipVM);
            long startTimeStamp=-1L;
            String appName="unknown";
            if(na!=null){
                startTimeStamp=na.getStartTime();
                appName=na.getAppName();
            }
            
            long end=System.currentTimeMillis();
            HashMap<String,String> additionalValues=new HashMap<String,String>(); //values custom to different resources
            additionalValues.put("imageId", this.getImageId(appName));            
            List param=new ArrayList(Arrays.asList(VMType,startTimeStamp, end, appName, this.getClass().getSimpleName(), failure, additionalValues));
            this.sendInfo(this.AgentMetricsAddress, this.AgentMetricsPort, "dbWriteAgentReleaseMachine", param);
        }
        
    }
    
    @Override
    public List releaseSomeWorkers(String workflowId, String stageId, List<String> workers, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning){
        List nodeNames;
        List types=new ArrayList();
        HashMap stages=wflNode.get(workflowId);
        System.out.println("CWDEBUG. resourcecloud. Before releaseSomeWorkers");
        printWflNode();
        printVMactive();
        if(stages!=null){
            nodeNames=(List)stages.get(stageId);            
            for(String work:(List<String>)workers){   
                String [] parts=work.split(":");
                String addr=parts[0];
                int port=Integer.parseInt(parts[1]);
                //ask worker to end
                terminateWorker(addr, port, true, nodeAcctList, failure, provisioning);
                
                if(nodeNames!=null){
                    nodeNames.remove(work);
                }
                types.add(getTypebyIp(addr));//add type of VM
            }                        
        }else{
            System.out.println("CWDEBUG: resourcecloud. releaseSomeWorkers.  workflow not registered");
        }
        
        System.out.println("CWDEBUG. resourcecloud. releaseSomeWorkers.  after releaseSomeWorkers");
        return types;
    }
    
    /**
     * Release all workers of the selected machines
     * 
     * @param workflowId
     * @param stageId
     * @param machineIps
     * @return List of workers {address:port,address:port,...}
     */
    @Override
    public List releaseSomeWorkersbyMachineIp(String workflowId, String stageId, List<String> machineIps, List<NodeAcct> nodeAcctList, boolean failure){
        List <String> terminatedworkers=new ArrayList<String>();
        List <String> allworkers;
        
        //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
        HashMap stages=wflNode.get(workflowId);
        System.out.println("CWDEBUG. resourcecloud. Before releaseSomeWorkersbyMachineIp");
        printWflNode();
        printVMactive();
        if(stages!=null){
            allworkers=(List)stages.get(stageId);  
            if(allworkers!=null){
                for(int i=allworkers.size()-1;i>=0;i--){            
                    String worker=allworkers.get(i);
                    String [] parts=worker.split(":");
                    String addr=parts[0];
                    int port=Integer.parseInt(parts[1]);   
                    if(machineIps.contains(addr)){    
                        allworkers.remove(i);//remove from data structure wflNode                        
                        terminatedworkers.add(worker);
                        //ask worker to end                        
                        terminateWorker(addr, port, true, nodeAcctList, failure, false); //releaseSomeWorkersbyMachineIp is never called when provisioning new resources
                    }
                }
                //remove VMs from data structures - later we call terminateVM
                for(String addr:machineIps){
                    releaseMachine(addr,nodeAcctList,failure);
                }
            }
        }else{
            System.out.println("CWDEBUG: resourcecloud.releaseSomeWorkersbyMachineIp. workflow not registered");
        }
                
        return terminatedworkers;
    }
        
    
    @Override
    protected void terminateWorker(String address, int port, boolean force, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning){
        if(useMeticsService){
            //currently we assume a worker stated when its hosting VM started
            
            NodeAcct na=findNodeAcctIP(nodeAcctList,address);
            long startTimeStamp=-1L;
            String appName="unknown";
            String vmtype="unknown";
            if(na!=null){
                startTimeStamp=na.getStartTime();
                appName=na.getAppName();
                vmtype=na.getType();
            }
            
            long end=System.currentTimeMillis();
            HashMap<String,String> additionalValues=new HashMap<String,String>(); //values custom to different resources
            additionalValues.put("imageId", this.getImageId(appName));            
            
            
            if(!provisioning){
                List param=new ArrayList(Arrays.asList(vmtype,startTimeStamp, end, appName, this.getClass().getSimpleName(), failure, additionalValues));
                this.sendInfo(this.AgentMetricsAddress, this.AgentMetricsPort, "dbWriteAgentReleaseWorker", param);
            }
           
        }        
        if(!failure){
            this.sendTerminateWorkerSignal(address, port, force);                
        }
    }
    
    
    /**
     * Return the number of workers running in the under the same IP
     * @param workflowId
     * @param stageId
     * @param ip
     * @return number of workers
     */
    public int numberWorkersinVM(String workflowId, String stageId, String ip){
        int count=0;
        List <String>wflNodeIps=wflNode.get(workflowId).get(stageId);
        String [] workName=ip.split(":");
        for(String one:wflNodeIps){
            if(one.startsWith(workName[0]+":")){
                count++;
            }
        }
        return count;
    }
    
    
    @Override
    public boolean checkAllocatePossibleStage(int numWorkers, String properties){  
        printVMactive();
        HashMap <String,List>VMActiveClone=(HashMap)VMActive.clone(); //because we just want to check
        int workersToget=numWorkers;
        System.out.println("CWDEBUG: resourcecloud. check it stage can be scheduled");
        if(properties.equals("")){ //check random nodes
            for(String nodename:nodeList.keySet()){
                System.out.println("CWDEBUG: resourcecloud. nodename: "+nodename);
                List activeType=VMActiveClone.get(nodename);                
                if (activeType==null){
                    activeType=new ArrayList();
                    VMActiveClone.put(nodename, activeType);
                }
                System.out.println("CWDEBUG: resourcecloud. active type size:"+activeType.size());
                System.out.println("CWDEBUG: resourcecloud. vm limits:"+VMLimits.get(nodename));
                
                int totalAvail=VMLimits.get(nodename)-activeType.size();
                Node n=nodeList.get(nodename);
                for(int j=0;j<totalAvail;j++){                      
                    System.out.println("CWDEBUG: resourcecloud. total workers:"+n.getTotalWorkers());
                    if(n.getTotalWorkers()>workersToget){
                        workersToget=0;
                    }else{
                        workersToget-=n.getTotalWorkers();
                    }
                    
                    activeType.add("dummyIp"); //add a value to reserve one VM of this type
                    if(workersToget==0){
                        break;
                    }                
                }
                if(workersToget==0){
                    break;
                }
            }
        }
        if(workersToget==0){
            return true;
        }else{
            return false;
        }
    }
    
    
    @Override
    public List allocateStage(String wflId,String stageId, List <String>MachineWorkers, String properties){
        List nodePortList=new ArrayList();
        List nodesForwflNode=new ArrayList();
        
        if(properties.equals("exact")){ //allocate nodes in the list
            //"VMtype:numWorkers","VMtype:numWorkers",...
            for(int z=0;z<MachineWorkers.size();z++){
                String elem=MachineWorkers.get(z);
                String []parts=elem.split(":");
                String nodename=parts[0];
                int numWorkers=Integer.parseInt(parts[1]);
                List activeType=VMActive.get(nodename);
                if (activeType==null){
                    activeType=new ArrayList();
                    VMActive.put(nodename, activeType);
                }
                int totalAvail=VMLimits.get(nodename)-activeType.size();
                
                if(totalAvail==0){//the scheduling is wrong
                    //Undo
                    for(int j=0;j<z;j++){                        
                        String []partsT=(MachineWorkers.get(j)).split(":");                                                
                        VMActive.get(partsT[0]).remove("dummyIp");
                    }
                    return new ArrayList();
                }
                
                //add a value to reserve one VM of this type
                activeType.add("dummyIp");    
                
                Node n=nodeList.get(nodename);
                int port=n.getMinPort();
                String portlist="";
                for(int i=0;i<numWorkers;i++){                        
                    portlist+=(port+i)+" ";
                    //add node for wflNode
                    nodesForwflNode.add(nodename+":"+(port+i));
                }
                portlist=portlist.trim();
                //i.e. m1.small:7777 7778
                nodePortList.add(nodename+":"+portlist);
                                         
                
            }
            
        } else if(properties.equals("random")){ //allocate any nodes
            int workersToget=MachineWorkers.size();
            //HashMap <String,Integer>VMLimits; //{m1.small:10,m1.medium:5;m1.large:3}
            //HashMap <String,List>VMActive; //{m1.small:{ip,ip},m1.medium:{ip,ip},m1.large:{ip,ip})
            //HashMap <String,List>VMIds; //{ip:id,ip:id)
            //protected HashMap <String,Node> nodeList; //ip/name:node or vmtype:node
            //protected HashMap <String,HashMap<String,List>> wflNode; //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
            for(String nodename:nodeList.keySet()){                
                List activeType=VMActive.get(nodename);
                if (activeType==null){
                    activeType=new ArrayList();
                    VMActive.put(nodename, activeType);
                }
                int totalAvail=VMLimits.get(nodename)-activeType.size();
                Node n=nodeList.get(nodename);
                for(int j=0;j<totalAvail;j++){                                    
                    int portsNeeded=0;
                    if(n.getTotalWorkers()>workersToget){
                        portsNeeded=workersToget;
                        workersToget=0;
                    }else{
                        workersToget-=n.getTotalWorkers();
                        portsNeeded=n.getTotalWorkers();
                    }
                    //add a value to reserve one VM of this type
                    activeType.add("dummyIp");                     
                    
                    int port=n.getMinPort();
                    String portlist="";
                    for(int i=0;i<portsNeeded;i++){                        
                        portlist+=(port+i)+" ";
                        nodesForwflNode.add(nodename+":"+(port+i));
                    }
                    portlist=portlist.trim();
                    //i.e. m1.small:7777 7778
                    nodePortList.add(nodename+":"+portlist);
                    if(workersToget==0){
                        break;
                    }                
                }  
                if(workersToget==0){
                    break;
                }
            }
        }
        
        //this value is temporal and it will be replaced in deployVM
        HashMap <String, List<String>>stages;        
        if(!wflNode.containsKey(wflId)){
            stages=new HashMap();
            //stages.put(stageId, nodesForwflNode);
            wflNode.put(wflId, stages);
        }/*else {
            stages=wflNode.get(wflId);
            if(!stages.containsKey(stageId)){
                stages.put(stageId, nodesForwflNode);
            }else{
                List temp=stages.get(stageId);
                temp.addAll(nodesForwflNode);                
            }            
            
        }    */    //The commented part is unnecesary because it is updated in deployVMs?? If not needed then delete nodesForwflNode too
        
        printWflNode();
        
        return nodePortList;        
    }
    
    public void printVMactive(){
        System.out.println("CWDEBUG:resourcecloud. printVMActive");
        for (String type:VMActive.keySet()){
            System.out.print(type+":{");
            List nodeNames=(List)VMActive.get(type);                
            for(int i=0;i<nodeNames.size();i++){
                System.out.print(nodeNames.get(i)+",");
            }
            System.out.print("}");
        }
    }
    
    public void printWflNode(){
        System.out.println("CWDEBUG:resourcecloud. printwflnode");
        HashMap <String, List<String>>stages;
        for (String wk:wflNode.keySet()){
            System.out.print(wk+":{");
            stages=wflNode.get(wk);            
            for (String st:stages.keySet()){
                System.out.print(st+":{");
                List nodeNames=(List)stages.get(st);                
                for(int i=0;i<nodeNames.size();i++){
                    System.out.print(nodeNames.get(i)+",");
                }
                System.out.print("}");
            }
            System.out.print("},");
        }
        System.out.println("");
    }
    /**
     * Deploy N VMs of type type
     * @param wflId
     * @param stageId
     * @param nodePortList i.e. m1.small:7777,m1.medium:7777 7778 (deploy two VMs)
     * @param appname
     * @param syncresource to minimize the critical zone
     * @param failedVMs add VM types that failed to start
     * @return List of ips i.e. ip:7777,ip:7777,ip:7778
     */
    public List deployVMs(String wflId,String stageId, List<String> nodePortList, String appname, Object synresource, List<String> failedVMs){
        List iplist=new ArrayList();
        //HashMap <String,List>VMActive; //{m1.small:{ip,ip},m1.medium:{ip,ip},m1.large:{ip,ip})
        //HashMap <String,List>VMIds; //{ip:id,ip:id)
        //protected HashMap <String,HashMap<String,List>> wflNode; //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
        
        int totalNumber=0;
        String resources="";
        System.out.println("CWDEBUG: resourcecloud. nodeportlist.size "+nodePortList.size());
        for(int i=0; i<nodePortList.size();i++){
            String elem=(String)nodePortList.get(i);
            String type=elem.split(":")[0];
            resources+=type+",";
        }
        //resources is <vmtype>,<vmtype>
        resources=resources.substring(0, resources.length()-1);        
        //call start VM        
        String cmd="";
        if(keyfile != null){
            cmd=cloudScript+" -i "+ this.getImageId(appname)+ " --varfile "+providerConfigFile+ 
                " -r "+region+ " -k "+keyfile + " --start-resources \""+resources + "\" --cloud "+providerType + " --user "+userWorker;
        }else{
            cmd=cloudScript+" -i "+ this.getImageId(appname)+ " --varfile "+providerConfigFile+ 
                " -r "+region+ " --start-resources \""+resources + "\" --cloud "+providerType + " --user "+ userWorker;
        }
        
        String[]output=CommonUtils.execute(cmd, false);
        //output[1] contains IP=i-00001dd1:10.143.0.27:m1.small;i-00001dd2:10.143.0.28:m1.small;failId:dummyIp:m1.large;failId:dummyIp:m1.medium;
        synchronized(synresource){
            if(output[0].equals("0")){
                String[] stdout=output[1].split("\n");
                for (int i=0;i<stdout.length;i++){                
                    if(stdout[i].startsWith("IP")){                    
                        //"IP=id:ip:type;ip:id:type;"
                        System.out.println("CWDEBUG: resourcecloud. "+stdout[i]);

                        String []ipsIds=(stdout[i].split("=")[1]).split(";");
                        //update workflonode                                
                        List stageinfo=new ArrayList();
                        for(int j=0;j<ipsIds.length;j++){
                            if(!ipsIds[j].trim().equals("")){
                                //id, ip, type
                                String [] parts=ipsIds[j].split(":");                                     
                                
                                if(!ipsIds[j].startsWith("failId")){    
                                    try{
                                        System.out.println("CWDEBUG: resourcecloud. adding "+parts[1] + " " + parts[0]);
                                        VMIds.put(parts[1], parts[0]);
                                    }catch(java.lang.ArrayIndexOutOfBoundsException e){
                                        Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "ERROR: String was "+ipsIds[j]);
                                    }
                                }
                                
                                //assume that the ip order in both lists is the same
                  //TODO: this may not be true. Although it only matters when more than 1 worker per machine
                                String ports=((String)nodePortList.get(j)).split(":")[1];

                                //update workflonode                                           
                                for(String z:ports.split(" ")){
                                    if(!(z.trim()).equals("")){
                                        if(!ipsIds[j].startsWith("failId")){ 
                                            stageinfo.add(parts[1]+":"+z);
                                            iplist.add(parts[1]+":"+z);
                                        }else{
                                            iplist.add("fail:"+parts[2]+":"+z);
                                        }
                                    }
                                }

                                List active=VMActive.get(parts[2]);
                                active.remove("dummyIp");
                                if(!ipsIds[j].startsWith("failId")){//VM started
                                    active.add(parts[1]);
                                    provisioningFailure(parts[2], "ok");//type, ok = reset
                                }else{//VM failed
                                    provisioningFailure(parts[2], "fail");//type, fail= add failure.at some point resources are decreased
                                    failedVMs.add(parts[2]); //add type to failures
                                }

                            }

                        }

                        HashMap <String, List<String>>stages;                    
                        stages=wflNode.get(wflId);
                        if(stages==null){
                            stages=new HashMap();
                            wflNode.put(wflId, stages);
                        }
                        if(stages.containsKey(stageId)){
                            List temp=stages.get(stageId);
                            temp.addAll(stageinfo);                        
                        }else{
                            stages.put(stageId,stageinfo); //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}                        
                        }

                        printWflNode();

                        break;
                    }else if(stdout[i].startsWith("ERROR")){//all failed
                        failedAll(iplist,failedVMs,nodePortList);
                    }
                }

            }else{
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, output[2]);
                //all failed
                failedAll(iplist,failedVMs,nodePortList);
                
            }
        }
        
        return iplist;
    }
    
    private void failedAll(List iplist, List<String> failedVMs, List<String> nodePortList){
        for (String elem:nodePortList){//each elem is "type:port" or "type:port port" ...
            String []parts=elem.split(":");
            for(String z:parts[1].split(" ")){
                if(!(z.trim()).equals("")){
                    iplist.add("fail:"+parts[0]+":"+z);
                }
            }
            List active=VMActive.get(parts[0]);
            active.remove("dummyIp");//VM failed
            provisioningFailure(parts[0], "fail");//type, fail= add failure.at some point resources are decreased
            failedVMs.add(parts[0]); //add type to failures

        }
    }
    
    
    /**
     * This is used to reduce resources when VM provisioning fails. 
     * This is typical in private clouds where resources are very limited.
     * 
     * @param type VM type
     * @param status ok or fail
     */
    private synchronized void provisioningFailure(String type, String status){
        if(VMFails.containsKey(type)){
            if(status.equals("ok")){
                VMFails.put(type, 0); //reset value
            }else{
                int value=VMFails.get(type)+1;
                if(value == 3){//REDUCE ONE UNIT OF THIS VM in VMLimits
                    value=0;
                    int limit=VMLimits.get(type)-1;
                    VMLimits.put(type, limit);
                }
                VMFails.put(type, value);
            }
        }else{
            VMFails.put(type, 1);
        }        
    }
    
    /**
     * Terminate VM calling to the IP
     * 
     * @param VMIps
     */
    public void terminateVMs(List VMIps){
        String resources="";
        System.out.println("CWDEBUG: resourcecloud. vmips: "+StringUtils.join(VMIps.toArray(), ","));
        //StringUtils.join(VMIps.toArray(), ",");
        synchronized(this){
            for(int i=0;i<VMIps.size();i++){
                resources+=VMIds.get((String)VMIps.get(i))+",";
                VMIds.remove((String)VMIps.get(i));
            }
        }
        System.out.println("CWDEBUG: resourcecloud. terminating: "+resources);
        if(!resources.equals("")){
            resources=resources.substring(0, resources.length()-1);
        //VMActive is already clean (releaseWorkers)
        
        //call terminate VM with id=VMIds.get(VMIps.get(i))            
            String[]output=CommonUtils.execute(cloudScript+" --terminate " +resources +" --varfile "+providerConfigFile+ 
                " -r "+region+ " --cloud "+providerType, false);  
        }
        
    }
    
    
    @Override
    public boolean isStatusActive(String ipVM){
        for(String key:VMActive.keySet()){
            if(VMActive.get(key).contains(ipVM)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean isActive(String ipVM){
        boolean status=false;        

        if(isStatusActive(ipVM)){
            String cmd;            
            if(keyfile != null){
                cmd="ssh -i " + keyfile + " -q -oBatchMode=yes -o \"ConnectTimeout 5\" "+userWorker+"@" + ipVM + " uname";
            }else{
                cmd="ssh -q -oBatchMode=yes -o \"ConnectTimeout 5\" "+userWorker+"@" + ipVM + " uname";
            }            
            String[]output=CommonUtils.execute(cmd,false);
            if(output[0].equals("0")){
                status=true;
            }else{
                status=false;
            }
        }
        return status;
    }
    
    @Override
    public List<NodeAcct> generateAccountingInfo(String wflId, String stageId, long timestamp, List <NodeAcct> currentNodeAcct, String appName) {
        
        List<NodeAcct> nod=new ArrayList();
        
        HashMap <String, List<String>>stages = wflNode.get(wflId);
        if(stages!=null){
            if(stages.containsKey(stageId)){
                //workers={"ip:port","ip:port"}
                List<String> workers =stages.get(stageId);
                List<String> knownIps=new ArrayList();
                for(String work:workers){
                    String ip=work.split(":")[0];
                    if (!checkIfExistsNodeAcct(currentNodeAcct, ip)){
                        if(!knownIps.contains(ip)){
                            knownIps.add(ip);
                            String type=getTypebyIp(ip);
                            double cost=nodeList.get(type).getCost();
                            NodeAcct na=new NodeAcct(type, ip, timestamp, cost,appName);
                            nod.add(na);
                        }
                    }
                }
            }else{
                return new ArrayList();
            }
        }else{
            return new ArrayList();
        }
        
        return nod;
        
    }
}
