/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tassl.workflow.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import tassl.application.utils.CommonUtils;
import tassl.application.utils.RunningStat;
import tassl.application.workflow.WorkerForScheduler;

/**
 *
 * @author moustafa
 */
public class ResourceOSG extends Resource {

    String osgDirectory; //directory where jobs will be submitted from (contains all osg scripts and worker folder

    int wait_time = 3000;
    String default_osg_script;
    //We can specify a script per osg node type
    HashMap<String, String> osgScripts;

    boolean disabled = false;//flag to enable/disable site
    HashMap<String, List> osgActive; //{osg.small:{jobID,jobID},osg.medium:{jobID,jobID},osg.large:{jobID,jobID})
    HashMap<String, Integer> osgLimits; //{osg.small:10,osg.medium,:5;osg.large:3}
    HashMap<String, Integer> osgLimits_backup; //Used to restore after site is disabled {osg.small:10,osg.medium:5;osg.large:3}

    public ResourceOSG(String publicIp, String AgentMetricsAddress, int AgentMetricsPort, boolean useMeticsService, String dbName) {
        super(publicIp, AgentMetricsAddress, AgentMetricsPort, useMeticsService, dbName);
        osgScripts = new HashMap();
        osgActive = new HashMap();
        osgLimits = new HashMap();
        osgLimits_backup = new HashMap();
    }

    public boolean loadConfig(String file) {
        boolean status = true;
        Properties p = new Properties();
        try {
            p.load(new FileInputStream(file));
            osgDirectory = p.getProperty("defaultOsgDirectory");
            wait_time = Integer.parseInt(p.getProperty("osgWaitTime"));//for monitoring osg jobs
            osgLimits = string2HashMapInt(p.getProperty("osgLimits"));
            default_osg_script = p.getProperty("defaultOsgScript");
            fileTransferType = p.getProperty("fileTransferType", "osgSCP");
            if (default_osg_script == null ) {
                Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, "Please make sure that the parameter "
                        + "osgScript (to interact with condor) is defined.");
                return false;
            }

            //load common info and create nodelist
            status = super.loadConfig(file, "osg");
            //previous command fill out the supportedApps object
            //now we identify if there are custom scripts for each osg type(this is optional)
            for (String sa : nodeList.keySet()) {
                String value = p.getProperty(sa + "osgScript");//the osg script for a worker is called <workertype>osgScript
                if (value != null) {
                    osgScripts.put(sa, value);
                }
            }

            if (osgLimits.size() != this.nodeList.size()) {
                Logger.getLogger(ResourceOSG.class.getName()).log(Level.WARNING, "The number of fields in osgLimits has to be the same that in "
                        + "WorkerLimit, Cost, Perf are different. Please check the syntax.");
                return false;
            }

        } catch (IOException ex) {
            Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return status;
    }

    public String getDefaultOSGDirectory() {
        return osgDirectory;
    }


    public String getDefaultOSGScript() {
        return default_osg_script;
    }

    public String getOSGScriptByType(String type) {
        if (osgScripts.containsKey(type)) {
            return osgScripts.get(type);
        } else {
            return default_osg_script;
        }
    }

    public HashMap<String, String> getOSGScripts() {
        return osgScripts;
    }

    public HashMap<String, Integer> getOSGLimits() {
        return osgLimits;
    }

    public void setDefaultOSGDirectory(String dir) {
        this.osgDirectory = dir;
    }


    public void setDefaultOSGScript(String script) {
        this.default_osg_script = script;
    }

    public void setOSGScriptByType(String type, String script) {
        if (osgScripts.containsKey(type)) {
            osgScripts.remove(type);
            osgScripts.put(type, script);
        } else {
            osgScripts.put(type, script);
        }
    }

    public void setOSGScripts(HashMap<String, String> oScript) {
        this.osgScripts = oScript;
    }

    public void setOSGLimits(HashMap<String, Integer> Limits) {
        this.osgLimits = Limits;
    }

    public String getTypebyJobID(String jobID) {
        for (String key : osgActive.keySet()) {
            if (osgActive.get(key).contains(jobID)) {
                return key;
            }
        }
        return null;
    }

    @Override
    public void startAgentToWorkerTunnel(String remoteport, String jobID) {
        int localport;
        boolean success = false;
        int status;
        List avoidPorts = new ArrayList();
        while (!success) {
            localport = this.getCandidateTunnelPort(avoidPorts);
            if (localport == 0) {
                Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, "There is no available ports to start SSH tunnel from Agent to Worker");
                success = true;//if no ports then we try to use it without it
            } else {
                status = OSGJobHandler.setupTunnel(jobID, localport, remoteport);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (status == 0) {
                    success = true;
                    portMapWorkers.put(jobID + ":" + remoteport, localport);
                    usedPorts.add(localport);
                } else {
                    avoidPorts.add(localport);//if fails, we temporarily avoid using this port
                }
            }
        }
    }

    public void startTunnelsWorker(String jobID) {
        OSGJobHandler.setupReverseTunnel(jobID, taskServicePort, taskServicePort);
        OSGJobHandler.setupReverseTunnel(jobID, mgmtServicePort, mgmtServicePort);
        OSGJobHandler.setupReverseTunnel(jobID, fileServerPort, fileServerPort);
    }

    @Override
    public List startWorkers(List<String> nodeinfo, String wflId, String stageId, String appname, String properties, List<Double> performance,
            String tasks, List<String> types) {
        //properties not used for now.
        List downWorkers = new ArrayList();
        //{"jobID:port","jobID:port","jobID:port","fail:type:port"} // nodeinfo and downNodes

        boolean leave;
        int maxTries = 3;
        int tries = 0;
        String classpath = softwareDirWorker + "/" + appname + "/dist/*:" + softwareDirWorker + "/" + appname + "/lib/*";
        for (int i = 0; i < nodeinfo.size(); i++) {
            if (nodeinfo.get(i).startsWith("fail")) {
                downWorkers.add(nodeinfo.get(i));
            } else {

                leave = false;
                String propertyfile = nodeinfo.get(i).replace(":", "_") + ".properties";
                double perf = performance.get(i);
                String type = "";
                if (types != null) {
                    type = types.get(i); //osg case, where we have a type
                }

                createWorkerProperties(propertyfile, wflId, stageId, appname, supportedApps.get(appname), properties, perf, tasks, type, fileTransferType);

                String[] parts = nodeinfo.get(i).split(":");

                //launch ssh tunnels to allow workers communicate with Agent (needed in OSG)
                if (startSSHtunnels) {
                    this.startTunnelsWorker(parts[0]);
                }

                //launch ssh tunnel to allow Agent communicate with worker (needed in OSG)
                if (this.startSSHtunnelAgent) {
                    String remoteaddress = parts[0];
                    String remoteport = parts[1];
                    this.startAgentToWorkerTunnel(remoteport, remoteaddress);
                }
                //create working directory if it was not there    
                OSGJobHandler.executeSSHCmd(parts[0], " -C \"mkdir -p " + workingDir + " \"");

                if (!OSGJobHandler.transferFileToJob(parts[0], propertyfile, workingDir, fileTransferType)[0].equals("0") || !OSGJobHandler.transferFileToJob(parts[0], workerBasePropertyFile, workingDir, fileTransferType)[0].equals("0")) {
                    Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, "Worker properties were not transfered. Please make sure that SCP OR RSYNC is installed in both the Agent and Woker machines.");
                }

                String basePropertiesName = new File(workerBasePropertyFile).getName();
                String logfile = workingDir + "/" + propertyfile + ".out";
                //start worker
                //Moustafa - added workerBaseDir parameter to account for multiple users running comet on the same machine
                OSGJobHandler.executeSSHCmd(parts[0], " -C \"nohup java -Xms256m -Xmx1024m -cp " + classpath
                        + " tassl.application.node.isolate.CloudBurstStarter -propertyFile " + workingDir + "/" + propertyfile
                        + " -propertyFile " + workingDir + "/" + basePropertiesName
                        + " -workerBaseDir " + workingDir + " -port " + parts[1] + " -publicIp " + parts[0] + " > " + logfile + " 2>&1 & \" ");

                try {
                    Thread.sleep(wait_time);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, null, ex);
                }

                tries = 0;
                //we can check if the worker is active. 
                String workerToCheck = nodeinfo.get(i);
                do {
                    String workerstatus = checkWorkerStatus(parts[0], Integer.parseInt(parts[1]));
                    Logger.getLogger(ResourceOSG.class.getName()).log(Level.INFO, "Status of Worker " + parts[0] + ":" + parts[1] + " is " + workerstatus);

                    //if it is not active, try again with a different port. if port changes, we need to update wflnode
                    if (workerstatus.equals("down")) {
                        Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, "DOWN!!!");

                        tries++;
                        if (tries == maxTries) {
                            //add node to list of down nodes
                            downWorkers.add(workerToCheck);
                            Logger.getLogger(ResourceOSG.class.getName()).log(Level.INFO, "Adding Worker " + parts[0] + ":" + parts[1] + " to the down failure list");
                            leave = true;

                        } else {
                            try {
                                Thread.sleep(wait_time);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    } else {//worker is OK
                        leave = true;
                    }

                } while (!leave);

                CommonUtils.execute("rm -f " + propertyfile, false);
            }
        }
        return downWorkers;
    }

    @Override
    protected void terminateWorker(String address, int port, boolean force, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning) {
        if (useMeticsService) {
            //currently we assume a worker started when its hosting osg job started

            NodeAcct na = findNodeAcctIP(nodeAcctList, address);
            long startTimeStamp = -1L;
            String appName = "unknown";
            String osgtype = "unknown";
            if (na != null) {
                startTimeStamp = na.getStartTime();
                appName = na.getAppName();
                osgtype = na.getType();
            }

            long end = System.currentTimeMillis();
            HashMap<String, String> additionalValues = new HashMap<String, String>(); //values custom to different resources
            additionalValues.put("osgScript", this.getOSGScriptByType(osgtype));

            if (!provisioning) {
                List param = new ArrayList(Arrays.asList(osgtype, startTimeStamp, end, appName, this.getClass().getSimpleName(), failure, additionalValues));
                this.sendInfo(this.AgentMetricsAddress, this.AgentMetricsPort, "dbWriteAgentReleaseWorker", param);
            }

        }
        if (!failure) {
            this.sendTerminateWorkerSignal(address, port, force);
        }
    }

    @Override
    public List releaseWorkers(String workflowId, String stageId, List<NodeAcct> nodeAcctList, boolean failure) {
        List allworkers;
        List terminatedworkers = new ArrayList();
        HashMap stages = wflNode.get(workflowId);
        System.out.println("CWDEBUG. resourceosg. Before release");
        printWflNode();
        printOSGactive();
        if (stages != null) {
            allworkers = (List) stages.get(stageId);
            if (allworkers != null) {
                System.out.println("CWDEBUG: resourceosg. stage size:" + allworkers.size());

                for (int i = 0; i < allworkers.size(); i++) {
                    System.out.println("CWDEBUG: resourceosg. nodename:" + allworkers.get(i));
                    String[] parts = ((String) allworkers.get(i)).split(":");
                    String jobID = parts[0];
                    int port = Integer.parseInt(parts[1]);

                    terminatedworkers.add((String) allworkers.get(i));

                    //ask worker to end                        
                    terminateWorker(jobID, port, true, nodeAcctList, failure, false); //releaseWorkers is never called when provisioning new resources

                    //remove it from osgActive                    
                    releaseMachine(jobID, nodeAcctList, failure);
                }
            }
            stages.remove(stageId); //remove stage info
            if (stages.isEmpty()) {
                wflNode.remove(workflowId);
            }
        } else {
            System.out.println("CWDEBUG: resourceosg. workflow not registered");
        }

        System.out.println("CWDEBUG. resourceosg. after release");

        return terminatedworkers;
    }

    @Override
    public List releaseSomeWorkers(String workflowId, String stageId, List<String> workers, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning) {
        List nodeNames;
        List types = new ArrayList();
        HashMap stages = wflNode.get(workflowId);
        System.out.println("CWDEBUG. resourceosg. Before releaseSomeWorkers");
        printWflNode();
        printOSGactive();
        if (stages != null) {
            nodeNames = (List) stages.get(stageId);
            for (String work : (List<String>) workers) {
                String[] parts = work.split(":");
                String jobID = parts[0];
                int port = Integer.parseInt(parts[1]);
                //ask worker to end
                terminateWorker(jobID, port, true, nodeAcctList, failure, provisioning);

                if (nodeNames != null) {
                    nodeNames.remove(work);
                }
                types.add(getTypebyJobID(jobID));//add type of osg node
            }
        } else {
            System.out.println("CWDEBUG: resourceosg. releaseSomeWorkers.  workflow not registered");
        }

        System.out.println("CWDEBUG. resourceosg. releaseSomeWorkers.  after releaseSomeWorkers");
        return types;
    }

    @Override
    public List releaseSomeWorkersbyMachineIp(String workflowId, String stageId, List<String> machineIps, List<NodeAcct> nodeAcctList, boolean failure) {
        List<String> terminatedworkers = new ArrayList<>();
        List<String> allworkers;

        //wkfId:{stageid:{"jobID:port","jobID:port"},stageid:{"jobID:port","jobID:port"}}
        HashMap stages = wflNode.get(workflowId);
        System.out.println("CWDEBUG. resourceosg. Before releaseSomeWorkersbyMachineIp");
        printWflNode();
        printOSGactive();
        if (stages != null) {
            allworkers = (List) stages.get(stageId);
            if (allworkers != null) {
                for (int i = allworkers.size() - 1; i >= 0; i--) {
                    String worker = allworkers.get(i);
                    String[] parts = worker.split(":");
                    String jobID = parts[0];
                    int port = Integer.parseInt(parts[1]);
                    if (machineIps.contains(jobID)) {
                        allworkers.remove(i);//remove from data structure wflNode                        
                        terminatedworkers.add(worker);
                        //ask worker to end                        
                        terminateWorker(jobID, port, true, nodeAcctList, failure, false); //releaseSomeWorkersbyMachineIp is never called when provisioning new resources
                    }
                }
                //remove nodes from data structures - later we call terminateVM
                for (String addr : machineIps) {
                    releaseMachine(addr, nodeAcctList, failure);
                }
            }
        } else {
            System.out.println("CWDEBUG: resourceosg.releaseSomeWorkersbyMachineIp. workflow not registered");
        }

        return terminatedworkers;

    }

    @Override
    public List allocateStage(String wflId, String stageId, List<String> MachineWorkers, String properties) {
        List nodePortList = new ArrayList();

        if (properties.equals("exact")) { //allocate nodes in the list
            //"OSGtype:numWorkers","OSGtype:numWorkers",...
            for (int z = 0; z < MachineWorkers.size(); z++) {
                String elem = MachineWorkers.get(z);
                String[] parts = elem.split(":");
                String nodename = parts[0];
                int numWorkers = Integer.parseInt(parts[1]);
                List activeType = osgActive.get(nodename);
                if (activeType == null) {
                    activeType = new ArrayList();
                    osgActive.put(nodename, activeType);
                }
                int totalAvail = osgLimits.get(nodename) - activeType.size();

                if (totalAvail == 0) {//the scheduling is wrong
                    //Undo
                    for (int j = 0; j < z; j++) {
                        String[] partsT = (MachineWorkers.get(j)).split(":");
                        osgActive.get(partsT[0]).remove("dummyIp");
                    }
                    return new ArrayList();
                }

                //add a value to reserve one VM of this type
                activeType.add("dummyIp");

                Node n = nodeList.get(nodename);
                int port = n.getMinPort();
                String portlist = "";
                for (int i = 0; i < numWorkers; i++) {
                    portlist += (port + i) + " ";
                }
                portlist = portlist.trim();
                //i.e. osg.small:7777 7778
                nodePortList.add(nodename + ":" + portlist);

            }

        } else if (properties.equals("random")) { //allocate any nodes
            int workersToget = MachineWorkers.size();
            //HashMap <String,Integer>VMLimits; //{osg.small:10,osg.medium:5;osg.large:3}
            //HashMap <String,List>VMActive; //{osg.small:{jobID,jobID},osg.medium:{jobID,jobID},osg.large:{jobID,jobID})
            //protected HashMap <String,Node> nodeList; //jobID:node or osgtype:node
            //protected HashMap <String,HashMap<String,List>> wflNode; //wkfId:{stageid:{"jobID:port","jobID:port"},stageid:{"jobID:port","jobID:port"}}
            for (String nodename : nodeList.keySet()) {
                List activeType = osgActive.get(nodename);
                if (activeType == null) {
                    activeType = new ArrayList();
                    osgActive.put(nodename, activeType);
                }
                int totalAvail = osgLimits.get(nodename) - activeType.size();
                Node n = nodeList.get(nodename);
                for (int j = 0; j < totalAvail; j++) {
                    int portsNeeded = 0;
                    if (n.getTotalWorkers() > workersToget) {
                        portsNeeded = workersToget;
                        workersToget = 0;
                    } else {
                        workersToget -= n.getTotalWorkers();
                        portsNeeded = n.getTotalWorkers();
                    }
                    //add a value to reserve one osg job of this type
                    activeType.add("dummyIp");

                    int port = n.getMinPort();
                    String portlist = "";
                    for (int i = 0; i < portsNeeded; i++) {
                        portlist += (port + i) + " ";
                    }
                    portlist = portlist.trim();
                    //i.e. osg.small:7777 7778
                    nodePortList.add(nodename + ":" + portlist);
                    if (workersToget == 0) {
                        break;
                    }
                }
                if (workersToget == 0) {
                    break;
                }
            }
        }

        //this value is temporal and it will be replaced in deployVM
        HashMap<String, List<String>> stages;
        if (!wflNode.containsKey(wflId)) {
            stages = new HashMap();
            //stages.put(stageId, nodesForwflNode);
            wflNode.put(wflId, stages);
        }
        printWflNode();

        return nodePortList;
    }

    @Override
    public boolean checkAllocatePossibleStage(int numWorkers, String properties) {
        printOSGactive();
        HashMap<String, List> VMActiveClone = (HashMap) osgActive.clone(); //because we just want to check
        int workersToget = numWorkers;
        System.out.println("CWDEBUG: resourceosg. check if stage can be scheduled");
        if (properties.equals("")) { //check random nodes
            for (String nodename : nodeList.keySet()) {
                System.out.println("CWDEBUG: resourceosg. nodename: " + nodename);
                List activeType = VMActiveClone.get(nodename);
                if (activeType == null) {
                    activeType = new ArrayList();
                    VMActiveClone.put(nodename, activeType);
                }
                System.out.println("CWDEBUG: resourceosg. active type size:" + activeType.size());
                System.out.println("CWDEBUG: resourceosg. osg limits:" + osgLimits.get(nodename));

                int totalAvail = osgLimits.get(nodename) - activeType.size();
                Node n = nodeList.get(nodename);
                for (int j = 0; j < totalAvail; j++) {
                    System.out.println("CWDEBUG: resourceosg. total workers:" + n.getTotalWorkers());
                    if (n.getTotalWorkers() > workersToget) {
                        workersToget = 0;
                    } else {
                        workersToget -= n.getTotalWorkers();
                    }

                    activeType.add("dummyIp"); //add a value to reserve one osg job of this type
                    if (workersToget == 0) {
                        break;
                    }
                }
                if (workersToget == 0) {
                    break;
                }
            }
        }
        return workersToget == 0;
    }

    @Override
    public boolean isActive(String jobID) {
        for (String key : osgActive.keySet()) {
            if (osgActive.get(key).contains(jobID)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isStatusActive(String jobID) {
        if (isStatusActive(jobID)) {
            return OSGJobHandler.canSSH(jobID) == 0;
        }
        return false;
    }

    @Override
    public List getAvailableSlotsScheduling(String siteAddress, String[] allocatedWorkers, double refBenchScore, HashMap<String, RunningStat> defaultAppExec, double defaultReferenceBenchmark) {
        //workers is {worker:1, worker:1}, the number could be greater than 1

        //ADD to each slot
        //DONE HashMap<String,RunningStat>defaultAppExec,double defaultReferenceBenchmark
        //DONE calculate Performance using the reference
        //DONE remove getPerformance from WorkerForScheduler (slot) and from Node
        //DONE add getExecTime(appname,time) (slot)
        List nodesForSched = new ArrayList();

        //this is to store the number of slots allocated of each type. This is used  to initialize i in the totalAvail loop
        HashMap<String, Integer> slotsType = new HashMap();

        String[] wkPart;
        //add first the allocated workers
        for (String wk : allocatedWorkers) {
            if (!wk.trim().isEmpty()) {
                wkPart = wk.split(":");
                //FOR SIMPLICITY WE DO NOT RESTART WORKERS THAT FAILED IN A SLOT i.e. if workerOSG is = 2 for a particular type, and there is only 1 in the 
                //list of allocated nodes, then only one slot is created.
                int allocated = 0;
                if (slotsType.containsKey(wkPart[0])) {
                    allocated = slotsType.get(wkPart[0]).intValue();
                    allocated++;
                    slotsType.put(wkPart[0], allocated);
                } else {
                    slotsType.put(wkPart[0], 0);
                }

                for (int j = 0; j < Integer.parseInt(wkPart[1]); j++) {
                    double benchScore = this.nodeList.get(wkPart[0]).getBenchScore();
                    WorkerForScheduler wfs = new WorkerForScheduler(siteAddress, this.identifier, this.zone, wkPart[0], allocated, j, 0, 0, defaultAppExec, defaultReferenceBenchmark, benchScore);
                    wfs.setCost(this.nodeList.get(wkPart[0]).getCost());

                    wfs.setPerformance(benchScore / refBenchScore);

                    nodesForSched.add(wfs);
                    System.out.println("CWDEBUG: getAvailableSlotsScheduling - previously allocated - OSG SLOT:" + wfs.toString());
                }
            }
        }

        System.out.println("CWDEBUG:resourceosg. nodelist= " + nodeList.keySet().toString());
        System.out.println("CWDEBUG:resourceosg. osgLimit= " + osgLimits.toString());
        System.out.println("CWDEBUG:resourceosg. osgActive= " + osgActive.toString());

        //jobID:node or osgType:node
        for (String type : this.nodeList.keySet()) {
            int max = osgLimits.get(type);
            int used = 0;
            List active = osgActive.get(type);
            if (active != null) {
                used = active.size();
            }
            int totalAvail = max - used;
            int workerVM = this.nodeList.get(type).getTotalWorkers();

            int addTotalAvail = 0;
            if (slotsType.containsKey(type)) {
                addTotalAvail = slotsType.get(type) + 1;//because it starts in 0. then if it is 1, it means that there are slot 0 and slot 1.
            }

            for (int i = addTotalAvail; i < totalAvail + addTotalAvail; i++) {
                for (int j = 0; j < workerVM; j++) {
                    double benchScore = this.nodeList.get(type).getBenchScore();
                    WorkerForScheduler wfs = new WorkerForScheduler(siteAddress, this.identifier, this.zone, type, i, j, this.getOverhead(), this.getOverhead(), defaultAppExec, defaultReferenceBenchmark, benchScore);
                    wfs.setCost(this.nodeList.get(type).getCost());
                    wfs.setPerformance(benchScore / refBenchScore);
                    nodesForSched.add(wfs);
                    System.out.println("CWDEBUG: getAvailableSlotsScheduling - OSG SLOT:" + wfs.toString());
                }
            }

        }

        return nodesForSched;
    }

    @Override
    public List<NodeAcct> generateAccountingInfo(String wflId, String stageId, long timestamp, List<NodeAcct> currentNodeAcct, String appName) {
        List<NodeAcct> nod = new ArrayList();

        HashMap<String, List<String>> stages = wflNode.get(wflId);
        if (stages != null) {
            if (stages.containsKey(stageId)) {
                //workers={"jobID:port","jobID:port"}
                List<String> workers = stages.get(stageId);
                List<String> knownIps = new ArrayList();
                for (String work : workers) {
                    String jobID = work.split(":")[0];
                    if (!checkIfExistsNodeAcct(currentNodeAcct, jobID)) {
                        if (!knownIps.contains(jobID)) {
                            knownIps.add(jobID);
                            String type = getTypebyJobID(jobID);
                            double cost = nodeList.get(type).getCost();
                            NodeAcct na = new NodeAcct(type, jobID, timestamp, cost, appName);
                            nod.add(na);
                        }
                    }
                }
            } else {
                return new ArrayList();
            }
        } else {
            return new ArrayList();
        }

        return nod;
    }

    @Override
    public void disableSite() {
        //if the site is active
        if (!disabled) {
            //clean backup just in case
            osgLimits_backup.clear();
            //create backup and set to 0
            for (String keyS : osgLimits.keySet()) {
                //create backup
                osgLimits_backup.put(keyS, osgLimits.get(keyS));
                //set to 0
                osgLimits.put(keyS, 0);
            }
            disabled = true;
        }
    }

    @Override
    public void restoreSite() {
        if (disabled) {
            for (String keyS : osgLimits.keySet()) {
                //restore value
                osgLimits.put(keyS, osgLimits_backup.get(keyS));
            }
        }
    }

    @Override
    public void releaseMachine(String jobID, List<NodeAcct> nodeAcctList, boolean failure) {
        String osgType = "unknown";
        boolean found = false;
        for (String key : osgActive.keySet()) {
            if (osgActive.get(key).contains(jobID)) {
                osgActive.get(key).remove(jobID);
                osgType = key;
                found = true;
                break;
            }
        }
        if (found && (useMeticsService)) {
            NodeAcct na = findNodeAcctIP(nodeAcctList, jobID);
            long startTimeStamp = -1L;
            String appName = "unknown";
            if (na != null) {
                startTimeStamp = na.getStartTime();
                appName = na.getAppName();
            }

            long end = System.currentTimeMillis();
            HashMap<String, String> additionalValues = new HashMap<String, String>(); //values custom to different resources
            additionalValues.put("osgScript", this.getOSGScriptByType(osgType));
            List param = new ArrayList(Arrays.asList(osgType, startTimeStamp, end, appName, this.getClass().getSimpleName(), failure, additionalValues));
            this.sendInfo(this.AgentMetricsAddress, this.AgentMetricsPort, "dbWriteAgentReleaseMachine", param);
        }

    }

    public void printOSGactive() {
        System.out.println("CWDEBUG:resourceosg. printosgActive");
        for (String type : osgActive.keySet()) {
            System.out.print(type + ":{");
            List nodeNames = (List) osgActive.get(type);
            for (int i = 0; i < nodeNames.size(); i++) {
                System.out.print(nodeNames.get(i) + ",");
            }
            System.out.print("}");
        }
    }

    public void printWflNode() {
        System.out.println("CWDEBUG:resourceosg. printwflnode");
        HashMap<String, List<String>> stages;
        for (String wk : wflNode.keySet()) {
            System.out.print(wk + ":{");
            stages = wflNode.get(wk);
            for (String st : stages.keySet()) {
                System.out.print(st + ":{");
                List nodeNames = (List) stages.get(st);
                for (int i = 0; i < nodeNames.size(); i++) {
                    System.out.print(nodeNames.get(i) + ",");
                }
                System.out.print("}");
            }
            System.out.print("},");
        }
        System.out.println("");
    }

    public List deployOSGJobs(String wflId, String stageId, List<String> nodePortList, String appname, Object synresource, List<String> failedVMs) {
        List jobIDlist = new ArrayList();
        //HashMap <String,List>VMActive; //{osg.small:{jobID,jobID},osg.medium:{jobID,jobID},osg.large:{jobID,jobID})
        //protected HashMap <String,HashMap<String,List>> wflNode; //wkfId:{stageid:{"jobID:port","jobID:port"},stageid:{"jobID:port","jobID:port"}}

        List stageinfo = new ArrayList();
        System.out.println("CWDEBUG: resourceosg. nodeportlist.size " + nodePortList.size());
        for (int i = 0; i < nodePortList.size(); i++) {
            String elem = (String) nodePortList.get(i);
            String type = elem.split(":")[0];
            String ports = elem.split(":")[1];
            //submit OSG job
            String jobID = OSGJobHandler.submitSSHJob(osgDirectory, this.getOSGScriptByType(type), wait_time);

            for (String z : ports.split(" ")) {
                if (!(z.trim()).equals("")) {
                    jobIDlist.add(jobID + ":" + z);
                    stageinfo.add(jobID + ":" + z);
                }
            }
            List active = osgActive.get(type);
            active.remove("dummyIp");
            active.add(jobID);

        }
        //update workflonode                                
        
        HashMap<String, List<String>> stages;
        stages = wflNode.get(wflId);
        if (stages == null) {
            stages = new HashMap();
            wflNode.put(wflId, stages);
        }
        if (stages.containsKey(stageId)) {
            List temp = stages.get(stageId);
            temp.addAll(stageinfo);
        } else {
            stages.put(stageId, stageinfo); //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}                        
        }
        printWflNode();
        return jobIDlist;
    }

    public void terminateOSGJobs(List jobIDs) {
        System.out.println("CWDEBUG: resourceosg. jobIDs: " + StringUtils.join(jobIDs.toArray(), ","));
        for (int i = 0; i < jobIDs.size(); i++) {
            String jobID = (String) jobIDs.get(i);
            System.out.println("CWDEBUG: resourceosg. terminating: " + jobID);
            OSGJobHandler.cancelJob(jobID);
        }
    }

    public List getOSGJobIDbyType(String workflowId, String stageId, List<String> osgTypes) {
        //{address,address}
        List<String> jobIDs = new ArrayList();

        List<String> allocatedWorkers = wflNode.get(workflowId).get(stageId);
        for (String type : osgTypes) {
            if (allocatedWorkers != null) {
                for (int i = 0; i < allocatedWorkers.size(); i++) {
                    String job = allocatedWorkers.get(i).split(":")[0];
                    if (osgActive.get(type).contains(job)) {
                        jobIDs.add(job);
                        break;
                    }
                }
            }
        }
        if (osgTypes.size() != jobIDs.size()) {
            System.out.println("CWDEBUG: ERROR: the number of JobIDs is different to the number of osg types");
            Logger.getLogger(ResourceOSG.class.getName()).log(Level.SEVERE, "ERROR: the number of JobIDs is different to the number of osg types");
        }
        return jobIDs;
    }

    public int numberWorkersinJob(String workflowId, String stageId, String jobID) {
        int count = 0;
        List<String> wflNodeIps = wflNode.get(workflowId).get(stageId);
        String[] workName = jobID.split(":");
        for (String one : wflNodeIps) {
            if (one.startsWith(workName[0] + ":")) {
                count++;
            }
        }
        return count;
    }
}
    //TODO: Might need to override releasePortMapWorker, releaseTunnelsWorker, 
