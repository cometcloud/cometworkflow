/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package tassl.workflow.resources;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.utils.CommonUtils;
import tassl.application.utils.RunningStat;

/**
 *
 * @author Javier Diaz-Montes
 */
public abstract class Resource implements Serializable {

    protected String publicIp; //publicIP or fqdn
    protected String identifier; //name we give site
    protected String zone;
    double overhead; //Time in seconds     
    protected HashMap<String, Node> nodeList; //ip/name:node or vmtype:node
    protected HashMap<String, HashMap<String, List<String>>> wflNode; //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
    protected HashMap<String, String> supportedApps; //appname:workerclasspath
    protected String softwareDirWorker;
    protected String userWorker;
    protected String workerBasePropertyFile;
    protected String workingDir;
    protected String keyfile;
    protected String fileTransferType;//default or osgSCP or osgRsync

    double costDataIn; //cost transfering data inside (MB)
    double costDataOut; //cost transfering data outside (MB)

    //start ssh tunnels (reverse) for the workers to talk with the Agent 
    boolean startSSHtunnels;
    int taskServicePort;
    int mgmtServicePort;
    int fileServerPort;

    //start ssh tunnels for the Agent to talk with workers
    boolean startSSHtunnelAgent;
    int[] agentSSHTunnelPortRange;//range of ports [min,max]
    List<Integer> usedPorts;
    // This is to do the worker map from remote Ip and port to localhost and local port. Format is {"ip:port":localport, "ip:port":localport}
    HashMap<String, Integer> portMapWorkers;

    String AgentMetricsAddress;
    int AgentMetricsPort;
    String dbName;
    boolean useMeticsService;

    public Resource(String publicIp, String AgentMetricsAddress, int AgentMetricsPort, boolean useMeticsService, String dbName) {
        usedPorts = new ArrayList<>();
        nodeList = new HashMap<>();
        supportedApps = new HashMap<>();
        wflNode = new HashMap<>();
        portMapWorkers = new HashMap<>();
        startSSHtunnels = false;
        startSSHtunnelAgent = false;

        this.publicIp = publicIp;
        this.AgentMetricsAddress = AgentMetricsAddress;
        this.AgentMetricsPort = AgentMetricsPort;
        this.dbName = dbName;
        this.useMeticsService = useMeticsService;

    }

    public String getAgentMetricsAddress() {
        return AgentMetricsAddress;
    }

    public int getAgentMetricsPort() {
        return AgentMetricsPort;
    }

    public String getDbName() {
        return dbName;
    }

    public boolean isUseMeticsService() {
        return useMeticsService;
    }

    public boolean loadConfig(String file, String type) {

        Properties p = new Properties();
        try {
            p.load(new FileInputStream(file));

            identifier = p.getProperty("Name");
            if (identifier == null) {
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Please indicate the name of the site in the site config file. "
                        + "Use Name=siteName");
                return false;
            } else {
                identifier = identifier.trim();
            }
            zone = p.getProperty("Zone");
            if (zone == null) {
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Please indicate the zone of the site  in the site config file. "
                        + "Use Zone=siteZone");
                return false;
            } else {
                zone = zone.trim();
            }
            keyfile = p.getProperty("key"); //it can be null if not key is needed
            overhead = Double.parseDouble(p.getProperty("Overhead"));

            String supapps = p.getProperty("SupportedApps");
            if (supapps == null) {
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Please indicate the supported applications in the site config file."
                        + " Use SupportedApps=appname1,appname1");
                return false;
            }
            List<String> appsnames = Arrays.asList(supapps.split(","));
            for (String i : appsnames) {
                String classname = p.getProperty(i);
                if (classname != null) {
                    supportedApps.put(i, classname);
                } else {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Please indicate the class of the applications."
                            + " application " + i + " class is missing. Use appname=package.path.to.class");
                    return false;
                }
            }

            //for clusters should be 0
            costDataIn = Double.parseDouble(p.getProperty("CostDataIn", "0.0"));
            costDataOut = Double.parseDouble(p.getProperty("CostDataOut", "0.0"));

            HashMap worklimit = string2HashMapInt(p.getProperty("WorkerLimit"));
            HashMap cost = string2HashMapDouble(p.getProperty("Cost"));
            HashMap benchScore = string2HashMapDouble(p.getProperty("BenchmarkScore"));
            String[] workerPortRange = p.getProperty("workerPortRange", "8888:8999").split(":");
            softwareDirWorker = p.getProperty("SoftwareDirWorker");
            userWorker = p.getProperty("UserWorker", "`whoami`");
            workingDir = p.getProperty("WorkingDir", "/tmp/");

            if (softwareDirWorker == null) {
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Software dir is missing. Please indiacate using SoftwareDirWorker=");
                return false;
            }

            int minPort, maxPort;
            if (workerPortRange.length != 2) {
                Logger.getLogger(Resource.class.getName()).log(Level.WARNING, "Wrong Input.Using default Worker port range(8888:8999)");
                minPort = 8888;
                maxPort = 8999;
            } else {
                minPort = Integer.parseInt(workerPortRange[0]);
                maxPort = Integer.parseInt(workerPortRange[1]);
            }
            if (worklimit.size() != cost.size() && cost.size() != benchScore.size()) {
                Logger.getLogger(Resource.class.getName()).log(Level.WARNING, "The number of fields in WorkerLimit, "
                        + "Cost, BenchScore are different. Please check the syntaxis. "
                        + "Remember we no longer support a static speedup using Perf field, now we use a benchmark score in the field BenchScore.");
                return false;
            }
            for (Iterator it = worklimit.keySet().iterator(); it.hasNext();) {
                String val = (String) it.next();
                Node n = new Node((Integer) worklimit.get(val), (Double) cost.get(val), (Double) benchScore.get(val));
                n.setName(val);
                if (type.equals("cluster")) {
                    n.setIsUp(true);
                } else {
                    n.setIsUp(false);
                }
                n.setMinPort(minPort);
                n.setMaxPort(maxPort);
                nodeList.put(val, n);
            }

        } catch (IOException ex) {
            Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Convert a string to HashMap
     *
     * @param values The format is m1.small:10;m1.medium:5;m1.large:3 or
     * ip:2;ip:2 or etc
     * @return HashMap<String,Integer>
     */
    public HashMap<String, Integer> string2HashMapInt(String values) {
        String[] part = values.split(";");
        HashMap<String, Integer> table = new HashMap<>();
        for (String j : part) {
            String[] v = j.split(":");
            table.put(v[0], Integer.parseInt(v[1]));
        }
        return table;
    }

    /**
     * Convert a string to HashMap
     *
     * @param values The format is m1.small:10;m1.medium:5;m1.large:3 or
     * ip:2;ip:2 or etc
     * @return HashMap<String,Double>
     */
    public HashMap<String, Double> string2HashMapDouble(String values) {
        String[] part = values.split(";");
        HashMap<String, Double> table = new HashMap<>();
        for (String j : part) {
            String[] v = j.split(":");
            table.put(v[0], Double.parseDouble(v[1]));
        }
        return table;
    }

    public HashMap<String, String> getSupportedApps() {
        return supportedApps;
    }

    public String getPublicIp() {
        return publicIp;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getZone() {
        return zone;
    }

    public String getKeyfile() {
        return keyfile;
    }

    public double getOverhead() {
        return overhead;
    }

    public String getUserWorker() {
        return userWorker;
    }

    public double getCostDataIn() {
        return costDataIn;
    }

    public double getCostDataOut() {
        return costDataOut;
    }

    public String getFileTransferType() {
        return fileTransferType;
    }

    public String getWorkerBasePropertyFile() {
        return workerBasePropertyFile;
    }

    /**
     * Return nodes assigned to each workflow
     *
     * @return HashMap
     * //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
     */
    public HashMap<String, HashMap<String, List<String>>> getWflNode() {
        return wflNode;
    }

    /**
     * Return the workers allocated to a specific stage workflow
     *
     * @param workflowId
     * @param stageId
     * @return List of workers (address:port) or empty list if it does not exist
     */
    public List<String> getWorkersWorkflowStage(String workflowId, String stageId) {
        HashMap<String, List<String>> stages = wflNode.get(workflowId);
        if (stages != null) {
            if (stages.containsKey(stageId)) {
                return stages.get(stageId);
            } else {
                return new ArrayList();
            }
        } else {
            return new ArrayList();
        }

    }

    /**
     * Return the list of nodes of this site in the format "name or ip": Node
     *
     * @return HashMap //name:node
     */
    public HashMap<String, Node> getNodeList() {
        return nodeList;
    }

    public void setStartSSHtunnels(boolean startSSHtunnels) {
        this.startSSHtunnels = startSSHtunnels;
    }

    public void setStartSSHtunnelAgent(boolean value) {
        this.startSSHtunnelAgent = value;
    }

    public void setAgentSSHTunnelPortRange(int[] portRange) {
        this.agentSSHTunnelPortRange = portRange;
    }

    /**
     * Return a port to use for the tunnel from Agent to Worker
     *
     * @return
     */
    protected int getCandidateTunnelPort(List avoidPorts) {
        int port = 0;

        for (int i = agentSSHTunnelPortRange[0]; i < agentSSHTunnelPortRange[1]; i++) {
            if (!usedPorts.contains(i) && !avoidPorts.contains(i)) {
                return i;
            }
        }
        return port;

    }

    public void setPorts(int taskServicePort, int mgmtServicePort, int fileServerPort) {
        this.taskServicePort = taskServicePort;
        this.mgmtServicePort = mgmtServicePort;
        this.fileServerPort = fileServerPort;
    }

    public void setNodeList(HashMap<String, Node> nodeList) {
        this.nodeList = nodeList;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setKeyfile(String keyfile) {
        this.keyfile = keyfile;
    }

    public void setSupportedApps(HashMap<String, String> supportedApps) {
        this.supportedApps = supportedApps;
    }

    public void setOverhead(double overhead) {
        this.overhead = overhead;
    }

    public void setWorkerBasePropertyFile(String workerBasePropertyFile) {
        this.workerBasePropertyFile = workerBasePropertyFile;
    }

    public void startAgentToWorkerTunnel(String remoteport, String remoteaddress) {
        int localport;
        boolean success = false;
        int status;
        String addkey = "";
        if (keyfile != null) {
            addkey = " -i " + keyfile;
        }
        List avoidPorts = new ArrayList();
        while (!success) {
            localport = this.getCandidateTunnelPort(avoidPorts);
            if (localport == 0) {
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "There is no available ports to start SSH tunnel from Agent to Worker");
                success = true;//if no ports then we try to use it without it
            } else {
                CommonUtils.execute("ssh " + addkey + " -f " + userWorker + "@" + remoteaddress + " -L " + localport + ":" + remoteaddress + ":" + remoteport + " -N", true);

                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }

                String[] run = CommonUtils.execute("ps ux | grep \"ssh " + addkey + " -f " + userWorker + "@" + remoteaddress + " -L " + localport + ":" + remoteaddress + ":" + remoteport + " -N\" | wc -l", false);

                if (run[1].trim().equals("2")) {
                    success = true;
                    portMapWorkers.put(remoteaddress + ":" + remoteport, localport);
                    usedPorts.add(localport);
                } else {
                    avoidPorts.add(localport);//if fails, we temporarily avoid using this port
                }
            }
        }

    }

    public void startTunnelsWorker(String addkey, String address) {
        CommonUtils.execute("nohup ssh " + addkey + " -f -R " + taskServicePort + ":localhost:" + taskServicePort + " " + userWorker + "@" + address + " -N", true);
        CommonUtils.execute("nohup ssh " + addkey + " -f -R " + mgmtServicePort + ":localhost:" + mgmtServicePort + " " + userWorker + "@" + address + " -N", true);
        CommonUtils.execute("nohup ssh " + addkey + " -f -R " + fileServerPort + ":localhost:" + fileServerPort + " " + userWorker + "@" + address + " -N", true);
    }

    /**
     * Release ports from SSH tunnel from Agent to Worker.
     *
     * @param addressWorker
     * @param portWorker
     */
    public void releasePortMapWorker(String addressWorker, int portWorker) {
        Integer localport = portMapWorkers.remove(addressWorker + ":" + portWorker);
        if (localport != null) {
            usedPorts.remove(localport);
        }

        CommonUtils.execute("kill -9 `ps ux | grep " + localport + ":" + addressWorker + ":" + portWorker + " | head -1 | awk '{print $2}'`");
    }

    /**
     * Release reverse SSH tunnel from Agent to Worker.
     *
     * @param addressWorker
     * @param portWorker
     */
    public void releaseTunnelsWorker(String addressWorker) {
        CommonUtils.execute("kill -9 `ps ux | grep " + "\"" + taskServicePort + ":localhost:" + taskServicePort + " " + userWorker + "@" + addressWorker + "\" | head -1 | awk '{print $2}'`");
        CommonUtils.execute("kill -9 `ps ux | grep " + "\"" + mgmtServicePort + ":localhost:" + mgmtServicePort + " " + userWorker + "@" + addressWorker + "\" | head -1 | awk '{print $2}'`");
        CommonUtils.execute("kill -9 `ps ux | grep " + "\"" + fileServerPort + ":localhost:" + fileServerPort + " " + userWorker + "@" + addressWorker + "\" | head -1 | awk '{print $2}'`");
    }

    /**
     * Start workers on allocated resources
     *
     * @param nodeinfo
     * @param wflId
     * @param stageId
     * @param appname
     * @param properties
     * @param performance
     * @param tasks
     * @param types
     * @return List of failed workers
     */
    public List startWorkers(List<String> nodeinfo, String wflId, String stageId, String appname, String properties, List<Double> performance,
            String tasks, List<String> types) {
        //properties not used for now.
        List downWorkers = new ArrayList();
        //{"ip:port","ip:port","ip:port","fail:type:port"} // nodeinfo and downNodes

        boolean leave;
        int maxTries = 5;
        int tries = 0;
        int waitTime = 5000; //3 seconds between tries
        //TODO: put as parameter
        String classpath = softwareDirWorker + "/" + appname + "/dist/*:" + softwareDirWorker + "/" + appname + "/lib/*";
        for (int i = 0; i < nodeinfo.size(); i++) {
            if (nodeinfo.get(i).startsWith("fail")) {
                downWorkers.add(nodeinfo.get(i));
            } else {

                leave = false;

                String propertyfile = nodeinfo.get(i).replace(":", "_") + ".properties";
                double perf = performance.get(i);
                String type;
                if (types != null) {
                    type = types.get(i); //cloud case, where we have a type
                } else {
                    type = nodeinfo.get(i).split(":")[0]; //cluster case, use ip
                }

                createWorkerProperties(propertyfile, wflId, stageId, appname, supportedApps.get(appname), properties, perf, tasks, type, "default");

                String[] parts = nodeinfo.get(i).split(":");
                //transfer files           
                String addkey = "";

                if (keyfile != null) {
                    addkey = " -i " + keyfile;
                }
                //Moustafa - added the key (only Javier's key is stored in the image) 
                //launch ssh tunnels to allow workers communicate with Agent (needed in OpenStack india and OSG)
                if (startSSHtunnels) {
                    startTunnelsWorker(addkey, parts[0]);
                }

                //launch ssh tunnel to allow Agent communicate with worker. (needed in OSG)
                if (this.startSSHtunnelAgent) {
                    String remoteaddress = parts[0];
                    String remoteport = parts[1];
                    this.startAgentToWorkerTunnel(remoteport, remoteaddress);
                }

                //create working directory if it was not there
                CommonUtils.execute("ssh " + addkey + " -q -oBatchMode=yes " + userWorker + "@" + parts[0] + " -C \"mkdir -p " + workingDir + " \"");

                String[] output = CommonUtils.execute("rsync -avz -e \"ssh " + addkey + " -o BatchMode=yes\" " + propertyfile + " " + workerBasePropertyFile + " "
                        + userWorker + "@" + parts[0] + ":" + workingDir + "/", false);
                if (!output[0].equals("0")) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "Worker properties were not transfered. Please make sure that RSYNC in installed in both the Agent and Woker machines.");
                }
                String basePropertiesName = new File(workerBasePropertyFile).getName();
                String logfile = workingDir + "/" + propertyfile + ".out";
                //start worker
                //Moustafa - added workerBaseDir parameter to account for multiple users running comet on the same machine
                CommonUtils.execute("ssh " + addkey + " -q -oBatchMode=yes " + userWorker + "@" + parts[0] + " -C \"nohup java -Xms256m -Xmx1024m -cp " + classpath
                        + " tassl.application.node.isolate.CloudBurstStarter -propertyFile " + workingDir + "/" + propertyfile
                        + " -propertyFile " + workingDir + "/" + basePropertiesName
                        + " -workerBaseDir " + workingDir + " -port " + parts[1] + " -publicIp " + parts[0] + " > " + logfile + " 2>&1 & \" ", false);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }

                tries = 0;
                //we can check if the worker is active. 
                String workerToCheck = nodeinfo.get(i);
                do {
                    String workerstatus = checkWorkerStatus(parts[0], Integer.parseInt(parts[1]));
                    Logger.getLogger(Resource.class.getName()).log(Level.INFO, "Status of Worker " + parts[0] + ":" + parts[1] + " is " + workerstatus);

                    //if it is not active, try again with a different port. if port changes, we need to update wflnode
                    if (workerstatus.equals("down")) {
                        Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, "DOWN!!!");

                        tries++;
                        if (tries == maxTries) {
                            //add node to list of down nodes
                            downWorkers.add(workerToCheck);
                            Logger.getLogger(Resource.class.getName()).log(Level.INFO, "Adding Worker " + parts[0] + ":" + parts[1] + " to the down failure list");
                            leave = true;

                        } else {
                            try {
                                Thread.sleep(waitTime);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        //TODO: DO SOMETHING. START IN DIFFERENT PORT??

                    } else {//worker is OK
                        leave = true;
                    }

                } while (!leave);

                CommonUtils.execute("rm -f " + propertyfile, false);
            }
        }

        return downWorkers;
    }

    /**
     * Return the workflowId.stageId where the worker is allocated worker
     * ip:port
     *
     * @return workflowId.stageId if found or null if not found
     */
    public String getWflStageWorker(String worker) {
        HashMap<String, List<String>> stages;
        for (String wk : wflNode.keySet()) {
            stages = wflNode.get(wk);
            for (String st : stages.keySet()) {
                if (stages.get(st).contains(worker)) {
                    return wk + "." + st;
                }
            }
        }

        return "null";

    }

    /**
     * Check the status of all the workers
     *
     * @return hashmap
     * {"wflId.stageId":{"ip:port",..},"wflId.stageId":{"ip:port",..},...}
     */
    public HashMap checkAllWorkerStatus() {
        HashMap<String, List<String>> down = new HashMap();
        HashMap<String, List<String>> stages;
        for (String wk : wflNode.keySet()) {
            stages = wflNode.get(wk);
            for (String st : stages.keySet()) {
                List nodeNames = (List) stages.get(st);
                for (int i = 0; i < nodeNames.size(); i++) {
                    String name = (String) nodeNames.get(i);
                    String[] parts = (name).split(":");
                    String workerstatus = checkWorkerStatus(parts[0], Integer.parseInt(parts[1]));
                    Logger.getLogger(Resource.class.getName()).log(Level.INFO, "Status of Worker " + parts[0] + ":" + parts[1] + " is " + workerstatus);
                    if (workerstatus.equals("down")) {
                        List temp = down.get(wk + "." + st);
                        if (temp == null) {
                            temp = new ArrayList();
                            temp.add(name);
                            down.put(wk + "." + st, temp);
                        } else {
                            temp.add(name);
                        }
                    }
                }
            }
        }
        return down;
    }

    public Integer getPortMapWorker(String address, int port) {
        return portMapWorkers.get(address + ":" + port);
    }

    public String checkWorkerStatus(String address, int port) {
        String status = "down";
        DataInputStream in = null;
        DataOutputStream out = null;

        if (this.startSSHtunnelAgent) {
            Integer localport = this.getPortMapWorker(address, port);
            if (localport != null) {
                address = "localhost";
                port = localport.intValue();
            }
        }

        Object[] stream = CommonUtils.getOutputInput(address, port);
        in = (DataInputStream) stream[0];
        out = (DataOutputStream) stream[1];
        if (in != null && out != null) {
            try {
                out.writeUTF("CheckStatus");
                status = in.readUTF();
            } catch (IOException ex) {
                Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }

                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }//else worker is down. 

        return status;
    }

    public boolean createWorkerProperties(String filename, String wflId, String stageId, String appname,
            String workerclass, String properties, double perf, String tasks, String machineType, String fileTransferType) {
        boolean status = true;
        FileWriter fw;
        BufferedWriter bw = null;
        try {
            fw = new FileWriter(new File(filename).getAbsoluteFile());
            bw = new BufferedWriter(fw);
            String towrite = "appName=" + appname + "\n"
                    + "WorkerClass=" + workerclass + "\n"
                    + "workflowID=" + wflId + "\n"
                    + "stageID=" + stageId + "\n"
                    + "softwarePath=" + softwareDirWorker + "\n"
                    + "WorkingDir=" + workingDir + "\n"
                    + "performance=" + perf + "\n"
                    + "machineType=" + machineType + "\n"
                    + "keyfile=" + keyfile + "\n"
                    + "taskids=" + tasks + "\n"
                    + "fileTransferType=" + fileTransferType;

            bw.write(towrite);

        } catch (IOException ex) {
            Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
            status = false;
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return status;
    }

    /**
     * Get the list of active resources organized by workflowid.stageid
     *
     * @return
     */
    public HashMap<String, List<String>> getActiveResourcesWorkflow() {
        HashMap<String, List<String>> listofResources = new HashMap();
        for (String workflowId : wflNode.keySet()) {
            for (String stageId : wflNode.get(workflowId).keySet()) {
                List<String> nodesCopy = new ArrayList();
                for (String s : wflNode.get(workflowId).get(stageId)) {
                    nodesCopy.add(s);
                }
                listofResources.put(workflowId + "." + stageId, nodesCopy);
            }
        }
        return listofResources;
    }

    /**
     * Check if an machine, identified by IP, is registered in the accounting
     * already. For use only in Agent side.
     *
     * @param currentNodeAcct
     * @param ip
     * @return
     */
    protected boolean checkIfExistsNodeAcct(List<NodeAcct> currentNodeAcct, String ip) {
        for (NodeAcct naObj : currentNodeAcct) {
            if (naObj.getIp().equals(ip)) {
                return true;
            }
        }
        return false;
    }

    protected NodeAcct findNodeAcctIP(List<NodeAcct> nodeAcctList, String ip) {
        for (NodeAcct na : nodeAcctList) {
            if (na.getIp().equals(ip)) {
                return na;
            }
        }
        return null;
    }

    /**
     * Contact a worker to end it
     *
     * @param address
     * @param port
     * @param force if true the worker cancel all work and leave. if false the
     * worker waits to finish current task.
     */
    protected void sendTerminateWorkerSignal(String address, int port, boolean force) {

        if (this.startSSHtunnelAgent) {
            Integer localport = this.getPortMapWorker(address, port);
            if (localport != null) {
                address = "localhost";
                port = localport.intValue();
            }
        }
        //terminate worker      
        DataOutputStream conn = null;
        try {
            conn = CommonUtils.getOutput(address, port);
            if (conn != null) {
                conn.writeUTF("TerminateWorker");
                conn.writeUTF(String.valueOf(force)); //true or false. To force exit
            }

        } catch (IOException e) {
            Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    /**
     * Return list of workers running in a machine or VM
     *
     * @param ip of the machine
     * @param workflowId
     * @param stageId
     * @return
     */
    public List<String> getWorkersIp(String ip, String workflowId, String stageId) {
        List<String> allworkers;
        List<String> workers = new ArrayList();
        //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
        HashMap stages = wflNode.get(workflowId);
        if (stages != null) {
            allworkers = (List) stages.get(stageId);
            for (int i = 0; i < allworkers.size(); i++) {
                String worker = allworkers.get(i);
                if (worker.startsWith(ip)) {
                    workers.add(worker);
                }
            }
        }

        return workers;
    }

    /**
     * Send information to a service. Do not wait reply.
     *
     * @param addess
     * @param port
     * @param command
     * @param param
     * @return
     */
    protected boolean sendInfo(String addess, int port, String command, Object param) {
        boolean status = true;
        DataOutputStream conn = null;
        DataInputStream in = null;
        try {
            Object[] both = CommonUtils.getOutputInput(addess, port);
            if (both[0] != null && both[1] != null) {
                in = (DataInputStream) both[0];
                conn = (DataOutputStream) both[1];

                if (command.equals("dbWriteAgentReleaseMachine") || command.equals("dbWriteAgentReleaseWorker")) {
                    conn.writeUTF(command);
                    conn.writeUTF(dbName);

                    conn.writeUTF((String) ((List) param).get(0));
                    conn.writeLong((Long) ((List) param).get(1));
                    conn.writeLong((Long) ((List) param).get(2));
                    conn.writeUTF((String) ((List) param).get(3));
                    conn.writeUTF((String) ((List) param).get(4));
                    conn.writeBoolean((Boolean) ((List) param).get(5));

                    //byte[] forwardObj;     
                    //send sites
//                    forwardObj= programming5.io.Serializer.serializeBytes(((List)param).get(6)); //HashMap<String,String>
//                    conn.writeInt(forwardObj.length);                    
//                    conn.write(forwardObj);                    
                    CommonUtils.sendObject(conn, ((List) param).get(6));
                }

            } else {
                Logger.getLogger(Resource.class.getName()).log(Level.WARNING, "Could not connect with " + addess + ":" + port);
                status = false;
            }
        } catch (IOException e) {
            Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, e);
            status = false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ex) {
                    Logger.getLogger(Resource.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return status;
    }

    /**
     * Terminate worker should call sendTerminateWorkerSignal and record worker
     * life in DB, when db active
     *
     * @param address
     * @param port
     * @param force
     * @param nodeAcctList
     * @param failure is this called because a worker failed?
     * @param provisioning is this called because a worker failed during
     * provisioning?
     */
    abstract protected void terminateWorker(String address, int port, boolean force, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning);

    /**
     * Release ALL workers allocated to the specific workflow stage
     *
     * @param workflowId
     * @param stageId
     * @param nodeAcctList
     * @param failure
     * @return list of workers to be terminated
     */
    abstract public List releaseWorkers(String workflowId, String stageId, List<NodeAcct> nodeAcctList, boolean failure);

    /**
     * Release SOME workers allocated to the specific workflow stage
     *
     * @param workflowId
     * @param stageId
     * @param workers
     * @param nodeAcctList
     * @param failure is this called because a worker failed?
     * @param provisioning is this called because a worker failed during
     * provisioning?
     * @return List of VM types (cloud), job types (OSG) or list of machine
     * ips(cluster) corresponding with the workers
     */
    abstract public List releaseSomeWorkers(String workflowId, String stageId, List<String> workers, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning);

    /**
     * Release all workers of the selected machines
     *
     * @param workflowId
     * @param stageId
     * @param machineIps
     * @param nodeAcctList
     * @param failure
     * @return List of workers {address:port,address:port,...}
     */
    abstract public List releaseSomeWorkersbyMachineIp(String workflowId, String stageId, List<String> machineIps, List<NodeAcct> nodeAcctList, boolean failure);

    /**
     * Allocate N workers to a specific stage. We can select the type of VM we
     * want with properties Call allocateStage from AgentResourceManager and get
     * the list of node:port
     *
     * @param wflId
     * @param stageId
     * @param MachineWorkers
     * @param properties This defines the type of allocation "random" or "exact"
     * as the list of MachineWorkers says
     * @return list of workers to be started
     */
    abstract public List allocateStage(String wflId, String stageId, List<String> MachineWorkers, String properties);

    /**
     * Check if the stage can be allocated in this resource
     *
     * @param numWorkers
     * @param properties This can be the type of VM we want, the min performance
     * of a machine, etc..
     * @return
     */
    abstract public boolean checkAllocatePossibleStage(int numWorkers, String properties);

    /**
     * Check if the node corresponding to this IP is active. It first checks the
     * records and then the real status using ssh.
     *
     * @param ip
     * @return
     */
    abstract public boolean isActive(String ip);

    /**
     * Check if the machine is in active status in the records.
     *
     * @param ip
     * @return
     */
    abstract public boolean isStatusActive(String ip);

    /**
     * It creates the list of available slots for scheduling considering the
     * workers per machine and the number of available machines
     *
     * @param address address:port of the site to be identified later
     * @return list of WorkerForScheduler objects
     */
    //abstract public List getAvailableSlotsScheduling(String address);
    /**
     * It creates the list of available slots for scheduling considering the
     * workers per machine and the number of available machines. It includes the
     * allocated slots if any
     *
     * @param siteAddress address:port of the site to be identified later
     * @param allocatedWorkers machine/vmtype:numWorkers
     * @param refBenchScore reference benchmark score given by user. Used to
     * calculate performance (speedup) of machine.
     * @param defaultAppExec application running times based on real data and
     * the defaultReferenceBenchmark
     * @param defaultReferenceBenchmark default benchmark score given by
     * workflow manager
     * @return list of WorkerForScheduler objects
     */
    abstract public List getAvailableSlotsScheduling(String siteAddress, String[] allocatedWorkers, double refBenchScore, HashMap<String, RunningStat> defaultAppExec, double defaultReferenceBenchmark);

    /**
     * Generate the accounting information of a stage. It returns only the new
     * information that is not already in the accounting. This info is related
     * to when the machines are provisioned and it will be deleted when they are
     * released.
     *
     * @param wflId
     * @param stageId
     * @param timestamp time when the machine is provisioned
     * @param currentNodeAcct this is the accounting info before the new workers
     * are assigned.
     * @param appName this is the name of the app using this node -- needed when
     * logging db info
     * @return List of provisioned machines
     */
    abstract public List<NodeAcct> generateAccountingInfo(String wflId, String stageId, long timestamp, List<NodeAcct> currentNodeAcct, String appName);

    /**
     * Disable a site by setting all resources to 0 and terminate currently
     * running workers/VMs. After this method, a different method should be
     * called if immediate resource termination is expected
     */
    abstract public void disableSite();

    /**
     * Restore a previously disabled site. After calling this method, the
     * Central Resource Manager/Autonomic Scheduler should be notified
     */
    abstract public void restoreSite();

    /**
     * Remove machine from pool of resources - only remove machines from data
     * structures.
     *
     * if Cluster it permanently disables machine when no access if Cloud it
     * removes VM from active list. Always called before terminating VM if OSG
     * it removes job from active list. Always called before terminating job
     *
     * @param ip
     * @param nodeAcctList,
     * @param failure
     */
    abstract public void releaseMachine(String ip, List<NodeAcct> nodeAcctList, boolean failure);

}
