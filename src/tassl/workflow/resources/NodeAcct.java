/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.workflow.resources;

/**
 * Information of provisioned machine for accounting purposes
 * 
 * @author Javier Diaz-Montes
 */
public class NodeAcct {

    String type; //cluster or VM-type
    String ip;   //ip address of the machine
    long startTime; //timestamp of the machine provisioning.
                      //since the central autonomic scheduler may not know the ip, this will help to identify when the machine finishes.                      
                      //e.g. this may happen in the case of clouds where we know the type only and for accounting this is enough.

    long stopTime; //timestamp that indicates when the machine was released
    
    //this is needed here because we may need to stop the clock of a site that does not exist anymore.
    double cost; // cost per hour.     
    
    private String appName; //name of the app used in this worker. Useful mainly in Agent for logging into db

    //on the autonomic scheduler we need the cost for each resource.
    public NodeAcct(String type, String ip, long startTime, double cost, String appName) {
        this.type = type;
        this.ip = ip;
        this.startTime = startTime;
        this.stopTime=0;
        this.cost=cost;
        this.appName=appName;
    }
    

    public double getCost() {
        return cost;
    }
    
    public String getType() {
        return type;
    }

    public String getIp() {
        return ip;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    
    public void setStopTime(long value){
        this.stopTime=value;
    }
    public void setStopTime(){
        this.stopTime=System.currentTimeMillis();
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    
    @Override
    public String toString() {
        return "NodeAcct{" + "type=" + type + ", ip=" + ip + ", startTime=" + startTime + ", stopTime=" + stopTime + ", cost=" + cost + '}';
    }
 
}
