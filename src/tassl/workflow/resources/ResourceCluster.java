/*
 * Copyright (c) 2009, NSF Center for Autonomic Computing, Rutgers University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of the NSF Center for Autonomic Computing, Rutgers University, nor the names of its
 * contributors may be used to endorse or promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tassl.workflow.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import tassl.application.utils.CommonUtils;
import tassl.application.utils.RunningStat;
import tassl.application.workflow.WorkerForScheduler;

/**
 *
 * @author Javier Diaz-Montes
 */
public class ResourceCluster extends Resource{
    
    //nodeList has the properties of the machines
    String queueType;
    String queueName;
    
    //to keep the totalnumworkers.
    HashMap <String,Integer> machineNumWorkers_backup;
    boolean disabled=false; //flag to prevent disabling this several times and loosing the backup

    public ResourceCluster(String publicIp, String AgentMetricsAddress, int AgentMetricsPort, boolean useMeticsService, String dbName) {
        super(publicIp,AgentMetricsAddress, AgentMetricsPort, useMeticsService, dbName);
        machineNumWorkers_backup=new HashMap();
    }
    
    
    public boolean loadConfig(String file){
        boolean status=true;
        Properties p = new Properties();                        
        try {
            p.load(new FileInputStream(file));                        
            queueType=p.getProperty("QueueType");
            queueName=p.getProperty("QueueName");                        
            fileTransferType = p.getProperty("fileTransferType", "default");//default is rsync change to support different file transfers e.g. SCP gridFTP, OSG
            
            status=super.loadConfig(file,"cluster");               
            
        } catch (IOException ex) {
                    Logger.getLogger(ResourceCluster.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
        }               
        return status;
    }

    public String getQueueType() {
        return queueType;
    }

    public String getQueueName() {
        return queueName;
    }
    
    @Override
    public void disableSite(){
        //if the site is active
        if(!disabled){
            //clean backup just in case
            machineNumWorkers_backup.clear();
            for(String ipS:nodeList.keySet()){
                //create backup
                machineNumWorkers_backup.put(ipS, nodeList.get(ipS).getTotalWorkers());
                //set 0 the number of workers
                nodeList.get(ipS).setTotalWorkers(0);
            }            
        disabled=true;
        }      
    }
    
    @Override
    public void restoreSite(){
         if(disabled){
             for(String ipS:nodeList.keySet()){
                 nodeList.get(ipS).setTotalWorkers(machineNumWorkers_backup.get(ipS));
             }
         }
    }
    
    /**
     * Free workers and remove the stage from wflNode
     * 
     * @param workflowId
     * @param stageId
     * @param nodeAcctList
     * @param failure
     * @return List of workers to terminate
     */
    @Override
    public List releaseWorkers(String workflowId, String stageId, List<NodeAcct> nodeAcctList, boolean failure){
        List nodeNames=null;
        HashMap stages=wflNode.get(workflowId);
        if(stages!=null){
            nodeNames=(List)stages.get(stageId);
            for(int i=0;i<nodeNames.size();i++){
                String [] parts=((String)nodeNames.get(i)).split(":");
                String addr=parts[0];
                int port=Integer.parseInt(parts[1]);
                
                Node n=nodeList.get(addr);
                System.out.println("CWDEBUG. resourcecluster. "+n.toString());
                n.decrementActiveWorkers(1); //make one worker available
                n.releasePort(port);
                
                //ask worker to end                        
                terminateWorker(addr, port, true, nodeAcctList, failure, false); //releaseWorkers is never called when provisioning new resources
                
            }
            stages.remove(stageId); //remove stage info
            if(stages.isEmpty()){
                wflNode.remove(workflowId);
            }
        }
        return nodeNames;
    }

    @Override
    public List releaseSomeWorkers(String workflowId, String stageId, List<String> workers, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning){
        List nodeNames;
        List terminatedWorkers=new ArrayList();
        HashMap stages=wflNode.get(workflowId);
        if(stages!=null){
            nodeNames=(List)stages.get(stageId);
            for(String work:(List<String>)workers){
                String [] parts=work.split(":");
                String addr=parts[0];
                int port=Integer.parseInt(parts[1]);
                
                Node n=nodeList.get(addr);
                n.decrementActiveWorkers(1); //make one worker available
                n.releasePort(port);
                                
                //ask worker to end
                terminateWorker(addr, port, true, nodeAcctList, failure, provisioning);
                
                if(nodeNames!=null)
                    nodeNames.remove(work);
                terminatedWorkers.add(addr);
            }            
        }        
        
        return terminatedWorkers;
    }
    
    public int getNumberActiveWorkers(String ip){
        return nodeList.get(ip).getActiveWorkers();
    }
    
    /**
     * Release all workers of the selected machines
     * 
     * @param workflowId
     * @param stageId
     * @param machineIps
     * @return List of workers {address:port,address:port,...}
     */
    @Override
    public List releaseSomeWorkersbyMachineIp(String workflowId, String stageId, List<String> machineIps, List<NodeAcct> nodeAcctList, boolean failure){
        List <String> terminatedWorkers=new ArrayList();
        List <String> allWorkers;
        HashMap stages=wflNode.get(workflowId);
        if(stages!=null){
            allWorkers=(List)stages.get(stageId);
            for(int i=allWorkers.size()-1;i>=0;i--){            
                String worker=allWorkers.get(i);
                if(machineIps.contains(worker.split(":")[0])){
                    String [] parts=worker.split(":");
                    String addr=parts[0];
                    int port=Integer.parseInt(parts[1]);                    
                    
                    Node n=nodeList.get(addr);
                    n.decrementActiveWorkers(1); //make one worker available
                    n.releasePort(port);                    
                    allWorkers.remove(i);
                    terminatedWorkers.add(worker);
                    
                    //ask worker to end
                    terminateWorker(addr, port, true, nodeAcctList, failure, false); //releaseSomeWorkersbyMachineIp is never called when provisioning new resources
                }
            }            
        }      
        return terminatedWorkers;
    }
    
    
    @Override
    protected void terminateWorker(String address, int port, boolean force, List<NodeAcct> nodeAcctList, boolean failure, boolean provisioning){
        if(useMeticsService){
            //currently we assume a worker stated when its hosting machine was allocated            
            
            HashMap<String,String> additionalValues=new HashMap<String,String>(); //values custom to different resources
            additionalValues.put("queueName", this.getQueueName());
            additionalValues.put("queueType", this.getQueueType());
            long end=System.currentTimeMillis();

            NodeAcct na=findNodeAcctIP(nodeAcctList,address);
            long startTimeStamp=-1L;
            String appName="unknown";
            if(na!=null){
                startTimeStamp=na.getStartTime();
                appName=na.getAppName();
            }
            
            if(!provisioning){
                List param=new ArrayList(Arrays.asList(address,startTimeStamp, end, appName, this.getClass().getSimpleName(), failure, additionalValues));
                this.sendInfo(this.AgentMetricsAddress, this.AgentMetricsPort, "dbWriteAgentReleaseWorker", param);
            }
            
        }        
        if(!failure){
            this.sendTerminateWorkerSignal(address, port, force);                
        }
    }
    
    @Override
    public boolean checkAllocatePossibleStage(int numWorkers, String properties){        
        int workersToget=numWorkers;
        if(properties.endsWith("")){ //check random nodes
            for(String nodename:nodeList.keySet()){
                Node n=nodeList.get(nodename);
                System.out.println("CWDEBUG. resourcecluster. is up "+n.getIsUp());
                System.out.println("CWDEBUG. resourcecluster. total workers "+n.getTotalWorkers());
                System.out.println("CWDEBUG. resourcecluster. active workers "+n.getActiveWorkers());
                if(n.getIsUp() && n.getTotalWorkers() > n.getActiveWorkers()){
                    int available=n.getTotalWorkers()- n.getActiveWorkers();
                    if(workersToget<available){
                        workersToget=0;
                    }else{
                        workersToget-=available;
                    }                    

                    if(workersToget==0){
                        break;
                    }                
                }
            }
        }
        if(workersToget==0){
            return true;
        }else{
            return false;
        }
    }
    
    @Override
    public List allocateStage(String wflId,String stageId, List <String>MachineWorkers, String properties){
        List <String>nodePortList=new ArrayList();
        
        if(properties.equals("exact")){
            //"machine:numWorkers","machine:numWorkers",...
            for(String elem:MachineWorkers){
                String []parts=elem.split(":");
                String nodename=parts[0];
                int numWorkers=Integer.parseInt(parts[1]);
                
                Node n=nodeList.get(nodename);
                
                System.out.println("CWDEBUG. resorucecluster. allocatestage. "+n.toString());
                
                if(n.getIsUp() && n.getTotalWorkers() >= (n.getActiveWorkers()+numWorkers)){
                    for(int i=0;i<numWorkers;i++){                        
                        int port=n.getFreePort();
                        nodePortList.add(nodename+":"+port);                    
                        n.setActiveWorkers(n.getActiveWorkers()+1);
                    }
                    
                }else{
                    //Undo
                    for(String st:nodePortList){
                        String []partsUndo=st.split(":");
                        Node nUndo=nodeList.get(partsUndo[0]);
                        nUndo.decrementActiveWorkers(1);
                        nUndo.releasePort(Integer.parseInt(partsUndo[1]));                        
                    }
                    return new ArrayList();
                }
            }
            
            
            
        }else if(properties.equals("random")){ //allocate any nodes
            int workersToget=MachineWorkers.size();
            for(String nodename:nodeList.keySet()){
                Node n=nodeList.get(nodename);
                if(n.getIsUp() && n.getTotalWorkers() > n.getActiveWorkers()){
                    int available=n.getTotalWorkers()- n.getActiveWorkers();
                    for (int i=0;i<available && workersToget>0; i++){
                        //nodeList; //ip/name:node
                        //wflNode; //wkfId:{stageid:{"ip:port","ip:port"},stageid:{"ip:port","ip:port"}}
                        int port=n.getFreePort();
                        nodePortList.add(nodename+":"+port);                    
                        n.setActiveWorkers(n.getActiveWorkers()+1);
                        workersToget--;
                    }
                    if(workersToget==0){
                        break;
                    }
                }
            }
        }
        HashMap <String, List<String>>stages;        
        if(!wflNode.containsKey(wflId)){
            stages=new HashMap();
            stages.put(stageId, nodePortList);
            wflNode.put(wflId, stages);
        }else{
            stages=wflNode.get(wflId);
            if(!stages.containsKey(stageId)){
                stages.put(stageId, nodePortList);
            }else{
                List temp=stages.get(stageId);
                temp.addAll(nodePortList);                
            }
        }        
        
        return nodePortList;        
    }
    
    @Override
    public boolean isStatusActive(String ip){        
        return nodeList.get(ip).getIsUp();
    }
    
    @Override
    public boolean isActive(String ip){
        boolean status=false;
        if(isStatusActive(ip)){
            String cmd;                        
            cmd="ssh -q -oBatchMode=yes -o \"ConnectTimeout 5\" "+userWorker+"@" + ip + " uname";                        
            String[]output=CommonUtils.execute(cmd,false);            
            if(output[0].equals("0")){
                status=true;
            }else{
                status=false;
            }
        }
        return status;
    }
    
    /**
     * Disable Machine so it cannot be used.
     * @param ip
     */
    @Override
    public void releaseMachine(String ip, List<NodeAcct> nodeAcctList, boolean failure){
        nodeList.get(ip).setIsUp(false);
        
        if(useMeticsService){//if db is enabled
            HashMap<String,String> additionalValues=new HashMap<String,String>(); //values custom to different resources
            additionalValues.put("queueName", this.getQueueName());
            additionalValues.put("queueType", this.getQueueType());
            long end=System.currentTimeMillis();

            NodeAcct na=findNodeAcctIP(nodeAcctList,ip);
            long startTimeStamp=-1L;
            String appName="unknown";
            if(na!=null){
                startTimeStamp=na.getStartTime();
                appName=na.getAppName();
            }
        
            List param=new ArrayList(Arrays.asList(ip,startTimeStamp, end, appName, this.getClass().getSimpleName(), failure, additionalValues));
            this.sendInfo(this.AgentMetricsAddress, this.AgentMetricsPort, "dbWriteAgentReleaseMachine", param);
            
        }
        
    }
    
//    @Override
//    public List getAvailableSlotsScheduling(String address){
//        List nodesForSched=new ArrayList();
//        //ip/name:node or vmtype:node
//        for(String name:this.nodeList.keySet()){            
//            int workerNum=this.nodeList.get(name).getTotalWorkers()-this.nodeList.get(name).getActiveWorkers();            
//            for(int j=0;j<workerNum;j++){
//                //"type,index,worker1,r" "type,index,worker2,r" "type1,index1,worker1,r"
//                //r is the expected time that the slot can execute next task
//                WorkerForScheduler wfs=new WorkerForScheduler(address,this.identifier,name, 0, j, this.getOverhead(), this.getOverhead());
//                wfs.setCost(this.nodeList.get(name).getCost());
//                wfs.setPerformance(this.nodeList.get(name).getPerformance());
//                nodesForSched.add(wfs);
//            }
//        }                        
//        return nodesForSched;
//    }
    
    @Override
    public List getAvailableSlotsScheduling(String siteAddress, String [] allocatedWorkers, double refBenchScore, HashMap<String,RunningStat>defaultAppExec,double defaultReferenceBenchmark){
        List nodesForSched=new ArrayList();
        
        //this is to store the number of slots allocated of each type. This is used  to initialize i in the totalAvail loop
        HashMap <String,Integer> slotsType=new HashMap();
        
        String [] wkPart;
        //add first the allocated workers
        for(String wk:allocatedWorkers){
            if(!wk.trim().isEmpty()){
                wkPart=wk.split(":");
            //FOR SIMPLICITY WE DO NOT RESTART WORKERS THAT FAILED IN A SLOT i.e. if workerVM is = 2 for a particular type, and there is only 1 in the 
                //list of allocated nodes, then only one slot is created.

                for (int j=0;j<Integer.parseInt(wkPart[1]);j++){     
                    double benchScore=this.nodeList.get(wkPart[0]).getBenchScore();
                    WorkerForScheduler wfs=new WorkerForScheduler(siteAddress,this.identifier, this.zone,wkPart[0], 0, j, 0, 0,defaultAppExec,defaultReferenceBenchmark,benchScore);
                    wfs.setCost(this.nodeList.get(wkPart[0]).getCost());
                    wfs.setPerformance(benchScore/refBenchScore);
                    nodesForSched.add(wfs);   
                    System.out.println("CWDEBUG: getAvailableSlotsScheduling - previously allocated - Cluster SLOT:"+wfs.toString());
                }            
            }
        }
        
        //ip/name:node or vmtype:node
        for(String name:this.nodeList.keySet()){  
            if(this.nodeList.get(name).getIsUp()){
                int workerNum=this.nodeList.get(name).getTotalWorkers()-this.nodeList.get(name).getActiveWorkers();            
                for(int j=0;j<workerNum;j++){
                    double benchScore=this.nodeList.get(name).getBenchScore();
                    //"type,index,worker1,r" "type,index,worker2,r" "type1,index1,worker1,r"
                    //r is the expected time that the slot can execute next task
                    WorkerForScheduler wfs=new WorkerForScheduler(siteAddress,this.identifier,this.zone,name, 0, j, this.getOverhead(), this.getOverhead(),defaultAppExec,defaultReferenceBenchmark,benchScore);
                    wfs.setCost(this.nodeList.get(name).getCost());
                    wfs.setPerformance(benchScore/refBenchScore);
                    nodesForSched.add(wfs);
                    System.out.println("CWDEBUG: getAvailableSlotsScheduling - Cluster SLOT:"+wfs.toString());
                }
            }
        }                        
        return nodesForSched;
    }
    
    
    @Override
    public List<NodeAcct> generateAccountingInfo(String wflId, String stageId, long timestamp, List <NodeAcct> currentNodeAcct, String appName) {
        List<NodeAcct> nod=new ArrayList();
        
        HashMap <String, List<String>>stages = wflNode.get(wflId);        
        if(stages!=null){
            if(stages.containsKey(stageId)){
                //workers={"ip:port","ip:port"}
                List<String> workers =stages.get(stageId);
                List<String> knownIps=new ArrayList();
                System.out.println("CWDEBUG. resourcecluster. generateaccountinginfo. workers="+workers);
                for(String work:workers){
                    String ip=work.split(":")[0];
                    if (!checkIfExistsNodeAcct(currentNodeAcct, ip)){
                        if(!knownIps.contains(ip)){
                            knownIps.add(ip);
                            double cost=nodeList.get(ip).getCost();
                            NodeAcct na=new NodeAcct("cluster", ip, timestamp,cost,appName);
                            nod.add(na);
                        }
                    }
                }
            }else{
                return new ArrayList();
            }
        }else{
            return new ArrayList();
        }
        
        return nod;
        
    }
    
}
