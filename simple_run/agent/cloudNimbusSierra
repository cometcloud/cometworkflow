#******************************************************************************
# Cloud Sierra configuration
#------------------------------------------------------------------------------
#  Name: Name of the site
#
#  Zone: Zone that this site is part of. This is used to control data movement.
#
#  key(Optional): Key used to interact with the cloud (e.g., jdiaznova.pem)
#
#  ProviderType: defines the type of cloud provider we use. Currently 
#  our script cloud.py supports nimbus and openstack_ec2
#
#  Region: Name of the Cloud region we are going to use. Please note that
#  multiple regions may have different VM images registered and different Ids.
#  
#  ProviderConfigFile: Configuration file with the environment variables needed
#  to interact with the cloud provider (e.g., in the case of EC2 we can have
#  EC2_URL or EC2_ACCESS_KEY)
#
#  CloudScript: URI of the script that will be used to interact with the cloud.
#  It should not be modified unless you know what you are doing.
#  
#******************************************************************************
Name=cloudTest
Zone=zoneB
#key=/N/u/jdiaz/OS-grizzly/ec2/jdiaznova.pem
ProviderType=nimbus
Region=nimbus
ProviderConfigFile=/N/u/jdiaz/nimbus-cloud-client-021/nimbusec2
CloudScript=../../scripts/cloud.py

#******************************************************************************
# VM Information
#------------------------------------------------------------------------------
#  VMLimits: Specify the type of VMs supported and the number of each type
#  that is available. Format is <type>:<number>;<type>:<number>. 
#  NOTE: Nimbus only has type small
#
#  Overhead: Time needed to provision resources (seconds)
#
#  WorkerLimit: Number of workers we can start for each type of VM type. 
#  Format is <type>:<number>;<type>:<number>. This feature is experimental 
#  and not well tested as in August 2014.
#
#  Cost: Cost for each type of resource. The cost is per hour. Format is 
#  <type>:<number>;<type>:<number>
#
#  BenchmarkScore: Benchmark score of each machine. Format is <type>:<scorenumber>;<type>:<scorenumber>
#
#  CostDataIn: Cost of transferring data inside the site. Cost per GB of data.
#
#  CostDataOut: Cost of transferring data outside the site. Cost per GB of data.
# 
#  workerPortRange: Range of ports that will be assigned to workers started
#  in the remote resources. Note that this range is independent for each
#  resource and therefore two different machines can reuse ports. Format 
#  is <initialPort>:<lastPort>
#
#******************************************************************************
VMLimits=m1.small:5;

Overhead=20
WorkerLimit=m1.small:1
Cost=m1.small:2;
BenchmarkScore=m1.small:17271;
CostDataIn=0.01
CostDataOut=0.12

workerPortRange=7777:7888


#******************************************************************************
# Application Information
#------------------------------------------------------------------------------
#  SupportedApps: List of supported applications. Format is <appname>,<appname>
#  For each application name we need to include the classpath of its worker.
#  Next we can see that we have two applications and we have defind two 
#  classpath (testapp1 and testapp2)
#  
#  defaultImageId: ImageId of the VM image that will be started to execute the
#  application, unless a customized one has been provided.
#
#  testapp1ImageId (Optional): ImageId for the application testapp1. This 
#  indicates that we want to use a specific VM image for that application.
#  We can one of these fields for each applications. If no specific VM image
#  is provided, then the default one will be used. In this example testapp1 has
#  a custom VM image, while testapp2 will use the default one.
#
#******************************************************************************
SupportedApps=testapp1,testapp2,sampleTutorial
testapp1=tassl.application.cometcloud.sampleMontage.AppWorkerMontage
testapp2=tassl.application.cometcloud.sample.AppWorker
sampleTutorial=tassl.application.cometcloud.sampleTutorial.AppWorker

defaultImageId=centos-5.7-comet1
testapp1ImageId=centos-5.7-comet2

#******************************************************************************
# Internal Resource Information
#------------------------------------------------------------------------------
#  SoftwareDirWorker: Directory where the software is expected to be in the
#  resource for each application. For example, the software of testapp1 is 
#  expected to be in /root/testapp1/ As part of the software it is required a
#  dist and lib directories with the CometCloud jar files as well as the
#  specific worker for this application. Additional any other software that the
#  application needs should be included inside /root/testapp1/, unless a
#  custome VM is setup differently. 
#
#  UserWorker: User id that will be used to ssh into the resource.
#
#  WorkingDir: Directory inside the resource that will be used to transfer
#  input files and write any data generated during the execution of the app.
#
#******************************************************************************
SoftwareDirWorker=/root/
UserWorker=root
WorkingDir=/root/
