#!/bin/bash
./startOverlayServer.sh > overlay_s.log 2>&1 &
./startProxy.sh > proxy_s.log 2>&1 &
./startWorkflowMaster.sh > master_s.log 2>&1 &

sleep 1
tail -f overlay_s.log -f proxy_s.log -f master_s.log
