#!/bin/bash

file=$1"_scaledRuntime30"
cp $1 $file


values=`awk '/runtime/ {print $6}' Montage_25.xml | cut -d"=" -f2 | sed -e 's/^"//'  -e 's/">$//'`

for i in $values;
do
    val=`echo $i"*30" | bc`
    sed -i s/\"$i\"/\"$val\"/g $file    

done
