#!/bin/bash

#Create folder with several examples

DIR=`pwd`"/cometWorkflowExample"

rm -rf $DIR

mkdir $DIR

cp -r src/tassl/application/cometcloud/sample $DIR
cp -r src/tassl/application/cometcloud/sampleLoop $DIR
cp -r src/tassl/application/cometcloud/sampleNonBlocking $DIR
cp -r src/tassl/application/cometcloud/sampleTutorial $DIR


mv cometWorkflowExample "cometWorkflowExample"`date +"%m%d%Y"`
tar cvfz cometWorkflowExample.tgz "cometWorkflowExample"`date +"%m%d%Y"`
rm -rf "cometWorkflowExample"`date +"%m%d%Y"`

mv -v cometWorkflowExample.tgz ../cometcloud.bitbucket.org/cometworkflow/doc/_downloads/
