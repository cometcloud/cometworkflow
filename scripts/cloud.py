#!/usr/bin/env python
"""
Client of IaaS clouds
"""
__author__ = 'Javier Diaz-Montes'

import argparse
import re
import logging
import os
import sys
import time

try:
    import boto
    import boto.ec2
except ImportError:
    boto_available=False

try:
    from novaclient.client import Client
except ImportError:
    nova_available=False

from subprocess import Popen
from subprocess import PIPE
import traceback


class cloudClass(object):

    #(Now we assume that the server is where the images are stored. We may want to change that)    
    ############################################################
    # __init__
    ############################################################
    def __init__(self, key, verbose, logfile, user, region, cloudType):
        super(cloudClass, self).__init__()
    
        self.cloudType=cloudType    
        self._key = key
        self.verbose=verbose
        self.user=user       
        self.region=region
        #config logs
        self._logfile = logfile
        self._log = logging.getLogger('cloud_ec2')
        self._formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        self._log.setLevel(logging.DEBUG)
        handler = logging.FileHandler(logfile)
        # handlers.RotatingFileHandler(logfile, maxBytes=2048, backupCount=5)
        handler.setFormatter(self._formatter)
        handler.setLevel(logging.DEBUG)        
        self._log.addHandler(handler)
        self._log.propagate = False
    

    def openstack_nova(self, imageidonsystem,  varfile, region, resources, command):
        """
        imageidonsystem = id of the image
        resources= number of vms of each type {<vmtype>,<vmtype>}
        varfile = openstack variable files(novarc typically)        
        """

        VERSION='2' ##hardcoded for now

        nova_key_dir = os.path.dirname(varfile)
        if nova_key_dir.strip() == "":
            nova_key_dir = "."
        os.environ["NOVA_KEY_DIR"] = nova_key_dir

        #read variables
        f = open(varfile, 'r')
        for line in f:
            if re.search("^export ", line):
                line = line.split()[1]
                parts = line.split("=")
                #parts[0] is the variable name
                #parts[1] is the value
                parts[0] = parts[0].strip()
                value = ""
                for i in range(1, len(parts)):
                    parts[i] = parts[i].strip()
                    parts[i] = os.path.expanduser(os.path.expandvars(parts[i]))
                    value += parts[i] + "="
                value = value.rstrip("=")
                value = value.strip('"')
                value = value.strip("'")
                os.environ[parts[0]] = value
        f.close()


        #connection
        nova = Client(str(VERSION), str(os.getenv("OS_USERNAME")), str(os.getenv("OS_PASSWORD")), str(os.getenv("OS_TENANT_ID")), str(os.getenv("OS_AUTH_URL")))

        networkLabel=str(os.getenv("OS_TENANT_ID"))+"-net"

        output=""
        if command=='start':
            output=self.startVMs(imageidonsystem, nova, resources, 'nova', networkLabel)
        elif command=='terminate':
            output=self.terminateVMs(resources, nova, 'nova')


        return output

    def aws_ec2(self, imageidonsystem,  varfile, region, resources, command):
        """
        imageidonsystem = id of the image
        resources= number of vms of each type {<vmtype>, <vmtype>}
        varfile = nimbusEC2 variable files(novarc typically)        
        """


        #read variables
        f = open(varfile, 'r')
        for line in f:
            if re.search("^export ", line):
                line = line.split()[1]
                parts = line.split("=")
                #parts[0] is the variable name
                #parts[1] is the value
                parts[0] = parts[0].strip()
                value = ""
                for i in range(1, len(parts)):
                    parts[i] = parts[i].strip()
                    parts[i] = os.path.expanduser(os.path.expandvars(parts[i]))
                    value += parts[i] + "="
                value = value.rstrip("=")
                value = value.strip('"')
                value = value.strip("'")
                os.environ[parts[0]] = value
        f.close()


        #connection (region e.g., us-west-2)
        conn =  boto.ec2.connect_to_region(region, aws_access_key_id=str(os.getenv("AWS_ACCESS_KEY")), aws_secret_access_key=str(os.getenv("AWS_SECRET_KEY")))

        output=""
        if command=='start':
            output=self.startVMs(imageidonsystem, conn, resources, 'ec2', '')
        elif command=='terminate':
            output=self.terminateVMs(resources, conn, 'ec2')

        return output


    
    def openstack_ec2(self, imageidonsystem,  varfile, region, resources, command):
        """
        imageidonsystem = id of the image
        resources= number of vms of each type {<vmtype>,<vmtype>}
        varfile = openstack variable files(novarc typically)        
        """
        
        nova_key_dir = os.path.dirname(varfile)            
        if nova_key_dir.strip() == "":
            nova_key_dir = "."
        os.environ["NOVA_KEY_DIR"] = nova_key_dir
    
        #read variables
        f = open(varfile, 'r')
        for line in f:
            if re.search("^export ", line):
                line = line.split()[1]                    
                parts = line.split("=")
                #parts[0] is the variable name
                #parts[1] is the value
                parts[0] = parts[0].strip()
                value = ""
                for i in range(1, len(parts)):
                    parts[i] = parts[i].strip()
                    parts[i] = os.path.expanduser(os.path.expandvars(parts[i]))                    
                    value += parts[i] + "="
                value = value.rstrip("=")
                value = value.strip('"')
                value = value.strip("'") 
                os.environ[parts[0]] = value
        f.close()
 

        ec2_url = os.getenv("EC2_URL")


        #path = "/services/Cloud"

        output = self.ec2_common(imageidonsystem, varfile, region, resources, ec2_url, command)

        return output
        
        
    def nimbus_ec2(self, imageidonsystem,  varfile, region, resources, command):
        """
        imageidonsystem = id of the image
        resources= number of vms of each type {<vmtype>, <vmtype>}
        varfile = nimbusEC2 variable files(novarc typically)        
        """
               
    
        #read variables
        f = open(varfile, 'r')
        for line in f:
            if re.search("^export ", line):
                line = line.split()[1]                    
                parts = line.split("=")
                #parts[0] is the variable name
                #parts[1] is the value
                parts[0] = parts[0].strip()
                value = ""
                for i in range(1, len(parts)):
                    parts[i] = parts[i].strip()
                    parts[i] = os.path.expanduser(os.path.expandvars(parts[i]))                    
                    value += parts[i] + "="
                value = value.rstrip("=")
                value = value.strip('"')
                value = value.strip("'") 
                os.environ[parts[0]] = value
        f.close()
        
        ec2_url = os.getenv("EC2_URL")
        
        
        #path = ""
                
        output = self.ec2_common(imageidonsystem, varfile, region, resources, ec2_url, command)
        
        return output
       
    def ec2_common(self, imageidonsystem, varfile, regionn, resources, ec2_url, command):
        if ec2_url.startswith("https://"):
            temp=ec2_url.replace("https://","").split(":")
        else:
            temp=ec2_url.lstrip("http://").split(":")
        endpoint = temp[0]
        temp1=temp[1].split("/")
        port=int(temp1[0])        
        path="/"+"/".join(temp1[1:])
        try:  
            region = boto.ec2.regioninfo.RegionInfo(name=regionn, endpoint=endpoint)
        except:
            msg = "ERROR: getting region information " + str(sys.exc_info())      
            self._log.error(msg)                      
            return msg
        
        try:
            if regionn=='nimbus':
                connection = boto.connect_ec2(str(os.getenv("EC2_ACCESS_KEY")), str(os.getenv("EC2_SECRET_KEY")), region=region, port=port)
            elif ec2_url.startswith("https://"):
                connection = boto.connect_ec2(str(os.getenv("EC2_ACCESS_KEY")), str(os.getenv("EC2_SECRET_KEY")), region=region, port=port, path=path)
            else:
                connection = boto.connect_ec2(str(os.getenv("EC2_ACCESS_KEY")), str(os.getenv("EC2_SECRET_KEY")), is_secure=False, region=region, port=port, path=path)
        except:
            msg = "ERROR:connecting to EC2 interface. " + str(sys.exc_info())
            self._log.error(msg)            
            return msg
        
        
        output=""
        if command=='start':
            output=self.startVMs(imageidonsystem, connection, resources,'ec2','')
        elif command=='terminate':
            output=self.terminateVMs(resources, connection,'ec2')
            
        return output
    
    
    
    def startVMs(self, imageidonsystem, connection, resources, typeAPI, networkLabel):
        image = None

        if (typeAPI == 'ec2'):

            try:
                image = connection.get_image(imageidonsystem)
            except:
                msg = "ERROR: getting the image " + str(sys.exc_info())
                self._log.error(msg)
                return msg
        elif (typeAPI == 'nova'):
            try:
                image=connection.images.find(name=imageidonsystem)
            except:
                msg = "ERROR: getting the image " + str(sys.exc_info())
                self._log.error(msg)
                return msg


        reservationList = []
            
        sshkeypair_name=(os.path.basename(self._key)).split(".")[0]
        
        
        totalVMs=0
        
        ##auxiliary object for NOVA API, to have a reservation like EC2
        MyObject = type('MyObject', (object,), {})
        
        for instancetype in resources:                
            try:
                if (typeAPI == 'ec2'):
                    reservation = image.run(1,1, sshkeypair_name, instance_type=instancetype) 
                elif (typeAPI == 'nova'):                                        
                    flavor = connection.flavors.find(name=instancetype)
                    network = connection.networks.find(label=networkLabel)
                    reservation = MyObject()
                    reservation.instances=[]
                    reservation.instances.append(connection.servers.create(name="VM-"+str(totalVMs), image=image, flavor=flavor, key_name=sshkeypair_name, nics=[{'net-id': network.id}]))

                totalVMs += len(reservation.instances)
                reservationList.append(reservation)           
            except:
                msg = "ERROR: launching the VM " + str(sys.exc_info())
                self._log.error(msg)                                
                traceback.print_exc()
                return msg
    
    
        #do a for to control status of all instances
        msg = "Waiting for running state in all the VMs"
        self._log.debug(msg)
        if self.verbose:
            print msg

        failedVMs=""
        #wait until all VMs have their final status
        finalstatus = False        
        failed = 0
        running = 0
        status=""
        while not finalstatus:
            running = 0    

            for reservation in reservationList:    
                for i in reservation.instances:
                    if (typeAPI == 'ec2'):
                        status = i.update()
                    elif (typeAPI == 'nova'):
                        try:
                            status = connection.servers.get(i.id).status 
                        except:
                            if self.verbose:
                                print sys.exc_info()

                    if self.verbose:
                        print str(str(i.id)) + ":" + status
                    if status == 'running' or status == 'ACTIVE':
                        running += 1                    
                    elif status == 'shutdown' or status == 'terminate' or status == 'terminated' or status == 'error' or status == 'ERROR':
                        failed += 1

                        if (typeAPI == 'ec2'):
                            failedVMs+="failId:dummyIp:"+str(i.instance_type)+";"
                            i.terminate();
                        elif (typeAPI == 'nova'):
                            flavorname=connection.flavors.find(id=i.flavor['id']).name
                            failedVMs+="failId:dummyIp:"+flavorname+";"
                            try:
                                i.delete()
                            except:
                                msg = "ERROR: terminating VM " + str(sys.exc_info())
                                self._log.error(msg)

                        reservation.instances.remove(i);

            if self.verbose:
                print "-------------------------"                    
            if ((running + failed) == totalVMs):
                finalstatus = True
            else:
                time.sleep(5)
 
        if self.verbose:
            print "Number of instances booted " + str(running)
            
                       
        #boto.ec2.instance.Instance.dns_name to get the public IP.
        #boto.ec2.instance.Instance.private_ip_address private IP.                          
        self._log.debug("Waiting to have access to VMs")
        accessible = self.wait_allaccesible(reservationList, self._key, totalVMs, typeAPI)
            
        msg = "Accessible VMs: " + str(accessible)            
        if self.verbose:
            print msg 
            
        iplist=""            
        ##print instance id and public ip
        for reservation in reservationList:
            for i in reservation.instances:
                flavor=""
                address=""
                if (typeAPI == 'ec2'):
                    flavor=i.instance_type
                    address=str(i.private_ip_address) 
                elif (typeAPI == 'nova'):
                    flavor=connection.flavors.find(id=i.flavor['id']).name    
                    #chameleon assumes privateNet is tenant_id-net
                    address=i.addresses[i.tenant_id+"-net"][0]['addr']
             
                if i.id in accessible:
                    iplist+=str(i.id)+ ":" + address+":"+flavor+";"
                else:
                    failedVMs+="failId:dummyIp:"+flavor+";"
                    if (typeAPI == 'ec2'):
                        i.terminate();
                    elif (typeAPI == 'nova'):
                        try:
                            i.delete()
                        except:
                            msg = "ERROR: terminating VM " + str(sys.exc_info())
                            self._log.error(msg)

                    reservation.instances.remove(i);
        
        output="IP="+iplist

        if failedVMs != "":
            output+=failedVMs

        return output
    
    def terminateVMs(self, instanceidonsystem, connection, typeAPI):
        try: 
            for i in instanceidonsystem:
                if (typeAPI == 'ec2'):
                    if re.search("^i-", i):
                        try:
                            reservations = connection.get_all_instances(i.strip())
                            self.stopEC2instances(connection, reservations[0])
                        except:
                            msg = "ERROR: terminating VM. " + str(sys.exc_info())
                            self._log.error(msg)
                    else: #delete all instances of a reservation
                        reservations = connection.get_all_instances()                        
                        for r in reservations:
                            if r.id == i.strip():                                
                                self.stopEC2instances(connection, r)
                                break
                elif (typeAPI == 'nova'):
                    connection.servers.get(i).delete()

            time.sleep(5)
        except:
            msg = "ERROR: terminating the instances " + str(sys.exc_info())
            self._log.error(msg)
            return msg
    
    def listVMs(self, instanceidonsystem, connection):
        try:
            if instanceidonsystem:
                reservations = connection.get_all_instances(instanceidonsystem)
            else:
                reservations = connection.get_all_instances()

            return reservations
        
        except:
            msg = "ERROR: getting the instance " + str(sys.exc_info())
            self._log.error(msg)
            return msg
    
    def wait_allaccesible(self, reservationList, sshkeypair_path, totalVMs, typeAPI):
        accessible = []
        naccessible = 0
        self.private_ips_for_hostlist = ""
        if sshkeypair_path == "":
            sshkeypair_path="~/.ssh/id_rsa"
            if not os.path.isfile(sshkeypair_path):
                sshkeypair_path=" -i ~/.ssh/id_dsa"
                if not os.path.isfile(sshkeypair_path):
                    sshkeypair_path=""
        else:
            sshkeypair_path=" -i "+sshkeypair_path

        address=""
 
        for reservation in reservationList:
            for i in reservation.instances:                
                access = False
                maxretry = 240  #this says that we wait 20 minutes maximum to allow the VM get online. 
                #this also prevent to get here forever if the ssh key was not injected propertly.
                retry = 0
            
                if (typeAPI == 'ec2'):
                    address=str(i.private_ip_address)
                elif (typeAPI == 'nova'): #chameleon assumes privateNet is tenant_id-net
                    address=i.addresses[i.tenant_id+"-net"][0]['addr']

                #print "Instance properties"
                #pprint (vars(i))
                #print "end instance properties"
                msg = "Waiting to have access to Instance " + str(i.id) + " associated with address " + address
                self._log.debug(msg)
                if self.verbose:
                    print msg
            
                while not access and retry < maxretry:                
                    cmd = "ssh " + sshkeypair_path + " -q -o StrictHostKeyChecking=no -oBatchMode=yes -o \"ConnectTimeout 5\" "+self.user+"@" + address + " uname"                    

                    p = Popen(cmd, shell=True, stdout=PIPE)
                    status = os.waitpid(p.pid, 0)[1]
                    #print status                  
                    if status == 0:
                        access = True
                        accessible.append(str(i.id))
                        naccessible += 1
                        self._log.debug("The instance " + str(str(i.id)) + " with private ip " + address + " is accessible")
                        self.private_ips_for_hostlist += address + "\n"
			#for testing
                        #cmd = "scp " + sshkeypair_path + " -q -o StrictHostKeyChecking=no -oBatchMode=yes -o \"ConnectTimeout 5\" "+os.path.dirname( __file__ )+"/../dist/cometWorkflow.jar "+self.user+"@" + address + ":/root/testapp1/dist/"
                        #p = Popen(cmd, shell=True, stdout=PIPE)
                    else:
                        if typeAPI == 'ec2':
                            i.update()

                        retry += 1
                        time.sleep(5)
                     
                if retry >= maxretry:
                    self._log.error("Could not get access to the instance " + str(str(i.id)) + " with private ip " + address + "\n")                                      
                    allaccessible = False
                    break
        
        return accessible
        
    def stopEC2instances(self, connection, reservation):
        status = False        
        try:
            #regioninfo = str(connection.get_all_regions()[0]).split(":")[1]
            #regioninfo = regioninfo.lower()
            #if regioninfo == 'eucalyptus' or regioninfo == 'nimbus':                
            if self.region == 'eucalyptus' or self.region == 'nimbus' or self.cloudType == 'aws_ec2':                
                for i in reservation.instances:
                    connection.terminate_instances([str(i).split(":")[1]])
            else:
                connection.terminate_instances(reservation.instances)
            status = True
        except:
            msg = "ERROR: terminating VM. " + str(sys.exc_info())
            self._log.error(msg)
        return status
        

def main():
     
    parser = argparse.ArgumentParser(prog="cloud", formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="OpenStack and Nimbus EC2 interface Help ")
    parser.add_argument('-i', '--imgid', dest='imgid', metavar='ImgId', help='Id of the image to use')    
    parser.add_argument('--logfile', dest='logfile', metavar='logfile', default='cloud.log', help='Logfile')    
    parser.add_argument('--varfile', dest='varfile', required=True, help='Path of the environment variable files. Currently this is used by Eucalyptus, OpenStack and Nimbus.')
    parser.add_argument('-k', '--key', dest='sshkey', help='SSH key that will be used in the VM.')
    parser.add_argument('-r', '--region', dest='region', required=True, help='Region of the cloud.')
    parser.add_argument('--verbose', action="store_true", default=False, dest='verbose', help='Print screen debug info')
    group1 = parser.add_mutually_exclusive_group()    
    group1.add_argument('-s', '--start-resources', dest='resources', metavar='resources', help='Start resources. The format is "<vmtype>,<vmtype>"')
    group1.add_argument('-t', '--terminate', dest='resources', metavar='resources', help='Terminate instances. The format is "<vmid>,<vmid>"')
    parser.add_argument('-c','--cloud', dest='cloud', metavar='cloud', required=True, help='Cloud to be used: openstack_ec2, nimbus, openstack_nova, aws_ec2')  
    parser.add_argument('-u', '--user', dest='user', default='root', help='User in VM.')
    #openstack region is typically nova 
    #nimbus region is nimbus
    #    
    args = parser.parse_args()
    
    varfile = ""
    if args.varfile != None:
        varfile = os.path.expandvars(os.path.expanduser(args.varfile))

    logfile = ""
    if args.logfile != None:
        logfile = os.path.expandvars(os.path.expanduser(args.logfile))
    

    used_args = sys.argv[1:]
    
    sshkey=args.sshkey
    if args.sshkey == None:
        sshkey=""    
    cloud = cloudClass(sshkey, args.verbose, logfile, args.user, args.region, args.cloud)
        
    if varfile == "":
        print "ERROR: You need to specify the path of the file with the cloud environment variables"
    elif not os.path.isfile(varfile):
        print "ERROR: Variable files not found. You need to specify the path of the file with the cloud environment variables"
    else:  
        output=None
        resources=args.resources.split(',')
        if ('-s' in used_args or '--start-resources' in used_args):
            if ( args.cloud == 'nimbus_ec2'):
                output = cloud.nimbus_ec2(args.imgid, varfile, args.region, resources, 'start')                    
            elif ( args.cloud == 'openstack_ec2'):
                output = cloud.openstack_ec2(args.imgid, varfile, args.region, resources, 'start')
            elif ( args.cloud == 'openstack_nova'):
                output = cloud.openstack_nova(args.imgid, varfile, args.region, resources, 'start')
            elif ( args.cloud == 'aws_ec2'):
                output = cloud.aws_ec2(args.imgid, varfile, args.region, resources, 'start')

            if output != None:
                print output
        elif ('-t' in used_args or '--terminate' in used_args):
            if ( args.cloud == 'nimbus_ec2'):
                output = cloud.nimbus_ec2(args.imgid, varfile, args.region, resources, 'terminate')                    
            elif ( args.cloud == 'openstack_ec2'):
                output = cloud.openstack_ec2(args.imgid, varfile, args.region, resources, 'terminate')
            elif ( args.cloud == 'openstack_nova'):
                output = cloud.openstack_nova(args.imgid, varfile, args.region, resources, 'terminate')
            elif ( args.cloud == 'aws_ec2'):
                output = cloud.aws_ec2(args.imgid, varfile, args.region, resources, 'terminate')
            if output != None:
                print output
        
if __name__ == "__main__":
    main()
#END

