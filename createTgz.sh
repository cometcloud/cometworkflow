#!/bin/bash

#create a package with all components needed to deploy cometWorkflow

DIR=`pwd`"/cometWorkflow"

rm -rf $DIR

mkdir $DIR
mkdir $DIR"/dist"
cp dist/cometWorkflow.jar $DIR"/dist/"
mkdir $DIR"/lib"
cp lib/* $DIR"/lib/"

mkdir $DIR"/scripts/"
cp scripts/cloud.py $DIR"/scripts/"

SR=$DIR"/simple_run"
mkdir $SR

mkdir $SR"/agent/"
cd simple_run/agent/; cp agent.properties  cloudAWS  cloudNimbusSierra  cloudOSChameleon  cloudOSsierra  clusterSpring  comet.properties  startAgent.sh $SR"/agent/"

mkdir $SR"/client/"
cd ../client/; cp workflowSample* workflowClient.sh sample.properties $SR"/client/"

mkdir $SR"/master/"
cd ../master/; cp chord.properties comet.properties master.properties portFile squid.properties  startOverlayServer.sh  startWorkflowMaster.sh clean.sh exceptionFile nodeFile RequestHandlerList startAll.sh startProxy.sh  $SR"/master/"

mkdir $SR"/workflow/"
cd ../workflow/; cp manager.properties startWorkflowManager.sh $SR"/workflow/"

cd ../../
mv cometWorkflow "cometWorkflow"`date +"%m%d%Y"`
tar cvfz cometWorkflow.tgz "cometWorkflow"`date +"%m%d%Y"`
rm -rf "cometWorkflow"`date +"%m%d%Y"`

mv -v cometWorkflow.tgz ../cometcloud.bitbucket.org/cometworkflow/doc/_downloads/
